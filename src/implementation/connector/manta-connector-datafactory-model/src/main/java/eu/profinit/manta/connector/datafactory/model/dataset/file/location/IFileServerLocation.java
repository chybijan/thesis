package eu.profinit.manta.connector.datafactory.model.dataset.file.location;

/**
 * Interface represents DatasetLocation resource of type FileServer.
 */
public interface IFileServerLocation extends IFileDatasetLocation {
}
