package eu.profinit.manta.connector.datafactory.model.dataset.file.location;

import eu.profinit.manta.connector.datafactory.model.dataset.file.location.visitor.ILocationVisitor;

/**
 * Parent interface for all DatasetLocation resources.
 */
public interface IDatasetLocation {

    <T> T accept(ILocationVisitor<T> visitor);

    LocationType getLocationType();

}
