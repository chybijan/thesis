package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.DataFlowDataType;

/**
 * Data type of DFS parameter
 */
public interface IAstDataType extends IDFSNode {

    /**
     * @return data type representation of this node
     */
    DataFlowDataType getDataType();

}
