package eu.profinit.manta.connector.datafactory.model.errors;

public class InvalidExpressionTypeException extends Exception {
    public InvalidExpressionTypeException(String errorMessage) {
            super(errorMessage);
    }
}

