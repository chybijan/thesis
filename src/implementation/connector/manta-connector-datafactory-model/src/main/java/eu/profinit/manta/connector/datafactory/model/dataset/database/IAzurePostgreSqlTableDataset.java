package eu.profinit.manta.connector.datafactory.model.dataset.database;

/**
 * Interface represents Dataset resource of type AzurePostgreSqlTable.
 */
public interface IAzurePostgreSqlTableDataset extends IDatabaseDataset {
}
