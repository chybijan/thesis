package eu.profinit.manta.connector.datafactory.model.dataset;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * Enum for Dataset resource types.
 */
public enum DatasetType {

    /**
     * The Amazon Redshift table dataset.
     */
    AMAZON_REDSHIFT_TABLE("AmazonRedshiftTable"),

    /**
     * Azure PostgreSQL dataset.
     */
    AZURE_POSTGRESQL_TABLE("AzurePostgreSqlTable"),

    /**
     * The Azure SQL Data Warehouse dataset.
     */
    AZURE_SQL_DW_TABLE("AzureSqlDWTable"),

    /**
     * The Azure SQL Managed Instance dataset.
     */
    AZURE_SQL_MI_TABLE("AzureSqlMITable"),

    /**
     * The Azure SQL Server database dataset.
     */
    AZURE_SQL_TABLE("AzureSqlTable"),

    /**
     * The Db2 table dataset.
     */
    DB2_TABLE("Db2Table"),

    /**
     * Google BigQuery service dataset.
     */
    GOOGLE_BIGQUERY_OBJECT("GoogleBigQueryObject"),

    /**
     * Greenplum Database dataset.
     */
    GREENPLUM_TABLE("GreenplumTable"),

    /**
     * Hive Server dataset.
     */
    HIVE_OBJECT("HiveObject"),

    /**
     * Netezza dataset.
     */
    NETEZZA_TABLE("NetezzaTable"),

    /**
     * The on-premises Oracle database dataset.
     */
    ORACLE_TABLE("OracleTable"),

    /**
     * The PostgreSQL table dataset.
     */
    POSTGRESQL_TABLE("PostgreSqlTable"),

    /**
     * The Snowflake dataset.
     */
    SNOWFLAKE_TABLE("SnowflakeTable"),

    /**
     * The on-premises SQL Server dataset.
     */
    SQL_SERVER_TABLE("SqlServerTable"),

    /**
     * The Sybase table dataset.
     */
    SYBASE_TABLE("SybaseTable"),

    /**
     * The Teradata database dataset.
     */
    TERADATA_TABLE("TeradataTable"),

    /**
     * Avro dataset.
     */
    AVRO("Avro"),

    /**
     * Binary dataset.
     */
    BINARY("Binary"),

    /**
     * Delimited text dataset.
     */
    DELIMITED_TEXT("DelimitedText"),

    /**
     * Excel dataset.
     */
    EXCEL("Excel"),

    /**
     * JSON dataset.
     */
    JSON("Json"),

    /**
     * Orc dataset.
     */
    ORC("Orc"),

    /**
     * Parquet dataset.
     */
    PARQUET("Parquet"),

    /**
     * XML dataset.
     */
    XML("Xml"),

    /**
     * Unknown dataset. This value is used in case the type is not recognized or not supported by Manta.
     */
    UNKNOWN("Unknown");

    /**
     * Value of the Dataset resource type as it is in the "type" field.
     */
    private final String value;

    /**
     * @param value value of the Dataset resource type as it is in the "type" field
     */
    DatasetType(String value) {
        this.value = value;
    }

    /**
     * Similar to {@link DatasetType#valueOf(String)} but compares values case-insensitively instead.
     * Returns the enum constant of this type with the specified value.
     *
     * @param value value to look up
     * @return the enum constant with the specified value
     * @throws IllegalArgumentException if this enum type has no constant with the specified value
     */
    public static DatasetType valueOfCI(String value) {
        return Arrays.stream(DatasetType.values())
                .filter(dt -> StringUtils.equalsIgnoreCase(dt.value, value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Value = %s doesn't represent any enum's value.", value)));
    }

}
