package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;

/**
 * Interface represents linked service which type is not recognizable or supported by Manta.
 */
public interface IUnknownLinkedService extends ILinkedService {
}
