package eu.profinit.manta.connector.datafactory.model.dataset.database;

/**
 * Interface represents Dataset resource of type NetezzaTable.
 */
public interface INetezzaTableDataset extends IDatabaseDataset {
}
