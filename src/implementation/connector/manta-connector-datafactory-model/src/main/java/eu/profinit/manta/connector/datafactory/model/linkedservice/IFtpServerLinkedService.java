package eu.profinit.manta.connector.datafactory.model.linkedservice;

/**
 * Interface represents FtpServerLinkedService resource.
 */
public interface IFtpServerLinkedService extends IGeneralFtpServerLinkedService {

}
