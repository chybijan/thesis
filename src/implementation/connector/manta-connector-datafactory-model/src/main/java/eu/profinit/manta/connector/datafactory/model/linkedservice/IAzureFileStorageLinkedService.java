package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents AzureFileStorageLinkedService resource.
 */
public interface IAzureFileStorageLinkedService extends ILinkedService {

    /**
     * @return The connection string. It is mutually exclusive with sasUri property.
     */
    IAdfFieldValue getConnectionString();

    /**
     * @return Host name of the server.
     */
    IAdfFieldValue getHost();

    /**
     * @return SAS URI of the Azure File resource. It is mutually exclusive with connectionString property.
     */
    IAdfFieldValue getSasUri();

    /**
     * @return The azure file share snapshot version.
     */
    IAdfFieldValue getSnapshot();

}
