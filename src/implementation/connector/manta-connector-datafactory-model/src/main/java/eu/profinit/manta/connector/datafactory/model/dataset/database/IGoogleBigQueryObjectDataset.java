package eu.profinit.manta.connector.datafactory.model.dataset.database;

/**
 * Interface represents Dataset resource of type GoogleBigQueryObject.
 */
public interface IGoogleBigQueryObjectDataset extends IDatabaseDataset {
}
