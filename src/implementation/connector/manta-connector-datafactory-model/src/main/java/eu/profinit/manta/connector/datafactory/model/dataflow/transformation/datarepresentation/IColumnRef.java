package eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;

/**
 * IReference to other column
 */
public interface IColumnRef extends IColumn{

    /**
     * @return Name of this column inside expression
     */
    IExpression getName();

    /**
     * @return Name of original column inside expression
     */
    IExpression getOrigin();

}
