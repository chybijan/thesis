package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

/**
 * DFS parameter
 */
public interface IAstParameter extends IDFSNode {

    /**
     * @return Parameter name
     */
    IAstIdentifier findName();

    /**
     * @return Parameter data type
     */
    IAstDataType findType();

    IAstExpression findDefaultValue();

}
