package eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;

import java.util.Collection;

/**
 * Column representing multiple columns matched via expression
 */
public interface IColumnMatch extends IColumn{

    /**
     * @return Match on expression
     */
    IExpression getMatchOn();

    /**
     * @return Column mapping on match
     */
    Collection<IColumn> getColumns();

}
