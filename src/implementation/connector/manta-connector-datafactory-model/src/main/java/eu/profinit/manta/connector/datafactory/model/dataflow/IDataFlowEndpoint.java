package eu.profinit.manta.connector.datafactory.model.dataflow;

import eu.profinit.manta.connector.datafactory.model.IADFElement;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.ReferenceType;


public interface IDataFlowEndpoint extends IADFElement {

    /**
     * @return type of this endpoint
     */
    ReferenceType getType();

    /**
     * @return endpoint reference
     * @throws IllegalStateException if no reference is found
     */
    IReference getReference() throws IllegalStateException;

}
