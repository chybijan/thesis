package eu.profinit.manta.connector.datafactory.model.dataset.file;

/**
 * Interface represents Dataset resource of type Orc.
 */
public interface IOrcDataset extends IFileDataset {
}
