package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents AmazonS3LinkedService resource.
 */
public interface IGoogleCloudStorageLinkedService extends ILinkedService {

    /**
     * @return value specifying the endpoint to access with the Google Cloud Storage Connector.
     */
    IAdfFieldValue getServiceUrl();

}
