package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents PostgreSqlLinkedService resource.
 */
public interface IPostgreSqlLinkedService extends ILinkedService {

    /**
     * @return The connection string.
     */
    IAdfFieldValue getConnectionString();

    /**
     * @return Name of the PostgreSQL server. It is used in legacy format.
     */
    IAdfFieldValue getServer();

    /**
     * @return Name of the PostgreSQL database. It is used in legacy format.
     */
    IAdfFieldValue getDatabase();

    /**
     * @return Username to login. It is used in legacy format.
     */
    IAdfFieldValue getUsername();

}
