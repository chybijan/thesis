package eu.profinit.manta.connector.datafactory.model.common;

import java.util.Collection;

/**
 * Interface for Dataset reference
 */
public interface IReference {

   Collection<IArgument> getArguments();

    String getReferenceName();

    ReferenceType getType();

    boolean equals(Object o);

    int hashCode();
}
