package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents AzureBlobStorageLinkedService resource.
 */
public interface IAzureBlobStorageLinkedService extends ILinkedService {

    /**
     * @return The connection string.
     * It is mutually exclusive with sasUri, serviceEndpoint property.
     */
    IAdfFieldValue getConnectionString();

    /**
     * @return SAS URI of the Azure Blob Storage resource.
     * It is mutually exclusive with connectionString, serviceEndpoint property.
     */
    IAdfFieldValue getSasUri();

    /**
     * @return Blob service endpoint of the Azure Blob Storage resource.
     * It is mutually exclusive with connectionString, sasUri property.
     */
    IAdfFieldValue getServiceEndpoint();

}
