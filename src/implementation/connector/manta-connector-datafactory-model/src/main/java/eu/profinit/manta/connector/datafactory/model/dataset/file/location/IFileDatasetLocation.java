package eu.profinit.manta.connector.datafactory.model.dataset.file.location;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Parent interface for all file-like DatasetLocation resources.
 */
public interface IFileDatasetLocation extends IDatasetLocation {

    IAdfFieldValue getFolderPath();

    IAdfFieldValue getFileName();

}
