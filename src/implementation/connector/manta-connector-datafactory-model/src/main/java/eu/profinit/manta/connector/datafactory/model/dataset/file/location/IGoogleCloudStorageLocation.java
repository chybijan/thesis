package eu.profinit.manta.connector.datafactory.model.dataset.file.location;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents DatasetLocation resource of type GoogleCloudStorage.
 */
public interface IGoogleCloudStorageLocation extends IFileDatasetLocation {

    IAdfFieldValue getBucketName();

}
