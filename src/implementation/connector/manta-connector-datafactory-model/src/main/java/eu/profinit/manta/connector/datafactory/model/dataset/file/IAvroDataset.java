package eu.profinit.manta.connector.datafactory.model.dataset.file;

/**
 * Interface represents Dataset resource of type Avro.
 */
public interface IAvroDataset extends IFileDataset {
}
