package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

/**
 * Format of data
 * example: '000,000,000.000' for decimal numbers
 *          'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'' for timestamp...
 */
public interface IAstDataTypeFormat extends IDFSNode {
}
