package eu.profinit.manta.connector.datafactory.model.dataset.file.location;

/**
 * Interface represents DatasetLocation resource of type Hdfs.
 */
public interface IHdfsLocation extends IFileDatasetLocation {
}
