package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents TeradataLinkedService resource.
 */
public interface ITeradataLinkedService extends ILinkedService {

    /**
     * @return Teradata ODBC connection string.
     */
    IAdfFieldValue getConnectionString();

    /**
     * @return Server name for connection.
     */
    IAdfFieldValue getServer();

    /**
     * @return Username for authentication.
     */
    IAdfFieldValue getUsername();
}
