package eu.profinit.manta.connector.datafactory.model.dataset.file;

/**
 * Interface represents Dataset resource of type Json.
 */
public interface IJsonDataset extends IFileDataset {
}
