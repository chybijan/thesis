package eu.profinit.manta.connector.datafactory.model.dataflow;

import eu.profinit.manta.connector.datafactory.model.common.IReference;

import java.util.Collection;
import java.util.Map;

/**
 * Interface represents data flow properties
 */
public interface IDataFlowProperties {

    /**
     * @return Data flow script
     */
    IDataFlowScript getScript();

    /**
     * @return Data flow sources
     */
    Map<String, IDataFlowEndpoint> getSources();

    /**
     * @return Type of data flow
     */
    DataFlowType getType();

    /**
     * @return Gets all references this data flow works with
     */
    Collection<IReference> getAllReferences();

    
}
