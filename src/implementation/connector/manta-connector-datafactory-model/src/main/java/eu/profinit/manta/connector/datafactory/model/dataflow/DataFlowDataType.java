package eu.profinit.manta.connector.datafactory.model.dataflow;

import java.util.Arrays;

/**
 * Type of data flow parameter based on spark types
 */
public enum DataFlowDataType {

    /**
     * String
     */
    DF_STRING("string"),

    /**
     * Integer
     */
    DF_INTEGER("integer"),

    /**
     *  Boolean
     */
    DF_BOOLEAN("boolean"),

    /**
     * Date DESCRIPTION
     */
    DF_DATE("date"),

    /**
     * Time stamp
     */
    DF_TIMESTAMP("timestamp"),

    /**
     * Any
     */
    DF_ANY("any"),

    /**
     * Short
     */
    DF_SHORT("short"),

    /**
     * Double
     */
    DF_DOUBLE("double"),

    /**
     * Float
     */
    DF_FLOAT("float"),

    /**
     * Long
     */
    DF_LONG("long"),

    /**
     * Decimal with precision
     */
    DF_DECIMAL("decimal"),

    /**
     * Map in DFS has format: [ DF_DATATYPE , DF_DATATYPE ]
     */
    DF_MAP("map"),

    /**
     * ARRAY DFS has format: DATYPE[]
     */
    DF_ARRAY("array"),

    /**
     * complex data type
     */
    DF_COMPLEX("()"),


    /**
     * String
     */
    DF_STRING_ARRAY("string[]"),

    /**
     * Integer
     */
    DF_INTEGER_ARRAY("integer[]"),

    /**
     *  Boolean
     */
    DF_BOOLEAN_ARRAY("boolean[]"),

    /**
     * Date DESCRIPTION
     */
    DF_DATE_ARRAY("date[]"),

    /**
     * Time stamp
     */
    DF_TIMESTAMP_ARRAY("timestamp[]"),

    /**
     * Any
     */
    DF_ANY_ARRAY("any[]"),

    /**
     * Short
     */
    DF_SHORT_ARRAY("short[]"),

    /**
     * Double
     */
    DF_DOUBLE_ARRAY("double[]"),

    /**
     * Float
     */
    DF_FLOAT_ARRAY("float[]"),

    /**
     * Long
     */
    DF_LONG_ARRAY("long[]"),

    /**
     * Decimal with precision
     */
    DF_DECIMAL_ARRAY("decimal[]"),

    /**
     * Number
     */
    DF_NUMBER_ARRAY("number[]"),

    /**
     * Complex data type
     */
    DF_COMPLEX_ARRAY("()[]"),

    /**
     * Unknown data type
     */
    UNKNOWN_TYPE("unknown type");

    /**
     * Value of the ParameterType resource.
     */
    private final String value;

    /**
     * @param value value of the ParameterType resource
     */
    DataFlowDataType(String value) {
        this.value = value;
    }

    /**
     * Similar to {@link DataFlowDataType#valueOf(String)} but compares values case-insensitively instead.
     * Returns the enum constant of this type with the specified value.
     *
     * @param value value to look up
     * @return the enum constant with the specified value
     * @throws IllegalArgumentException if this enum type has no constant with the specified value
     */
    public static DataFlowDataType valueOfCI(String value) {
        return Arrays.stream(DataFlowDataType.values())
                .filter(pt -> pt.value.equalsIgnoreCase(value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Value = %s doesn't represent any enum's value.", value)));
    }

}
