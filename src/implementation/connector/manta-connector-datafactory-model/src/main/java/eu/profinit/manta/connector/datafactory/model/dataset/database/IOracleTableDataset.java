package eu.profinit.manta.connector.datafactory.model.dataset.database;

/**
 * Interface represents Dataset resource of type OracleTable.
 */
public interface IOracleTableDataset extends IDatabaseDataset {
}
