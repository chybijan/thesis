package eu.profinit.manta.connector.datafactory.model.dataset.file.location;

/**
 * Interface represents DatasetLocation resource of type Sftp.
 */
public interface ISftpLocation extends IFileDatasetLocation {
}
