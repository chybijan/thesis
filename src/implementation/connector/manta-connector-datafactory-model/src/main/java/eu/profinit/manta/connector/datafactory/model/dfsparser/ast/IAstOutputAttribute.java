package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

import java.util.List;

/**
 * Interface for output attribute of transformation
 */
public interface IAstOutputAttribute extends IDFSNode {

    /**
     * @return columns defined by this attribute
     */
    List<IAstColumnDefinition> findColumns();

}
