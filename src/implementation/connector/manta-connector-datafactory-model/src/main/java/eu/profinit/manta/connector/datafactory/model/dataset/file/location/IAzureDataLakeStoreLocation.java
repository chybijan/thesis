package eu.profinit.manta.connector.datafactory.model.dataset.file.location;

/**
 * Interface represents DatasetLocation resource of type AzureDataLakeStoreLocation.
 */
public interface IAzureDataLakeStoreLocation extends IFileDatasetLocation {
}
