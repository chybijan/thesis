package eu.profinit.manta.connector.datafactory.model.dataset.file.location.visitor;

import eu.profinit.manta.connector.datafactory.model.dataset.file.location.*;

public interface ILocationVisitor<T>{

    T visit(IAmazonS3Location location);

    T visit(IAzureBlobFSLocation location);

    T visit(IAzureBlobStorageLocation location);

    T visit(IAzureDataLakeStoreLocation location);

    T visit(IAzureFileStorageLocation location);

    T visit(IFileServerLocation location);

    T visit(IFtpServerLocation location);

    T visit(IGoogleCloudStorageLocation location);

    T visit(IHdfsLocation location);

    T visit(IHttpServerLocation location);

    T visit(ISftpLocation location);

}
