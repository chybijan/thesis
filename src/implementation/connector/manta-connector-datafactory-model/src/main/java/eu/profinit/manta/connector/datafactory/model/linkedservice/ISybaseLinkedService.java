package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents SybaseLinkedService resource.
 */
public interface ISybaseLinkedService extends ILinkedService {

    /**
     * @return Database name for connection.
     */
    IAdfFieldValue getDatabase();

    /**
     * @return Schema name for connection.
     */
    IAdfFieldValue getSchema();

    /**
     * @return Server name for connection.
     */
    IAdfFieldValue getServer();

    /**
     * @return Username for authentication.
     */
    IAdfFieldValue getUsername();

}
