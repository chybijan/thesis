package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;

/**
 * AST node for filter transformation
 */
public interface IAstFilterTransformation extends IAstConcreteTransformation {

    IExpression findFilterCondition();

}
