package eu.profinit.manta.connector.datafactory.model.dataset.database;

/**
 * Interface represents Dataset resource of type HiveObject.
 */
public interface IHiveObjectDataset extends IDatabaseDataset {
}
