package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents SqlServerLinkedService resource.
 */
public interface ISqlServerLinkedService extends ILinkedService {

    /**
     * @return The connection string.
     */
    IAdfFieldValue getConnectionString();

}
