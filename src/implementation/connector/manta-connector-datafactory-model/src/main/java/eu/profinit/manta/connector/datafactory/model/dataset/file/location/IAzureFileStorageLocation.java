package eu.profinit.manta.connector.datafactory.model.dataset.file.location;

/**
 * Interface represents DatasetLocation resource of type AzureFileStorage.
 */
public interface IAzureFileStorageLocation extends IFileDatasetLocation {
}
