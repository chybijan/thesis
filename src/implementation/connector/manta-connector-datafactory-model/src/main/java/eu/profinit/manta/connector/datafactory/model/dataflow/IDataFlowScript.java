package eu.profinit.manta.connector.datafactory.model.dataflow;

import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

/**
 * Interface representing data flow script
 * Keeps internall representation of transformations correct
 */
public interface IDataFlowScript {

    Collection<IDataflowParameter> getParameters();

    /**
     * @return Map with all the transformations
     */
    Map<String, ITransformation> getTransformations();

    /**
     * @param name name of transformation to get
     * @return Optional of transformation or optional empty if no transformation of specified name is found
     */
    Optional<ITransformation> getTransformation(String name);

    /**
     * @param name name of lookued parameter
     * @return Optional of looked parameter or optional empty if no parameter of specified name is found
     */
    Optional<IDataflowParameter> getParameter(String name);

    /**
     * Creates new data flow script class with new parameters
     * @param newParameters
     * @return updated data flow script
     */
    IDataFlowScript updatedParameters(Collection<IDataflowParameter> newParameters);

    /**
     * Creates new data flow script class with new transformations
     * @param newTransformations
     * @return updated data flow script
     */
    IDataFlowScript updatedTransformations(Map<String, ITransformation> newTransformations);

    /**
     * Creates new data flow script class with added parameter
     * @param newParameter ne parameter to add
     * @return updated data flow script
     */
    IDataFlowScript addedParameter(IDataflowParameter newParameter);

    /**
     * Creates new data flow script class with new transformations
     * @param transformationId identifier of added transformation
     * @param newTransformation new transformation to add
     * @return updated data flow script
     */
    IDataFlowScript addedTransformation(String transformationId, ITransformation newTransformation);

    /**
     * @param parsedWithoutErrors true if any error occurred while parsing
     * @return updated data flow script
     */
    IDataFlowScript updatedParsedWithoutErrors(boolean parsedWithoutErrors);

    /**
     *
     * @return true if during parsing this annotation were no errors
     */
    boolean isParsedWithoutErrors();

}
