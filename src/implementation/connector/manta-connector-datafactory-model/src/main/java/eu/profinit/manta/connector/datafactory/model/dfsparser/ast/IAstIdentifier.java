package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

/**
 * DFS node for identifier
 */
public interface IAstIdentifier extends IDFSNode {

    /**
     * @return name of identifier.
     */
    String getIdentifier();

}
