package eu.profinit.manta.connector.datafactory.model.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;

public interface IFilterTransformation extends ITransformation{

    IExpression getFilterCondition();

}
