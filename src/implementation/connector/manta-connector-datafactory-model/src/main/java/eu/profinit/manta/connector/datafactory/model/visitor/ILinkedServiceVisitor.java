package eu.profinit.manta.connector.datafactory.model.visitor;

import eu.profinit.manta.connector.datafactory.model.linkedservice.*;

public interface ILinkedServiceVisitor<T> {

    T visitAmazonRedshiftLinkedService(IAmazonRedshiftLinkedService ls);

    T visitAmazonS3LinkedService(IAmazonS3LinkedService ls);

    T visitAzureBlobFS(IAzureBlobFSLinkedService ls);

    T visitAzureBlobStorageLinkedService(IAzureBlobStorageLinkedService ls);

    T visitAzureDataLakeStoreLinkedService(IAzureDataLakeStoreLinkedService ls);

    T visitAzureFileStorageLinkedService(IAzureFileStorageLinkedService ls);

    T visitAzurePostgresSqlLinkedService(IAzurePostgreSqlLinkedService ls);

    T visitAzureSqlDatabaseLinkedService(IAzureSqlDatabaseLinkedService ls);

    T visitAzureSqlDWLLinkedService(IAzureSqlDWLinkedService ls);

    T visitAzureSqlMILLinkedService(IAzureSqlMILinkedService ls);

    T visitDb2LinkedService(IDb2LinkedService ls);

    T visitFileServerLinkedService(IFileServerLinkedService ls);

    T visitFtpServerLinkedService(IFtpServerLinkedService ls);

    T visitGoogleBigQueryLinkedService(IGoogleBigQueryLinkedService ls);

    T visitGoogleCloudStorageLinkedService(IGoogleCloudStorageLinkedService ls);

    T visitGreenplumLinkedService(IGreenplumLinkedService ls);

    T visitHdfsLinkedService(IHdfsLinkedService ls);

    T visitHiveLinkedService(IHiveLinkedService ls);

    T visitNetezzaLinkedService(INetezzaLinkedService ls);

    T visitOracleLinkedService(IOracleLinkedService ls);

    T visitPostgreSqlLinkedService(IPostgreSqlLinkedService ls);

    T visitSapHanaLinkedService(ISapHanaLinkedService ls);

    T visitSftpServerLinkedService(ISftpServerLinkedService ls);

    T visitSnowflakeLinkedService(ISnowflakeLinkedService ls);

    T visitSqlServerLinkedService(ISqlServerLinkedService ls);

    T visitSybaseLinkedService(ISybaseLinkedService ls);

    T visitTeradataLinkedService(ITeradataLinkedService ls);

    T visitUnknownLinkedService(IUnknownLinkedService ls);

}
