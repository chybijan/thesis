package eu.profinit.manta.connector.datafactory.model.common;

import java.util.Arrays;

/**
 * Type of parameter.
 */
public enum ParameterType {

    /**
     * Array.
     * JSON array. Not all the fields of the array have to have the same type or structure.
     */
    ARRAY("Array"),

    /**
     * Boolean.
     */
    BOOL("Bool"),

    /**
     * Float.
     */
    FLOAT("Float"),

    /**
     * Int.
     */
    INT("Int"),

    /**
     * Object.
     * <br>
     * Represents JSON object.
     */
    OBJECT("Object"),

    /**
     * Secure string.
     */
    SECURE_STRING("SecureString"),

    /**
     * String.
     */
    STRING("String");

    /**
     * Value of the ParameterType resource.
     */
    private final String value;

    /**
     * @param value value of the ParameterType resource
     */
    ParameterType(String value) {
        this.value = value;
    }

    /**
     * Similar to {@link ParameterType#valueOf(String)} but compares values case-insensitively instead.
     * Returns the enum constant of this type with the specified value.
     *
     * @param value value to look up
     * @return the enum constant with the specified value
     * @throws IllegalArgumentException if this enum type has no constant with the specified value
     */
    public static ParameterType valueOfCI(String value) {
        return Arrays.stream(ParameterType.values())
                .filter(pt -> pt.value.equalsIgnoreCase(value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Value = %s doesn't represent any enum's value.", value)));
    }

}
