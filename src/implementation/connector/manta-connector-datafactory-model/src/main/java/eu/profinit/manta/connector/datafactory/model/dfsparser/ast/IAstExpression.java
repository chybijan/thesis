package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.errors.InvalidExpressionTypeException;

/**
 * Interface representing data flow expressions
 */
public interface IAstExpression extends IDFSNode, IExpression {

    /**
     * Get return value of an expression
     * @return value of expression
     * @throws InvalidExpressionTypeException if expression cannot be executed
     */
    boolean resolveAsBoolean() throws InvalidExpressionTypeException;

}
