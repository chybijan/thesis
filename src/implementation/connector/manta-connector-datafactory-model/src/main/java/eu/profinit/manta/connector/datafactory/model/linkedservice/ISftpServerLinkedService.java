package eu.profinit.manta.connector.datafactory.model.linkedservice;

/**
 * Interface represents SftpServerLinkedService resource.
 */
public interface ISftpServerLinkedService extends IGeneralFtpServerLinkedService {

}
