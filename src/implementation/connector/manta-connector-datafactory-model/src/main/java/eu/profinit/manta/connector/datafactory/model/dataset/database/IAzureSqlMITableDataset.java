package eu.profinit.manta.connector.datafactory.model.dataset.database;

/**
 * Interface represents Dataset resource of type AzureSqlMITable.
 */
public interface IAzureSqlMITableDataset extends IDatabaseDataset {
}
