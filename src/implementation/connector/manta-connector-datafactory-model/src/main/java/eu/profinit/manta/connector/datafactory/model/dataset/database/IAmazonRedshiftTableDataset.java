package eu.profinit.manta.connector.datafactory.model.dataset.database;

/**
 * Interface represents Dataset resource of type AmazonRedshiftTable.
 */
public interface IAmazonRedshiftTableDataset extends IDatabaseDataset {
}
