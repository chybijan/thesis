package eu.profinit.manta.connector.datafactory.model.common;

/**
 * Representation of parameters specification for Azure Data Factory resource.
 */
public interface IParameters {

    /**
     * @param name of parameter
     * @return parameter specification or null if parameter name us unknown
     */
    IParameterSpecification get(String name);

    /**
     * @return number of parameters or zero if empty
     */
    int size();
}
