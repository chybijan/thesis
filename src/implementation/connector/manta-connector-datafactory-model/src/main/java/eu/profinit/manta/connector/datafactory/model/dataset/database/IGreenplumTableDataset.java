package eu.profinit.manta.connector.datafactory.model.dataset.database;

/**
 * Interface represents Dataset resource of type GreenplumTable.
 */
public interface IGreenplumTableDataset extends IDatabaseDataset {
}
