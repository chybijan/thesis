package eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.visitor;

import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumnDef;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumnMatch;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumnRef;

/**
 * Column visitor
 */
public interface IColumnVisitor<T> {

    T visitColumnDef(IColumnDef columnDef);

    T visitColumnRef(IColumnRef columnRef);

    T visitColumnMatch(IColumnMatch columnMatch);

}
