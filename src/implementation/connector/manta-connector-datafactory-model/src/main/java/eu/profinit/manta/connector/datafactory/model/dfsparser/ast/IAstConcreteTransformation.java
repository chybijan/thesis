package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowScript;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;

import java.util.List;
import java.util.Set;

/**
 * Interface for concrete transformation
 */
public interface IAstConcreteTransformation extends IDFSNode {

    /**
     * Creates concrete transformation in DFS
     * @param thisStreams streams leading from this transformation
     * @param currentDFS current state of DFS
     * @return updated DFS
     */
    IDataFlowScript resolveConcreteTransformation(Set<IADFStream> inputStreams, Set<IADFStream> thisStreams, IDataFlowScript currentDFS);

    IAstTransformationAttributes findTransformationAttributes();
}
