package eu.profinit.manta.connector.datafactory.model.dataset.database;

/**
 * Interface represents Dataset resource of type SqlServerTable.
 */
public interface ISqlServerTableDataset extends IDatabaseDataset {
}
