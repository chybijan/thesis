package eu.profinit.manta.connector.datafactory.model.factory;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Interface represents GlobalParameterSpecification resource.
 */
public interface IGlobalParameterSpecification {

    // TODO this won't be JSON node
    JsonNode getValue();

    GlobalParameterType getType();

}
