package eu.profinit.manta.connector.datafactory.model;

import java.util.Collection;
import java.util.Optional;

/**
 * Interface represents result of reader
 * It can either be data flow or pipeline, never both and never neither
 */
public interface IADF {

    Optional<IFactory> getFactory();

    Optional<IDataFlow> getDataFlow();

    Optional<IPipeline> getPipeline();

    /**
     * @return Saved datasets needed for pipeline or data flow resource
     */
    Collection<IDataset> getDatasets();

    /**
     * @return Saved linked services needed for pipeline or data flow resource
     */
    Collection<ILinkedService> getLinkedServices();

    /**
     * Adds datasets to ADF resource
     * @param datasets
     */
    void addDatasets(Collection<IDataset> datasets);

    /**
     * Adds linked services to ADF resource
     * @param linkedServices
     */
    void addLinkedServices(Collection<ILinkedService> linkedServices);

    void setFactory(IFactory factory);

    /**
     * Checks if this class is in valid state
     *
     * @return validation result
     */
    boolean isValid();

}
