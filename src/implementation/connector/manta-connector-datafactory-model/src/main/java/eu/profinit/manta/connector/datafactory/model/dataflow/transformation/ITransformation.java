package eu.profinit.manta.connector.datafactory.model.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.IADFElement;
import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.ITable;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstExpression;

import java.util.Collection;
import java.util.Optional;

/**
 * Interface for data flow transformation
 */
public interface ITransformation extends IADFElement {

    /**
     * @return subStream leading from this transformation
     */
    Collection<IADFStream> getStreams();

    /**
     * @return Transformation streams leading to this transformation
     */
    Collection<IADFStream> getInputs();

    /**
     * @return Transformation names following this transformation
     */
    Collection<String> getOutputs();

    /**
     * Tries to find attribute value by name
     * @param attributeName name of looked attribute
     * @return Option of attribute value or nothing
     */
    Optional<IExpression> findAttributeByName(String attributeName);

    /**
     * add output transformations
     * @param newOutput new outputs to add
     */
    void addOutput(String newOutput);

    /**
     * @return Table of this transformation
     */
    ITable getTable();

    void setTable(ITable newTable);

}
