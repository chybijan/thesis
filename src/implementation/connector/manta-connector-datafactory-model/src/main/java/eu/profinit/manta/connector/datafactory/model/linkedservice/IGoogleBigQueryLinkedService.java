package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents GoogleBigQueryLinkedService resource.
 */
public interface IGoogleBigQueryLinkedService extends ILinkedService {

    /**
     * @return The default BigQuery project to query against.
     */
    IAdfFieldValue getProject();

}
