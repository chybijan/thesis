package eu.profinit.manta.connector.datafactory.model.dataset.file;

/**
 * Interface represents Dataset resource of type Excel.
 */
public interface IExcelDataset extends IFileDataset {
}
