package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents AmazonRedshiftLinkedService resource.
 */
public interface IAmazonRedshiftLinkedService extends ILinkedService {

    /**
     * @return The database name of the Amazon Redshift source.
     */
    IAdfFieldValue getDatabase();

    /**
     * @return The TCP port number that the Amazon Redshift server uses to listen for client connections.
     */
    IAdfFieldValue getPort();

    /**
     * @return The name of the Amazon Redshift server.
     */
    IAdfFieldValue getServer();

    /**
     * @return The username of the Amazon Redshift source.
     */
    IAdfFieldValue getUsername();
}
