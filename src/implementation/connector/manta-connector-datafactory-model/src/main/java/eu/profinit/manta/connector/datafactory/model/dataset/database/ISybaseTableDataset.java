package eu.profinit.manta.connector.datafactory.model.dataset.database;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

import java.util.Optional;

/**
 * Interface represents Dataset resource of type SybaseTable.
 */
public interface ISybaseTableDataset extends IDatabaseDataset {

    /**
     * @return The Sybase table name.
     */
    Optional<IAdfFieldValue> getTableName();

}
