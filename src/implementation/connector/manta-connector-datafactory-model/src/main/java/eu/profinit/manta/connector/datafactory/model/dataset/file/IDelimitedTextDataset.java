package eu.profinit.manta.connector.datafactory.model.dataset.file;

/**
 * Interface represents Dataset resource of type DelimitedText.
 */
public interface IDelimitedTextDataset extends IFileDataset {
}
