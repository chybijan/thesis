package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;

/**
 * AST node for filter transformation
 */
public interface IAstJoinTransformation extends IAstConcreteTransformation {

    IExpression findJoinCondition();

}
