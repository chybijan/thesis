package eu.profinit.manta.connector.datafactory.model.dataset.file;

import eu.profinit.manta.connector.datafactory.model.IDataset;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.IDatasetLocation;

/**
 * Parent interface for the representation of all file-like Dataset resources.
 */
public interface IFileDataset extends IDataset {

    /**
     * @return The location of the file data storage.
     */
    IDatasetLocation getLocation();

}
