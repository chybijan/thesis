package eu.profinit.manta.connector.datafactory.model.dataset.database;

/**
 * Interface represents Dataset resource of type Db2Table.
 */
public interface IDb2TableDataset extends IDatabaseDataset {
}
