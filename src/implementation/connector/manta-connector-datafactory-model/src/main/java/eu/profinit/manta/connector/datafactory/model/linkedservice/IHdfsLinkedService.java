package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents HdfsLinkedService resource.
 */
public interface IHdfsLinkedService extends ILinkedService {

    /**
     * @return The URL of the HDFS service endpoint, e.g. http://myhostname:50070/webhdfs/v1 .
     */
    IAdfFieldValue getUrl();

}
