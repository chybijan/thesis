package eu.profinit.manta.connector.datafactory.model.dataset.database;

/**
 * Interface represents Dataset resource of type AzureSqlDWTable.
 */
public interface IAzureSqlDWTableDataset extends IDatabaseDataset {
}
