package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

import java.util.List;

/**
 * AST node for whole DFS
 */
public interface IAstDFS extends IDFSNode {

    /**
     * @return DFS parameters
     */
    List<IAstParameter> findParameters();

    /**
     * @return all transformations
     */
    List<IAstTransformation> findTransformations();


}
