package eu.profinit.manta.connector.datafactory.model.dataflow.transformation;

public interface ISourceTransformation extends ITransformation{

    boolean getAllowSchemaDrift();

}
