package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;

import java.util.List;
import java.util.Set;

/**
 * DFS node for identifier
 */
public interface IAstADFStream extends IDFSNode {

    /**
     * @return name of transformation
     */
    IAstIdentifier findTransformationName();

    /**
     * @return names of substreams this transformation creates
     * If no substreams are found, transformation has only one stream, which can be obtained by {@link #findTransformationName()}
     */
    List<IAstIdentifier> findSubstreamNames();

    Set<IADFStream> getADFStreams();

}
