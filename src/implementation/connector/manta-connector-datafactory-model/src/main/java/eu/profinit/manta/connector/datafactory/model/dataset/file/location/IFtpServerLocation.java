package eu.profinit.manta.connector.datafactory.model.dataset.file.location;

/**
 * Interface represents DatasetLocation resource of type FtpServer.
 */
public interface IFtpServerLocation extends IFileDatasetLocation {
}
