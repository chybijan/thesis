package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents SnowflakeLinkedService resource.
 */
public interface ISnowflakeLinkedService extends ILinkedService {

    /**
     * @return The connection string of snowflake.
     */
    IAdfFieldValue getConnectionString();

}
