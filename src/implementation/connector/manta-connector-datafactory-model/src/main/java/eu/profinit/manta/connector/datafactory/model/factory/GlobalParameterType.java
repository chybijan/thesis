package eu.profinit.manta.connector.datafactory.model.factory;

import java.util.Arrays;

/**
 * Enum of possible types of global parameter.
 */
public enum GlobalParameterType {

    /**
     * Array.
     * JSON array. Not all the fields of the array have to have the same type or structure.
     */
    ARRAY("Array"),

    /**
     * Boolean.
     */
    BOOL("Bool"),

    /**
     * Float.
     */
    FLOAT("Float"),

    /**
     * Int.
     */
    INT("Int"),

    /**
     * Object.
     * <br>
     * Represents JSON object.
     */
    OBJECT("Object"),

    /**
     * String.
     */
    STRING("String");

    /**
     * Value of the GlobalParameterType resource.
     */
    private final String value;

    /**
     * @param value value of the GlobalParameterType resource
     */
    GlobalParameterType(String value) {
        this.value = value;
    }

    /**
     * Similar to {@link GlobalParameterType#valueOf(String)} but compares values case-insensitively instead.
     * Returns the enum constant of this type with the specified value.
     *
     * @param value value to look up
     * @return the enum constant with the specified value
     * @throws IllegalArgumentException if this enum type has no constant with the specified value
     */
    public static GlobalParameterType valueOfCI(String value) {
        return Arrays.stream(GlobalParameterType.values())
                .filter(gpt -> gpt.value.equalsIgnoreCase(value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Value = %s doesn't represent any enum's value.", value)));
    }

}
