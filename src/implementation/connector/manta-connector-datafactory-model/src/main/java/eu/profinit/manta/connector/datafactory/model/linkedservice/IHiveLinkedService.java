package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents HiveLinkedService resource.
 */
public interface IHiveLinkedService extends ILinkedService {

    /**
     * @return IP address or host name of the Hive server, separated by ';' for multiple hosts (only when serviceDiscoveryMode is enable).
     */
    IAdfFieldValue getHost();

    /**
     * @return The partial URL corresponding to the Hive server.
     */
    IAdfFieldValue getHttpPath();

    /**
     * @return The TCP port that the Hive server uses to listen for client connections.
     */
    IAdfFieldValue getPort();

    /**
     * @return The type of Hive server.
     */
    HiveServerType getServerType();

    /**
     * @return Specifies whether the driver uses native HiveQL queries,or converts them into an equivalent form in HiveQL.
     */
    IAdfFieldValue getUseNativeQuery();

    /**
     * @return The user name that you use to access Hive Server.
     */
    IAdfFieldValue getUsername();

}
