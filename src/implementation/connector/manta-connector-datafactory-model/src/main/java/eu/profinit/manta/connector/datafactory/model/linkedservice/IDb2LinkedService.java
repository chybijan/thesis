package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents Db2LinkedService resource.
 */
public interface IDb2LinkedService extends ILinkedService {

    /**
     * @return The connection string.
     * It is mutually exclusive with server, database, authenticationType, userName, packageCollection and certificateCommonName property.
     */
    IAdfFieldValue getConnectionString();

    /**
     * @return Database name for connection. It is mutually exclusive with connectionString property.
     */
    IAdfFieldValue getDatabase();

    /**
     * @return Under where packages are created when querying database. It is mutually exclusive with connectionString property.
     */
    IAdfFieldValue getPackageCollection();

    /**
     * @return Server name for connection. It is mutually exclusive with connectionString property.
     */
    IAdfFieldValue getServer();

    /**
     * @return Username for authentication. It is mutually exclusive with connectionString property.
     */
    IAdfFieldValue getUsername();

}
