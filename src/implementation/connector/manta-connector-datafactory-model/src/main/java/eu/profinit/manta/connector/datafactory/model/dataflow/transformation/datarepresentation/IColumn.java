package eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation;

import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.visitor.IColumnVisitor;

public interface IColumn {

    <T> T accept(IColumnVisitor<T> visitor);

}
