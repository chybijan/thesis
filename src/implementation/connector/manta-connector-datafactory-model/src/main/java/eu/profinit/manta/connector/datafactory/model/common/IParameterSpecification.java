package eu.profinit.manta.connector.datafactory.model.common;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * IParameterSpecification resource.
 */
public interface IParameterSpecification {

    JsonNode getDefaultValue();

    public ParameterType getType();
}
