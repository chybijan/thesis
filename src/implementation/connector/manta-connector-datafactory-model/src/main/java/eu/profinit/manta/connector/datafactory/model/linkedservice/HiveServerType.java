package eu.profinit.manta.connector.datafactory.model.linkedservice;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * The type of Hive server.
 */
public enum HiveServerType {

    HIVE_SERVER_1("HiveServer1"),

    HIVE_SERVER_2("HiveServer2"),

    HIVE_THRIFT_SERVER("HiveThriftServer");

    /**
     * Value of the HiveServerType resource type as it is.
     */
    private final String value;

    HiveServerType(String value) {
        this.value = value;
    }

    /**
     * Similar to {@link HiveServerType#valueOf(String)} but compares values case-insensitively instead.
     * Returns the enum constant of this type with the specified value.
     *
     * @param value value to look up
     * @return the enum constant with the specified value
     * @throws IllegalArgumentException if this enum type has no constant with the specified value
     */
    public static HiveServerType valueOfCI(String value) {
        return Arrays.stream(HiveServerType.values())
                .filter(dt -> StringUtils.equalsIgnoreCase(dt.value, value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Value = %s doesn't represent any enum's value.", value)));
    }

}
