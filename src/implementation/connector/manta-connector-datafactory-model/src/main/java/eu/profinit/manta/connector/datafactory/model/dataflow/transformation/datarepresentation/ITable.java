package eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation;

import java.util.Collection;

/**
 * Representation of table
 */
public interface ITable {

    /**
     * @return table columns
     */
    Collection<IColumn> getColumns();

}
