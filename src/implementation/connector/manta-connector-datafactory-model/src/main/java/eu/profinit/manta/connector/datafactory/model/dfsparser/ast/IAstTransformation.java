package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

import java.util.List;

/**
 * AST node for transformation
 */
public interface IAstTransformation extends IDFSNode {

    /**
     * @return input streams to this transformation
     */
    List<IAstADFStream> findInputStreams();

    /**
     * @return Stream of this transformation
     */
    IAstADFStream findTransformationStream();

    IAstConcreteTransformation findConcreteTransformation();

}
