package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents AzureSqlMILinkedService resource.
 */
public interface IAzureSqlMILinkedService extends ILinkedService {

    /**
     * @return The connection string.
     */
    IAdfFieldValue getConnectionString();

}
