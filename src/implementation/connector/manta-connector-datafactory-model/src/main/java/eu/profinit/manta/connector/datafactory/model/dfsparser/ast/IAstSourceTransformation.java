package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

/**
 * AST node for source transformation
 */
public interface IAstSourceTransformation extends IAstConcreteTransformation {

    IAstOutputAttribute findOutputAttribute();

}
