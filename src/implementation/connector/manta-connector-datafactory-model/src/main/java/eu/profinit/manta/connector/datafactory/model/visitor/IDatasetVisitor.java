package eu.profinit.manta.connector.datafactory.model.visitor;

import eu.profinit.manta.connector.datafactory.model.dataset.IUnknownDataset;
import eu.profinit.manta.connector.datafactory.model.dataset.database.IDatabaseDataset;
import eu.profinit.manta.connector.datafactory.model.dataset.database.ISybaseTableDataset;
import eu.profinit.manta.connector.datafactory.model.dataset.file.IFileDataset;

public interface IDatasetVisitor<T> {

    T visitSybaseTable(ISybaseTableDataset sybaseTableDataset);

    T visitDatabaseDataset(IDatabaseDataset databaseDataset);

    T visitFileDataset(IFileDataset fileDataset);

    T visitUnknownDataset(IUnknownDataset unknownDataset);

}
