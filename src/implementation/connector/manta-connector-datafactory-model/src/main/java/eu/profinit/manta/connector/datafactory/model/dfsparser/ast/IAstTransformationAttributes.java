package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;

import java.util.Map;
import java.util.Optional;

/**
 * Interface representing general transformation attribute
 */
public interface IAstTransformationAttributes extends IDFSNode {

    /**
     * Finds value of attribute by attribute name
     * @param attributeName name of attribute
     * @return expression node with value
     */
    Optional<IAstExpression> findValueByName(String attributeName);

    /**
     * Finds attribute node by attribute name
     * @param attributeName name of attribute
     * @return attribute AST node
     */
    Optional<IAstTransformationAttribute> findAttributeByName(String attributeName);

    /**
     * Transforms ast attributes node to map of attributes
     * @return map of attributes
     */
    Map<String, IExpression> resolveAttributes();


}
