package eu.profinit.manta.connector.datafactory.model;

import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;

/**
 * Interface represents LinkedService resource.
 */
public interface ILinkedService {

    LinkedServiceType getType();

    String getName();

    String getDescription();

    IParameters getParameters();

    <T> T accept(ILinkedServiceVisitor<T> visitor);

}
