package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents AzureDataLakeStoreLinkedService resource.
 */
public interface IAzureDataLakeStoreLinkedService extends ILinkedService {

    /**
     * @return Data Lake Store account name.
     */
    IAdfFieldValue getAccountName();

    /**
     * @return Data Lake Store service URI.
     */
    IAdfFieldValue getDataLakeStoreUri();

}
