package eu.profinit.manta.connector.datafactory.model.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;

import java.util.Arrays;

public interface IJoinTransformation extends ITransformation{

    IExpression getJoinCondition();

    JoinType getJoinType();

    enum JoinType{
        OUTER("outer"),
        INNER("inner"),
        LEFT("left"),
        RIGHT("right"),
        CROSS("cross"),
        UNKNOWN("unknown");

        private final String value;

        /**
         * @param value value of the join type resource
         */
        JoinType(String value) {
            this.value = value;
        }

        /**
         * Similar to {@link JoinType#valueOf(String)} but compares values case-insensitively instead.
         * Returns the enum constant of this type with the specified value.
         *
         * @param value value to look up
         * @return the enum constant with the specified value
         * @throws IllegalArgumentException if this enum type has no constant with the specified value
         */
        public static JoinType valueOfCI(String value) {
            return Arrays.stream(JoinType.values())
                    .filter(pt -> pt.value.equalsIgnoreCase(value))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException(String.format("Value = %s doesn't represent any enum's value.", value)));
        }
    }

}
