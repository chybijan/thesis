package eu.profinit.manta.connector.datafactory.model.dataset.database;

import eu.profinit.manta.connector.datafactory.model.IDataset;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

import java.util.Optional;

/**
 * Parent interface for the representation of all database-like Dataset resources.
 */
public interface IDatabaseDataset extends IDataset {

    /**
     * @return The database schema/dataset/database name of the given dataset.
     */
    Optional<IAdfFieldValue> getSchema();

    /**
     * @return The table name of the given dataset.
     */
    Optional<IAdfFieldValue> getTable();

}
