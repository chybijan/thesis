package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

/**
 * AST node for sink transformation
 */
public interface IAstSinkTransformation extends IAstConcreteTransformation {

}
