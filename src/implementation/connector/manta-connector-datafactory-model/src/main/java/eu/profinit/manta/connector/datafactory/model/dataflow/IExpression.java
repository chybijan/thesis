package eu.profinit.manta.connector.datafactory.model.dataflow;

public interface IExpression {

    String getRawForm();

    String asString();

}
