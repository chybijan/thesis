package eu.profinit.manta.connector.datafactory.model;

/**
 * Common interface representing important ADF elements like Factory, Dataflow
 */
public interface IADFElement {

    /**
     * @return name of element
     */
    String getName();

}
