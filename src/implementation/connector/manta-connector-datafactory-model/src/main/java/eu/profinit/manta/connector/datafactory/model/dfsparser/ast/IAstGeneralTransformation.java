package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

/**
 * AST node for general transformation
 * This is default transformatoin for unsupported transformations
 */
public interface IAstGeneralTransformation extends IAstConcreteTransformation {

}
