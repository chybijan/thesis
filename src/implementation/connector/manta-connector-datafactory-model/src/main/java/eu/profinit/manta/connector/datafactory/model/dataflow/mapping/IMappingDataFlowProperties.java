package eu.profinit.manta.connector.datafactory.model.dataflow.mapping;

import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowProperties;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowEndpoint;

import java.util.Collection;
import java.util.Map;

/**
 * Interface represents mapping data flow properties
 */
public interface IMappingDataFlowProperties extends IDataFlowProperties {

    /**
     * @return Sinks of data flow
     */
    Map<String, IDataFlowEndpoint> getSinks();

    /**
     * @return Transformation names
     */
    Collection<String> getTransformationNames();
    
}
