package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents FtpServerLinkedService resource.
 */
public interface IGeneralFtpServerLinkedService extends ILinkedService {

    /**
     * @return Host name of the server.
     */
    IAdfFieldValue getHost();

    /**
     * @return The TCP port number that the FTP server uses to listen for client connections.
     */
    IAdfFieldValue getPort();

    /**
     * @return Username to logon the FTP server.
     */
    IAdfFieldValue getUserName();

}
