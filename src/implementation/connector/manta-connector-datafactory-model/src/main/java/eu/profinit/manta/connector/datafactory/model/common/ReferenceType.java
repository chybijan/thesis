package eu.profinit.manta.connector.datafactory.model.common;

import java.util.Arrays;

public enum ReferenceType {

    DATASET("DatasetReference"),
    LINKED_SERVICE("LinkedServiceReference"),
    SCHEMA_LINKED_SERVICE("SchemaLinkedServiceReference"),
    UNKNOWN("unknown");

    /**
     * Value of the ParameterType resource.
     */
    private final String value;

    /**
     * @param value value of the ParameterType resource
     */
    ReferenceType(String value) {
        this.value = value;
    }

    /**
     * Similar to {@link ReferenceType#valueOf(String)} but compares values case-insensitively instead.
     * Returns the enum constant of this type with the specified value.
     *
     * @param value value to look up
     * @return the enum constant with the specified value
     * @throws IllegalArgumentException if this enum type has no constant with the specified value
     */
    public static ReferenceType valueOfCI(String value) {
        return Arrays.stream(ReferenceType.values())
                .filter(pt -> pt.value.equalsIgnoreCase(value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Value = %s doesn't represent any enum's value.", value)));
    }
}
