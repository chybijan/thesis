package eu.profinit.manta.connector.datafactory.model.common;

/**
 * Enum representing field types
 *
 * Field types are similar to {@link ParameterType} but only certain types can be recognized at field level
 */
public enum AdfFieldType {

    STRING,
    INT,
    FLOAT,
    ARRAY,
    OBJECT,
    EXPRESSION

}
