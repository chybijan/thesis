package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents AzureBlobFSLinkedService resource.
 */
public interface IAzureBlobFSLinkedService extends ILinkedService {

    /**
     * @return Endpoint for the Azure Data Lake Storage Gen2 service.
     */
    IAdfFieldValue getUrl();

}
