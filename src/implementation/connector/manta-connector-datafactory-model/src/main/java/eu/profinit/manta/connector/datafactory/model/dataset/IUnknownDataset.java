package eu.profinit.manta.connector.datafactory.model.dataset;

import eu.profinit.manta.connector.datafactory.model.IDataset;

/**
 * Interface represents dataset which type is not recognizable or supported by Manta.
 */
public interface IUnknownDataset extends IDataset {
}
