package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents SapHanaLinkedService resource.
 */
public interface ISapHanaLinkedService extends ILinkedService {

    /**
     * @return SAP HANA ODBC connection string.
     */
    IAdfFieldValue getConnectionString();

    /**
     * @return Host name of the SAP HANA server.
     */
    IAdfFieldValue getServer();

    /**
     * @return Username to access the SAP HANA server.
     */
    IAdfFieldValue getUserName();

}
