package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

import eu.profinit.manta.ast.IMantaAstNode;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowScript;

import java.util.List;

public interface IDFSNode extends IMantaAstNode {

    @Override
    IDFSNode getParent();

    @Override
    List<? extends IDFSNode> getChildren();

    /**
     * Fill data flow script with transformation data
     * @return filled dataflowScript
     */
    IDataFlowScript resolve(IDataFlowScript currentDFS);

}
