package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents OracleLinkedService resource.
 */
public interface IOracleLinkedService extends ILinkedService {

    /**
     * @return The connection string.
     */
    IAdfFieldValue getConnectionString();
}
