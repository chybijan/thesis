package eu.profinit.manta.connector.datafactory.model.dataset.database;

/**
 * Interface represents Dataset resource of type SnowflakeTable.
 */
public interface ISnowflakeDataset extends IDatabaseDataset {
}
