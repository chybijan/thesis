package eu.profinit.manta.connector.datafactory.model.dataset.file;

/**
 * Interface represents Dataset resource of type Xml.
 */
public interface IXmlDataset extends IFileDataset {
}
