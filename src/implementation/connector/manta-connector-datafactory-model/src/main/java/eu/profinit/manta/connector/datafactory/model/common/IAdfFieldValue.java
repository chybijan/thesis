package eu.profinit.manta.connector.datafactory.model.common;

/**
 * Interface represents the value of the Azure Data Factory resource field.
 * <br>
 * Value of the Azure Data Factory resource field can be one of the following:
 * <ul>
 *     <li>
 *         atomic value of any data type,
 *     </li>
 *     <li>
 *         expression resulting in the atomic value of the corresponding type.
 *     </li>
 * </ul>
 */
public interface IAdfFieldValue {

    /**
     * Alternative to toString method
     *
     * @return string representation of field
     */
    String getStringValue();

    AdfFieldType getType();

}
