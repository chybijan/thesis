package eu.profinit.manta.connector.datafactory.model.dataset.file.location;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents DatasetLocation resource of type AmazonS3.
 */
public interface IAmazonS3Location extends IFileDatasetLocation {

    IAdfFieldValue getBucketName();

    IAdfFieldValue getVersion();
}
