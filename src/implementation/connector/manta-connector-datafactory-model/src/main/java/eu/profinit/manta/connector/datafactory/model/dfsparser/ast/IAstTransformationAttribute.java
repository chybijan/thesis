package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

/**
 * Interface representing general transformation attribute
 */
public interface IAstTransformationAttribute extends IDFSNode {

    /**
     * @return name of attribute
     */
    IAstIdentifier findName();

    /**
     * @return value of attribute
     */
    IAstExpression findValue();

}
