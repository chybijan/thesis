package eu.profinit.manta.connector.datafactory.model.dataset.file;

/**
 * Interface represents Dataset resource of type Parquet.
 */
public interface IParquetDataset extends IFileDataset {
}
