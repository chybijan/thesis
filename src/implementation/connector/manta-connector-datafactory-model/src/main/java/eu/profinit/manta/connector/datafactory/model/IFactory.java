package eu.profinit.manta.connector.datafactory.model;

import eu.profinit.manta.connector.datafactory.model.factory.IGlobalParameterSpecification;

import java.util.Map;

/**
 * Interface represents Factory resource.
 */
public interface IFactory {
    String getName();

    Map<String, IGlobalParameterSpecification> getGlobalParameters();
}
