package eu.profinit.manta.connector.datafactory.model.common;

/**
 * Argument of the Azure Data Factory resource.
 * <br>
 * Consists of formal name of the parameter specified by the target resource, and actual value.
 */
public interface IArgument {

    String getName();

    IAdfFieldValue getFieldValue();
}
