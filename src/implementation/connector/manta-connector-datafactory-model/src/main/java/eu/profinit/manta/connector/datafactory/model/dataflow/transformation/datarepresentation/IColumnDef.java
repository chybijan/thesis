package eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation;

import eu.profinit.manta.connector.datafactory.model.dataflow.DataFlowDataType;

/**
 * Column definition
 */
public interface IColumnDef extends IColumn{

    /**
     * @return Name of the column
     */
    String getName();

    /**
     * @return Column data type
     */
    DataFlowDataType getType();

    /**
     * @return Format of this column
     */
    String getFormat();

}
