package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents AmazonS3LinkedService resource.
 */
public interface IAmazonS3LinkedService extends ILinkedService {

    /**
     * This value specifies the endpoint to access with the S3 Connector.
     */
    IAdfFieldValue getServiceUrl();

}
