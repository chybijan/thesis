package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents AzurePostgreSqlLinkedService resource.
 */
public interface IAzurePostgreSqlLinkedService extends ILinkedService {

    /**
     * @return An ODBC connection string.
     */
    IAdfFieldValue getConnectionString();

}
