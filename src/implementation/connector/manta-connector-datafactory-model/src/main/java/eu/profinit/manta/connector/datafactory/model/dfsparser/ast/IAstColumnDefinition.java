package eu.profinit.manta.connector.datafactory.model.dfsparser.ast;

/**
 * Interface for column definition inside source transformation
 */
public interface IAstColumnDefinition extends IDFSNode {

    /**
     * @return name of defined column
     */
    IAstIdentifier findColumnName();

    /**
     * @return data type of defined column
     */
    IAstDataType findDataType();

    /**
     * @return data format of defined column
     */
    IAstDataTypeFormat findDataTypeFormat();

}
