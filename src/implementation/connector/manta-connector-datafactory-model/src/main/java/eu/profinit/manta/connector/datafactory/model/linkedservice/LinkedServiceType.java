package eu.profinit.manta.connector.datafactory.model.linkedservice;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * Enum for LinkedService resource types.
 */
public enum LinkedServiceType {

    /**
     * Amazon Redshift.
     */
    AMAZON_REDSHIFT("AmazonRedshift"),

    /**
     * Amazon S3.
     */
    AMAZON_S3("AmazonS3"),

    /**
     * Azure Data Lake Storage Gen2.
     */
    AZURE_BLOB_FS("AzureBlobFS"),

    /**
     * Azure Blob storage.
     */
    AZURE_BLOB_STORAGE("AzureBlobStorage"),

    /**
     * Azure Data Lake Store.
     */
    AZURE_DATA_LAKE_STORE("AzureDataLakeStore"),

    /**
     * Azure File Storage.
     */
    AZURE_FILE_STORAGE("AzureFileStorage"),

    /**
     * Azure PostgreSQL.
     */
    AZURE_POSTGRESQL("AzurePostgreSql"),

    /**
     * Microsoft Azure SQL Database.
     */
    AZURE_SQL_DATABASE("AzureSqlDatabase"),

    /**
     * Azure SQL Data Warehouse.
     */
    AZURE_SQL_DW("AzureSqlDW"),

    /**
     * Azure SQL Managed Instance.
     */
    AZURE_SQL_MI("AzureSqlMI"),

    /**
     * DB2.
     */
    DB2("Db2"),

    /**
     * File system.
     */
    FILE_SERVER("FileServer"),

    /**
     * FTP server.
     */
    FTP_SERVER("FtpServer"),

    /**
     * Google BigQuery.
     */
    GOOGLE_BIGQUERY("GoogleBigQuery"),

    /**
     * Google Cloud Storage.
     */
    GOOGLE_CLOUD_STORAGE("GoogleCloudStorage"),

    /**
     * Greenplum Database.
     */
    GREENPLUM("Greenplum"),

    /**
     * Hadoop Distributed File System (HDFS).
     */
    HDFS("Hdfs"),

    /**
     * Hive Server.
     */
    HIVE("Hive"),

    /**
     * Netezza.
     */
    NETEZZA("Netezza"),

    /**
     * Oracle database.
     */
    ORACLE("Oracle"),

    /**
     * PostgreSQL data source.
     */
    POSTGRESQL("PostgreSql"),

    /**
     * SAP HANA.
     */
    SAP_HANA("SapHana"),

    /**
     * SSH File Transfer Protocol (SFTP) server.
     */
    SFTP("Sftp"),

    /**
     * Snowflake.
     */
    SNOWFLAKE("Snowflake"),

    /**
     * SQL Server.
     */
    SQL_SERVER("SqlServer"),

    /**
     * Sybase data source.
     */
    SYBASE("Sybase"),

    /**
     * Teradata data source.
     */
    TERADATA("Teradata"),

    /**
     * Unknown dataset. This value is used in case the type is not recognized or not supported by Manta.
     */
    UNKNOWN("Unknown"),
    ;

    /**
     * Value of the LinkedService resource type as it is in the "type" field.
     */
    private final String value;

    LinkedServiceType(String value) {
        this.value = value;
    }

    /**
     * Similar to {@link LinkedServiceType#valueOf(String)} but compares values case-insensitively instead.
     * Returns the enum constant of this type with the specified value.
     *
     * @param value value to look up
     * @return the enum constant with the specified value
     * @throws IllegalArgumentException if this enum type has no constant with the specified value
     */
    public static LinkedServiceType valueOfCI(String value) {
        return Arrays.stream(LinkedServiceType.values())
                .filter(dt -> StringUtils.equalsIgnoreCase(dt.value, value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Value = %s doesn't represent any enum's value.", value)));
    }

}
