package eu.profinit.manta.connector.datafactory.model.dataset.file.location;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * Enum for DatasetLocation resource types.
 */
public enum LocationType {

    /**
     * Amazon S3.
     */
    AMAZON_S3_LOCATION("AmazonS3Location"),

    /**
     * Azure Blob FS. (Azure Data Lake Storage Gen2)
     */
    AZURE_BLOB_FS_LOCATION("AzureBlobFSLocation"),

    /**
     * Azure Blob Storage.
     */
    AZURE_BLOB_STORAGE_LOCATION("AzureBlobStorageLocation"),

    /**
     * Azure Data Lake Store. (Azure Data Lake Storage Gen1)
     */
    AZURE_DATA_LAKE_STORE_LOCATION("AzureDataLakeStoreLocation"),

    /**
     * Azure File Storage.
     */
    AZURE_FILE_STORAGE_LOCATION("AzureFileStorageLocation"),

    /**
     * File Server. (File System)
     */
    FILE_SERVER_LOCATION("FileServerLocation"),

    /**
     * FTP Server. (FTP)
     */
    FTP_SERVER_LOCATION("FtpServerLocation"),

    /**
     * Google Cloud Storage.
     */
    GOOGLE_CLOUD_STORAGE_LOCATION("GoogleCloudStorageLocation"),

    /**
     * HDFS.
     */
    HDFS_LOCATION("HdfsLocation"),

    /**
     * HTTP.
     */
    HTTP_SERVER_LOCATION("HttpServerLocation"),

    /**
     * SFTP.
     */
    SFTP_LOCATION("SftpLocation");

    /**
     * Value of the DatasetLocation resource type as it is in the "type" field.
     */
    private final String value;

    /**
     * @param value value of the DatasetLocation resource type as it is in the "type" field
     */
    LocationType(String value) {
        this.value = value;
    }

    /**
     * Similar to {@link LocationType#valueOf(String)} but compares values case-insensitively instead.
     * Returns the enum constant of this type with the specified value.
     *
     * @param value value to look up
     * @return the enum constant with the specified value
     * @throws IllegalArgumentException if this enum type has no constant with the specified value
     */
    public static LocationType valueOfCI(String value) {
        return Arrays.stream(LocationType.values())
                .filter(dt -> StringUtils.equalsIgnoreCase(dt.value, value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Value = %s doesn't represent any enum's value.", value)));
    }

}
