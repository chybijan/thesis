package eu.profinit.manta.connector.datafactory.model;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.model.dataset.DatasetType;
import eu.profinit.manta.connector.datafactory.model.visitor.IDatasetVisitor;


import java.util.Collection;

/**
 * Interface represents Dataset resource.
 */
public interface IDataset {

    /**
     * @return Gets all references dataset works with
     */
    Collection<IReference> getAllReferences();

    /**
     * @return Gets unique name of dataset
     */
    String getName();

    IReference getLinkedServiceReference();

    IParameters getParameters();

    IDatasetSchema getDatasetSchema();

    JsonNode getDatasetStructure();

    DatasetType getType();

    <T> T accept(IDatasetVisitor<T> visitor);

}
