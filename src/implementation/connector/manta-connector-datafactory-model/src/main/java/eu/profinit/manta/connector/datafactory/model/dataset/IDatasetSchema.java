package eu.profinit.manta.connector.datafactory.model.dataset;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

import java.util.List;

/**
 * Physical type schema of the dataset.
 */
public interface IDatasetSchema {

    List<IDataElement> getDataElements();

    IAdfFieldValue getExpression();

    /**
     * Single element in the dataset schema representation.
     */
    interface IDataElement{

        IAdfFieldValue getName();

        IAdfFieldValue getType();
    }
}
