package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents FileServerLinkedService resource.
 */
public interface IFileServerLinkedService extends ILinkedService {

    /**
     * @return Host name of the server.
     */
    IAdfFieldValue getHost();

}
