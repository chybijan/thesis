package eu.profinit.manta.connector.datafactory.model.dataflow;

public interface IDataflowParameter {

    String getName();

    DataFlowDataType getDataType();

    /**
     * @return AST node containing expression
     */
    IExpression getDefaultValue();

}
