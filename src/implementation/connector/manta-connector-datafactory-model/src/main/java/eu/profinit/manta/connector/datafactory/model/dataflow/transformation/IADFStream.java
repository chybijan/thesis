package eu.profinit.manta.connector.datafactory.model.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.IADFElement;

/**
 * Interface for data flow transformation
 */
public interface IADFStream extends IADFElement {

    /**
     * @return main part of stream
     */
    String getName();

    /**
     * @return secondary name of stream
     */
    String getSubstreamName();

}
