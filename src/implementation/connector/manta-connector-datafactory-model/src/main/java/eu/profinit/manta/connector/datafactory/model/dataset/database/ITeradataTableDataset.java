package eu.profinit.manta.connector.datafactory.model.dataset.database;

/**
 * Interface represents Dataset resource of type TeradataTable.
 */
public interface ITeradataTableDataset extends IDatabaseDataset {
}
