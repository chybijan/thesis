package eu.profinit.manta.connector.datafactory.model;

import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowProperties;

import java.util.Collection;

/**
 * Interface represents data flow resource.
 */
public interface IDataFlow extends IADFElement {

    /**
     * @return Dataflow properties
     */
     IDataFlowProperties getProperties();

    /**
     * @return Gets all references data flow needs for its correct execution
     */
    Collection<IReference> getAllReferences();

}
