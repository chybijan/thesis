package eu.profinit.manta.connector.datafactory.model.dataflow;

import java.util.Arrays;

/**
 * Type of data flow parameter based on spark types
 */
public enum DataFlowType {

    /**
     * Classical mapping data flow
     */
    MAPPING_DATA_FLOW("MappingDataFlow"),

    /**
     * Wrangling data flow for Power query
     */
    WRANGLING_DATA_FLOW("WranglingDataFlow");

    /**
     * Value of the ParameterType resource.
     */
    private final String value;

    /**
     * @param value value of the ParameterType resource
     */
    DataFlowType(String value) {
        this.value = value;
    }

    /**
     * Similar to {@link DataFlowType#valueOf(String)} but compares values case-insensitively instead.
     * Returns the enum constant of this type with the specified value.
     *
     * @param value value to look up
     * @return the enum constant with the specified value
     * @throws IllegalArgumentException if this enum type has no constant with the specified value
     */
    public static DataFlowType valueOfCI(String value) {
        return Arrays.stream(DataFlowType.values())
                .filter(pt -> pt.value.equalsIgnoreCase(value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Value = %s doesn't represent any enum's value.", value)));
    }

}
