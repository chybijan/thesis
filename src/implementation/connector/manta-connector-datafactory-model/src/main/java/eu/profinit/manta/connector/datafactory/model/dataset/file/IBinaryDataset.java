package eu.profinit.manta.connector.datafactory.model.dataset.file;

/**
 * Interface represents Dataset resource of type Binary.
 */
public interface IBinaryDataset extends IFileDataset {
}
