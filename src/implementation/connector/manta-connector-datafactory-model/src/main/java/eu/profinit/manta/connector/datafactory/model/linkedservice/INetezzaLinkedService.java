package eu.profinit.manta.connector.datafactory.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Interface represents NetezzaLinkedService resource.
 */
public interface INetezzaLinkedService extends ILinkedService {

    /**
     * @return An ODBC connection string.
     */
    IAdfFieldValue getConnectionString();
}
