package eu.profinit.manta.connector.datafactory.model.dataflow.transformation;

/**
 * Interface for unknown transformations
 */
public interface IUnknownTransformation extends ITransformation{

}
