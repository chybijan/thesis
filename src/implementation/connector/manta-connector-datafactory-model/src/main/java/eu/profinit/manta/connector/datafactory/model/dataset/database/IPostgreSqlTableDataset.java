package eu.profinit.manta.connector.datafactory.model.dataset.database;

/**
 * Interface represents Dataset resource of type PostgreSqlTable.
 */
public interface IPostgreSqlTableDataset extends IDatabaseDataset {
}
