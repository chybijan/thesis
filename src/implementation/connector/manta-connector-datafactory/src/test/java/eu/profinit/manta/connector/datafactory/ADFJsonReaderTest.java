package eu.profinit.manta.connector.datafactory;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.profinit.manta.connector.datafactory.model.IADF;
import eu.profinit.manta.connector.datafactory.model.IDataset;
import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.ReferenceType;
import eu.profinit.manta.connector.datafactory.resolver.ParserService;

import eu.profinit.manta.connector.datafactory.resolver.model.common.Reference;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.AmazonRedshiftTableDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AmazonRedshiftLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.ParserServiceImpl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Test for ADF JSON reader
 */
@RunWith(MockitoJUnitRunner.class)
public class ADFJsonReaderTest {

    private final Path TEST_INPUT_FOLDER_PATH = Paths.get("src/test/resources/input");

    private final Path FULL_FACTORY_PATH = TEST_INPUT_FOLDER_PATH.resolve("testFactory");

    private final File FULL_FACTORY = FULL_FACTORY_PATH.toFile();

    private final File EMPTY_FACTORY = TEST_INPUT_FOLDER_PATH.resolve("emptyFactory").toFile();

    /**
     * Location of spring configuration file.
     */
    protected static final String SPRING_CONFIG = "spring/DataFactoryReaderTest.xml";

    /**
     * Spring context.
     */
    private static ClassPathXmlApplicationContext springContext;

    private ADFJsonReader adfJsonReader;

    private ObjectMapper mapper;

    @Mock
    private ParserService parserServiceMock;

    private static ParserServiceImpl realParserService;

    @BeforeClass
    public static void setUpClass() {
        // Init Spring context, get ParserService
        springContext = new ClassPathXmlApplicationContext(SPRING_CONFIG);
        realParserService = springContext.getBean(ParserServiceImpl.class);
    }

    @Before
    public void init() throws JsonProcessingException {

        adfJsonReader = new ADFJsonReader();
        adfJsonReader.setInputFile(FULL_FACTORY);

        Mockito.when(parserServiceMock.parseDataset(Mockito.any())).thenReturn(null);
        Mockito.when(parserServiceMock.parseLinkedService(Mockito.any())).thenReturn(null);
        Mockito.when(parserServiceMock.parseDataFlow(Mockito.any())).thenReturn(null);

        adfJsonReader.setParserService(parserServiceMock);

        // Setup mapper
        mapper = new ObjectMapper();

    }

    @Test
    public void readDataFlowFile() throws IOException {
        // Test this without mocking
        adfJsonReader.setParserService(realParserService);

        File file = FULL_FACTORY_PATH.resolve("dataflow/filterUsers.json").toFile();

        IADF dataFlowResult = adfJsonReader.readFile(file);

        Assert.assertTrue(dataFlowResult.isValid());
        Assert.assertFalse(dataFlowResult.getFactory().isPresent());
        Assert.assertFalse(dataFlowResult.getPipeline().isPresent());
        Assert.assertTrue(dataFlowResult.getDataFlow().isPresent());
        Assert.assertEquals(3, dataFlowResult.getDatasets().size());
        Assert.assertEquals(3, dataFlowResult.getLinkedServices().size());
    }

    @Test
    public void readPipelineFile() throws IOException {
        // Test this without mocking
        adfJsonReader.setParserService(realParserService);

        File file = FULL_FACTORY_PATH.resolve("pipeline/dynamicTest.json").toFile();

        IADF adf = adfJsonReader.readFile(file);
        Assert.assertFalse(adf.isValid());
    }

    @Test
    public void readPipeline(){
        Assert.assertThrows(UnsupportedOperationException.class, () -> adfJsonReader.readPipeline(null));
    }

    @Test
    public void readDataFlow() throws IOException {

        // Test this without mocking
        adfJsonReader.setParserService(realParserService);

        File file = FULL_FACTORY_PATH.resolve("dataflow/filterUsers.json").toFile();
        JsonNode dataFlowNode = mapper.readTree(file);

        IADF dataFlowResult = adfJsonReader.readDataFlow(dataFlowNode);

        Assert.assertTrue(dataFlowResult.isValid());
        Assert.assertFalse(dataFlowResult.getFactory().isPresent());
        Assert.assertFalse(dataFlowResult.getPipeline().isPresent());
        Assert.assertTrue(dataFlowResult.getDataFlow().isPresent());
        Assert.assertEquals(3, dataFlowResult.getDatasets().size());
        Assert.assertEquals(3, dataFlowResult.getLinkedServices().size());

    }

    @Test
    public void readDataFlowNull() throws IOException {

        JsonNode dataFlowNode = mapper.readTree("{}");

        Assert.assertThrows(IOException.class, () -> adfJsonReader.readDataFlow(dataFlowNode));

    }

    @Test
    public void getFolderNameFromReference(){
        IReference ref1 = new Reference("ref1", new ArrayList<>(), ReferenceType.DATASET);
        IReference ref2 = new Reference("ref2", new ArrayList<>(), ReferenceType.LINKED_SERVICE);

        String folderName = adfJsonReader.getFolderNameFromReference(ref1);
        Assert.assertEquals(ADFJsonReader.DATASET_FOLDER, folderName);

        folderName = adfJsonReader.getFolderNameFromReference(ref2);
        Assert.assertEquals(ADFJsonReader.LINKED_SERVICE_FOLDER, folderName);
    }

    @Test
    public void findReferenceInFileSystem() throws IOException {

        IReference datasetReference = new Reference("DelimitedText1", new ArrayList<>(), ReferenceType.DATASET);

        JsonNode referenceNode =  adfJsonReader.findReferenceInFileSystem(datasetReference);

        // Setup reader
        JsonNode expectedNode = mapper.readTree(new File(FULL_FACTORY.getAbsolutePath() + "/dataset/DelimitedText1.json"));

        Assert.assertEquals(expectedNode, referenceNode);

    }

    @Test
    public void findReferenceInFileSystemMissingResourceFolder(){

        adfJsonReader.setInputFile(EMPTY_FACTORY);

        IReference datasetReference = new Reference("DelimitedText1", new ArrayList<>(), ReferenceType.DATASET);

        JsonNode referenceNode =  adfJsonReader.findReferenceInFileSystem(datasetReference);

        Assert.assertNull(referenceNode);

    }

    @Test
    public void findReferenceInFileSystemBadJSON(){

        adfJsonReader.setInputFile(FULL_FACTORY);

        IReference datasetReference = new Reference("badJSON", new ArrayList<>(), ReferenceType.DATASET);

        JsonNode referenceNode =  adfJsonReader.findReferenceInFileSystem(datasetReference);

        Assert.assertNull(referenceNode);

    }

    @Test
    public void parseDatasetResourcesFromReferences(){

        Collection<IReference> referencesToParse = new ArrayList<>();

        IReference ref1 = new Reference("Json1", new ArrayList<>(), ReferenceType.DATASET);
        IReference ref2 = new Reference("Users", new ArrayList<>(), ReferenceType.DATASET);

        referencesToParse.add(ref1);
        referencesToParse.add(ref2);

        IDataset dataset = new AmazonRedshiftTableDataset("mockDataset", null, null, null, null, null, null);
        IDataset dataset2 = new AmazonRedshiftTableDataset("mockDataset2", null, null, null, null, null, null);

        Mockito.when(parserServiceMock.parseDataset(Mockito.any())).thenReturn(dataset).thenReturn(dataset2);

        Collection<IDataset> parsedResources = adfJsonReader.parseResourcesFromReferences(referencesToParse, parserServiceMock::parseDataset);

        Assert.assertEquals(2, parsedResources.size());
        Assert.assertNotEquals(parsedResources.stream().collect(Collectors.toList()).get(0), parsedResources.stream().collect(Collectors.toList()).get(1));
    }

    @Test
    public void parseLinkedServiceResourcesFromReferences(){

        Collection<IReference> referencesToParse = new ArrayList<>();

        IReference ref1 = new Reference("AzureBlobStorage1", new ArrayList<>(), ReferenceType.LINKED_SERVICE);
        IReference ref2 = new Reference("postgresqlConnection", new ArrayList<>(), ReferenceType.LINKED_SERVICE);

        referencesToParse.add(ref1);
        referencesToParse.add(ref2);

        ILinkedService ls1 = new AmazonRedshiftLinkedService("lsname1", null, null, null, null, null, null);
        ILinkedService ls2 = new AmazonRedshiftLinkedService("lsname2", null, null, null, null, null, null);

        Mockito.when(parserServiceMock.parseLinkedService(Mockito.any())).thenReturn(ls1).thenReturn(ls2);

        Collection<ILinkedService> parsedResources = adfJsonReader.parseResourcesFromReferences(referencesToParse, parserServiceMock::parseLinkedService);

        Assert.assertEquals(2, parsedResources.size());
        Assert.assertNotEquals(parsedResources.stream().collect(Collectors.toList()).get(0), parsedResources.stream().collect(Collectors.toList()).get(1));

    }

    @Test
    public void parseMixedResourcesFromReferences(){

        Collection<IReference> referencesToParse = new ArrayList<>();

        IReference ref1 = new Reference("Json1", new ArrayList<>(), ReferenceType.DATASET);
        IReference ref2 = new Reference("AzureBlobStorage1", new ArrayList<>(), ReferenceType.LINKED_SERVICE);

        referencesToParse.add(ref1);
        referencesToParse.add(ref2);

        IDataset dataset = new AmazonRedshiftTableDataset("mockDataset", null, null, null, null, null, null);

        Mockito.when(parserServiceMock.parseDataset(Mockito.any())).thenReturn(dataset).thenReturn(null);

        Collection<IDataset> parsedResources = adfJsonReader.parseResourcesFromReferences(referencesToParse, parserServiceMock::parseDataset);

        Assert.assertEquals(1, parsedResources.size());

    }

    @Test
    public void parseLinkedServicesFromDatasets(){

        Collection<IDataset> datasetCollection = new ArrayList<>();

        IDataset dataset1 = new AmazonRedshiftTableDataset("mockDataset", null, null, null, null, null, null);
        IDataset dataset2 = new AmazonRedshiftTableDataset("mockDataset2", null, null, null, null, null, null);

        datasetCollection.add(dataset1);
        datasetCollection.add(dataset2);

        Collection<ILinkedService> serviceCollection = new ArrayList<>();
        ILinkedService ls1 = new AmazonRedshiftLinkedService("lsname1", null, null, null, null, null, null);
        ILinkedService ls2 = new AmazonRedshiftLinkedService("lsname2", null, null, null, null, null, null);
        serviceCollection.add(ls1);
        serviceCollection.add(ls2);

        ADFJsonReader spyReader = Mockito.spy(adfJsonReader);
        Mockito.doReturn(serviceCollection).when(spyReader).parseResourcesFromReferences(Mockito.any(), Mockito.any());

        Collection<ILinkedService> parsedServices = spyReader.parseLinkedServicesFromDatasets(datasetCollection);

        Assert.assertEquals(4, parsedServices.size());
    }

    @Test
    public void parentIs(){

        File file = new File("root/parent1/parent2/file.json");
        File folder = new File("root/parent1/parent2");

        Assert.assertTrue(ADFJsonReader.parentIs(file, "parent2"));
        Assert.assertFalse(ADFJsonReader.parentIs(file, "parent1"));
        Assert.assertFalse(ADFJsonReader.parentIs(file, "nonsense"));

        Assert.assertTrue(ADFJsonReader.parentIs(folder, "parent1"));
        Assert.assertFalse(ADFJsonReader.parentIs(folder, "root"));
        Assert.assertFalse(ADFJsonReader.parentIs(folder, "nonsense"));

    }

    @Test
    public void parentIsWithoutParent(){

        File file = new File("file.json");

        Assert.assertFalse(ADFJsonReader.parentIs(file, "parent2"));

    }

    @Test
    public void resourceComparatorTest(){

        ADFJsonReader.ADFResourceComparator comparator = new ADFJsonReader.ADFResourceComparator(adfJsonReader.priorityTable);

        File dataFlowFolder = FULL_FACTORY_PATH.resolve("dataflow").toFile();
        File pipelineFolder = FULL_FACTORY_PATH.resolve("pipeline").toFile();
        File factoryFolder = FULL_FACTORY_PATH.resolve("factory").toFile();
        File nonsenseFolder = FULL_FACTORY_PATH.resolve("nonsense").toFile();

        File dataFlow = FULL_FACTORY_PATH.resolve("dataflow/dataflowName.json").toFile();
        File pipeline = FULL_FACTORY_PATH.resolve("pipeline/pipelineName.json").toFile();
        File factory = FULL_FACTORY_PATH.resolve("factory/factoryName.json").toFile();

        File nonsense = FULL_FACTORY_PATH.resolve("nonsense/someName.json").toFile();

        Assert.assertEquals(0, comparator.compare(dataFlowFolder, dataFlow));
        Assert.assertEquals(0, comparator.compare(pipelineFolder, pipeline));
        Assert.assertEquals(0, comparator.compare(factory, factoryFolder));

        Assert.assertTrue(comparator.compare(dataFlowFolder, pipelineFolder) > 0);
        Assert.assertTrue(comparator.compare(pipeline, dataFlow) < 0);

        Assert.assertTrue(comparator.compare(dataFlowFolder, factoryFolder) > 0);
        Assert.assertTrue(comparator.compare(factory, dataFlow) < 0);

        Assert.assertTrue(comparator.compare(pipelineFolder, factoryFolder) > 0);
        Assert.assertTrue(comparator.compare(factory, pipeline) < 0);

        Assert.assertEquals(0, comparator.compare(dataFlowFolder, nonsenseFolder));

    }

}
