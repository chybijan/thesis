package eu.profinit.manta.connector.datafactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.profinit.manta.connector.datafactory.errors.Categories;
import eu.profinit.manta.connector.datafactory.model.IADF;
import eu.profinit.manta.connector.datafactory.model.IDataFlow;
import eu.profinit.manta.connector.datafactory.model.IDataset;
import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.ReferenceType;
import eu.profinit.manta.connector.datafactory.resolver.ParserService;
import eu.profinit.manta.connector.datafactory.resolver.model.ADF;
import eu.profinit.manta.connector.datafactory.resolver.model.Factory;

import eu.profinit.manta.platform.automation.AbstractFileInputReader;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import org.apache.commons.io.filefilter.*;

import java.io.File;
import java.io.IOException;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ADFJsonReader extends AbstractFileInputReader<IADF> {

    private static final Logger LOGGER = new Logger(ADFJsonReader.class);

    protected static final String FACTORY_FOLDER        = "factory";
    protected static final String PIPELINE_FOLDER       = "pipeline";
    protected static final String DATAFLOW_FOLDER       = "dataflow";
    protected static final String DATASET_FOLDER        = "dataset";
    protected static final String LINKED_SERVICE_FOLDER = "linkedService";

    protected final String REFERENCE_SUFFIX = ".json";

    /**
     * Table of folder priorities
     */
    protected Map<String, Integer> priorityTable;

    private ParserService parserService;

    // TODO potentially add factory to know global parameters
    private Optional<Factory> parsedFactory;

    public void setParserService(ParserService parserService) {
        this.parserService = parserService;
    }

    public ADFJsonReader() {

        priorityTable = new HashMap<>();

        priorityTable.put(FACTORY_FOLDER, -1);
        priorityTable.put(PIPELINE_FOLDER, 0);
        priorityTable.put(DATAFLOW_FOLDER, 1);
        priorityTable.put(DATASET_FOLDER, 2);
        priorityTable.put(LINKED_SERVICE_FOLDER, 2);


        Comparator<File> adfComparator = new ADFResourceComparator(priorityTable);
        setComparator(adfComparator);
    }

    public Map<String, Integer> getPriorityTable() {
        return priorityTable;
    }

    public void setPriorityTable(Map<String, Integer> priorityTable) {
        this.priorityTable = priorityTable;
    }

    @Override
    public void setInputFile(File inputFile) {

        // Filters so only important resources are scanned
        List<IOFileFilter> resourceFilters = Arrays.asList(
                new ParentIsFilter(PIPELINE_FOLDER),
                new ParentIsFilter(DATAFLOW_FOLDER)
        );

        super.setInputFile(inputFile);

        // Set all the filters
        setFilter(
                new OrFileFilter(
                        new AndFileFilter(
                            new SuffixFileFilter(".json"),
                            new OrFileFilter(
                                    resourceFilters
                            )
                        ),
                        DirectoryFileFilter.DIRECTORY
                )
        );
    }

    @Override
    protected IADF readFile(File file) throws IOException {
        LOGGER.info("Reading file: " + file.getPath());

        IADF adf = new ADF();
        try{
             // Setup reader
            ObjectMapper mapper = new ObjectMapper();
            JsonNode rootNode = mapper.readTree(file);

            // Check what kind of resource we are dealing with
            if(parentIs(file, PIPELINE_FOLDER)) {
                return readPipeline(rootNode);
            }
            else if(parentIs(file, DATAFLOW_FOLDER)){
                return readDataFlow(rootNode);
            }
            else {
                LOGGER.log(Categories.inputStructureErrors().unsupportedResourceType().file(Paths.get(file.getAbsolutePath()).normalize().toString()));
                return adf;
            }

        } catch (IOException | NullPointerException e) {
            LOGGER.log(Categories.inputStructureErrors().parsingJson().file(Paths.get(file.getAbsolutePath()).normalize().toString()).catching(e));
            return adf;
        } catch (UnsupportedOperationException e) {
            LOGGER.log(Categories.inputStructureErrors().unsupportedResourceType().file(Paths.get(file.getAbsolutePath()).normalize().toString()).catching(e));
            return adf;
        } catch (Exception e) {
            LOGGER.log(Categories.inputStructureErrors().parsingJson().file(Paths.get(file.getAbsolutePath()).normalize().toString()).catching(e));
            return adf;
        }

    }

    /**
     * Read pipeline json resource
     * @param node pipeline node
     * @return ADF result with pipeline
     */
    protected IADF readPipeline(JsonNode node){
        // TODO parse pipeline
        throw new UnsupportedOperationException();
    }

    /**
     * Read data flow json resource
     * @param node data flow node
     * @return ADF result with data flow
     * @throws IOException node couldn't be parsed
     */
    protected IADF readDataFlow(JsonNode node) throws IOException {

        IDataFlow df = parserService.parseDataFlow(node);
        if(df != null){

            // Create resource
            ADF adfResource = new ADF(df);

            Set<IReference> datasetReferences = new HashSet<>();
            Set<IReference> linkedServiceReferences = new HashSet<>();

            // Split references into datasets and linked services
            for(IReference reference : df.getAllReferences()){
                if(reference.getType().equals(ReferenceType.LINKED_SERVICE)){
                    linkedServiceReferences.add(reference);
                }
                else{
                    datasetReferences.add(reference);
                }
            }

            // Parse real resources from references from data flow
            Collection<IDataset> datasets = parseResourcesFromReferences(datasetReferences, parserService::parseDataset);
            Collection<ILinkedService> dataFlowLinkedServices = parseResourcesFromReferences(linkedServiceReferences, parserService::parseLinkedService);

            // Parse real resources from references from dataset
            Collection<ILinkedService> linkedServices = parseLinkedServicesFromDatasets(datasets);

            // Join all linked services
            linkedServices.addAll(dataFlowLinkedServices);

            // Remove duplicate linked services
            Set<String> parsedLinkedServicesNames = new HashSet<>();
            List<ILinkedService> uniqueLinkedServices = linkedServices.stream()
                    .filter(iLinkedService -> parsedLinkedServicesNames.add(iLinkedService.getName()))
                    .collect(Collectors.toList());


            adfResource.addDatasets(datasets);
            adfResource.addLinkedServices(uniqueLinkedServices);

            return adfResource;

        }

        throwIO("Data flow is null");
        return null;
    }

    protected String getFolderNameFromReference(IReference reference){
        switch (reference.getType()){
            case DATASET:
                return DATASET_FOLDER;
            case LINKED_SERVICE:
                return LINKED_SERVICE_FOLDER;
            default:
                // This should not happen
                return DATASET_FOLDER;
        }
    }

    /**
     * Find reference in file system, based on its type
     * @param reference reference to resource
     * @return root json node of resource file referenced by reference,
     * or null if referenced file is does not exist, or it has bad format
     */
    protected JsonNode findReferenceInFileSystem(IReference reference){

        // Get root input folder
        File rootFolder = getInputFile();
        Path rootPath = Paths.get(rootFolder.getAbsolutePath());

        // Get folder name where reference stores its files
        String currentFolderName = getFolderNameFromReference(reference);

        // Try to get folder path
        Path concreteFolderPath;
        try {

            concreteFolderPath = rootPath.resolve(currentFolderName);

        }catch (InvalidPathException e){

           LOGGER.log(Categories.inputStructureErrors().resourceFolderNotFoundInRoot()
                   .rootPath(rootPath.toAbsolutePath())
                   .folderName(currentFolderName));

           return null;

        }

        // Try to get path to the resource file
        Path filePath;
        try {

            filePath = concreteFolderPath.resolve(reference.getReferenceName() + REFERENCE_SUFFIX);

        }catch (InvalidPathException e){

            LOGGER.log(Categories.inputStructureErrors().resourceFileNotFoundInFolder()
                    .folderPath(concreteFolderPath.toAbsolutePath())
                    .fileName(reference.getReferenceName()));

            return null;

        }

        File resourceFile = filePath.toFile();

        if(resourceFile.exists()){
            // Setup reader
            ObjectMapper mapper = new ObjectMapper();
            try {
                return mapper.readTree(resourceFile);
            }
            catch (IOException e){
                return null;
            }
        }

        LOGGER.log(Categories.inputStructureErrors().resourceFileNotFound()
                .filePath(resourceFile.getPath()));

        return null;
    }

    /**
     * Finds resources referenced by references and creates it's models
     * @param references Collection of references
     * @param parseFunction function accepting JsonNode, which can parse correct resource type from the json
     * @param <T> Resource type we want to get from references. References have
     * @return Collection of resources
     */
    protected <T> Collection<T> parseResourcesFromReferences(Collection<IReference> references,
                                                           Function<JsonNode, T> parseFunction){

        // TODO this method could be optimalized with cache DEV-19339
        Collection<T> resources = new ArrayList<>();

        for(IReference reference : references){

            JsonNode datasetNode = findReferenceInFileSystem(reference);

            T dataset = parseFunction.apply(datasetNode);

            if(dataset == null){
                LOGGER.log(Categories.inputStructureErrors().referenceCouldNotBeRead()
                        .referenceName(reference.getReferenceName())
                        .referenceType(reference.getType().toString()));
            }
            else{
                resources.add(dataset);
            }
        }

        return resources;
    }

    /**
     * For each dataset find it's linked service and return it
     * @param datasets dataset which are searched
     * @return linked services for searched datasets
     */
    protected Collection<ILinkedService> parseLinkedServicesFromDatasets(Collection<IDataset> datasets){

        Collection<ILinkedService> linkedServices = new ArrayList<>();

        for(IDataset dataset : datasets){
            Collection<IReference> ref = dataset.getAllReferences();
            Collection<ILinkedService> ll = parseResourcesFromReferences(ref, parserService::parseLinkedService);
            linkedServices.addAll(ll);
        }

        return linkedServices;
    }

    /**
     * Checks if parent of file is equal to parent
     * @param file file
     * @param parent name of the parent we are trying to find
     * @return if "parent" string is parent of file
     */
    protected static boolean parentIs(File file, String parent){

        if(file.getParent() == null)
            return false;

        String parentName = file.getParentFile().getName();
        return parentName.equals(parent);

    }

    /**
     * Filter for files, which are included in a directory.
     */
    protected static class ParentIsFilter extends AbstractFileFilter {
        private final String parentFolderName;

        public ParentIsFilter(String parentFolderName) {
            this.parentFolderName = parentFolderName;
        }

        @Override
        public boolean accept(File file) {
            return parentIs(file, parentFolderName);
        }
    }

    /**
     * Comparator for two files based on compare table.
     * If at least one of files is not in compare table, result of comparison is 0
     */
    protected static class ADFResourceComparator implements Comparator<File> {

        /**
         * This table contains names of folders in which resources are.
         */
        Map<String, Integer> nameToPriorityTable;

        public ADFResourceComparator(Map<String, Integer> nameToPriorityTable) {
            this.nameToPriorityTable = nameToPriorityTable;
        }

        /**
         * Compares two names, that we already know are in nameToPriorityTable.
         * @param fileName1 first filename
         * @param fileName2 second filename
         * @return result of camparison
         */
        private int compareExistingFileNames(String fileName1, String fileName2){
            Integer priority1 = nameToPriorityTable.get(fileName1);
            Integer priority2 = nameToPriorityTable.get(fileName2);
            return Integer.compare(priority1, priority2);
        }

        /**
         * Compare based on compare table passed in constructor.
         * Comparison compares two files and if files are folders, they are compared together.
         * If files are basic files, compare looks at their parents and parents are compared
         * @param file1 first file
         * @param file2 second file
         * @return result of comparison between folders in priority table
         */
        @Override
        public int compare(File file1, File file2) {
            LOGGER.info("COMPARING: " + file1.getName() + " and " + file2.getName());
            if( nameToPriorityTable.containsKey(file1.getName())
                    && nameToPriorityTable.containsKey(file2.getName())) {
                LOGGER.info("RESULT FOLDER: " + compareExistingFileNames(file1.getName(), file2.getName()));
                return compareExistingFileNames(file1.getName(), file2.getName());
            }
            else if(file1.getParent() != null && file2.getParent() != null){
                String parentName1 = file1.getParentFile().getName();
                String parentName2 = file2.getParentFile().getName();
                if(nameToPriorityTable.containsKey(parentName1) && nameToPriorityTable.containsKey(parentName2)){
                    LOGGER.info("RESULT FILE: " + compareExistingFileNames(parentName1, parentName2));
                    return compareExistingFileNames(parentName1, parentName2);
                }
            }
            LOGGER.info("RESULT: 0");
            return 0;
        }
    }
}
