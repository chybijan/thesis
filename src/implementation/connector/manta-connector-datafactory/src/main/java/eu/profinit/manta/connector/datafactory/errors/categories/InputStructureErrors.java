package eu.profinit.manta.connector.datafactory.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.Error;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.common.errordefinitions.technologies.reporting.ParsingJsonBuilder;
import eu.profinit.manta.platform.logging.common.messages.CommonMessages;

public class InputStructureErrors extends eu.profinit.manta.platform.logging.common.errordefinitions.technologies.reporting.InputStructureErrors {

    @Error(
            userMessage = "Unsupported resource type encountered at %{file}.",
            technicalMessage = "Unsupported resource type encountered at %{file}.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SCRIPT
    )
    public UnsupportedResourceTypeBuilder unsupportedResourceType() {
        return new UnsupportedResourceTypeBuilder(this);
    }

    @Error(
            userMessage = "Reference %{referenceName} of type %{referenceType} could not be read.",
            technicalMessage = "Reference %{referenceName} of type %{referenceType} could not be read.",
            solution = "Please check if resource of type %{referenceType} named %{referenceName} exists or " + CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.WARN,
            lineageImpact = Impact.SCRIPT
    )
    public ReferenceCouldNotBeReadBuilder referenceCouldNotBeRead() {
        return new ReferenceCouldNotBeReadBuilder(this);
    }

    @Error(
            userMessage = "Folder %{folderName} containing resources not found in %{rootPath}.",
            technicalMessage = "Folder %{folderName} containing resources not found in %{rootPath}.",
            solution = "Please check file structure or " + CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.WARN,
            lineageImpact = Impact.SCRIPT
    )
    public ResourceFolderNotFoundInRootBuilder resourceFolderNotFoundInRoot() {
        return new ResourceFolderNotFoundInRootBuilder(this);
    }

    @Error(
            userMessage = "File %{fileName} containing resource not found in %{folderPath}.",
            technicalMessage = "File %{fileName} containing resource not found in %{folderPath}.",
            solution = "Please check file structure or " + CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.WARN,
            lineageImpact = Impact.SCRIPT
    )
    public ResourceFileNotFoundInFolderBuilder resourceFileNotFoundInFolder() {
        return new ResourceFileNotFoundInFolderBuilder(this);
    }

    @Error(
            userMessage = "File %{filePath} containing resource not found.",
            technicalMessage = "File %{filePath} containing resource not found.",
            solution = "Please check file structure or " + CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.WARN,
            lineageImpact = Impact.SCRIPT
    )
    public ResourceFileNotFoundBuilder resourceFileNotFound() {
        return new ResourceFileNotFoundBuilder(this);
    }
}
