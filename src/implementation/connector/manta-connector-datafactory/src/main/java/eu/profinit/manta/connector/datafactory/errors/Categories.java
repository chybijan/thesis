package eu.profinit.manta.connector.datafactory.errors;

import eu.profinit.manta.connector.datafactory.errors.categories.InputStructureErrors;
import eu.profinit.manta.platform.logging.common.errordefinitions.CommonErrors;


public class Categories extends CommonErrors {

    public static InputStructureErrors inputStructureErrors() {
        return new InputStructureErrors();
    }
}
