package eu.profinit.manta.connector.datafactory.resolver.model.dataset;

import eu.profinit.manta.connector.datafactory.model.IDataset;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.ReferenceType;

import eu.profinit.manta.connector.datafactory.resolver.model.common.Reference;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.AmazonRedshiftTableDataset;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

public class AbstractDatasetTest {

    @Test
    public void getAllReferences() {

        IReference linkedServiceReference = new Reference("ref", new ArrayList<>(), ReferenceType.LINKED_SERVICE);
        IDataset dataset = new AmazonRedshiftTableDataset("name", linkedServiceReference, null, null, null, null, null );

        Collection<IReference> allReferences = dataset.getAllReferences();

        Assert.assertEquals(1, allReferences.size());
        Assert.assertEquals("ref", allReferences.stream().findFirst().get().getReferenceName());

    }

}
