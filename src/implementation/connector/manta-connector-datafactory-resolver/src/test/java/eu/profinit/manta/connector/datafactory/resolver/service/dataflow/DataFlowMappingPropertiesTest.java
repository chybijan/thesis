package eu.profinit.manta.connector.datafactory.resolver.service.dataflow;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.ReferenceType;
import eu.profinit.manta.connector.datafactory.model.dataflow.*;
import eu.profinit.manta.connector.datafactory.model.dataflow.mapping.IMappingDataFlowProperties;
import eu.profinit.manta.connector.datafactory.resolver.model.common.Reference;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.DataFlowEndpoint;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.DataFlowScript;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.mapping.MappingDataFlowProperties;
import eu.profinit.manta.connector.datafactory.resolver.service.AdfResourceParserTestBase;
import eu.profinit.manta.connector.datafactory.resolver.service.common.ReferenceParser;
import org.apache.commons.io.FileUtils;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Tests for parsing of properties.
 */
@RunWith(MockitoJUnitRunner.class)
public class DataFlowMappingPropertiesTest extends AdfResourceParserTestBase {

    /**
     * Directory with test inout files.
     */
    private static final File TEST_INPUT_DIRECTORY = FileUtils.getFile(TEST_FILES_ROOT, "dataflow/properties");

    /**
     * Parser for properties.
     */
    private MappingDataFlowPropertiesParser propertiesParser;

    @Mock
    private ReferenceParser referenceParserMock;

    @Mock
    private DFSParserService parserServiceMock;

    @Before
    public void init() throws JsonProcessingException {

        IReference datasetReference = new Reference("referenceName", new ArrayList<>(), ReferenceType.DATASET);
        Mockito.when(referenceParserMock.parseReference(Mockito.any(), Mockito.any())).thenReturn(datasetReference);
        Mockito.when(parserServiceMock.processScript(Mockito.any())).thenReturn(new DataFlowScript(Collections.emptyList(), Collections.emptyMap()));

        propertiesParser = new MappingDataFlowPropertiesParser(referenceParserMock, parserServiceMock);

    }

    @Override
    protected File getTestInputDirectory() {
        return TEST_INPUT_DIRECTORY;
    }

    @Test
    public void parseSources(){
        JsonNode sourcesArrayNode = readFile("sourcesArray1.json");

        Map<String, IDataFlowEndpoint> parsedSources = propertiesParser.parseEndpoints(sourcesArrayNode);

        Mockito.verify(referenceParserMock, Mockito.times(6)).parseReference(Mockito.any(), Mockito.any());
    }

    @Test
    public void parseSource(){
        JsonNode sourceNode = readFile("source1.json");

        IDataFlowEndpoint source = propertiesParser.parseEndpoint(sourceNode);

        Mockito.verify(referenceParserMock, Mockito.times(3)).parseReference(Mockito.any(), Mockito.any());

        assertEquals("sourceAlone1", source.getName());
    }

    @Test
    public void parseSourceEmptyName(){
        JsonNode sourceNode = readFile("emptyNameSource.json");

        IDataFlowEndpoint source = propertiesParser.parseEndpoint(sourceNode);

        assertEquals(AbstractDataFlowPropertiesParser.UNKNOWN_ENDPOINT_NAME, source.getName());
    }

    @Test
    public void parseSourceEmptyDataset(){
        JsonNode sourceNode = readFile("emptyReferenceSource.json");

        Mockito.when(referenceParserMock.parseReference(Mockito.any(), Mockito.any())).thenReturn(null);
        IDataFlowEndpoint source = propertiesParser.parseEndpoint(sourceNode);

        Assert.assertThrows(IllegalStateException.class, source::getReference);
    }

    @Test
    public void parseSinks(){
        JsonNode sinksArrayNode = readFile("sourcesArray1.json");

        Map<String, IDataFlowEndpoint> parsedSinks = propertiesParser.parseEndpoints(sinksArrayNode);

        Mockito.verify(referenceParserMock, Mockito.times(6)).parseReference(Mockito.any(), Mockito.any());
    }

    @Test
    public void parseSink(){
        JsonNode sinkNode = readFile("source1.json");

        IDataFlowEndpoint sink = propertiesParser.parseEndpoint(sinkNode);

        Mockito.verify(referenceParserMock, Mockito.times(3)).parseReference(Mockito.any(), Mockito.any());

        assertEquals("sourceAlone1", sink.getName());
    }

    @Test
    public void parseSinkEmptyName(){
        JsonNode sourceNode = readFile("emptyNameSource.json");

        IDataFlowEndpoint source = propertiesParser.parseEndpoint(sourceNode);

        assertEquals(MappingDataFlowPropertiesParser.UNKNOWN_ENDPOINT_NAME, source.getName());
    }

    @Test
    public void parseSinkEmptyDataset(){
        JsonNode sourceNode = readFile("emptyReferenceSource.json");

        Mockito.when(referenceParserMock.parseReference(Mockito.any(), Mockito.any())).thenReturn(null);
        IDataFlowEndpoint source = propertiesParser.parseEndpoint(sourceNode);

        Assert.assertThrows(IllegalStateException.class, source::getReference);

    }

    @Test
    public void onlyDataSetTest(){
        JsonNode sourceNode = readFile("onlyDatasetSource.json");

        IReference insideReference = new Reference("testDataRef1", new ArrayList<>(), ReferenceType.DATASET);

        Mockito.when(referenceParserMock.parseReference(sourceNode.at(AbstractDataFlowPropertiesParser.LINKED_SERVICE_PATH), ReferenceType.LINKED_SERVICE)).thenReturn(null);
        Mockito.when(referenceParserMock.parseReference(sourceNode.at(AbstractDataFlowPropertiesParser.SCHEMA_LINKED_SERVICE_PATH), ReferenceType.LINKED_SERVICE)).thenReturn(null);
        Mockito.when(referenceParserMock.parseReference(sourceNode.at(AbstractDataFlowPropertiesParser.DATASET_PATH), ReferenceType.DATASET)).thenReturn(insideReference);

        IDataFlowEndpoint source = propertiesParser.parseEndpoint(sourceNode);

        IDataFlowEndpoint expectedEndpoint = new DataFlowEndpoint(
                "sourceAlone1",
                insideReference,
                null,
                null);

        assertEquals(expectedEndpoint.getReference(), source.getReference());

    }

    @Test
    public void onlyLinkedServiceTest(){
        JsonNode sourceNode = readFile("onlyLinkedServiceSource.json");

        IReference insideReference = new Reference("testDataRef1", new ArrayList<>(), ReferenceType.LINKED_SERVICE);

        Mockito.when(referenceParserMock.parseReference(sourceNode.at(AbstractDataFlowPropertiesParser.LINKED_SERVICE_PATH), ReferenceType.LINKED_SERVICE)).thenReturn(insideReference);
        Mockito.when(referenceParserMock.parseReference(sourceNode.at(AbstractDataFlowPropertiesParser.SCHEMA_LINKED_SERVICE_PATH), ReferenceType.LINKED_SERVICE)).thenReturn(null);
        Mockito.when(referenceParserMock.parseReference(sourceNode.at(AbstractDataFlowPropertiesParser.DATASET_PATH), ReferenceType.DATASET)).thenReturn(null);
        IDataFlowEndpoint source = propertiesParser.parseEndpoint(sourceNode);

        IDataFlowEndpoint expectedEndpoint = new DataFlowEndpoint(
                "sourceAlone1",
                null,
                insideReference,
                null);

        assertEquals(expectedEndpoint.getReference(), source.getReference());

    }

    @Test
    public void onlySchemaLinkedService(){
        JsonNode sourceNode = readFile("onlySchemaLinkedServiceSource.json");

        IReference insideReference = new Reference("testDataRef1", new ArrayList<>(), ReferenceType.LINKED_SERVICE);

        Mockito.when(referenceParserMock.parseReference(sourceNode.at(AbstractDataFlowPropertiesParser.LINKED_SERVICE_PATH), ReferenceType.LINKED_SERVICE)).thenReturn(null);
        Mockito.when(referenceParserMock.parseReference(sourceNode.at(AbstractDataFlowPropertiesParser.SCHEMA_LINKED_SERVICE_PATH), ReferenceType.LINKED_SERVICE)).thenReturn(insideReference);
        Mockito.when(referenceParserMock.parseReference(sourceNode.at(AbstractDataFlowPropertiesParser.DATASET_PATH), ReferenceType.DATASET)).thenReturn(null);
        IDataFlowEndpoint source = propertiesParser.parseEndpoint(sourceNode);

        IDataFlowEndpoint expectedEndpoint = new DataFlowEndpoint(
                "sourceAlone1",
                null,
                null,
                insideReference);

        assertEquals(expectedEndpoint.getReference(), source.getReference());
    }

    @Test
    public void parseTransformations(){
        JsonNode transformationsNode = readFile("transformations1.json");

        Collection<String> transformations = propertiesParser.parseTransformations(transformationsNode);

        Collection<String> expectedTransformtions = new ArrayList<>();
        expectedTransformtions.add("trans1");
        expectedTransformtions.add("trans2");
        expectedTransformtions.add("trans3");

        assertEquals(expectedTransformtions.size(), transformations.size());

        List<String> expectedTransformationsList = new ArrayList<>(expectedTransformtions);
        List<String> realTransformationsList = new ArrayList<>(transformations);

        for(int i = 0 ; i < expectedTransformationsList.size(); ++i){
            assertEquals(expectedTransformationsList.get(i), realTransformationsList.get(i));
        }
    }

    @Test
    public void parseEmptyScript(){

        JsonNode propertiesNode = readFile("emptyScript.json");

        IDataFlowProperties properties = propertiesParser.parseProperties(propertiesNode);

        assertTrue(properties.getScript().getParameters().isEmpty());
        assertTrue(properties.getScript().getTransformations().isEmpty());

    }

    @Test
    public void parseScriptLines(){

        JsonNode propertiesNode = readFile("scriptLines.json");

        IDataFlowProperties properties = propertiesParser.parseProperties(propertiesNode);

        String expectedScript = "parameters{\n"+
                "     parameter1 as string\n"+
                "}\n"+
                "source(allowSchemaDrift: true,\n"+
                "     validateSchema: false) ~> source1\n"+
                "source1 sink(allowSchemaDrift: true,\n"+
                "     validateSchema: false,\n"+
                "     skipDuplicateMapInputs: true,\n"+
                "     skipDuplicateMapOutputs: true) ~> sink1\n";

        Mockito.verify(parserServiceMock, Mockito.times(1)).processScript(expectedScript);

    }

    private void cmpSinks(IDataFlowEndpoint expectedSink, IDataFlowEndpoint sink){
        assertEquals(expectedSink.getName(), sink.getName());
    }

    private void cmpSources(IDataFlowEndpoint expectedSource, IDataFlowEndpoint source){
        assertEquals(expectedSource.getName(), source.getName());
    }

    @Test
    public void parseProperties(){
        JsonNode propertiesNode = readFile("properties1.json");

        IDataFlowProperties properties = propertiesParser.parseProperties(propertiesNode);

        assertEquals(DataFlowType.MAPPING_DATA_FLOW,properties.getType());

        IMappingDataFlowProperties mappingProperties = (IMappingDataFlowProperties) properties;

//        String script = "source(output(\n\t\tName as string,\n\t\tSurname as string,\n\t\tYear as string\n\t),\n\tallowSchemaDrift: true,\n\tvalidateSchema: false,\n\tignoreNoFilesFound: false) ~> source1\nsource1 filter(Surname=='Bulat') ~> Filter1\nFilter1 sink(allowSchemaDrift: true,\n\tvalidateSchema: false,\n\tskipDuplicateMapInputs: true,\n\tskipDuplicateMapOutputs: true) ~> sink2";
        IDataFlowScript script = new DataFlowScript(Collections.emptyList(), Collections.emptyMap());

        Map<String, IDataFlowEndpoint> sources = new HashMap<>();
            sources.put("source1", new DataFlowEndpoint(
                    "source1",
                            new Reference("testData", new ArrayList<>(), ReferenceType.DATASET),
                    null, null));

        Map<String, IDataFlowEndpoint> sinks = new HashMap<>();
            sinks.put("sink2",new DataFlowEndpoint(
                    "sink2",
                            new Reference("users_csv", new ArrayList<>(), ReferenceType.DATASET),
                    null, null));

        List<String> transformations = new ArrayList<>();
            transformations.add("Filter1");

        MappingDataFlowProperties expectedMappingProperties = new MappingDataFlowProperties(
                script, sources, sinks, transformations
        );

        Mockito.verify(parserServiceMock, Mockito.times(1)).processScript(Mockito.any());
        assertTrue(properties.getScript().getParameters().isEmpty());
        assertTrue(properties.getScript().getTransformations().isEmpty());

        // Compare tranformations
        assertEquals(expectedMappingProperties.getTransformationNames().size(), mappingProperties.getTransformationNames().size());
        assertEquals(expectedMappingProperties.getTransformationNames().stream().findFirst().get(), mappingProperties.getTransformationNames().stream().findFirst().get());

        // Compare sources
        assertEquals(expectedMappingProperties.getSources().size(), properties.getSources().size());
        cmpSources(expectedMappingProperties.getSources().values().stream().findFirst().get(), properties.getSources().values().stream().findFirst().get());

        // Compare sinks
        assertEquals(expectedMappingProperties.getSinks().size(), mappingProperties.getSinks().size());
        cmpSinks(expectedMappingProperties.getSinks().values().stream().findFirst().get(), mappingProperties.getSinks().values().stream().findFirst().get());

    }
}