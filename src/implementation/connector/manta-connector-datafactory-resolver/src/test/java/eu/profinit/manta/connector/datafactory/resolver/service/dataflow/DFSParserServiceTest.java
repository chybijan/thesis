package eu.profinit.manta.connector.datafactory.resolver.service.dataflow;

import com.fasterxml.jackson.core.JsonProcessingException;
import eu.profinit.manta.connector.datafactory.model.dataflow.DataFlowDataType;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowScript;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataflowParameter;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.*;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumn;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.ITable;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.datarepresentation.ColumnDef;
import eu.profinit.manta.connector.datafactory.resolver.service.AdfResourceParserTestBase;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.stringtemplate.v4.ST;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * Tests for parsing of properties.
 */
@RunWith(MockitoJUnitRunner.class)
public class DFSParserServiceTest extends AdfResourceParserTestBase {

    /**
     * Directory with test inout files.
     */
    private static final File TEST_INPUT_DIRECTORY = FileUtils.getFile(TEST_FILES_ROOT, "dataflow/dfs");

    /**
     * The parser service
     */
    private DFSParserServiceImpl parserService;


    @Before
    public void init() throws JsonProcessingException {

        parserService = new DFSParserServiceImpl();

    }

    @Override
    protected File getTestInputDirectory() {
        return TEST_INPUT_DIRECTORY;
    }

    @Test
    public void parseOnlyParameters(){
        String script = readFileRaw("parameters.dfs");

        IDataFlowScript proccesedScript = parserService.processScript(script);
        assertTrue(proccesedScript.isParsedWithoutErrors());

        assertEquals(18, proccesedScript.getParameters().size());
        List<IDataflowParameter> params = new ArrayList<>(proccesedScript.getParameters());

        IDataflowParameter par = proccesedScript.getParameter("parameter0").get();
        assertEquals("parameter0",par.getName());
        assertEquals(DataFlowDataType.DF_STRING, par.getDataType());
        assertEquals("('Hello there general Kenobi')", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter1").get();
        assertEquals("parameter1",par.getName());
        assertEquals(DataFlowDataType.DF_INTEGER, par.getDataType());
        assertEquals("42", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter1andHalf").get();
        assertEquals("parameter1andHalf",par.getName());
        assertEquals(DataFlowDataType.DF_INTEGER, par.getDataType());
        assertEquals("-69", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter2").get();
        assertEquals("parameter2",par.getName());
        assertEquals(DataFlowDataType.DF_BOOLEAN, par.getDataType());
        assertEquals("true", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter3").get();
        assertEquals("parameter3",par.getName());
        assertEquals(DataFlowDataType.DF_DATE, par.getDataType());
        assertEquals("(((56)))", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter4").get();
        assertEquals("parameter4",par.getName());
        assertEquals(DataFlowDataType.DF_TIMESTAMP, par.getDataType());
        assertEquals("", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter5").get();
        assertEquals("parameter5",par.getName());
        assertEquals(DataFlowDataType.DF_ANY, par.getDataType());
        assertEquals("", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter6").get();
        assertEquals("parameter6",par.getName());
        assertEquals(DataFlowDataType.DF_SHORT, par.getDataType());
        assertEquals("-12.34e-56", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter7").get();
        assertEquals("parameter7",par.getName());
        assertEquals(DataFlowDataType.DF_DOUBLE, par.getDataType());
        assertEquals("-.12e-34", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter8").get();
        assertEquals("parameter8",par.getName());
        assertEquals(DataFlowDataType.DF_FLOAT, par.getDataType());
        assertEquals("-12.34e56", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter9").get();
        assertEquals("parameter9",par.getName());
        assertEquals(DataFlowDataType.DF_LONG, par.getDataType());
        assertEquals("12.34e-56", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter10").get();
        assertEquals("parameter10",par.getName());
        assertEquals(DataFlowDataType.DF_DECIMAL, par.getDataType());
        assertEquals("12.34e56", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter11").get();
        assertEquals("parameter11",par.getName());
        assertEquals(DataFlowDataType.DF_MAP, par.getDataType());
        assertEquals("", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter12").get();
        assertEquals("parameter12",par.getName());
        assertEquals(DataFlowDataType.DF_MAP, par.getDataType());
        assertEquals("", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter13").get();
        assertEquals("parameter13",par.getName());
        assertEquals(DataFlowDataType.DF_STRING_ARRAY, par.getDataType());
        assertEquals("['Hello']", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter13andHalf").get();
        assertEquals("parameter13andHalf",par.getName());
        assertEquals(DataFlowDataType.DF_STRING_ARRAY, par.getDataType());
        assertEquals("['Hello', 'there']", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter14").get();
        assertEquals("parameter14",par.getName());
        assertEquals(DataFlowDataType.DF_LONG_ARRAY, par.getDataType());
        assertEquals("[]", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter15").get();
        assertEquals("parameter15",par.getName());
        assertEquals(DataFlowDataType.DF_COMPLEX, par.getDataType());
        assertEquals("()", par.getDefaultValue().getRawForm());
    }

    @Test
    public void parseSource(){
        String script = readFileRaw("source.dfs");

        IDataFlowScript proccesedScript = parserService.processScript(script);
        assertTrue(proccesedScript.isParsedWithoutErrors());

        assertEquals(2, proccesedScript.getParameters().size());
        List<IDataflowParameter> params = new ArrayList<>(proccesedScript.getParameters());

        IDataflowParameter par = proccesedScript.getParameter("parameter1").get();
        assertEquals("parameter1",par.getName());
        assertEquals(DataFlowDataType.DF_STRING, par.getDataType());
        assertEquals("('Default value')", par.getDefaultValue().getRawForm());

        par = proccesedScript.getParameter("parameter2").get();
        assertEquals("parameter2",par.getName());
        assertEquals(DataFlowDataType.DF_DATE, par.getDataType());
        assertEquals("", par.getDefaultValue().getRawForm());

        Map<String, ITransformation> transformations = proccesedScript.getTransformations();
        assertEquals(1, transformations.size());
        ITransformation source1 = proccesedScript.getTransformation("Source1").get();
        assertEquals(0,source1.getInputs().size());
        assertEquals(0,source1.getOutputs().size());
        assertEquals("Source1",source1.getName());
        List<IADFStream> streams = new ArrayList<>(source1.getStreams());
        assertEquals(3,streams.size());
        assertEquals("Source1",streams.stream().filter(e -> Objects.equals(e.getSubstreamName(), "streamone")).findFirst().get().getName());
        assertEquals("streamone",streams.stream().filter(e -> Objects.equals(e.getSubstreamName(), "streamone")).findFirst().get().getSubstreamName());
        assertEquals("Source1",streams.stream().filter(e -> Objects.equals(e.getSubstreamName(), "streamTwo")).findFirst().get().getName());
        assertEquals("streamTwo",streams.stream().filter(e -> Objects.equals(e.getSubstreamName(), "streamTwo")).findFirst().get().getSubstreamName());
        assertEquals("Source1",streams.stream().filter(e -> Objects.equals(e.getSubstreamName(), "streamThree")).findFirst().get().getName());
        assertEquals("streamThree",streams.stream().filter(e -> Objects.equals(e.getSubstreamName(), "streamThree")).findFirst().get().getSubstreamName());

        ITable table = source1.getTable();
        List<IColumn> columns = new ArrayList<>(table.getColumns());
        assertEquals(4, columns.size());
        assertTrue(columns.get(0) instanceof ColumnDef);
        assertTrue(columns.get(1) instanceof ColumnDef);
        assertTrue(columns.get(2) instanceof ColumnDef);
        assertTrue(columns.get(3) instanceof ColumnDef);

        ColumnDef col0 = (ColumnDef) columns.get(0);
        ColumnDef col1 = (ColumnDef) columns.get(1);
        ColumnDef col2 = (ColumnDef) columns.get(2);
        ColumnDef col3 = (ColumnDef) columns.get(3);

        assertEquals("normal", col0.getName());
        assertEquals(DataFlowDataType.DF_INTEGER, col0.getType());
        assertEquals("'$###'", col0.getFormat());

        // TODO this name will have less spaces, but for now do this
        assertEquals("as with __ {{ and $)!*%)@&)!#* more", col1.getName());
        assertEquals(DataFlowDataType.DF_TIMESTAMP, col1.getType());
        assertEquals("'yyyy-MM-dd\\'T\\'HH:mm:ss\\'Z\\''", col1.getFormat());

        assertEquals("", col2.getName());
        assertEquals(DataFlowDataType.DF_DECIMAL, col2.getType());
        assertEquals("'000,000,000.000'", col2.getFormat());

        assertEquals("second_normal", col3.getName());
        assertEquals(DataFlowDataType.DF_COMPLEX, col3.getType());
        assertEquals("", col3.getFormat());

    }

    @Test
    public void parseFilterTransformation(){
        String script = readFileRaw("filter.dfs");

        IDataFlowScript proccesedScript = parserService.processScript(script);
        assertTrue(proccesedScript.isParsedWithoutErrors());

        assertEquals(0, proccesedScript.getParameters().size());

        Map<String, ITransformation> transformations = proccesedScript.getTransformations();
        assertEquals(3, transformations.size());

        ITransformation filterTransformation = proccesedScript.getTransformation("filter1").get();

        assertTrue(filterTransformation instanceof IFilterTransformation);

        IFilterTransformation filter1 = (IFilterTransformation) filterTransformation;

        assertEquals("filter1",filter1.getName());
        assertEquals("true == '\"Column name is 42\"'" ,filter1.getFilterCondition().asString());

        List<IADFStream> inputs = new ArrayList<>(filter1.getInputs());
        assertEquals(1, inputs.size());
        assertEquals("source1",inputs.get(0).getName());
        assertEquals("",inputs.get(0).getSubstreamName());


        List<String> outputs = new ArrayList<>(filter1.getOutputs());
        assertEquals(1, outputs.size());
        assertEquals("sink1",outputs.get(0));

        List<IADFStream> streams = new ArrayList<>(filter1.getStreams());
        assertEquals(1,streams.size());
        assertEquals("filter1",streams.get(0).getName());
        assertEquals("",streams.get(0).getSubstreamName());

        ITable table = filter1.getTable();
        List<IColumn> columns = new ArrayList<>(table.getColumns());
        assertEquals(0, columns.size());

    }

    @Test
    public void parseJoinTransformation(){
        String script = readFileRaw("join.dfs");

        IDataFlowScript proccesedScript = parserService.processScript(script);
        assertTrue(proccesedScript.isParsedWithoutErrors());

        assertEquals(1, proccesedScript.getParameters().size());

        Map<String, ITransformation> transformations = proccesedScript.getTransformations();
        assertEquals(4, transformations.size());

        ITransformation filterTransformation = proccesedScript.getTransformation("join1").get();

        assertTrue(filterTransformation instanceof IJoinTransformation);

        IJoinTransformation join1 = (IJoinTransformation) filterTransformation;

        assertEquals("join1",join1.getName());
        assertEquals("\"str1\" <=> $param_for_join" ,join1.getJoinCondition().asString());

        List<IADFStream> inputs = new ArrayList<>(join1.getInputs());
        //split1@stream1, source2
        assertEquals(2, inputs.size());

        IADFStream stream1 = inputs.stream().filter(s -> s.getName().equals("split1")).collect(Collectors.toList()).get(0);
        assertEquals("split1", stream1.getName());
        assertEquals("stream1", stream1.getSubstreamName());

        IADFStream stream2 = inputs.stream().filter(s -> s.getName().equals("source2")).collect(Collectors.toList()).get(0);
        assertEquals("source2", stream2.getName());
        assertEquals("", stream2.getSubstreamName());


        List<String> outputs = new ArrayList<>(join1.getOutputs());
        assertEquals(1, outputs.size());
        assertEquals("sink1",outputs.get(0));

        List<IADFStream> streams = new ArrayList<>(join1.getStreams());
        assertEquals(1,streams.size());
        assertEquals("join1",streams.get(0).getName());
        assertEquals("",streams.get(0).getSubstreamName());

        ITable table = join1.getTable();
        List<IColumn> columns = new ArrayList<>(table.getColumns());
        assertEquals(0, columns.size());

    }

    @Test
    public void parseSinkTransformation(){
        String script = readFileRaw("sink.dfs");

        IDataFlowScript proccesedScript = parserService.processScript(script);
        assertTrue(proccesedScript.isParsedWithoutErrors());

        assertEquals(1, proccesedScript.getParameters().size());

        Map<String, ITransformation> transformations = proccesedScript.getTransformations();
        assertEquals(2, transformations.size());

        ITransformation sinkTransformation = proccesedScript.getTransformation("sink1").get();

        assertTrue(sinkTransformation instanceof ISinkTransformation);

    }

    @Test
    public void transformationStreams(){
        String script = readFileRaw("transformationStreams.dfs");

        IDataFlowScript proccesedScript = parserService.processScript(script);
        assertTrue(proccesedScript.isParsedWithoutErrors());

        ITransformation source1 = proccesedScript.getTransformation("source1").get();
        ITransformation source2 = proccesedScript.getTransformation("source2").get();
        ITransformation filter1 = proccesedScript.getTransformation("filter1").get();
        ITransformation join1 = proccesedScript.getTransformation("join1").get();
        ITransformation split = proccesedScript.getTransformation("split1").get();
        ITransformation sink = proccesedScript.getTransformation("sink1").get();
        ITransformation sink2 = proccesedScript.getTransformation("sink2").get();
        ITransformation sink3 = proccesedScript.getTransformation("sink3").get();

        assertTrue(source1.getInputs().isEmpty());
        assertEquals("filter1", source1.getOutputs().stream().findFirst().get());

        assertTrue(source2.getInputs().isEmpty());
        assertEquals("join1", source2.getOutputs().stream().findFirst().get());

        assertEquals("source1", filter1.getInputs().stream().findFirst().get().getName());
        assertEquals("", filter1.getInputs().stream().findFirst().get().getSubstreamName());
        assertEquals("split1", filter1.getOutputs().stream().findFirst().get());

        IADFStream stream1 = join1.getInputs().stream().filter(s->s.getName().equals("source2")).findFirst().get();
        IADFStream stream2 = join1.getInputs().stream().filter(s->s.getName().equals("split1")).findFirst().get();
        assertEquals("source2", stream1.getName());
        assertEquals("", stream1.getSubstreamName());
        assertEquals("split1", stream2.getName());
        assertEquals("stream1", stream2.getSubstreamName());
        assertEquals("sink2", join1.getOutputs().stream().findFirst().get());

        assertEquals("filter1", split.getInputs().stream().findFirst().get().getName());
        assertEquals("", split.getInputs().stream().findFirst().get().getSubstreamName());
        assertTrue(split.getOutputs().stream().anyMatch(o->o.equals("join1")));
        assertTrue(split.getOutputs().stream().anyMatch(o->o.equals("sink3")));

        assertEquals("split1", sink.getInputs().stream().findFirst().get().getName());
        assertEquals("stream1", sink.getInputs().stream().findFirst().get().getSubstreamName());
        assertTrue(sink.getOutputs().isEmpty());

        assertEquals("join1", sink2.getInputs().stream().findFirst().get().getName());
        assertEquals("", sink2.getInputs().stream().findFirst().get().getSubstreamName());
        assertTrue(sink2.getOutputs().isEmpty());

        assertEquals("split1", sink3.getInputs().stream().findFirst().get().getName());
        assertEquals("stream2", sink3.getInputs().stream().findFirst().get().getSubstreamName());
        assertTrue(sink3.getOutputs().isEmpty());



    }

    @Test
    public void parseGartnerSample(){
        String script = readFileRaw("gartner/SIL_Custom_Reusable_Type2_Dimension_Parameters.dfs");

        IDataFlowScript proccesedScript = parserService.processScript(script);
        assertFalse(proccesedScript.isParsedWithoutErrors());

        // TODO Check inputs and outputs
        // TODO check if all stream names were correctly transformed to internall representation
    }

}