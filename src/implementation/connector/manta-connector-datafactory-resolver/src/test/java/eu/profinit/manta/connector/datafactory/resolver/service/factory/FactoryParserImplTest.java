package eu.profinit.manta.connector.datafactory.resolver.service.factory;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.factory.GlobalParameterType;
import eu.profinit.manta.connector.datafactory.model.factory.IGlobalParameterSpecification;
import eu.profinit.manta.connector.datafactory.resolver.model.Factory;
import eu.profinit.manta.connector.datafactory.resolver.model.factory.GlobalParameterSpecification;
import eu.profinit.manta.connector.datafactory.resolver.service.AdfResourceParserTestBase;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests for parsing of Factory resource.
 */
public class FactoryParserImplTest extends AdfResourceParserTestBase {

    /**
     * Directory with test inout files.
     */
    private static final File TEST_INPUT_DIRECTORY = FileUtils.getFile(TEST_FILES_ROOT, "factory");

    /**
     * Spring context.
     */
    private static ClassPathXmlApplicationContext springContext;

    /**
     * Parser for Factory resource.
     */
    private static FactoryParser factoryParser;

    @BeforeClass
    public static void setUpClass() {
        // Init Spring context, get Factory resource parser
        springContext = new ClassPathXmlApplicationContext(SPRING_CONFIG);
        factoryParser = springContext.getBean(FactoryParser.class);
    }

    @AfterClass
    public static void tearDown() {
        if (springContext != null) {
            springContext.close();
        }
    }

    @Test
    public void parseStandardFactory() {
        JsonNode factoryRootNode = readFile("StandardFactory.json");
        Factory factory = factoryParser.parseFactory(factoryRootNode);

        assertEquals("Manta-Data-Factory", factory.getName());

        Map<String, IGlobalParameterSpecification> globalParameters = factory.getGlobalParameters();
        assertNotNull(globalParameters);

        IGlobalParameterSpecification param1 = globalParameters.get("param1");
        assertNotNull(param1);
        assertEquals(GlobalParameterType.STRING, param1.getType());
        assertEquals("val_param_1", param1.getValue().asText());
    }

    @Test
    public void parseMultipleParamsFactory() {
        JsonNode factoryRootNode = readFile("MultipleParamsFactory.json");
        Factory factory = factoryParser.parseFactory(factoryRootNode);

        assertEquals("Manta-Data-Factory", factory.getName());

        Map<String, IGlobalParameterSpecification> globalParameters = factory.getGlobalParameters();
        assertNotNull(globalParameters);

        IGlobalParameterSpecification param1 = globalParameters.get("param1");
        assertNotNull(param1);
        IGlobalParameterSpecification param2 = globalParameters.get("param2");
        assertNotNull(param2);
        IGlobalParameterSpecification param3 = globalParameters.get("param3");
        assertNotNull(param3);

        assertEquals(GlobalParameterType.ARRAY, param3.getType());
        assertTrue(param3.getValue().isArray());
    }

    @Test
    public void parseNoParamsFactory() {
        JsonNode factoryRootNode = readFile("NoParamsFactory.json");
        Factory factory = factoryParser.parseFactory(factoryRootNode);

        Map<String, IGlobalParameterSpecification> globalParameters = factory.getGlobalParameters();
        assertNotNull(globalParameters);
        assertTrue(globalParameters.isEmpty());
    }

    @Test
    public void parseMissingParamTypeFactory() {
        JsonNode factoryRootNode = readFile("MissingParamTypeFactory.json");
        Factory factory = factoryParser.parseFactory(factoryRootNode);

        Map<String, IGlobalParameterSpecification> globalParameters = factory.getGlobalParameters();
        assertNotNull(globalParameters);

        IGlobalParameterSpecification param1 = globalParameters.get("param1");
        assertNotNull(param1);

        assertEquals(GlobalParameterType.OBJECT, param1.getType());
    }

    @Override
    protected File getTestInputDirectory() {
        return TEST_INPUT_DIRECTORY;
    }

}