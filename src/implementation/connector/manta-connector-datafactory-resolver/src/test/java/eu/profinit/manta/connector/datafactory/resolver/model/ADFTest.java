package eu.profinit.manta.connector.datafactory.resolver.model;

import eu.profinit.manta.connector.datafactory.model.IPipeline;
import org.junit.Assert;
import org.junit.Test;

public class ADFTest {

    ADF validADF = new ADF(new IPipeline() {
        @Override
        public String getName() {
            return null;
        }
    });

    ADF validADF2 = new ADF(new DataFlow("DataflowName", null));

    @Test
    public void isValid() {
        Assert.assertTrue(validADF.isValid());
        Assert.assertTrue(validADF2.isValid());
    }
}
