package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.linkedservice.HiveServerType;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AmazonRedshiftLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AmazonS3LinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AzureBlobFSLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AzureBlobStorageLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AzureDataLakeStoreLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AzureFileStorageLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AzurePostgreSqlLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AzureSqlDWLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AzureSqlDatabaseLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AzureSqlMILinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.Db2LinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.FileServerLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.FtpServerLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.GoogleBigQueryLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.GoogleCloudStorageLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.GreenplumLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.HdfsLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.HiveLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.NetezzaLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.OracleLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.PostgreSqlLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.SapHanaLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.SftpServerLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.SnowflakeLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.SqlServerLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.SybaseLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.TeradataLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.UnknownLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.AdfResourceParserTestBase;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class LinkedServiceParserTest extends AdfResourceParserTestBase {

    /**
     * Directory with test input files.
     */
    private static final File TEST_INPUT_DIRECTORY = FileUtils.getFile(TEST_FILES_ROOT, "linkedservice");

    /**
     * Spring context.
     */
    private static ClassPathXmlApplicationContext springContext;

    /**
     * Map of parsers for LinkedService resources.
     */
    private static Map<LinkedServiceType, LinkedServiceParser<? extends LinkedService>> linkedServiceParserMap;

    @BeforeClass
    @SuppressWarnings("unchecked")
    public static void setUpClass() {
        // Init Spring context, get LinkedService resource parsers
        springContext = new ClassPathXmlApplicationContext(SPRING_CONFIG);
        linkedServiceParserMap = (Map<LinkedServiceType, LinkedServiceParser<? extends LinkedService>>)
                springContext.getBean("linkedServiceParserMap");
    }

    @AfterClass
    public static void tearDown() {
        if (springContext != null) {
            springContext.close();
        }
    }

    private LinkedServiceType parseLinkedServiceType(JsonNode linkedServiceNode) {
        String linkedServiceTypeTypeString = JsonUtil.pathAsText(linkedServiceNode.path("properties"), "type");
        return LinkedServiceType.valueOfCI(linkedServiceTypeTypeString);
    }

    @Test
    public void parseAmazonRedshiftLinkedService() {
        AmazonRedshiftLinkedService linkedService =
                getLinkedService("AmazonRedshiftLinkedService.json", AmazonRedshiftLinkedService.class, LinkedServiceType.AMAZON_REDSHIFT);

        assertNotNull(linkedService.getServer());
        assertNotNull(linkedService.getPort());
        assertNotNull(linkedService.getDatabase());
        assertNotNull(linkedService.getUsername());
    }

    @Test
    public void parseAmazonS3LinkedService() {
        AmazonS3LinkedService linkedService =
                getLinkedService("AmazonS3LinkedService.json", AmazonS3LinkedService.class, LinkedServiceType.AMAZON_S3);

        assertNotNull(linkedService.getServiceUrl());
    }

    @Test
    public void parseAzureBlobFSLinkedService() {
        AzureBlobFSLinkedService linkedService =
                getLinkedService("AzureBlobFSLinkedService.json", AzureBlobFSLinkedService.class, LinkedServiceType.AZURE_BLOB_FS);

        assertNotNull(linkedService.getUrl());
    }

    @Test
    public void parseAzureBlobStorageLinkedService() {
        AzureBlobStorageLinkedService linkedService =
                getLinkedService("AzureBlobStorageLinkedService.json", AzureBlobStorageLinkedService.class, LinkedServiceType.AZURE_BLOB_STORAGE);

        assertNotNull(linkedService.getConnectionString());
        assertNull(linkedService.getSasUri());
        assertNull(linkedService.getServiceEndpoint());
    }

    @Test
    public void parseAzureDataLakeStoreLinkedService() {
        AzureDataLakeStoreLinkedService linkedService =
                getLinkedService("AzureDataLakeStoreLinkedService.json", AzureDataLakeStoreLinkedService.class, LinkedServiceType.AZURE_DATA_LAKE_STORE);

        assertNotNull(linkedService.getAccountName());
        assertNotNull(linkedService.getDataLakeStoreUri());
    }

    @Test
    public void parseAzureFileStorageLinkedService() {
        AzureFileStorageLinkedService linkedService =
                getLinkedService("AzureFileStorageLinkedService.json", AzureFileStorageLinkedService.class, LinkedServiceType.AZURE_FILE_STORAGE);

        assertNotNull(linkedService.getHost());
        assertNotNull(linkedService.getSasUri());
        assertNotNull(linkedService.getSnapshot());
        assertNotNull(linkedService.getConnectionString());
    }

    @Test
    public void parseAzurePostgreSqlLinkedService() {
        AzurePostgreSqlLinkedService linkedService =
                getLinkedService("AzurePostgreSqlLinkedService.json", AzurePostgreSqlLinkedService.class, LinkedServiceType.AZURE_POSTGRESQL);

        assertNotNull(linkedService.getConnectionString());
    }

    @Test
    public void parseAzureSqlDatabaseLinkedService() {
        AzureSqlDatabaseLinkedService linkedService =
                getLinkedService("AzureSqlDatabaseLinkedService.json", AzureSqlDatabaseLinkedService.class, LinkedServiceType.AZURE_SQL_DATABASE);

        assertNotNull(linkedService.getConnectionString());
    }

    @Test
    public void parseAzureSqlDWLinkedService() {
        AzureSqlDWLinkedService linkedService =
                getLinkedService("AzureSqlDWLinkedService.json", AzureSqlDWLinkedService.class, LinkedServiceType.AZURE_SQL_DW);

        assertNotNull(linkedService.getConnectionString());
    }

    @Test
    public void parseAzureSqlMILinkedService() {
        AzureSqlMILinkedService linkedService =
                getLinkedService("AzureSqlMILinkedService.json", AzureSqlMILinkedService.class, LinkedServiceType.AZURE_SQL_MI);

        assertNotNull(linkedService.getConnectionString());
    }

    @Test
    public void parseDb2LinkedService() {
        Db2LinkedService linkedService =
                getLinkedService("Db2LinkedService.json", Db2LinkedService.class, LinkedServiceType.DB2);

        assertNotNull(linkedService.getConnectionString());
    }

    @Test
    public void parseDb2LinkedServiceLegacy() {
        Db2LinkedService linkedService =
                getLinkedService("Db2LinkedServiceLegacy.json", Db2LinkedService.class, LinkedServiceType.DB2);

        assertNotNull(linkedService.getServer());
        assertNotNull(linkedService.getDatabase());
        assertNotNull(linkedService.getUsername());
        assertNotNull(linkedService.getPackageCollection());
    }

    @Test
    public void parseFileServerLinkedService() {
        FileServerLinkedService linkedService =
                getLinkedService("FileServerLinkedService.json", FileServerLinkedService.class, LinkedServiceType.FILE_SERVER);

        assertNotNull(linkedService.getHost());
    }

    @Test
    public void parseFtpServerLinkedService() {
        FtpServerLinkedService linkedService =
                getLinkedService("FtpServerLinkedService.json", FtpServerLinkedService.class, LinkedServiceType.FTP_SERVER);

        assertNotNull(linkedService.getHost());
        assertNotNull(linkedService.getPort());
        assertNotNull(linkedService.getUserName());
    }

    @Test
    public void parseGoogleBigQueryLinkedService() {
        GoogleBigQueryLinkedService linkedService =
                getLinkedService("GoogleBigQueryLinkedService.json", GoogleBigQueryLinkedService.class, LinkedServiceType.GOOGLE_BIGQUERY);

        assertNotNull(linkedService.getProject());
    }

    @Test
    public void parseGoogleCloudStorageLinkedService() {
        GoogleCloudStorageLinkedService linkedService =
                getLinkedService("GoogleCloudStorageLinkedService.json", GoogleCloudStorageLinkedService.class, LinkedServiceType.GOOGLE_CLOUD_STORAGE);

        assertNotNull(linkedService.getServiceUrl());
    }

    @Test
    public void parseGreenplumLinkedService() {
        GreenplumLinkedService linkedService =
                getLinkedService("GreenplumLinkedService.json", GreenplumLinkedService.class, LinkedServiceType.GREENPLUM);

        assertNotNull(linkedService.getConnectionString());
    }

    @Test
    public void parseHdfsLinkedService() {
        HdfsLinkedService linkedService =
                getLinkedService("HdfsLinkedService.json", HdfsLinkedService.class, LinkedServiceType.HDFS);

        assertNotNull(linkedService.getUrl());
    }

    @Test
    public void parseHiveLinkedService() {
        HiveLinkedService linkedService =
                getLinkedService("HiveLinkedService.json", HiveLinkedService.class, LinkedServiceType.HIVE);

        assertNotNull(linkedService.getHost());
        assertNotNull(linkedService.getHttpPath());
        assertEquals(HiveServerType.HIVE_SERVER_2, linkedService.getServerType());
        assertNotNull(linkedService.getUseNativeQuery());
        assertNotNull(linkedService.getUsername());

    }

    @Test
    public void parseNetezzaLinkedService() {
        NetezzaLinkedService linkedService =
                getLinkedService("NetezzaLinkedService.json", NetezzaLinkedService.class, LinkedServiceType.NETEZZA);

        assertNotNull(linkedService.getConnectionString());
    }

    @Test
    public void parseOracleLinkedService() {
        OracleLinkedService linkedService =
                getLinkedService("OracleLinkedService.json", OracleLinkedService.class, LinkedServiceType.ORACLE);

        assertNotNull(linkedService.getConnectionString());
    }

    @Test
    public void parsePostgreSqlLinkedService() {
        PostgreSqlLinkedService linkedService =
                getLinkedService("PostgreSqlLinkedService.json", PostgreSqlLinkedService.class, LinkedServiceType.POSTGRESQL);

        assertNotNull(linkedService.getConnectionString());
    }

    @Test
    public void parsePostgreSqlLinkedServiceLegacy() {
        PostgreSqlLinkedService linkedService =
                getLinkedService("PostgreSqlLinkedServiceLegacy.json", PostgreSqlLinkedService.class, LinkedServiceType.POSTGRESQL);

        assertNull(linkedService.getConnectionString());
        assertNotNull(linkedService.getServer());
        assertNotNull(linkedService.getDatabase());
        assertNotNull(linkedService.getUsername());
    }

    @Test
    public void parseSapHanaLinkedService() {
        SapHanaLinkedService linkedService =
                getLinkedService("SapHanaLinkedService.json", SapHanaLinkedService.class, LinkedServiceType.SAP_HANA);

        assertNotNull(linkedService.getConnectionString());
    }

    @Test
    public void parseSapHanaLinkedServiceLegacy() {
        SapHanaLinkedService linkedService =
                getLinkedService("SapHanaLinkedServiceLegacy.json", SapHanaLinkedService.class, LinkedServiceType.SAP_HANA);

        assertNotNull(linkedService.getServer());
        assertNotNull(linkedService.getUserName());
    }

    @Test
    public void parseSftpServerLinkedService() {
        SftpServerLinkedService linkedService =
                getLinkedService("SftpServerLinkedService.json", SftpServerLinkedService.class, LinkedServiceType.SFTP);

        assertNotNull(linkedService.getHost());
        assertNotNull(linkedService.getPort());
        assertNotNull(linkedService.getUserName());
    }

    @Test
    public void parseSnowflakeLinkedService() {
        SnowflakeLinkedService linkedService =
                getLinkedService("SnowflakeLinkedService.json", SnowflakeLinkedService.class, LinkedServiceType.SNOWFLAKE);

        assertNotNull(linkedService.getConnectionString());
    }

    @Test
    public void parseSqlServerLinkedService() {
        SqlServerLinkedService linkedService =
                getLinkedService("SqlServerLinkedService.json", SqlServerLinkedService.class, LinkedServiceType.SQL_SERVER);

        assertNotNull(linkedService.getConnectionString());
    }

    @Test
    public void parseSybaseLinkedService() {
        SybaseLinkedService linkedService =
                getLinkedService("SybaseLinkedService.json", SybaseLinkedService.class, LinkedServiceType.SYBASE);

        assertNotNull(linkedService.getServer());
        assertNotNull(linkedService.getDatabase());
        assertNotNull(linkedService.getSchema());
        assertNotNull(linkedService.getUsername());
    }

    @Test
    public void parseTeradataLinkedService() {
        TeradataLinkedService linkedService =
                getLinkedService("TeradataLinkedService.json", TeradataLinkedService.class, LinkedServiceType.TERADATA);

        assertNotNull(linkedService.getConnectionString());
    }

    @Test
    public void parseTeradataLinkedServiceLegacy() {
        TeradataLinkedService linkedService =
                getLinkedService("TeradataLinkedServiceLegacy.json", TeradataLinkedService.class, LinkedServiceType.TERADATA);

        assertNull(linkedService.getConnectionString());
        assertNotNull(linkedService.getServer());
        assertNotNull(linkedService.getUsername());
    }

    @Test(expected = IllegalArgumentException.class)
    public void parseUnknownLinkedService() {
        UnknownLinkedService linkedService =
                getLinkedService("UnknownLinkedService.json", UnknownLinkedService.class, LinkedServiceType.UNKNOWN);
    }

    /**
     * Parses the file, checks the class and type of the resulting object. Casts the object and returns it.
     *
     * @param fileName           name of the file with resource to parse
     * @param linkedServiceClass expected class
     * @param linkedServiceType  expected type of the parsed resource
     * @param <T>                Resulting class
     * @return parsed resource
     */
    private <T> T getLinkedService(String fileName, Class<T> linkedServiceClass, LinkedServiceType linkedServiceType) {
        JsonNode linkedServiceNode = readFile(fileName);
        LinkedService linkedService = linkedServiceParserMap.get(parseLinkedServiceType(linkedServiceNode))
                .parseLinkedService(linkedServiceNode);

        assertTrue(linkedServiceClass.isInstance(linkedService));
        assertEquals(linkedServiceType, linkedService.getType());
        return linkedServiceClass.cast(linkedService);
    }

    @Override
    protected File getTestInputDirectory() {
        return TEST_INPUT_DIRECTORY;
    }
}
