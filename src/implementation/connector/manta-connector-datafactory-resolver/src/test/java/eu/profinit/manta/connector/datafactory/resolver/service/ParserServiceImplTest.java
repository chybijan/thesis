package eu.profinit.manta.connector.datafactory.resolver.service;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.dataset.DatasetType;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.resolver.model.Dataset;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.UnknownDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.UnknownLinkedService;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;

public class ParserServiceImplTest extends AdfResourceParserTestBase {

    /**
     * Directory with test input files.
     */
    private static final File TEST_INPUT_DIRECTORY = FileUtils.getFile(TEST_FILES_ROOT, "parserService");

    /**
     * Spring context.
     */
    private static ClassPathXmlApplicationContext springContext;

    /**
     * Map od parsers for Dataset resources.
     */
    private static ParserServiceImpl parserService;

    @BeforeClass
    public static void setUpClass() {
        // Init Spring context, get ParserService
        springContext = new ClassPathXmlApplicationContext(SPRING_CONFIG);
        parserService = springContext.getBean(ParserServiceImpl.class);
    }

    @AfterClass
    public static void tearDown() {
        if (springContext != null) {
            springContext.close();
        }
    }

    @Test
    public void parseUnsupportedDataset() {
        JsonNode datasetNode = readFile("UnsupportedDataset.json");
        Dataset dataset = parserService.parseDataset(datasetNode);

        Assert.assertNotNull(dataset);
        Assert.assertTrue(dataset instanceof UnknownDataset);
        Assert.assertEquals("MyGoogleAdWordsDataset", dataset.getName());
        Assert.assertEquals(DatasetType.UNKNOWN, dataset.getType());

        IReference lsName = dataset.getLinkedServiceReference();
        Assert.assertNotNull(lsName);
        Assert.assertEquals("MyGoogleAdWords", lsName.getReferenceName());
    }

    @Test
    public void parseUnsupportedLinkedService() {
        JsonNode linkedServiceNode = readFile("UnsupportedLinkedService.json");
        LinkedService linkedService = parserService.parseLinkedService(linkedServiceNode);

        Assert.assertNotNull(linkedService);
        Assert.assertTrue(linkedService instanceof UnknownLinkedService);
        Assert.assertEquals("PayPalLinkedService", linkedService.getName());
        Assert.assertEquals(LinkedServiceType.UNKNOWN, linkedService.getType());
    }

    @Override
    protected File getTestInputDirectory() {
        return TEST_INPUT_DIRECTORY;
    }
}