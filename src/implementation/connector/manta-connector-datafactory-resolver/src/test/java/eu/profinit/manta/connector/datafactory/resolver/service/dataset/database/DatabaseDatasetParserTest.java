package eu.profinit.manta.connector.datafactory.resolver.service.dataset.database;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.dataset.DatasetType;
import eu.profinit.manta.connector.datafactory.resolver.model.Dataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.AmazonRedshiftTableDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.AzurePostgreSqlTableDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.AzureSqlDWTableDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.AzureSqlMITableDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.AzureSqlTableDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.Db2TableDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.GoogleBigQueryObjectDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.GreenplumTableDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.HiveObjectDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.NetezzaTableDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.OracleTableDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.PostgreSqlTableDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.SnowflakeDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.SqlServerTableDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.SybaseTableDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.TeradataTableDataset;
import eu.profinit.manta.connector.datafactory.resolver.service.AdfResourceParserTestBase;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;
import eu.profinit.manta.connector.datafactory.resolver.service.dataset.DatasetParser;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DatabaseDatasetParserTest extends AdfResourceParserTestBase {

    /**
     * Directory with test input files.
     */
    private static final File TEST_INPUT_DIRECTORY = FileUtils.getFile(TEST_FILES_ROOT, "dataset", "database");

    /**
     * Spring context.
     */
    private static ClassPathXmlApplicationContext springContext;

    /**
     * Map of parsers for Dataset resources.
     */
    private static Map<DatasetType, DatasetParser<? extends Dataset>> datasetParserMap;

    @BeforeClass
    @SuppressWarnings("unchecked")
    public static void setUpClass() {
        // Init Spring context, get Dataset resource parsers
        springContext = new ClassPathXmlApplicationContext(SPRING_CONFIG);
        datasetParserMap = (Map<DatasetType, DatasetParser<? extends Dataset>>)
                springContext.getBean("datasetParserMap");
    }

    @AfterClass
    public static void tearDown() {
        if (springContext != null) {
            springContext.close();
        }
    }

    private DatasetType parseDatasetType(JsonNode datasetNode) {
        String datasetTypeTypeString = JsonUtil.pathAsText(datasetNode.path("properties"), "type");
        return DatasetType.valueOfCI(datasetTypeTypeString);
    }

    @Test
    public void parseAmazonRedshiftTableDataset() {
        AmazonRedshiftTableDataset dataset = getDataset("AmazonRedshiftTableDataset.json", AmazonRedshiftTableDataset.class, DatasetType.AMAZON_REDSHIFT_TABLE);
        assertTrue(dataset.getSchema().isPresent());
        assertTrue(dataset.getTable().isPresent());
    }

    @Test
    public void parseAzurePostgreSqlTableDataset() {
        AzurePostgreSqlTableDataset dataset = getDataset("AzurePostgreSqlTableDataset.json", AzurePostgreSqlTableDataset.class, DatasetType.AZURE_POSTGRESQL_TABLE);
        assertTrue(dataset.getSchema().isPresent());
        assertTrue(dataset.getTable().isPresent());
    }

    @Test
    public void parseAzureSqlDWTableDataset() {
        AzureSqlDWTableDataset dataset = getDataset("AzureSqlDWTableDataset.json", AzureSqlDWTableDataset.class, DatasetType.AZURE_SQL_DW_TABLE);
        assertTrue(dataset.getSchema().isPresent());
        assertTrue(dataset.getTable().isPresent());
    }

    @Test
    public void parseAzureSqlMITableDataset() {
        AzureSqlMITableDataset dataset = getDataset("AzureSqlMITableDataset.json", AzureSqlMITableDataset.class, DatasetType.AZURE_SQL_MI_TABLE);
        assertTrue(dataset.getSchema().isPresent());
        assertTrue(dataset.getTable().isPresent());
    }

    @Test
    public void parseAzureSqlTableDataset() {
        AzureSqlTableDataset dataset = getDataset("AzureSqlTableDataset.json", AzureSqlTableDataset.class, DatasetType.AZURE_SQL_TABLE);
        assertTrue(dataset.getSchema().isPresent());
        assertTrue(dataset.getTable().isPresent());
    }

    @Test
    public void parseDb2TableDataset() {
        Db2TableDataset dataset = getDataset("Db2TableDataset.json", Db2TableDataset.class, DatasetType.DB2_TABLE);
        assertTrue(dataset.getSchema().isPresent());
        assertTrue(dataset.getTable().isPresent());
    }

    @Test
    public void parseGoogleBigQueryObjectDataset() {
        GoogleBigQueryObjectDataset dataset = getDataset("GoogleBigQueryObjectDataset.json", GoogleBigQueryObjectDataset.class, DatasetType.GOOGLE_BIGQUERY_OBJECT);
        assertTrue(dataset.getSchema().isPresent());
        assertTrue(dataset.getTable().isPresent());
    }

    @Test
    public void parseGreenplumTableDataset() {
        GreenplumTableDataset dataset = getDataset("GreenplumTableDataset.json", GreenplumTableDataset.class, DatasetType.GREENPLUM_TABLE);
        assertTrue(dataset.getSchema().isPresent());
        assertTrue(dataset.getTable().isPresent());
    }

    @Test
    public void parseHiveObjectDataset() {
        HiveObjectDataset dataset = getDataset("HiveObjectDataset.json", HiveObjectDataset.class, DatasetType.HIVE_OBJECT);
        assertTrue(dataset.getSchema().isPresent());
        assertTrue(dataset.getTable().isPresent());
    }

    @Test
    public void parseNetezzaTableDataset() {
        NetezzaTableDataset dataset = getDataset("NetezzaTableDataset.json", NetezzaTableDataset.class, DatasetType.NETEZZA_TABLE);
        assertTrue(dataset.getSchema().isPresent());
        assertTrue(dataset.getTable().isPresent());
    }

    @Test
    public void parseOracleTableDataset() {
        OracleTableDataset dataset = getDataset("OracleTableDataset.json", OracleTableDataset.class, DatasetType.ORACLE_TABLE);
        assertTrue(dataset.getSchema().isPresent());
        assertTrue(dataset.getTable().isPresent());
    }

    @Test
    public void parsePostgreSqlTableDataset() {
        PostgreSqlTableDataset dataset = getDataset("PostgreSqlTableDataset.json", PostgreSqlTableDataset.class, DatasetType.POSTGRESQL_TABLE);
        assertTrue(dataset.getSchema().isPresent());
        assertTrue(dataset.getTable().isPresent());
    }

    @Test
    public void parseSnowflakeDataset() {
        SnowflakeDataset dataset = getDataset("SnowflakeDataset.json", SnowflakeDataset.class, DatasetType.SNOWFLAKE_TABLE);
        assertTrue(dataset.getSchema().isPresent());
        assertTrue(dataset.getTable().isPresent());
    }

    @Test
    public void parseSqlServerTableDataset() {
        SqlServerTableDataset dataset = getDataset("SqlServerTableDataset.json", SqlServerTableDataset.class, DatasetType.SQL_SERVER_TABLE);
        assertTrue(dataset.getSchema().isPresent());
        assertTrue(dataset.getTable().isPresent());
    }

    @Test
    public void parseSybaseTableDataset() {
        SybaseTableDataset dataset = getDataset("SybaseTableDataset.json", SybaseTableDataset.class, DatasetType.SYBASE_TABLE);
        assertFalse(dataset.getSchema().isPresent());
        assertFalse(dataset.getTable().isPresent());
        assertTrue(dataset.getTableName().isPresent());
    }

    @Test
    public void parseTeradataTableDataset() {
        TeradataTableDataset dataset = getDataset("TeradataTableDataset.json", TeradataTableDataset.class, DatasetType.TERADATA_TABLE);
        assertTrue(dataset.getSchema().isPresent());
        assertTrue(dataset.getTable().isPresent());
    }

    private <T> T getDataset(String fileName, Class<T> datasetClass, DatasetType datasetType) {
        JsonNode datasetNode = readFile(fileName);
        Dataset linkedService = datasetParserMap.get(parseDatasetType(datasetNode))
                .parseDataset(datasetNode);

        assertTrue(datasetClass.isInstance(linkedService));
        assertEquals(datasetType, linkedService.getType());
        return datasetClass.cast(linkedService);
    }

    @Override
    protected File getTestInputDirectory() {
        return TEST_INPUT_DIRECTORY;
    }
}