package eu.profinit.manta.connector.datafactory.resolver.service.dataset;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IArgument;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.ParameterType;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.dataset.DatasetType;
import eu.profinit.manta.connector.datafactory.resolver.model.Dataset;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.resolver.service.AdfResourceParserTestBase;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class AbstractDatasetParserTest extends AdfResourceParserTestBase {

    /**
     * Directory with test input files.
     */
    private static final File TEST_INPUT_DIRECTORY = FileUtils.getFile(TEST_FILES_ROOT, "dataset");

    /**
     * Spring context.
     */
    private static ClassPathXmlApplicationContext springContext;

    /**
     * Map od parsers for Dataset resources.
     */
    private static Map<DatasetType, DatasetParser<? extends Dataset>> datasetParserMap;

    @BeforeClass
    @SuppressWarnings("unchecked")
    public static void setUpClass() {
        // Init Spring context, get Dataset resource parsers
        springContext = new ClassPathXmlApplicationContext(SPRING_CONFIG);
        datasetParserMap = (Map<DatasetType, DatasetParser<? extends Dataset>>)
                springContext.getBean("datasetParserMap");
    }

    @AfterClass
    public static void tearDown() {
        if (springContext != null) {
            springContext.close();
        }
    }

    private DatasetType parseDatasetType(JsonNode datasetNode) {
        String datasetTypeTypeString = JsonUtil.pathAsText(datasetNode.path("properties"), "type");
        return DatasetType.valueOfCI(datasetTypeTypeString);
    }

    @Test
    public void parseStandardDataset() {
        JsonNode datasetNode = readFile("StandardDataset.json");
        Dataset dataset = datasetParserMap.get(parseDatasetType(datasetNode)).parseDataset(datasetNode);

        // verify name and type
        assertEquals("source_bak_data", dataset.getName());
        assertEquals(DatasetType.DELIMITED_TEXT, dataset.getType());

        // verify LinkedServiceName field
        IReference linkedServiceName = dataset.getLinkedServiceReference();
        assertEquals("GetManta storage account", linkedServiceName.getReferenceName());

        List<IArgument> arguments = linkedServiceName.getArguments().stream().collect(Collectors.toList());
        assertNotNull(arguments);
        assertEquals(3, arguments.size());
        arguments.sort(Comparator.comparing(IArgument::getName));
        assertEquals("p1", arguments.get(0).getName());
        //assertTrue(arguments.get(0).getFieldValue().getStringValue().isObject());
        assertEquals("p2", arguments.get(1).getName());
        //assertTrue(arguments.get(1).getFieldValue().getStringValue().isArray());
        assertEquals("p3", arguments.get(2).getName());
        //assertTrue(arguments.get(2).getFieldValue().getStringValue().isObject());

        // verify IParameters field
        IParameters parameters = dataset.getParameters();
        assertNotNull(parameters);
        assertEquals(6, parameters.size());
        assertEquals(ParameterType.INT, parameters.get("a").getType());
        assertEquals(1, parameters.get("a").getDefaultValue().asInt());
        assertEquals(ParameterType.STRING, parameters.get("b").getType());
        assertEquals("default string value", parameters.get("b").getDefaultValue().asText());
        assertEquals(ParameterType.OBJECT, parameters.get("c").getType());
        assertTrue(parameters.get("c").getDefaultValue().isObject());
        assertEquals(ParameterType.ARRAY, parameters.get("d").getType());
        assertTrue(parameters.get("d").getDefaultValue().isArray());
        assertEquals(ParameterType.STRING, parameters.get("e").getType());
        assertTrue(parameters.get("e").getDefaultValue().isMissingNode());
        assertEquals(ParameterType.OBJECT, parameters.get("f").getType());
        assertEquals("def value for f", parameters.get("f").getDefaultValue().asText());

        // verify properties.schema and properties.structure
        IDatasetSchema datasetSchema = dataset.getDatasetSchema();
        assertNotNull(datasetSchema);
        assertNotNull(datasetSchema.getDataElements());
        assertEquals(5, datasetSchema.getDataElements().size());
        assertNull(datasetSchema.getExpression());

        assertNotNull(dataset.getDatasetStructure());
    }

    @Test
    public void parseStandardDatasetSchemaExpression() {
        JsonNode datasetNode = readFile("StandardDatasetSchemaExpression.json");
        Dataset dataset = datasetParserMap.get(parseDatasetType(datasetNode)).parseDataset(datasetNode);

        IDatasetSchema datasetSchema = dataset.getDatasetSchema();
        assertNotNull(datasetSchema);

        assertEquals(new ArrayList<>(), datasetSchema.getDataElements());
        assertNotNull(datasetSchema.getExpression());
    }

    @Test
    public void parseMinimalDataset() {
        JsonNode datasetNode = readFile("MinimalDataset.json");
        Dataset dataset = datasetParserMap.get(parseDatasetType(datasetNode)).parseDataset(datasetNode);

        // verify name and type
        assertEquals("MinimalDataset", dataset.getName());
        assertEquals(DatasetType.DELIMITED_TEXT, dataset.getType());

        // verify LinkedServiceName field
        IReference linkedServiceName = dataset.getLinkedServiceReference();
        assertEquals("AnotherStorageAccount", linkedServiceName.getReferenceName());
        assertTrue(linkedServiceName.getArguments().isEmpty());

        // verify empty params
        assertEquals(0, dataset.getParameters().size());

        // verify properties.schema and properties.structure
        assertNull(dataset.getDatasetSchema());
        assertTrue(dataset.getDatasetStructure().isMissingNode());
    }

    @Override
    protected File getTestInputDirectory() {
        return TEST_INPUT_DIRECTORY;
    }
}