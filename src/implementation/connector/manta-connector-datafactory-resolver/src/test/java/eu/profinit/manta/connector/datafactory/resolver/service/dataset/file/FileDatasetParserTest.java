package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.dataset.DatasetType;
import eu.profinit.manta.connector.datafactory.resolver.model.Dataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.AvroDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.BinaryDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.DelimitedTextDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.ExcelDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.JsonDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.OrcDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.ParquetDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.XmlDataset;
import eu.profinit.manta.connector.datafactory.resolver.service.AdfResourceParserTestBase;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;
import eu.profinit.manta.connector.datafactory.resolver.service.dataset.DatasetParser;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class FileDatasetParserTest extends AdfResourceParserTestBase {

    /**
     * Directory with test input files.
     */
    private static final File TEST_INPUT_DIRECTORY = FileUtils.getFile(TEST_FILES_ROOT, "dataset", "file");

    /**
     * Spring context.
     */
    private static ClassPathXmlApplicationContext springContext;

    /**
     * Map od parsers for Dataset resources.
     */
    private static Map<DatasetType, DatasetParser<? extends Dataset>> datasetParserMap;

    @BeforeClass
    @SuppressWarnings("unchecked")
    public static void setUpClass() {
        // Init Spring context, get Dataset resource parsers
        springContext = new ClassPathXmlApplicationContext(SPRING_CONFIG);
        datasetParserMap = (Map<DatasetType, DatasetParser<? extends Dataset>>)
                springContext.getBean("datasetParserMap");
    }

    @AfterClass
    public static void tearDown() {
        if (springContext != null) {
            springContext.close();
        }
    }

    private DatasetType parseDatasetType(JsonNode datasetNode) {
        String datasetTypeTypeString = JsonUtil.pathAsText(datasetNode.path("properties"), "type");
        return DatasetType.valueOfCI(datasetTypeTypeString);
    }

    @Test
    public void parseAvroDataset() {
        JsonNode datasetNode = readFile("AvroDataset.json");
        Dataset dataset = datasetParserMap.get(parseDatasetType(datasetNode)).parseDataset(datasetNode);

        assertTrue(dataset instanceof AvroDataset);
        assertEquals(DatasetType.AVRO, dataset.getType());
        AvroDataset avroDataset = (AvroDataset) dataset;

        assertEquals("AvroDataset", avroDataset.getName());
        assertNotNull(avroDataset.getLocation());
    }

    @Test
    public void parseBinaryDataset() {
        JsonNode datasetNode = readFile("BinaryDataset.json");
        Dataset dataset = datasetParserMap.get(parseDatasetType(datasetNode)).parseDataset(datasetNode);

        assertTrue(dataset instanceof BinaryDataset);
        assertEquals(DatasetType.BINARY, dataset.getType());
        BinaryDataset binaryDataset = (BinaryDataset) dataset;

        assertEquals("BinaryDataset", binaryDataset.getName());
        assertNotNull(binaryDataset.getLocation());
    }

    @Test
    public void parseDelimitedTextDataset() {
        JsonNode datasetNode = readFile("DelimitedTextDataset.json");
        Dataset dataset = datasetParserMap.get(parseDatasetType(datasetNode)).parseDataset(datasetNode);

        assertTrue(dataset instanceof DelimitedTextDataset);
        assertEquals(DatasetType.DELIMITED_TEXT, dataset.getType());
        DelimitedTextDataset delimitedTextDataset = (DelimitedTextDataset) dataset;

        assertEquals("DelimitedTextDataset", delimitedTextDataset.getName());
        assertNotNull(delimitedTextDataset.getLocation());
    }

    @Test
    public void parseExcelDataset() {
        JsonNode datasetNode = readFile("ExcelDataset.json");
        Dataset dataset = datasetParserMap.get(parseDatasetType(datasetNode)).parseDataset(datasetNode);

        assertTrue(dataset instanceof ExcelDataset);
        assertEquals(DatasetType.EXCEL, dataset.getType());
        ExcelDataset excelDataset = (ExcelDataset) dataset;

        assertEquals("ExcelDataset", excelDataset.getName());
        assertNotNull(excelDataset.getLocation());
    }

    @Test
    public void parseJsonDataset() {
        JsonNode datasetNode = readFile("JsonDataset.json");
        Dataset dataset = datasetParserMap.get(parseDatasetType(datasetNode)).parseDataset(datasetNode);

        assertTrue(dataset instanceof JsonDataset);
        assertEquals(DatasetType.JSON, dataset.getType());
        JsonDataset excelDataset = (JsonDataset) dataset;

        assertEquals("JSONDataset", excelDataset.getName());
        assertNotNull(excelDataset.getLocation());
    }

    @Test
    public void parseOrcDataset() {
        JsonNode datasetNode = readFile("OrcDataset.json");
        Dataset dataset = datasetParserMap.get(parseDatasetType(datasetNode)).parseDataset(datasetNode);

        assertTrue(dataset instanceof OrcDataset);
        assertEquals(DatasetType.ORC, dataset.getType());
        OrcDataset excelDataset = (OrcDataset) dataset;

        assertEquals("OrcDataset", excelDataset.getName());
        assertNotNull(excelDataset.getLocation());
    }

    @Test
    public void parseParquetDataset() {
        JsonNode datasetNode = readFile("ParquetDataset.json");
        Dataset dataset = datasetParserMap.get(parseDatasetType(datasetNode)).parseDataset(datasetNode);

        assertTrue(dataset instanceof ParquetDataset);
        assertEquals(DatasetType.PARQUET, dataset.getType());
        ParquetDataset excelDataset = (ParquetDataset) dataset;

        assertEquals("ParquetDataset", excelDataset.getName());
        assertNotNull(excelDataset.getLocation());
    }

    @Test
    public void parseXmlDataset() {
        JsonNode datasetNode = readFile("XmlDataset.json");
        Dataset dataset = datasetParserMap.get(parseDatasetType(datasetNode)).parseDataset(datasetNode);

        assertTrue(dataset instanceof XmlDataset);
        assertEquals(DatasetType.XML, dataset.getType());
        XmlDataset excelDataset = (XmlDataset) dataset;

        assertEquals("XMLDataset", excelDataset.getName());
        assertNotNull(excelDataset.getLocation());
    }

    @Override
    protected File getTestInputDirectory() {
        return TEST_INPUT_DIRECTORY;
    }
}