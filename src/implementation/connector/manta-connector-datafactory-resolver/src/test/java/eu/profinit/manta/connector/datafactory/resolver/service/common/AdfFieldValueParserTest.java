package eu.profinit.manta.connector.datafactory.resolver.service.common;

import eu.profinit.manta.connector.datafactory.model.common.*;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.model.common.AdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.service.AdfResourceParserTestBase;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.io.FileUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class AdfFieldValueParserTest extends AdfResourceParserTestBase {

    /**
     * Directory with test inout files.
     */
    private static final File TEST_INPUT_DIRECTORY = FileUtils.getFile(TEST_FILES_ROOT, "common");

    protected AdfFieldValueParser fieldValueParser;

    private ObjectMapper mapper;


    @Before
    public void init(){
        mapper = new ObjectMapper();
        fieldValueParser = new AdfFieldValueParserImpl();
    }

    @Override
    protected File getTestInputDirectory() {
        return TEST_INPUT_DIRECTORY;
    }

    /**
     * Prepare mock for some tests
     * @return list of fieldValues that were set as return of a mock
     */
    private List<IAdfFieldValue> prepareReferenceParing() throws JsonProcessingException {

        // Prepare data
        ObjectMapper mapper = new ObjectMapper();

        JsonNode json1 = mapper.readTree("\"def1\"");
        JsonNode json2 = mapper.readTree("{\"value\":\"@concat('aaa','bbb')\",\"type\":\"Expression\"}");
        JsonNode json3 = mapper.readTree("\"def3\"");

        IAdfFieldValue val1 = new AdfFieldValue(AdfFieldType.STRING, json1.toString());
        IAdfFieldValue val2 = new AdfFieldValue(AdfFieldType.OBJECT, json2.toString());
        IAdfFieldValue val3 = new AdfFieldValue(AdfFieldType.STRING, json3.toString());

        List<IAdfFieldValue> values = new ArrayList<>();
        values.add(val1);
        values.add(val2);
        values.add(val3);

        return values;
    }

    private void cmpAdfFieldValue(IAdfFieldValue expectedFieldValue, IAdfFieldValue realFieldValue){
        assertEquals(expectedFieldValue.getType(), realFieldValue.getType());
        assertEquals(expectedFieldValue.getStringValue(), realFieldValue.getStringValue());
    }

    @Test
    public void parseExpressionValue() throws JsonProcessingException {

        // Test expression

        JsonNode fieldNode = readFile("adfFieldValueExpression1.json");

        IAdfFieldValue fieldValue = fieldValueParser.parseAdfFieldValue(fieldNode);

        IAdfFieldValue expectedValue = new AdfFieldValue(AdfFieldType.EXPRESSION, "@concat('aaa','bbb')");

        cmpAdfFieldValue(expectedValue, fieldValue);


        // Test array

        fieldNode = readFile("adfFieldValueInteger1.json");

        fieldValue = fieldValueParser.parseAdfFieldValue(fieldNode);

        expectedValue = new AdfFieldValue(AdfFieldType.INT, "1");

        cmpAdfFieldValue(expectedValue, fieldValue);

        // Test object

    }

    @Test
    public void parseIntegerValue(){

        JsonNode fieldNode = readFile("adfFieldValueInteger1.json");

        IAdfFieldValue fieldValue = fieldValueParser.parseAdfFieldValue(fieldNode);

        IAdfFieldValue expectedValue = new AdfFieldValue(AdfFieldType.INT, "1");

        cmpAdfFieldValue(expectedValue, fieldValue);

    }

    @Test
    public void parseFloatValue(){

        JsonNode fieldNode = readFile("adfFieldValueFloat1.json");

        IAdfFieldValue fieldValue = fieldValueParser.parseAdfFieldValue(fieldNode);

        IAdfFieldValue expectedValue = new AdfFieldValue(AdfFieldType.FLOAT, "420.69");

        cmpAdfFieldValue(expectedValue, fieldValue);

    }

    @Test
    public void parseString(){

        JsonNode fieldNode = readFile("adfFieldValueString1.json");

        IAdfFieldValue fieldValue = fieldValueParser.parseAdfFieldValue(fieldNode);

        IAdfFieldValue expectedValue = new AdfFieldValue(AdfFieldType.STRING, "String value of anything");

        cmpAdfFieldValue(expectedValue, fieldValue);

    }

    @Test
    public void parseArray(){

        JsonNode fieldNode = readFile("adfFieldValueArray1.json");

        IAdfFieldValue fieldValue = fieldValueParser.parseAdfFieldValue(fieldNode);

        IAdfFieldValue expectedValue = new AdfFieldValue(AdfFieldType.ARRAY, "[1,2,3]");

        cmpAdfFieldValue(expectedValue, fieldValue);

    }

    @Test
    public void parseObject(){

        JsonNode fieldNode = readFile("adfFieldValueObject1.json");

        IAdfFieldValue fieldValue = fieldValueParser.parseAdfFieldValue(fieldNode);

        IAdfFieldValue expectedValue = new AdfFieldValue(AdfFieldType.OBJECT, "{\"name\":\"value\",\"name2\":2}");

        cmpAdfFieldValue(expectedValue, fieldValue);

    }

}
