package eu.profinit.manta.connector.datafactory.resolver.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public abstract class AdfResourceParserTestBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdfResourceParserTestBase.class);

    /**
     * Location of spring configuration file.
     */
    protected static final String SPRING_CONFIG = "spring/DataFactoryResolverTest.xml";

    /**
     * Mapper to map input JSON files.
     */
    private final ObjectMapper mapper = new ObjectMapper();

    /**
     * Root folder for test files.
     */
    public static final File TEST_FILES_ROOT = new File("target/test-classes");

    /**
     * Encoding of the input files.
     */
    private static final String INPUT_FILE_ENCODING = "utf8";


    /**
     * Reads the file in the {@link AdfResourceParserTestBase#getTestInputDirectory()}.
     *
     * @param fileName name of the file
     * @return JsonNode pointing to the root of the read JSON or null if the file is not found, or cannot be read
     */
    protected JsonNode readFile(String fileName) {
        File inputFile = new File(getTestInputDirectory(), fileName);

        JsonNode root = null;
        try (FileInputStream is = new FileInputStream(inputFile.getAbsolutePath());
             InputStreamReader isr = new InputStreamReader(is, INPUT_FILE_ENCODING)) {
            root = mapper.readTree(isr);
        } catch (FileNotFoundException e) {
            LOGGER.error("Provided file = {} is not found", inputFile.getAbsolutePath(), e);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Provided file = {} has unsupported encoding. Used encoding is = {}",
                    inputFile.getAbsolutePath(), INPUT_FILE_ENCODING, e);
        } catch (IOException e) {
            LOGGER.error("Provided file = {} cannot be read", inputFile.getAbsolutePath(), e);
        }
        return root;
    }

    /**
     * Reads the file in the {@link AdfResourceParserTestBase#getTestInputDirectory()}.
     *
     * @param fileName name of the file
     * @return raw string data of file
     */
    protected String readFileRaw(String fileName) {
        File inputFile = new File(getTestInputDirectory(), fileName);

        String text = "";
        try (FileInputStream is = new FileInputStream(inputFile.getAbsolutePath());
             InputStreamReader isr = new InputStreamReader(is, INPUT_FILE_ENCODING)) {
             text = IOUtils.toString(isr);
        } catch (FileNotFoundException e) {
            LOGGER.error("Provided file = {} is not found", inputFile.getAbsolutePath(), e);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Provided file = {} has unsupported encoding. Used encoding is = {}",
                    inputFile.getAbsolutePath(), INPUT_FILE_ENCODING, e);
        } catch (IOException e) {
            LOGGER.error("Provided file = {} cannot be read", inputFile.getAbsolutePath(), e);
        }
        return text;
    }

    protected abstract File getTestInputDirectory();

}
