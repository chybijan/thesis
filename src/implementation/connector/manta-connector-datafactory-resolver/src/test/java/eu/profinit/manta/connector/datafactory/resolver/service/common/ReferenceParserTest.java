package eu.profinit.manta.connector.datafactory.resolver.service.common;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.profinit.manta.connector.datafactory.model.common.*;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IArgument;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.resolver.model.common.AdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.model.common.Argument;
import eu.profinit.manta.connector.datafactory.resolver.model.common.Reference;
import eu.profinit.manta.connector.datafactory.resolver.service.AdfResourceParserTestBase;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ReferenceParserTest extends AdfResourceParserTestBase {

    /**
     * Directory with test inout files.
     */
    private static final File TEST_INPUT_DIRECTORY = FileUtils.getFile(TEST_FILES_ROOT, "common");

    protected ReferenceParser referenceParser;

    @Mock
    protected AdfFieldValueParser fieldValueParserMock;

    @Before
    public void init(){
        referenceParser = new ReferenceParserImpl(fieldValueParserMock);
    }

    @Override
    protected File getTestInputDirectory() {
        return TEST_INPUT_DIRECTORY;
    }

    private void cmpReferences(IReference expectedReference, IReference reference){
        assertEquals(expectedReference.getReferenceName(), reference.getReferenceName());
        assertEquals(expectedReference.getType(), reference.getType());
        cmpCollectionOfArguments(expectedReference.getArguments(), reference.getArguments());
    }

    private void cmpCollectionOfArguments(Collection<IArgument> expectedArgs, Collection<IArgument> args){
        assertEquals(expectedArgs.size(), args.size());

        List<IArgument> expectedList = new ArrayList<>(expectedArgs);
        List<IArgument> gotList = new ArrayList<>(args);
        for(int i = 0; i < expectedArgs.size(); ++i){
            cmpArguments(expectedList.get(i), gotList.get(i));
        }
    }

    private void cmpArguments(IArgument expectedArgument, IArgument argument){
        assertEquals(expectedArgument.getName(), argument.getName());
        assertEquals(expectedArgument.getFieldValue().getType(), argument.getFieldValue().getType());
        assertEquals(expectedArgument.getFieldValue().getStringValue(), argument.getFieldValue().getStringValue());
    }

    /**
     * Prepare mock for some tests
     * @return list of fieldValues that were set as return of a mock
     */
    private List<IAdfFieldValue> prepareReferenceParing() throws JsonProcessingException {

        // Prepare data
        ObjectMapper mapper = new ObjectMapper();

        JsonNode json1 = mapper.readTree("\"def1\"");
        JsonNode json2 = mapper.readTree("{\"value\":\"@concat('aaa','bbb')\",\"type\":\"Expression\"}");
        JsonNode json3 = mapper.readTree("\"def3\"");

        IAdfFieldValue val1 = new AdfFieldValue(AdfFieldType.STRING, json1.toString());
        IAdfFieldValue val2 = new AdfFieldValue(AdfFieldType.OBJECT, json2.toString());
        IAdfFieldValue val3 = new AdfFieldValue(AdfFieldType.STRING, json3.toString());

        Mockito.when(fieldValueParserMock.parseAdfFieldValue(json1)).thenReturn(val1);
        Mockito.when(fieldValueParserMock.parseAdfFieldValue(json2)).thenReturn(val2);
        Mockito.when(fieldValueParserMock.parseAdfFieldValue(json3)).thenReturn(val3);

        List<IAdfFieldValue> values = new ArrayList<>();
        values.add(val1);
        values.add(val2);
        values.add(val3);

        return values;
    }

    @Test
    public void parseDatasetReference() throws JsonProcessingException {

        // Data preparation
        List<IAdfFieldValue> values = prepareReferenceParing();

        // Test
        JsonNode referenceNode = readFile("datasetReference1.json");

        IReference reference = referenceParser.parseReference(referenceNode, ReferenceType.DATASET);

        assertEquals(ReferenceType.DATASET, reference.getType());

        Collection<IArgument> arguments = new ArrayList<>();

        IArgument arg1 = new Argument("par1", values.get(0));
        IArgument arg2 = new Argument("par2", values.get(1));
        IArgument arg3 = new Argument("par3", values.get(2));
        arguments.add(arg1);
        arguments.add(arg2);
        arguments.add(arg3);

        IReference expectedReference = new Reference("DelimitedText1", arguments, ReferenceType.DATASET);

        cmpReferences(expectedReference, reference);

    }

    @Test
    public void parseLinkedServiceReference() throws JsonProcessingException {

        // Data preparation
        List<IAdfFieldValue> values = prepareReferenceParing();

        JsonNode referenceNode = readFile("linkedServiceReference1.json");

        IReference reference = referenceParser.parseReference(referenceNode, ReferenceType.LINKED_SERVICE);

        assertEquals(ReferenceType.LINKED_SERVICE, reference.getType());

        Collection<IArgument> arguments = new ArrayList<>();

        IArgument arg1 = new Argument("par1", values.get(0));
        IArgument arg2 = new Argument("par2", values.get(1));
        IArgument arg3 = new Argument("par3", values.get(2));
        arguments.add(arg1);
        arguments.add(arg2);
        arguments.add(arg3);

        IReference expectedReference = new Reference("DelimitedText1", arguments, ReferenceType.LINKED_SERVICE);

        cmpReferences(expectedReference, reference);

    }

    @Test
    public void defaultReferenceType(){

        JsonNode referenceNode = readFile("defaultTypeReference1.json");

        IReference reference = referenceParser.parseReference(referenceNode, ReferenceType.LINKED_SERVICE);

        assertEquals(ReferenceType.LINKED_SERVICE, reference.getType());

        reference = referenceParser.parseReference(referenceNode, ReferenceType.DATASET);

        assertEquals(ReferenceType.DATASET, reference.getType());

    }

    @Test
    public void emptyNameTest(){
        JsonNode referenceNode = readFile("emptyNameDatasetReference.json");

        IReference reference = referenceParser.parseReference(referenceNode, ReferenceType.DATASET);

        assertNull(reference);
    }
}
