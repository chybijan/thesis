package eu.profinit.manta.connector.datafactory.resolver.service.dataflow;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.IDataFlow;
import eu.profinit.manta.connector.datafactory.model.common.IArgument;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.ReferenceType;
import eu.profinit.manta.connector.datafactory.model.dataflow.DataFlowType;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowProperties;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowEndpoint;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowScript;
import eu.profinit.manta.connector.datafactory.resolver.model.common.Reference;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.DataFlowEndpoint;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.DataFlowScript;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.mapping.MappingDataFlowProperties;
import eu.profinit.manta.connector.datafactory.resolver.service.AdfResourceParserTestBase;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Tests for parsing of Data flow resource.
 */
@RunWith(MockitoJUnitRunner.class)
public class DataFlowParserImplTest extends AdfResourceParserTestBase {

    /**
     * Directory with test inout files.
     */
    private static final File TEST_INPUT_DIRECTORY = FileUtils.getFile(TEST_FILES_ROOT, "dataflow");

    /**
     * Spring context.
     */
    private static ClassPathXmlApplicationContext springContext;

    /**
     * Parser for Data flow resource.
     */
    private static DataFlowParser dataFlowParser;

    @BeforeClass
    public static void setUpClass() {

        // Init Spring context, get Factory resource parser
        springContext = new ClassPathXmlApplicationContext(SPRING_CONFIG);
        dataFlowParser = springContext.getBean(DataFlowParserImpl.class);

    }

    @AfterClass
    public static void tearDown() {
        if (springContext != null) {
            springContext.close();
        }
    }

    private void cmpSinks(IDataFlowEndpoint expectedSink, IDataFlowEndpoint sink){
        cmpSources(expectedSink, sink);
    }

    private void cmpSources(IDataFlowEndpoint expectedSource, IDataFlowEndpoint source){
        assertEquals(expectedSource.getName(), source.getName());

        assertEquals(expectedSource.getType(), source.getType());
        cmpReferences(expectedSource.getReference(), source.getReference());

    }

    private void cmpLinkedServiceReferences(IReference expectedLinkedServiceReference, IReference realLinkedServiceReference) {
        cmpReferences(expectedLinkedServiceReference, realLinkedServiceReference);
    }

    private void cmpReferences(IReference expectedReference, IReference reference){
        assertEquals(expectedReference.getReferenceName(), reference.getReferenceName());
        assertEquals(expectedReference.getArguments().size(), reference.getArguments().size());
        cmpCollectionOfArguments(expectedReference.getArguments(), reference.getArguments());
    }

    private void cmpCollectionOfArguments(Collection<IArgument> expectedArgs, Collection<IArgument> args){
        assertEquals(expectedArgs.size(), args.size());

        List<IArgument> expectedList = new ArrayList<>(expectedArgs);
        List<IArgument> gotList = new ArrayList<>(args);
        for(int i = 0; i < expectedArgs.size(); ++i){
            cmpArguments(expectedList.get(i), gotList.get(i));
        }
    }

    private void cmpArguments(IArgument expectedArgument, IArgument argument){
        assertEquals(expectedArgument.getName(), argument.getName());
        assertEquals(expectedArgument.getFieldValue().toString(), argument.getFieldValue().toString());
    }


    @Test
    public void parseSimpleMappingDataFlow() {
        JsonNode dataFlowRootNode = readFile("SimpleMappingDataFlow.json");
        IDataFlow dataFlow = dataFlowParser.parseDataFlow(dataFlowRootNode);

        assertEquals("filterUsers", dataFlow.getName());

        IDataFlowProperties properties = dataFlow.getProperties();

        assertEquals(DataFlowType.MAPPING_DATA_FLOW, properties.getType());

        assertTrue(properties instanceof MappingDataFlowProperties);

        // Properties expected
//        String script = "source(output(\n\t\tName as string,\n\t\tSurname as string,\n\t\tYear as string\n\t),\n\tallowSchemaDrift: true,\n\tvalidateSchema: false,\n\tignoreNoFilesFound: false) ~> source1\nsource1 filter(Surname=='Bulat') ~> Filter1\nFilter1 sink(allowSchemaDrift: true,\n\tvalidateSchema: false,\n\tskipDuplicateMapInputs: true,\n\tskipDuplicateMapOutputs: true) ~> sink2";
        IDataFlowScript script = new DataFlowScript(Collections.emptyList(), Collections.emptyMap());

        Map<String, IDataFlowEndpoint> sources = new HashMap<>();
        sources.put("source1", new DataFlowEndpoint(
                "source1",
                new Reference("testData", new ArrayList<>(), ReferenceType.DATASET),
                null, null));

        Map<String, IDataFlowEndpoint> sinks = new HashMap<>();
        sinks.put("sink2", new DataFlowEndpoint(
                "sink2",
                new Reference("users_csv", new ArrayList<>(), ReferenceType.DATASET),
                null, null));

        List<String> transformations = new ArrayList<>();
        transformations.add("Filter1");

        MappingDataFlowProperties expectedMappingProperties = new MappingDataFlowProperties(
                script, sources, sinks, transformations
        );

        MappingDataFlowProperties mappingProperties = (MappingDataFlowProperties) properties;

        // TODO compare script
//        assertEquals(expectedMappingProperties.getScript(), mappingProperties.getScript());

        // Compare tranformations
        assertEquals(expectedMappingProperties.getTransformationNames().size(), mappingProperties.getTransformationNames().size());
        assertEquals(expectedMappingProperties.getTransformationNames().stream().findFirst().get(), mappingProperties.getTransformationNames().stream().findFirst().get());

        // Compare sources
        assertEquals(expectedMappingProperties.getSources().size(), mappingProperties.getSources().size());
        cmpSources(expectedMappingProperties.getSources().values().stream().findFirst().get(), mappingProperties.getSources().values().stream().findFirst().get());

        // Compare sinks
        assertEquals(expectedMappingProperties.getSinks().size(), mappingProperties.getSinks().size());
        cmpSinks(expectedMappingProperties.getSinks().values().stream().findFirst().get(), mappingProperties.getSinks().values().stream().findFirst().get());


    }

    @Test
    public void parseDynamicMappingDataFlow(){
        JsonNode dataFlowRootNode = readFile("DynamicMappingDataFlow.json");
        IDataFlow dataFlow = dataFlowParser.parseDataFlow(dataFlowRootNode);

        assertEquals("dynamicDF", dataFlow.getName());

        IDataFlowProperties properties = dataFlow.getProperties();

        assertEquals(DataFlowType.MAPPING_DATA_FLOW, properties.getType());

        assertTrue(properties instanceof MappingDataFlowProperties);

//        String script = "parameters{\n\tfolderName as string\n}\nsource(allowSchemaDrift: true,\n\tvalidateSchema: false,\n\tignoreNoFilesFound: false,\n\trowUrlColumn: 'FileName',\n\twildcardPaths:[($folderName + '*.csv')]) ~> BloblSource\nBloblSource derive({Dervived column 0} = toString(byName('_col0_')),\n\t\t{antoher column with name} = FileName,\n\t\teach(match(type=='string'), 'string matched ' + $$ = $$),\n\t\t{each(match(type=='string')} = FileName,\n\t\teach = FileName) ~> DerivedColumn1\nDerivedColumn1 sink(allowSchemaDrift: true,\n\tvalidateSchema: false,\n\tinput(\n\t\tColumn_1 as string,\n\t\tColumn_2 as string,\n\t\tColumn_3 as string,\n\t\tColumn_4 as string,\n\t\tColumn_5 as string,\n\t\tColumn_6 as string,\n\t\tColumn_7 as string,\n\t\tColumn_8 as string,\n\t\tColumn_9 as string,\n\t\tColumn_10 as string,\n\t\tColumn_11 as string,\n\t\tColumn_12 as string\n\t),\n\tskipDuplicateMapInputs: true,\n\tskipDuplicateMapOutputs: true) ~> sink1";
        IDataFlowScript script = new DataFlowScript(Collections.emptyList(), Collections.emptyMap());


        Map<String, IDataFlowEndpoint> sources = new HashMap<>();
        sources.put("BloblSource", new DataFlowEndpoint(
                "BloblSource",
                new Reference("DelimitedText1", new ArrayList<>(), ReferenceType.DATASET),
                null, null));

        Map<String, IDataFlowEndpoint> sinks = new HashMap<>();
        sinks.put("sink1", new DataFlowEndpoint(
                "sink1",
                new Reference("DelimitedText2", new ArrayList<>(), ReferenceType.DATASET),
                null, null));

        List<String> transformations = new ArrayList<>();
        transformations.add("DerivedColumn1");

        MappingDataFlowProperties expectedMappingProperties = new MappingDataFlowProperties(
                script, sources, sinks, transformations
        );

        MappingDataFlowProperties mappingProperties = (MappingDataFlowProperties) properties;

        // TODO compare script
//        assertEquals(expectedMappingProperties.getScript(), mappingProperties.getScript());

        // Compare transformations
        assertEquals(expectedMappingProperties.getTransformationNames().size(), mappingProperties.getTransformationNames().size());
        assertEquals(expectedMappingProperties.getTransformationNames().stream().findFirst().get(), mappingProperties.getTransformationNames().stream().findFirst().get());

        // Compare sources
        assertEquals(expectedMappingProperties.getSources().size(), mappingProperties.getSources().size());
        cmpSources(expectedMappingProperties.getSources().values().stream().findFirst().get(), mappingProperties.getSources().values().stream().findFirst().get());

        // Compare sinks
        assertEquals(expectedMappingProperties.getSinks().size(), mappingProperties.getSinks().size());
        cmpSinks(expectedMappingProperties.getSinks().values().stream().findFirst().get(), mappingProperties.getSinks().values().stream().findFirst().get());

    }

    @Test
    public void emptyDataFlowName(){
        JsonNode dataFlowRootNode = readFile("emptyNameDataFlow.json");

        IDataFlow dataFlow = dataFlowParser.parseDataFlow(dataFlowRootNode);

        assertEquals(DataFlowParserImpl.UNKNOWN_DATA_FLOW_NAME, dataFlow.getName());
    }

    @Test
    public void emptyPropertiesDataFlow(){
        JsonNode dataFlowRootNode = readFile("emptyPropertiesDataFlow.json");

        IDataFlow dataFlow = dataFlowParser.parseDataFlow(dataFlowRootNode);

        assertNull(dataFlow);
    }

    @Test
    public void unknownPropertyTypeDataFlow(){
        JsonNode dataFlowRootNode = readFile("unknownPropertyTypeDataFlow.json");

        IDataFlow dataFlow = dataFlowParser.parseDataFlow(dataFlowRootNode);

        assertNotNull(dataFlow);
        assertTrue(dataFlow.getProperties() instanceof MappingDataFlowProperties);
    }

    @Test
    public void badProperties(){
        JsonNode dataFlowRootNode = readFile("badPropertiesDataFlow.json");

        IDataFlow dataFlow = dataFlowParser.parseDataFlow(dataFlowRootNode);

        assertNull(dataFlow);
    }

    @Override
    protected File getTestInputDirectory() {
        return TEST_INPUT_DIRECTORY;
    }

}