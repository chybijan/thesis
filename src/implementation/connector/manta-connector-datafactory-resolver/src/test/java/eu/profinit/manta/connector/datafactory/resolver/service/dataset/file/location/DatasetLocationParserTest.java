package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file.location;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.LocationType;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.AmazonS3Location;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.AzureBlobFSLocation;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.AzureBlobStorageLocation;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.AzureDataLakeStoreLocation;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.AzureFileStorageLocation;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.DatasetLocation;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.FileServerLocation;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.FtpServerLocation;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.GoogleCloudStorageLocation;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.HdfsLocation;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.HttpServerLocation;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.SftpLocation;
import eu.profinit.manta.connector.datafactory.resolver.service.AdfResourceParserTestBase;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DatasetLocationParserTest extends AdfResourceParserTestBase {

    /**
     * Directory with test inout files.
     */
    private static final File TEST_INPUT_DIRECTORY = FileUtils.getFile(TEST_FILES_ROOT, "dataset", "file", "location");

    /**
     * Spring context.
     */
    private static ClassPathXmlApplicationContext springContext;

    /**
     * Parser for Dataset Location resources.
     */
    private static Map<LocationType, DatasetLocationParser<? extends DatasetLocation>> locationParserMap;

    @BeforeClass
    @SuppressWarnings("unchecked")
    public static void setUpClass() {
        // Init Spring context, get map of Dataset Location resource parsers
        springContext = new ClassPathXmlApplicationContext(SPRING_CONFIG);
        locationParserMap = (Map<LocationType, DatasetLocationParser<? extends DatasetLocation>>)
                springContext.getBean("locationParserMap");
    }

    @AfterClass
    public static void tearDown() {
        if (springContext != null) {
            springContext.close();
        }
    }

    private LocationType parseLocationType(JsonNode locationNode) {
        String locationTypeString = JsonUtil.pathAsText(locationNode, "type");
        return LocationType.valueOfCI(locationTypeString);
    }

    @Test
    public void parseAmazonS3Location() {
        JsonNode locationNode = readFile("AmazonS3Location.json");
        DatasetLocation datasetLocation = locationParserMap.get(parseLocationType(locationNode)).parseDatasetLocation(locationNode);

        assertTrue(datasetLocation instanceof AmazonS3Location);
        assertEquals(LocationType.AMAZON_S3_LOCATION, datasetLocation.getLocationType());
    }

    @Test
    public void parseAzureBlobFSLocation() {
        JsonNode locationNode = readFile("AzureBlobFSLocation.json");
        DatasetLocation datasetLocation = locationParserMap.get(parseLocationType(locationNode)).parseDatasetLocation(locationNode);

        assertTrue(datasetLocation instanceof AzureBlobFSLocation);
        assertEquals(LocationType.AZURE_BLOB_FS_LOCATION, datasetLocation.getLocationType());
    }

    @Test
    public void parseAzureBlobStorageLocation() {
        JsonNode locationNode = readFile("AzureBlobStorageLocation.json");
        DatasetLocation datasetLocation = locationParserMap.get(parseLocationType(locationNode)).parseDatasetLocation(locationNode);

        assertTrue(datasetLocation instanceof AzureBlobStorageLocation);
        assertEquals(LocationType.AZURE_BLOB_STORAGE_LOCATION, datasetLocation.getLocationType());
    }

    @Test
    public void parseAzureDataLakeStoreLocation() {
        JsonNode locationNode = readFile("AzureDataLakeStoreLocation.json");
        DatasetLocation datasetLocation = locationParserMap.get(parseLocationType(locationNode)).parseDatasetLocation(locationNode);

        assertTrue(datasetLocation instanceof AzureDataLakeStoreLocation);
        assertEquals(LocationType.AZURE_DATA_LAKE_STORE_LOCATION, datasetLocation.getLocationType());
    }

    @Test
    public void parseAzureFileStorageLocation() {
        JsonNode locationNode = readFile("AzureFileStorageLocation.json");
        DatasetLocation datasetLocation = locationParserMap.get(parseLocationType(locationNode)).parseDatasetLocation(locationNode);

        assertTrue(datasetLocation instanceof AzureFileStorageLocation);
        assertEquals(LocationType.AZURE_FILE_STORAGE_LOCATION, datasetLocation.getLocationType());
    }

    @Test
    public void parseFileServerLocation() {
        JsonNode locationNode = readFile("FileServerLocation.json");
        DatasetLocation datasetLocation = locationParserMap.get(parseLocationType(locationNode)).parseDatasetLocation(locationNode);

        assertTrue(datasetLocation instanceof FileServerLocation);
        assertEquals(LocationType.FILE_SERVER_LOCATION, datasetLocation.getLocationType());
    }

    @Test
    public void parseFtpServerLocation() {
        JsonNode locationNode = readFile("FtpServerLocation.json");
        DatasetLocation datasetLocation = locationParserMap.get(parseLocationType(locationNode)).parseDatasetLocation(locationNode);

        assertTrue(datasetLocation instanceof FtpServerLocation);
        assertEquals(LocationType.FTP_SERVER_LOCATION, datasetLocation.getLocationType());
    }

    @Test
    public void parseGoogleCloudStorageLocation() {
        JsonNode locationNode = readFile("GoogleCloudStorageLocation.json");
        DatasetLocation datasetLocation = locationParserMap.get(parseLocationType(locationNode)).parseDatasetLocation(locationNode);

        assertTrue(datasetLocation instanceof GoogleCloudStorageLocation);
        assertEquals(LocationType.GOOGLE_CLOUD_STORAGE_LOCATION, datasetLocation.getLocationType());
    }

    @Test
    public void parseHdfsLocation() {
        JsonNode locationNode = readFile("HdfsLocation.json");
        DatasetLocation datasetLocation = locationParserMap.get(parseLocationType(locationNode)).parseDatasetLocation(locationNode);

        assertTrue(datasetLocation instanceof HdfsLocation);
        assertEquals(LocationType.HDFS_LOCATION, datasetLocation.getLocationType());
    }

    @Test
    public void parseHttpServerLocation() {
        JsonNode locationNode = readFile("HttpServerLocation.json");
        DatasetLocation datasetLocation = locationParserMap.get(parseLocationType(locationNode)).parseDatasetLocation(locationNode);

        assertTrue(datasetLocation instanceof HttpServerLocation);
        assertEquals(LocationType.HTTP_SERVER_LOCATION, datasetLocation.getLocationType());
        HttpServerLocation httpServerLocation = (HttpServerLocation) datasetLocation;

        assertNotNull(httpServerLocation.getRelativeUrl());
    }

    @Test
    public void parseSftpLocation() {
        JsonNode locationNode = readFile("SftpLocation.json");
        DatasetLocation datasetLocation = locationParserMap.get(parseLocationType(locationNode)).parseDatasetLocation(locationNode);

        assertTrue(datasetLocation instanceof SftpLocation);
        assertEquals(LocationType.SFTP_LOCATION, datasetLocation.getLocationType());
    }


    @Override
    protected File getTestInputDirectory() {
        return TEST_INPUT_DIRECTORY;
    }
}