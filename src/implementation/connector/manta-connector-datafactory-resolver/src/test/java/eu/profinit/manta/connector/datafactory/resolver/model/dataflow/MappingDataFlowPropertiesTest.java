package eu.profinit.manta.connector.datafactory.resolver.model.dataflow;

import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.ReferenceType;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowEndpoint;
import eu.profinit.manta.connector.datafactory.resolver.model.common.Reference;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.mapping.MappingDataFlowProperties;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class MappingDataFlowPropertiesTest {

    @Test
    public void getAllReferencesEmpty() {

        Map<String, IDataFlowEndpoint> sources = new HashMap<>();

        Map<String, IDataFlowEndpoint> sinks = new HashMap<>();

        MappingDataFlowProperties properties = new MappingDataFlowProperties(
            new DataFlowScript(Collections.emptyList(), Collections.emptyMap()),
                sources,
                sinks,
                new ArrayList<>()
        );

        Collection<IReference> allReferences = properties.getAllReferences();

        Assert.assertEquals(0, allReferences.size());

    }

    @Test
    public void getAllReferences() {

        Map<String, IDataFlowEndpoint> sources = new HashMap<>();

        IDataFlowEndpoint end1 = new DataFlowEndpoint(
                "end1",
                new Reference("ref1", null, ReferenceType.DATASET),
                new Reference("ref2", null, ReferenceType.LINKED_SERVICE),
                new Reference("ref3", null, ReferenceType.LINKED_SERVICE));

        IDataFlowEndpoint end2 = new DataFlowEndpoint(
                "end2",
                new Reference("ref1", null, ReferenceType.DATASET),
                new Reference("ref4", null, ReferenceType.LINKED_SERVICE),
                null);

        sources.put("end1", end1);
        sources.put("end2", end2);

        Map<String, IDataFlowEndpoint> sinks = new HashMap<>();

        IDataFlowEndpoint end3 = new DataFlowEndpoint(
                "end3",
                null,
                new Reference("ref5", null, ReferenceType.LINKED_SERVICE),
                new Reference("ref1", null, ReferenceType.LINKED_SERVICE)
                );

        sinks.put("end3", end3);

        MappingDataFlowProperties properties = new MappingDataFlowProperties(
                new DataFlowScript(Collections.emptyList(), Collections.emptyMap()),
                sources,
                sinks,
                new ArrayList<>()
        );

        Set<IReference> allReferences = new HashSet<>(properties.getAllReferences());

        Assert.assertEquals(2, allReferences.size());

        Set<IReference> shouldContain = new HashSet<>();
        shouldContain.add(new Reference("ref1", null, ReferenceType.DATASET));
        shouldContain.add(new Reference("ref5", null, ReferenceType.LINKED_SERVICE));
//        shouldContain.add(new Reference("ref2", null, ReferenceType.LINKED_SERVICE));
//        shouldContain.add(new Reference("ref3", null, ReferenceType.LINKED_SERVICE));
//        shouldContain.add(new Reference("ref4", null, ReferenceType.LINKED_SERVICE));
//        shouldContain.add(new Reference("ref5", null, ReferenceType.LINKED_SERVICE));

        Assert.assertTrue(allReferences.containsAll(shouldContain));

    }

}
