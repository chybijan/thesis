// $ANTLR 3.5-rc-2 DFSNonReserveredKW.g 2022-04-24 18:44:27

    package eu.profinit.manta.connector.datafactory.resolver.dfsparser.parser;

    import eu.profinit.manta.platform.logging.api.logging.Logger;

    import eu.profinit.manta.ast.parser.base.MantaAbstractParser;
    import eu.profinit.manta.ast.token.impl.SqlMantaToken;

    import eu.profinit.manta.connector.datafactory.resolver.dfsparser.AbstractDFSParser;
    import eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast.*;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.function.Function;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class DFSParser_DFSNonReserveredKW extends AbstractDFSParser {
	public static final int EOF=-1;
	public static final int AMPERSAND=4;
	public static final int ANY_CHAR=5;
	public static final int APOSTROPHE=6;
	public static final int ARRAY_ANY=7;
	public static final int ARRAY_BOOLEAN=8;
	public static final int ARRAY_DATE=9;
	public static final int ARRAY_DECIMAL=10;
	public static final int ARRAY_DOUBLE=11;
	public static final int ARRAY_FLOAT=12;
	public static final int ARRAY_INTEGER=13;
	public static final int ARRAY_LONG=14;
	public static final int ARRAY_SHORT=15;
	public static final int ARRAY_STRING=16;
	public static final int ARRAY_TIMESTAMP=17;
	public static final int ASTERISK=18;
	public static final int AST_ADDITION_EXPRESSION=19;
	public static final int AST_ADDITION_OPERATOR=20;
	public static final int AST_AND_EXPRESSION=21;
	public static final int AST_ARRAY_ELEMENT=22;
	public static final int AST_ARRAY_LITERAL=23;
	public static final int AST_ATTRIBUTE_NAME=24;
	public static final int AST_COLUMN_DEFINITION=25;
	public static final int AST_COLUMN_DEFINITIONS=26;
	public static final int AST_COLUMN_NAME=27;
	public static final int AST_COLUMN_REFERENCE=28;
	public static final int AST_COMPARE_EXPRESSION=29;
	public static final int AST_COMPARE_OPERATOR=30;
	public static final int AST_CONCRETE_TRANSFORMATION=31;
	public static final int AST_DATA_TYPE=32;
	public static final int AST_DATA_TYPE_FORMAT=33;
	public static final int AST_DFS=34;
	public static final int AST_ERROR=35;
	public static final int AST_EXPRESSION=36;
	public static final int AST_EXPRESSION_FUNCTION=37;
	public static final int AST_EXPRESSION_LITERAL=38;
	public static final int AST_FLOATING_POINT_LITERAL=39;
	public static final int AST_INPUT_STREAMS=40;
	public static final int AST_INTEGER_LITERAL=41;
	public static final int AST_LEFT_OPERAND=42;
	public static final int AST_MAP_DATA_TYPE=43;
	public static final int AST_MAP_KEY_TYPE=44;
	public static final int AST_MAP_VALUE_TYPE=45;
	public static final int AST_MULT_EXPRESSION=46;
	public static final int AST_MULT_OPERATOR=47;
	public static final int AST_NOT_EXPRESSION=48;
	public static final int AST_OPERAND=49;
	public static final int AST_OR_EXPRESSION=50;
	public static final int AST_OUTPUT_ATTRIBUTE=51;
	public static final int AST_PARAMETER=52;
	public static final int AST_PARAMETERS=53;
	public static final int AST_PARAMETER_DEFAULT_VALUE=54;
	public static final int AST_PARAMETER_INVOCATION=55;
	public static final int AST_PARAMETER_NAME=56;
	public static final int AST_POW_EXPRESSION=57;
	public static final int AST_RIGHT_OPERAND=58;
	public static final int AST_STREAM_NAME=59;
	public static final int AST_STRING_LITERAL=60;
	public static final int AST_SUBSTREAM_NAME=61;
	public static final int AST_TRANSFORMATION=62;
	public static final int AST_TRANSFORMATIONS=63;
	public static final int AST_TRANSFORMATION_ATTRIBUTE=64;
	public static final int AST_TRANSFORMATION_ATTRIBUTES=65;
	public static final int AST_TRANSFORMATION_NAME=66;
	public static final int AST_UNARY_EXPRESSION=67;
	public static final int AST_UNARY_OPERATOR=68;
	public static final int AT_SIGN=69;
	public static final int BACK_SLASH=70;
	public static final int BAR=71;
	public static final int BLANK=72;
	public static final int CARET=73;
	public static final int COLON=74;
	public static final int COMMA=75;
	public static final int CR=76;
	public static final int CROSSHATCH=77;
	public static final int DECIMAL_TYPE=78;
	public static final int DIGIT=79;
	public static final int DOLLAR_SIGN=80;
	public static final int EQUALS=81;
	public static final int EXCLAMATION_MARK=82;
	public static final int FLOATING_POINT_LITERAL=83;
	public static final int GREATER_THAN=84;
	public static final int INTEGER_LITERAL=85;
	public static final int KW_ANY=86;
	public static final int KW_AS=87;
	public static final int KW_BOOLEAN=88;
	public static final int KW_DATE=89;
	public static final int KW_DECIMAL=90;
	public static final int KW_DOUBLE=91;
	public static final int KW_FALSE=92;
	public static final int KW_FILTER=93;
	public static final int KW_FLOAT=94;
	public static final int KW_INTEGER=95;
	public static final int KW_JOIN=96;
	public static final int KW_LONG=97;
	public static final int KW_OUTPUT=98;
	public static final int KW_PARAMETERS=99;
	public static final int KW_SHORT=100;
	public static final int KW_SINK=101;
	public static final int KW_SOURCE=102;
	public static final int KW_STRING=103;
	public static final int KW_TIMESTAMP=104;
	public static final int KW_TRUE=105;
	public static final int LEFT_BRACE=106;
	public static final int LEFT_BRACKET=107;
	public static final int LEFT_PARENT=108;
	public static final int LESS_THAN=109;
	public static final int LETTER=110;
	public static final int LF=111;
	public static final int MINUS_SIGN=112;
	public static final int NEWLINE=113;
	public static final int OUTPUT_STREAM=114;
	public static final int PERCENT_SIGN=115;
	public static final int PERIOD=116;
	public static final int PLUS_SIGN=117;
	public static final int QUESTION_MARK=118;
	public static final int QUOTATION_MARK=119;
	public static final int RIGHT_BRACE=120;
	public static final int RIGHT_BRACKET=121;
	public static final int RIGHT_PARENT=122;
	public static final int SEMICOLON=123;
	public static final int SIMPLE_ID=124;
	public static final int SLASH=125;
	public static final int STRING_LITERAL=126;
	public static final int TAB=127;
	public static final int UNDERSCORE=128;
	public static final int UNICODE_BOM=129;
	public static final int WHITESPACE=130;

	// delegates
	public AbstractDFSParser[] getDelegates() {
		return new AbstractDFSParser[] {};
	}

	// delegators
	public DFSParser gDFSParser;
	public DFSParser gParent;


	public DFSParser_DFSNonReserveredKW(TokenStream input, DFSParser gDFSParser) {
		this(input, new RecognizerSharedState(), gDFSParser);
	}
	public DFSParser_DFSNonReserveredKW(TokenStream input, RecognizerSharedState state, DFSParser gDFSParser) {
		super(input, state);
		this.gDFSParser = gDFSParser;
                    
		gParent = gDFSParser;
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return DFSParser.tokenNames; }
	@Override public String getGrammarFileName() { return "DFSNonReserveredKW.g"; }




	public static class identifier_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "identifier"
	// DFSNonReserveredKW.g:27:1: identifier : ( SIMPLE_ID | non_reserved_words );
	public final DFSParser_DFSNonReserveredKW.identifier_return identifier() throws RecognitionException {
		DFSParser_DFSNonReserveredKW.identifier_return retval = new DFSParser_DFSNonReserveredKW.identifier_return();
		retval.start = input.LT(1);
		int identifier_StartIndex = input.index();

		Object root_0 = null;

		Token SIMPLE_ID1=null;
		ParserRuleReturnScope non_reserved_words2 =null;

		Object SIMPLE_ID1_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return retval; }

			// DFSNonReserveredKW.g:28:2: ( SIMPLE_ID | non_reserved_words )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==SIMPLE_ID) ) {
				alt1=1;
			}
			else if ( ((LA1_0 >= KW_ANY && LA1_0 <= KW_TRUE)) ) {
				alt1=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// DFSNonReserveredKW.g:28:4: SIMPLE_ID
					{
					root_0 = (Object)adaptor.nil();


					SIMPLE_ID1=(Token)match(input,SIMPLE_ID,FOLLOW_SIMPLE_ID_in_identifier59); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					SIMPLE_ID1_tree = (Object)adaptor.create(SIMPLE_ID1);
					adaptor.addChild(root_0, SIMPLE_ID1_tree);
					}

					}
					break;
				case 2 :
					// DFSNonReserveredKW.g:29:4: non_reserved_words
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_non_reserved_words_in_identifier64);
					non_reserved_words2=non_reserved_words();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, non_reserved_words2.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 1, identifier_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "identifier"


	public static class all_identifier_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "all_identifier"
	// DFSNonReserveredKW.g:32:1: all_identifier : ( identifier | reserved_words );
	public final DFSParser_DFSNonReserveredKW.all_identifier_return all_identifier() throws RecognitionException {
		DFSParser_DFSNonReserveredKW.all_identifier_return retval = new DFSParser_DFSNonReserveredKW.all_identifier_return();
		retval.start = input.LT(1);
		int all_identifier_StartIndex = input.index();

		Object root_0 = null;

		ParserRuleReturnScope identifier3 =null;
		ParserRuleReturnScope reserved_words4 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return retval; }

			// DFSNonReserveredKW.g:33:2: ( identifier | reserved_words )
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( ((LA2_0 >= KW_ANY && LA2_0 <= KW_TRUE)||LA2_0==SIMPLE_ID) ) {
				alt2=1;
			}
			else if ( (LA2_0==EOF) ) {
				alt2=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}

			switch (alt2) {
				case 1 :
					// DFSNonReserveredKW.g:33:4: identifier
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_identifier_in_all_identifier75);
					identifier3=identifier();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, identifier3.getTree());

					}
					break;
				case 2 :
					// DFSNonReserveredKW.g:34:4: reserved_words
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_reserved_words_in_all_identifier80);
					reserved_words4=reserved_words();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, reserved_words4.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 2, all_identifier_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "all_identifier"


	public static class non_reserved_words_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "non_reserved_words"
	// DFSNonReserveredKW.g:37:1: non_reserved_words : ( KW_PARAMETERS | KW_SOURCE | KW_AS | KW_INTEGER | KW_BOOLEAN | KW_DATE | KW_TIMESTAMP | KW_ANY | KW_SHORT | KW_DOUBLE | KW_FLOAT | KW_LONG | KW_DECIMAL | KW_STRING | KW_SOURCE | KW_OUTPUT | KW_SINK | KW_STRING | KW_FILTER | KW_TRUE | KW_FALSE | KW_JOIN );
	public final DFSParser_DFSNonReserveredKW.non_reserved_words_return non_reserved_words() throws RecognitionException {
		DFSParser_DFSNonReserveredKW.non_reserved_words_return retval = new DFSParser_DFSNonReserveredKW.non_reserved_words_return();
		retval.start = input.LT(1);
		int non_reserved_words_StartIndex = input.index();

		Object root_0 = null;

		Token set5=null;

		Object set5_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return retval; }

			// DFSNonReserveredKW.g:38:2: ( KW_PARAMETERS | KW_SOURCE | KW_AS | KW_INTEGER | KW_BOOLEAN | KW_DATE | KW_TIMESTAMP | KW_ANY | KW_SHORT | KW_DOUBLE | KW_FLOAT | KW_LONG | KW_DECIMAL | KW_STRING | KW_SOURCE | KW_OUTPUT | KW_SINK | KW_STRING | KW_FILTER | KW_TRUE | KW_FALSE | KW_JOIN )
			// DFSNonReserveredKW.g:
			{
			root_0 = (Object)adaptor.nil();


			set5=input.LT(1);
			if ( (input.LA(1) >= KW_ANY && input.LA(1) <= KW_TRUE) ) {
				input.consume();
				if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set5));
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 3, non_reserved_words_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "non_reserved_words"


	public static class reserved_words_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "reserved_words"
	// DFSNonReserveredKW.g:62:1: reserved_words :;
	public final DFSParser_DFSNonReserveredKW.reserved_words_return reserved_words() throws RecognitionException {
		DFSParser_DFSNonReserveredKW.reserved_words_return retval = new DFSParser_DFSNonReserveredKW.reserved_words_return();
		retval.start = input.LT(1);
		int reserved_words_StartIndex = input.index();

		Object root_0 = null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return retval; }

			// DFSNonReserveredKW.g:63:2: ()
			// DFSNonReserveredKW.g:64:2: 
			{
			root_0 = (Object)adaptor.nil();


			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 4, reserved_words_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "reserved_words"

	// Delegated rules



	/* PJAR: rozdeleni deklarace a inicializace statickych promennych. Presunuti inicializace do samostatne metody */
	public static /* final */ BitSet FOLLOW_SIMPLE_ID_in_identifier59;
	public static /* final */ BitSet FOLLOW_non_reserved_words_in_identifier64;
	public static /* final */ BitSet FOLLOW_identifier_in_all_identifier75;
	public static /* final */ BitSet FOLLOW_reserved_words_in_all_identifier80;

	/** PJAR: Inicializace statickych promennych v samostatne metode kvuli "code too large" chybe (generovany kod je prilis velky pro statickou inicializaci). */ 
	static void initializeFollow1() {
	FOLLOW_SIMPLE_ID_in_identifier59 = new BitSet(new long[]{0x0000000000000002L});



	}

	static void initializeFollow2() {
	FOLLOW_non_reserved_words_in_identifier64 = new BitSet(new long[]{0x0000000000000002L});


	}

	static void initializeFollow3() {
	FOLLOW_identifier_in_all_identifier75 = new BitSet(new long[]{0x0000000000000002L});

	}

	static void initializeFollow4() {
	FOLLOW_reserved_words_in_all_identifier80 = new BitSet(new long[]{0x0000000000000002L});
	}

	/* PJAR Inicializace statickych metod rozdelena do 2 metod kvuli "code too large" chybe. */
	static {
	   initializeFollow1();	
	   initializeFollow2();
	   initializeFollow3();
	   initializeFollow4();		
	}
}
