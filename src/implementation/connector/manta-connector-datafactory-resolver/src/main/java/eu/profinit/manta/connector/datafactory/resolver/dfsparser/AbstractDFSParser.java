package eu.profinit.manta.connector.datafactory.resolver.dfsparser;

import eu.profinit.manta.ast.parser.base.MantaAbstractParser;
import org.antlr.runtime.*;

public abstract class AbstractDFSParser extends MantaAbstractParser {

    protected DFSContextState contextState;

    protected AbstractDFSParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

    public void setContextState(DFSContextState contextState) {
        this.contextState = contextState;
    }

}
