package eu.profinit.manta.connector.datafactory.resolver.model;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;

/**
 * Implementation of the LinkedService resource interface.
 */
public abstract class LinkedService implements ILinkedService {

    /**
     * Linked service name.
     */
    private final String name;

    /**
     * Linked service description.
     */
    private final String description;

    /**
     * IParameters for linked service.
     */
    private final IParameters parameters;

    protected LinkedService(String name, String description, IParameters parameters) {
        this.name = name;
        this.description = description;
        this.parameters = parameters;
    }

    public abstract LinkedServiceType getType();

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public IParameters getParameters() {
        return parameters;
    }

}
