package eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.IFileDatasetLocation;

/**
 * Implementation of interface for all file-like DatasetLocation resources.
 */
public abstract class FileDatasetLocation extends DatasetLocation implements IFileDatasetLocation {

    /**
     * The path to the folder.
     */
    private final IAdfFieldValue folderPath;

    /**
     * The file name under the specified folderPath.
     */
    private final IAdfFieldValue fileName;

    /**
     * @param folderPath the path to the folder
     * @param fileName   the file name under the specified folderPath
     */
    protected FileDatasetLocation(IAdfFieldValue folderPath, IAdfFieldValue fileName) {
        this.folderPath = folderPath;
        this.fileName = fileName;
    }

    @Override
    public IAdfFieldValue getFolderPath() {
        return folderPath;
    }

    @Override
    public IAdfFieldValue getFileName() {
        return fileName;
    }
}
