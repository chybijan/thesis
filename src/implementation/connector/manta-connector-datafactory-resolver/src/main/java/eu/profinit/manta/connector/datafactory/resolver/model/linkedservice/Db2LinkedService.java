package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.IDb2LinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;

/**
 * Implementation of the Db2LinkedService resource interface.
 */
public class Db2LinkedService extends LinkedService implements IDb2LinkedService {

    /**
     * The connection string.
     * It is mutually exclusive with server, database, authenticationType, userName, packageCollection and certificateCommonName property.
     */
    private final IAdfFieldValue connectionString;

    /**
     * Database name for connection. It is mutually exclusive with connectionString property.
     */
    private final IAdfFieldValue database;

    /**
     * Under where packages are created when querying database. It is mutually exclusive with connectionString property.
     */
    private final IAdfFieldValue packageCollection;

    /**
     * Server name for connection. It is mutually exclusive with connectionString property.
     */
    private final IAdfFieldValue server;

    /**
     * Username for authentication. It is mutually exclusive with connectionString property.
     */
    private final IAdfFieldValue username;

    public Db2LinkedService(String name,
                            String description,
                            IParameters parameters,
                            IAdfFieldValue connectionString,
                            IAdfFieldValue database,
                            IAdfFieldValue packageCollection,
                            IAdfFieldValue server,
                            IAdfFieldValue username) {
        super(name, description, parameters);
        this.connectionString = connectionString;
        this.database = database;
        this.packageCollection = packageCollection;
        this.server = server;
        this.username = username;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.DB2;
    }

    @Override
    public IAdfFieldValue getConnectionString() {
        return connectionString;
    }

    @Override
    public IAdfFieldValue getDatabase() {
        return database;
    }

    @Override
    public IAdfFieldValue getPackageCollection() {
        return packageCollection;
    }

    @Override
    public IAdfFieldValue getServer() {
        return server;
    }

    @Override
    public IAdfFieldValue getUsername() {
        return username;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitDb2LinkedService(this);
    }
}
