package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.SapHanaLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for SapHanaLinkedService resource.
 */
public class SapHanaLinkedServiceParser extends AbstractLinkedServiceParser<SapHanaLinkedService> {

    @Override
    protected SapHanaLinkedService parseTypeSpecificFields(String name,
                                                           String description,
                                                           String type,
                                                           IParameters parameters,
                                                           JsonNode typePropertiesNode) {
        IAdfFieldValue connectionString = null;
        IAdfFieldValue server = null;
        IAdfFieldValue userName = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            connectionString = processAdfFieldValue(typePropertiesNode.path("connectionString"));
            server = processAdfFieldValue(typePropertiesNode.path("server"));
            userName = processAdfFieldValue(typePropertiesNode.path("userName"));
        }

        return new SapHanaLinkedService(name, description, parameters, connectionString, server, userName);
    }
}
