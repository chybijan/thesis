package eu.profinit.manta.connector.datafactory.resolver.service.dataflow;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.ReferenceType;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowEndpoint;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.DataFlowEndpoint;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;
import eu.profinit.manta.connector.datafactory.resolver.service.common.ReferenceParser;
import eu.profinit.manta.platform.logging.api.logging.Logger;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractDataFlowPropertiesParser implements DataFlowPropertiesParser {

    private static final Logger LOGGER = new Logger(AbstractDataFlowPropertiesParser.class);

    protected static final String NAME_PATH = "name";

    protected static final String DATASET_PATH = "/dataset";
    protected static final String LINKED_SERVICE_PATH = "/linkedService";
    protected static final String SCHEMA_LINKED_SERVICE_PATH = "/schemaLinkedService";

    protected static final String UNKNOWN_ENDPOINT_NAME = "UNKNOWN ENDPOINT";

    protected final ReferenceParser referenceParser;
    protected final DFSParserService parserService;

    protected AbstractDataFlowPropertiesParser(ReferenceParser referenceParser, DFSParserService parserService) {
        this.referenceParser = referenceParser;
        this.parserService = parserService;
    }

    /**
     * Finds endpoints in endpoints array node and returns collection of endpoints
     *
     * @param endpointsNode endpoints array node containing endpoints
     * @return Collection of data flow endpoints,
     *         or empty Collection if endpointsNode is null or missing
     */
    protected Map<String, IDataFlowEndpoint> parseEndpoints(JsonNode endpointsNode){

        Map<String, IDataFlowEndpoint> endpoints = new HashMap<>();

        // Node exists and is array
        if (JsonUtil.isNotNullAndNotMissing(endpointsNode) && endpointsNode.isArray()) {

            endpointsNode.elements().forEachRemaining(endpointNodeNode -> {
                IDataFlowEndpoint endpoint = parseEndpoint(endpointNodeNode);
                if(endpoint != null)
                    endpoints.put(endpoint.getName(), endpoint);
            });

        }

        return endpoints;
    }

    /**
     * Parse json node to endpoint
     *
     * @param endpointNode endpoint node
     * @return data flow endpoint,
     *         or null if endpointNode is null, missing or it is not parsed correctly
     */
    protected IDataFlowEndpoint parseEndpoint(JsonNode endpointNode){

        IDataFlowEndpoint endpoint = null;

        if(JsonUtil.isNotNullAndNotMissing(endpointNode)){

            // Load endpoint name
            String name = JsonUtil.pathAsText(endpointNode, NAME_PATH);
            if(StringUtils.isBlank(name)){
                LOGGER.log(Categories.parsingErrors().missingEndpointName().endpointNode(endpointNode));
                name = UNKNOWN_ENDPOINT_NAME;
            }

            // Load dataset reference
            IReference datasetReference = referenceParser.parseReference(endpointNode.at(DATASET_PATH), ReferenceType.DATASET);

            if(datasetReference == null){
                LOGGER.log(Categories.parsingErrors().missingDatasetReferenceOfEndpoint().endpointName(name));
            }

            // TODO some day add flowlet

            // Load linked service reference
            IReference linkedServiceReference = referenceParser.parseReference(endpointNode.at(LINKED_SERVICE_PATH), ReferenceType.LINKED_SERVICE);
            if(linkedServiceReference == null){
                LOGGER.log(Categories.parsingErrors().missingLinkedServiceReferenceOfEndpoint().endpointName(name));
            }

            //Load schema linked service reference
            IReference schemaLinkedServiceReference = referenceParser.parseReference(endpointNode.at(SCHEMA_LINKED_SERVICE_PATH), ReferenceType.LINKED_SERVICE);
            if(schemaLinkedServiceReference == null){
                LOGGER.log(Categories.parsingErrors().missingSchemaLinkedServiceReferenceOfEndpoint().endpointName(name));
            }

            endpoint = new DataFlowEndpoint(name, datasetReference, linkedServiceReference, schemaLinkedServiceReference);

        }

        return endpoint;
    }
}
