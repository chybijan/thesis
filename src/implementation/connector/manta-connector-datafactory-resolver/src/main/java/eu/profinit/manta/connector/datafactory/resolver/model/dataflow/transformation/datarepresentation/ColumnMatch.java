package eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.datarepresentation;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumn;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumnMatch;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.visitor.IColumnVisitor;

import java.util.Collection;

public class ColumnMatch implements IColumnMatch {

    final IExpression matchOn;

    final Collection<IColumn> columns;

    public ColumnMatch(IExpression matchOn, Collection<IColumn> columns) {
        this.matchOn = matchOn;
        this.columns = columns;
    }

    @Override
    public <T> T accept(IColumnVisitor<T> visitor) {
        return visitor.visitColumnMatch(this);
    }

    @Override
    public IExpression getMatchOn() {
        return matchOn;
    }

    @Override
    public Collection<IColumn> getColumns() {
        return columns;
    }
}
