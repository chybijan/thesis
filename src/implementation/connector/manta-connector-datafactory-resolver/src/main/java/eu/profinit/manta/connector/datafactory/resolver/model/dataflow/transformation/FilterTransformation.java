package eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IFilterTransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ISourceTransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.ITable;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class FilterTransformation extends AbstractTransformation implements IFilterTransformation {

    private final IExpression filterCondition;

    public FilterTransformation(String name, Set<String> substreams,
                                Collection<IADFStream> inputs, Collection<String> outputs, ITable table,
                                Map<String, IExpression> attributes,
                                IExpression filterCondition) {
        super(name, substreams, inputs, outputs, table, attributes);
        this.filterCondition = filterCondition;
    }

    @Override
    public IExpression getFilterCondition() {
        return filterCondition;
    }
}
