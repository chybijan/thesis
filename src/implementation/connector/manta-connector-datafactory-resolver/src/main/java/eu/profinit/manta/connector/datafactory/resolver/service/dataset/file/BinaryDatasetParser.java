package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.BinaryDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.DatasetLocation;

/**
 * Parser for BinaryDataset resource.
 */
public class BinaryDatasetParser extends FileDatasetParser<BinaryDataset> {

    @Override
    protected BinaryDataset parseFileDatasetSpecificFields(String name,
                                                           IReference lsName,
                                                           IParameters parameters,
                                                           IDatasetSchema datasetSchema,
                                                           JsonNode datasetStructureNode,
                                                           DatasetLocation location,
                                                           JsonNode typePropertiesNode) {
        return new BinaryDataset(name, lsName, parameters, datasetSchema, datasetStructureNode, location);
    }
}
