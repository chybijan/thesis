package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowScript;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstADFStream;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstConcreteTransformation;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstTransformation;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import eu.profinit.manta.platform.logging.api.logging.Logger;
import org.antlr.runtime.Token;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AstTransformation extends DFSNode implements IAstTransformation {


    public AstTransformation() {
    }

    public AstTransformation(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstTransformation(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    @Override
    public List<IAstADFStream> findInputStreams() {
        return selectNodes("AST_INPUT_STREAMS/AST_STREAM_NAME");
    }

    @Override
    public IAstADFStream findTransformationStream() {
        return (IAstADFStream) selectSingleNode("AST_STREAM_NAME");

    }

    @Override
    public IAstConcreteTransformation findConcreteTransformation() {
        return (IAstConcreteTransformation) selectSingleNode("AST_CONCRETE_TRANSFORMATION");
    }

    @Override
    public IDataFlowScript resolve(IDataFlowScript currentDFS) {

        List<IAstADFStream> astInputStreams = findInputStreams();
        Set<IADFStream> inputStreams = new HashSet<>();

        for(IAstADFStream astStream : astInputStreams){
            Set<IADFStream> realStreams = astStream.getADFStreams();
            inputStreams.addAll(realStreams);
        }

        IAstADFStream astADFStream = findTransformationStream();

        Set<IADFStream> thisStreams = astADFStream != null ? astADFStream.getADFStreams() : Collections.emptySet();

        IAstConcreteTransformation astConcreteTransformation = findConcreteTransformation();

        return astConcreteTransformation.resolveConcreteTransformation(inputStreams, thisStreams, currentDFS);
    }
}
