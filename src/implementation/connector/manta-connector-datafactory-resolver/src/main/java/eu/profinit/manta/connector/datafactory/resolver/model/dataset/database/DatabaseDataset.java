package eu.profinit.manta.connector.datafactory.resolver.model.dataset.database;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.model.dataset.database.IDatabaseDataset;
import eu.profinit.manta.connector.datafactory.model.visitor.IDatasetVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.Dataset;

import java.util.Optional;

/**
 * Implementation of the interface for all database-like Dataset resources.
 */
public abstract class DatabaseDataset extends Dataset implements IDatabaseDataset {

    /**
     * The database schema/dataset/database name of the given dataset.
     */
    private final IAdfFieldValue schema;

    /**
     * The table name of the given dataset.
     */
    private final IAdfFieldValue table;

    /**
     * @param name              see {@link Dataset#Dataset(String, IReference, IParameters, IDatasetSchema, JsonNode)}
     * @param linkedServiceName see {@link Dataset#Dataset(String, IReference, IParameters, IDatasetSchema, JsonNode)}
     * @param parameters        see {@link Dataset#Dataset(String, IReference, IParameters, IDatasetSchema, JsonNode)}
     * @param datasetSchema     see {@link Dataset#Dataset(String, IReference, IParameters, IDatasetSchema, JsonNode)}
     * @param datasetStructure  see {@link Dataset#Dataset(String, IReference, IParameters, IDatasetSchema, JsonNode)}
     * @param schema            the database schema/dataset/database name of the given dataset
     * @param table             the table name of the given dataset
     */
    public DatabaseDataset(String name,
                           IReference linkedServiceName,
                           IParameters parameters,
                           IDatasetSchema datasetSchema,
                           JsonNode datasetStructure,
                           IAdfFieldValue schema,
                           IAdfFieldValue table) {
        super(name, linkedServiceName, parameters, datasetSchema, datasetStructure);
        this.schema = schema;
        this.table = table;
    }

    @Override
    public Optional<IAdfFieldValue> getSchema() {
        return schema != null ? Optional.of(schema) : Optional.empty();
    }

    @Override
    public  Optional<IAdfFieldValue> getTable() {
        return table != null ? Optional.of(table) : Optional.empty();
    }

    @Override
    public <T> T accept(IDatasetVisitor<T> visitor) {
        return visitor.visitDatabaseDataset(this);
    }
}
