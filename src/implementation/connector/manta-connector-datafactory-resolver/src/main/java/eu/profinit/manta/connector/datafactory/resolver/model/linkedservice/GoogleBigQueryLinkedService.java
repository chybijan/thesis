package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.IGoogleBigQueryLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;

/**
 * Implementation of the GoogleBigQueryLinkedService resource interface.
 */
public class GoogleBigQueryLinkedService extends LinkedService implements IGoogleBigQueryLinkedService {

    /**
     * The default BigQuery project to query against.
     */
    private final IAdfFieldValue project;

    public GoogleBigQueryLinkedService(String name,
                                       String description,
                                       IParameters parameters,
                                       IAdfFieldValue project) {
        super(name, description, parameters);
        this.project = project;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.GOOGLE_BIGQUERY;
    }

    @Override
    public IAdfFieldValue getProject() {
        return project;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitGoogleBigQueryLinkedService(this);
    }
}
