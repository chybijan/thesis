package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AzureSqlDatabaseLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for AzureSqlDatabaseLinkedService resource.
 */
public class AzureSqlDatabaseLinkedServiceParser extends AbstractLinkedServiceParser<AzureSqlDatabaseLinkedService> {

    @Override
    protected AzureSqlDatabaseLinkedService parseTypeSpecificFields(String name,
                                                                    String description,
                                                                    String type,
                                                                    IParameters parameters,
                                                                    JsonNode typePropertiesNode) {
        IAdfFieldValue connectionString = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            connectionString = processAdfFieldValue(typePropertiesNode.path("connectionString"));
        }

        return new AzureSqlDatabaseLinkedService(name, description, parameters, connectionString);
    }
}
