package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.linkedservice.IAmazonRedshiftLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;

/**
 * Implementation of the AmazonRedshiftLinkedService resource interface.
 */
public class AmazonRedshiftLinkedService extends LinkedService implements IAmazonRedshiftLinkedService {

    private final IAdfFieldValue database;

    private final IAdfFieldValue port;

    private final IAdfFieldValue server;

    private final IAdfFieldValue username;

    public AmazonRedshiftLinkedService(String name,
                                       String description,
                                       IParameters parameters,
                                       IAdfFieldValue database,
                                       IAdfFieldValue port,
                                       IAdfFieldValue server,
                                       IAdfFieldValue username) {
        super(name, description, parameters);
        this.database = database;
        this.port = port;
        this.server = server;
        this.username = username;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.AMAZON_REDSHIFT;
    }

    @Override
    public IAdfFieldValue getDatabase() {
        return database;
    }

    @Override
    public IAdfFieldValue getPort() {
        return port;
    }

    @Override
    public IAdfFieldValue getServer() {
        return server;
    }

    @Override
    public IAdfFieldValue getUsername() {
        return username;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitAmazonRedshiftLinkedService(this);
    }
}
