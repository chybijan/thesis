package eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.ITable;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractTransformation implements ITransformation {

    protected final String name;

    protected final Set<String> substreams;

    protected final Collection<IADFStream> inputs;

    protected Collection<String> outputs;

    protected final Map<String, IExpression> attributes;

    protected ITable table;

    protected AbstractTransformation(String name, Set<String> substreams, Collection<IADFStream> inputs, Collection<String> outputs, ITable table, Map<String, IExpression> attributes) {
        this.name = name;
        this.substreams = substreams;
        this.inputs = inputs;
        this.outputs = outputs;
        this.table = table;
        this.attributes = attributes;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Collection<IADFStream> getStreams() {

        return substreams.stream()
                .map(e -> new ADFStream(name, e))
                .collect(Collectors.toSet());
    }

    @Override
    public Collection<IADFStream> getInputs() {
        return inputs;
    }

    @Override
    public Collection<String> getOutputs() {
        return outputs;
    }

    @Override
    public void addOutput(String newOutput) {
        outputs.add(newOutput);
    }

    @Override
    public ITable getTable() {
        return table;
    }

    @Override
    public void setTable(ITable newTable){
        table = newTable;
    }

    @Override
    public Optional<IExpression> findAttributeByName(String attributeName) {
        IExpression value = attributes.get(attributeName);
        return (value != null) ? Optional.of(value) : Optional.empty();
    }
}
