package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.IAzureSqlDatabaseLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;

/**
 * Implementation of the AzureSqlDatabaseLinkedService resource interface.
 */
public class AzureSqlDatabaseLinkedService extends LinkedService implements IAzureSqlDatabaseLinkedService {

    /**
     * The connection string.
     */
    private final IAdfFieldValue connectionString;

    public AzureSqlDatabaseLinkedService(String name,
                                         String description,
                                         IParameters parameters,
                                         IAdfFieldValue connectionString) {
        super(name, description, parameters);
        this.connectionString = connectionString;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.AZURE_SQL_DATABASE;
    }

    @Override
    public IAdfFieldValue getConnectionString() {
        return connectionString;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitAzureSqlDatabaseLinkedService(this);
    }
}
