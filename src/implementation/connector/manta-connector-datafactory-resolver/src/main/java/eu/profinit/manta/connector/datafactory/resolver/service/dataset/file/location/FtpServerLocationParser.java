package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file.location;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.FtpServerLocation;

public class FtpServerLocationParser extends FileDatasetLocationParser<FtpServerLocation> {

    @Override
    public FtpServerLocation parseFileDatasetSpecificLocation(IAdfFieldValue fileName, IAdfFieldValue folderPath, JsonNode locationNode) {
        return new FtpServerLocation(folderPath, fileName);
    }
}
