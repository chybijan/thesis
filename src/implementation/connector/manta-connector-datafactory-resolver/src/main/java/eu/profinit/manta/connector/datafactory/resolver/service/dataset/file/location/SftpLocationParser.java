package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file.location;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.SftpLocation;

public class SftpLocationParser extends FileDatasetLocationParser<SftpLocation> {

    @Override
    public SftpLocation parseFileDatasetSpecificLocation(IAdfFieldValue fileName, IAdfFieldValue folderPath, JsonNode locationNode) {
        return new SftpLocation(folderPath, fileName);
    }
}
