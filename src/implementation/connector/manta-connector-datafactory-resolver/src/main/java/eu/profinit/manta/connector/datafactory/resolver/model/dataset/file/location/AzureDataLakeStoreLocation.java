package eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.IAzureDataLakeStoreLocation;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.LocationType;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.visitor.ILocationVisitor;

/**
 * Implementation of the DatasetLocation resource of type AzureDataLakeStore.
 */
public class AzureDataLakeStoreLocation extends FileDatasetLocation implements IAzureDataLakeStoreLocation {

    /**
     * @param folderPath see {@link FileDatasetLocation#FileDatasetLocation(IAdfFieldValue, IAdfFieldValue)}
     * @param fileName   see {@link FileDatasetLocation#FileDatasetLocation(IAdfFieldValue, IAdfFieldValue)}
     */
    public AzureDataLakeStoreLocation(IAdfFieldValue folderPath, IAdfFieldValue fileName) {
        super(folderPath, fileName);
    }

    @Override
    public LocationType getLocationType() {
        return LocationType.AZURE_DATA_LAKE_STORE_LOCATION;
    }

    @Override
    public <T> T accept(ILocationVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
