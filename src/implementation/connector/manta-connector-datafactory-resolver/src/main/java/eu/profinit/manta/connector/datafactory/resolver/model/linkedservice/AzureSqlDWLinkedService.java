package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.IAzureSqlDWLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;

/**
 * Implementation of the AzureSqlDWLinkedService resource interface.
 */
public class AzureSqlDWLinkedService extends LinkedService implements IAzureSqlDWLinkedService {

    /**
     * The connection string.
     */
    private final IAdfFieldValue connectionString;

    public AzureSqlDWLinkedService(String name,
                                   String description,
                                   IParameters parameters,
                                   IAdfFieldValue connectionString) {
        super(name, description, parameters);
        this.connectionString = connectionString;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.AZURE_SQL_DW;
    }

    public IAdfFieldValue getConnectionString() {
        return connectionString;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitAzureSqlDWLLinkedService(this);
    }
}
