package eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.datarepresentation;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumnRef;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.visitor.IColumnVisitor;

public class ColumnRef implements IColumnRef {

    private final IExpression name;

    private final IExpression origin;

    public ColumnRef(IExpression name, IExpression origin) {
        this.name = name;
        this.origin = origin;
    }

    @Override
    public <T> T accept(IColumnVisitor<T> visitor) {
        return visitor.visitColumnRef(this);
    }

    @Override
    public IExpression getName() {
        return name;
    }

    @Override
    public IExpression getOrigin() {
        return origin;
    }
}
