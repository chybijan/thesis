package eu.profinit.manta.connector.datafactory.resolver.model;

import eu.profinit.manta.connector.datafactory.model.IFactory;
import eu.profinit.manta.connector.datafactory.model.factory.IGlobalParameterSpecification;
import eu.profinit.manta.connector.datafactory.resolver.model.factory.GlobalParameterSpecification;

import java.util.Map;

/**
 * Implementation of the Factory resource interface.
 */
public class Factory implements IFactory {

    /**
     * Name of the factory.
     */
    private final String name;

    /**
     * Map of global parameters registered within the factory instance.
     */
    private final Map<String, IGlobalParameterSpecification> globalParameters;

    /**
     * @param name             name of the factory
     * @param globalParameters map of global parameters registered within the factory instance
     */
    public Factory(String name, Map<String, IGlobalParameterSpecification> globalParameters) {
        this.name = name;
        this.globalParameters = globalParameters;
    }

    @Override
    public String getName() {
        return name;
    }

    public Map<String, IGlobalParameterSpecification> getGlobalParameters() {
        return globalParameters;
    }
}
