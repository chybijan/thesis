package eu.profinit.manta.connector.datafactory.resolver.model.dataset.file;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.dataset.file.IFileDataset;
import eu.profinit.manta.connector.datafactory.model.visitor.IDatasetVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.Dataset;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.DatasetLocation;

/**
 * Implementation of the interface for all file-like Dataset resources.
 */
public abstract class FileDataset extends Dataset implements IFileDataset {

    /**
     * The location of the file data storage.
     */
    private final DatasetLocation location;

    /**
     * @param name              see {@link Dataset#Dataset(String, IReference, IParameters, IDatasetSchema, JsonNode)}
     * @param linkedServiceName see {@link Dataset#Dataset(String, IReference, IParameters, IDatasetSchema, JsonNode)}
     * @param parameters        see {@link Dataset#Dataset(String, IReference, IParameters, IDatasetSchema, JsonNode)}
     * @param datasetSchema     see {@link Dataset#Dataset(String, IReference, IParameters, IDatasetSchema, JsonNode)}
     * @param datasetStructure  see {@link Dataset#Dataset(String, IReference, IParameters, IDatasetSchema, JsonNode)}
     * @param location          the location of the file data storage
     */
    protected FileDataset(String name,
                       IReference linkedServiceName,
                       IParameters parameters,
                       IDatasetSchema datasetSchema,
                       JsonNode datasetStructure,
                       DatasetLocation location) {
        super(name, linkedServiceName, parameters, datasetSchema, datasetStructure);
        this.location = location;
    }

    @Override
    public DatasetLocation getLocation() {
        return location;
    }

    @Override
    public <T> T accept(IDatasetVisitor<T> visitor) {
        return visitor.visitFileDataset(this);
    }
}
