package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.IFileServerLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;

/**
 * Implementation of the FileServerLinkedService resource interface.
 */
public class FileServerLinkedService extends LinkedService implements IFileServerLinkedService {

    /**
     * Host name of the server.
     */
    private final IAdfFieldValue host;

    public FileServerLinkedService(String name,
                                   String description,
                                   IParameters parameters,
                                   IAdfFieldValue host) {
        super(name, description, parameters);
        this.host = host;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.FILE_SERVER;
    }

    @Override
    public IAdfFieldValue getHost() {
        return host;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitFileServerLinkedService(this);
    }
}
