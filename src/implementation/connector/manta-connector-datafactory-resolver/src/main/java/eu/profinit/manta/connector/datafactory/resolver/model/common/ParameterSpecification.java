package eu.profinit.manta.connector.datafactory.resolver.model.common;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IParameterSpecification;
import eu.profinit.manta.connector.datafactory.model.common.ParameterType;

/**
 * Implementation of IParameterSpecification resource.
 */
public class ParameterSpecification implements IParameterSpecification {

    /**
     * Default value of parameter.
     * <br>
     * The actual value depends on the type of the parameter.
     * It can be atomic value of any data type, array, object.
     */
    private final JsonNode defaultValue;

    /**
     * Parameter type.
     */
    private final ParameterType type;

    /**
     * @param defaultValue default value of the parameter
     * @param type         parameter type
     */
    public ParameterSpecification(JsonNode defaultValue, ParameterType type) {
        this.defaultValue = defaultValue;
        this.type = type;
    }

    @Override
    public JsonNode getDefaultValue() {
        return defaultValue;
    }

    @Override
    public ParameterType getType() {
        return type;
    }
}
