package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file.location;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.DatasetLocation;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.FileDatasetLocation;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

public abstract class FileDatasetLocationParser<T extends FileDatasetLocation> extends DatasetLocationParser<DatasetLocation> {

    public T parseDatasetLocation(JsonNode locationNode) {
        IAdfFieldValue folderPath = null;
        IAdfFieldValue fileName = null;
        if (JsonUtil.isNotNullAndNotMissing(locationNode)) {
            folderPath = adfFieldValueParser.parseAdfFieldValue(locationNode.path("folderPath"));
            fileName = adfFieldValueParser.parseAdfFieldValue(locationNode.path("fileName"));
        }
        return parseFileDatasetSpecificLocation(fileName, folderPath, locationNode);
    }

    protected abstract T parseFileDatasetSpecificLocation(IAdfFieldValue fileName, IAdfFieldValue folderPath, JsonNode locationNode);

}
