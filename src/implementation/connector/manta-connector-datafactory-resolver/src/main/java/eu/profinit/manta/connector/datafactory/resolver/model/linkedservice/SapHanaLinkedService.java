package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.linkedservice.ISapHanaLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;

/**
 * Implementation of the SapHanaLinkedService resource interface.
 */
public class SapHanaLinkedService extends LinkedService implements ISapHanaLinkedService {

    /**
     * SAP HANA ODBC connection string.
     */
    private final IAdfFieldValue connectionString;

    /**
     * Host name of the SAP HANA server.
     */
    private final IAdfFieldValue server;

    /**
     * Username to access the SAP HANA server.
     */
    private final IAdfFieldValue userName;

    public SapHanaLinkedService(String name,
                                String description,
                                IParameters parameters,
                                IAdfFieldValue connectionString,
                                IAdfFieldValue server,
                                IAdfFieldValue userName) {
        super(name, description, parameters);
        this.connectionString = connectionString;
        this.server = server;
        this.userName = userName;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.SAP_HANA;
    }

    @Override
    public IAdfFieldValue getConnectionString() {
        return connectionString;
    }

    @Override
    public IAdfFieldValue getServer() {
        return server;
    }

    @Override
    public IAdfFieldValue getUserName() {
        return userName;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitSapHanaLinkedService(this);
    }
}
