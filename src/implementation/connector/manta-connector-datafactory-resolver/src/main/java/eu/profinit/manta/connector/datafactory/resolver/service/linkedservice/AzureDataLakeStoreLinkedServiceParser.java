package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AzureDataLakeStoreLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for AzureDataLakeStoreLinkedService resource.
 */
public class AzureDataLakeStoreLinkedServiceParser extends AbstractLinkedServiceParser<AzureDataLakeStoreLinkedService> {

    @Override
    protected AzureDataLakeStoreLinkedService parseTypeSpecificFields(String name,
                                                                      String description,
                                                                      String type,
                                                                      IParameters parameters,
                                                                      JsonNode typePropertiesNode) {
        IAdfFieldValue accountName = null;
        IAdfFieldValue dataLakeStoreUri = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            accountName = processAdfFieldValue(typePropertiesNode.path("accountName"));
            dataLakeStoreUri = processAdfFieldValue(typePropertiesNode.path("dataLakeStoreUri"));
        }

        return new AzureDataLakeStoreLinkedService(name, description, parameters, accountName, dataLakeStoreUri);
    }
}
