package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.ISybaseLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;

/**
 * Implementation of the SybaseLinkedService resource interface.
 */
public class SybaseLinkedService extends LinkedService implements ISybaseLinkedService {

    /**
     * Database name for connection.
     */
    private final IAdfFieldValue database;

    /**
     * Schema name for connection.
     */
    private final IAdfFieldValue schema;

    /**
     * Server name for connection.
     */
    private final IAdfFieldValue server;

    /**
     * Username for authentication.
     */
    private final IAdfFieldValue username;

    public SybaseLinkedService(String name,
                               String description,
                               IParameters parameters,
                               IAdfFieldValue database,
                               IAdfFieldValue schema,
                               IAdfFieldValue server,
                               IAdfFieldValue username) {
        super(name, description, parameters);
        this.database = database;
        this.schema = schema;
        this.server = server;
        this.username = username;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.SYBASE;
    }

    @Override
    public IAdfFieldValue getDatabase() {
        return database;
    }

    @Override
    public IAdfFieldValue getSchema() {
        return schema;
    }

    @Override
    public IAdfFieldValue getServer() {
        return server;
    }

    @Override
    public IAdfFieldValue getUsername() {
        return username;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitSybaseLinkedService(this);
    }
}
