package eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location;

import eu.profinit.manta.connector.datafactory.model.dataset.file.location.IDatasetLocation;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.LocationType;

/**
 * Implementation of interface for all DatasetLocation resources.
 */
public abstract class DatasetLocation implements IDatasetLocation {

    @Override
    public abstract LocationType getLocationType();
}
