package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file.location;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.AzureBlobFSLocation;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

public class AzureBlobFSLocationParser extends FileDatasetLocationParser<AzureBlobFSLocation> {

    @Override
    public AzureBlobFSLocation parseFileDatasetSpecificLocation(IAdfFieldValue fileName, IAdfFieldValue folderPath, JsonNode locationNode) {
        IAdfFieldValue fileSystem = null;
        if (JsonUtil.isNotNullAndNotMissing(locationNode)) {
            fileSystem = adfFieldValueParser.parseAdfFieldValue(locationNode.path("fileSystem"));
        }
        return new AzureBlobFSLocation(fileSystem, folderPath, fileName);
    }
}
