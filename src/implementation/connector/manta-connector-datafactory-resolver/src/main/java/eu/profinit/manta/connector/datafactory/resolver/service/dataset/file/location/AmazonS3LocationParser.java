package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file.location;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.AmazonS3Location;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

public class AmazonS3LocationParser extends FileDatasetLocationParser<AmazonS3Location> {

    @Override
    public AmazonS3Location parseFileDatasetSpecificLocation(IAdfFieldValue fileName, IAdfFieldValue folderPath, JsonNode locationNode) {
        IAdfFieldValue bucketName = null;
        IAdfFieldValue version = null;
        if (JsonUtil.isNotNullAndNotMissing(locationNode)) {
            bucketName = adfFieldValueParser.parseAdfFieldValue(locationNode.path("bucketName"));
            version = adfFieldValueParser.parseAdfFieldValue(locationNode.path("version"));
        }
        return new AmazonS3Location(bucketName, folderPath, fileName, version);
    }
}
