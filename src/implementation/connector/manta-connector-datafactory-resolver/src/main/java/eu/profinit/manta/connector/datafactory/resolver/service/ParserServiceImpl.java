package eu.profinit.manta.connector.datafactory.resolver.service;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.IDataFlow;
import eu.profinit.manta.connector.datafactory.model.dataset.DatasetType;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.resolver.ParserService;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.resolver.model.DataFlow;
import eu.profinit.manta.connector.datafactory.resolver.model.Dataset;
import eu.profinit.manta.connector.datafactory.resolver.model.Factory;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.dataflow.DataFlowParser;
import eu.profinit.manta.connector.datafactory.resolver.service.dataset.DatasetParser;
import eu.profinit.manta.connector.datafactory.resolver.service.factory.FactoryParser;
import eu.profinit.manta.connector.datafactory.resolver.service.linkedservice.LinkedServiceParser;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import java.util.Map;

/**
 * Implementation of the parser service for Azure Data Factory resources.
 */
public class ParserServiceImpl implements ParserService {

    private static final Logger LOGGER = new Logger(ParserServiceImpl.class);

    /**
     * Default dataset type. It is used in case Dataset resource parser cannot be found.
     */
    private static final DatasetType DEFAULT_DATASET_TYPE = DatasetType.UNKNOWN;

    /**
     * Default linked service type. It is used in case Dataset resource parser cannot be found.
     */
    private static final LinkedServiceType DEFAULT_LINKED_SERVICE_TYPE = LinkedServiceType.UNKNOWN;

    /**
     * Parser for the Factory resource.
     */
    private final FactoryParser factoryParser;

    /**
     * Parser for Data flow resource.
     */
    private final DataFlowParser dataFlowParser;

    /**
     * Map of dataset resource parsers.
     */
    private final Map<DatasetType, DatasetParser<? extends Dataset>> datasetParserMap;

    /**
     * Map of linked service resource parsers.
     */
    private final Map<LinkedServiceType, LinkedServiceParser<? extends LinkedService>> linkedServiceParserMap;

    /**
     * @param factoryParser          parser for the Factory resource
     * @param datasetParserMap       map of parsers for different Dataset resources
     * @param linkedServiceParserMap map of parsers for different LinkedService resources
     */
    public ParserServiceImpl(FactoryParser factoryParser,
                             DataFlowParser dataFlowParser,
                             Map<DatasetType, DatasetParser<? extends Dataset>> datasetParserMap,
                             Map<LinkedServiceType, LinkedServiceParser<? extends LinkedService>> linkedServiceParserMap
    ) {
        this.factoryParser = factoryParser;
        this.dataFlowParser = dataFlowParser;
        this.datasetParserMap = datasetParserMap;
        this.linkedServiceParserMap = linkedServiceParserMap;
    }

    /**
     * {@inheritDoc}
     * <br>
     * Tries to find a parser for the provided linked service type.
     * If the parser for the provided linked service type is not found,
     * it is parsed as {@link #DEFAULT_LINKED_SERVICE_TYPE} type.
     */
    @Override
    public LinkedService parseLinkedService(JsonNode linkedServiceNode) {
        LinkedService linkedService = null;
        if (JsonUtil.isNotNullAndNotMissing(linkedServiceNode)) {
            LinkedServiceParser<? extends LinkedService> linkedServiceParser
                    = linkedServiceParserMap.get(parseLinkedServiceType(linkedServiceNode));

            if (linkedServiceParser == null) {
                linkedServiceParser = linkedServiceParserMap.get(DEFAULT_LINKED_SERVICE_TYPE);
            }
            linkedService = linkedServiceParser.parseLinkedService(linkedServiceNode);
        }
        return linkedService;
    }

    /**
     * Parses the type of the linked service resource.
     *
     * @param linkedServiceNode JSON node representing the root of the linked service resource
     * @return linked service type or {@link LinkedServiceType#UNKNOWN}  if cannot be parsed
     */
    private LinkedServiceType parseLinkedServiceType(JsonNode linkedServiceNode) {
        LinkedServiceType linkedServiceType = LinkedServiceType.UNKNOWN;
        if (JsonUtil.isNotNullAndNotMissing(linkedServiceNode)) {
            String linkedServiceName = JsonUtil.pathAsText(linkedServiceNode, "name");
            String linkedServiceTypeString = JsonUtil.pathAsText(linkedServiceNode.path("properties"), "type");
            try {
                linkedServiceType = LinkedServiceType.valueOfCI(linkedServiceTypeString);
            } catch (IllegalArgumentException e) {
                LOGGER.log(Categories.parsingErrors()
                        .unknownLinkedServiceType()
                        .linkedServiceType(linkedServiceTypeString)
                        .linkedServiceName(linkedServiceName)
                        .defaultLinkedServiceType(linkedServiceType)
                        .catching(e)
                );
            }
        }
        return linkedServiceType;
    }

    @Override
    public Factory parseFactory(JsonNode factoryNode) {
        return factoryParser.parseFactory(factoryNode);
    }

    @Override
    public IDataFlow parseDataFlow(JsonNode dataFlowNode){
        return dataFlowParser.parseDataFlow(dataFlowNode);
    }

    /**
     * {@inheritDoc}
     * <br>
     * Tries to find a parser for the provided dataset type.
     * If the parser for the provided dataset type is not found,
     * it is parsed as {@link #DEFAULT_DATASET_TYPE} type.
     */
    @Override
    public Dataset parseDataset(JsonNode datasetNode) {
        Dataset dataset = null;
        if (JsonUtil.isNotNullAndNotMissing(datasetNode)) {
            DatasetParser<? extends Dataset> datasetParser = datasetParserMap.get(parseDatasetType(datasetNode));

            if (datasetParser == null) {
                datasetParser = datasetParserMap.get(DEFAULT_DATASET_TYPE);
            }
            dataset = datasetParser.parseDataset(datasetNode);
        }
        return dataset;
    }

    /**
     * Parses the type of the dataset resource.
     *
     * @param datasetNode JSON node representing the root of the dataset resource
     * @return dataset type
     */
    private DatasetType parseDatasetType(JsonNode datasetNode) {
        DatasetType datasetType = DatasetType.UNKNOWN;
        if (JsonUtil.isNotNullAndNotMissing(datasetNode)) {
            String datasetName = JsonUtil.pathAsText(datasetNode, "name");
            String datasetTypeString = JsonUtil.pathAsText(datasetNode.path("properties"), "type");
            try {
                datasetType = DatasetType.valueOfCI(datasetTypeString);
            } catch (IllegalArgumentException e) {
                LOGGER.log(Categories.parsingErrors()
                        .unknownDatasetType()
                        .datasetType(datasetTypeString)
                        .datasetName(datasetName)
                        .defaultDatasetType(datasetType)
                        .catching(e)
                );
            }
        }
        return datasetType;
    }

}
