package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file.location;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.HdfsLocation;

public class HdfsLocationParser extends FileDatasetLocationParser<HdfsLocation> {

    @Override
    public HdfsLocation parseFileDatasetSpecificLocation(IAdfFieldValue fileName, IAdfFieldValue folderPath, JsonNode locationNode) {
        return new HdfsLocation(folderPath, fileName);
    }
}
