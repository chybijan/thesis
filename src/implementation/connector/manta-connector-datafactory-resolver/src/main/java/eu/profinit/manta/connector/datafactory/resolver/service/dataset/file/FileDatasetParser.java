package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.LocationType;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.FileDataset;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.DatasetLocation;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;
import eu.profinit.manta.connector.datafactory.resolver.service.dataset.AbstractDatasetParser;
import eu.profinit.manta.connector.datafactory.resolver.service.dataset.file.location.DatasetLocationParser;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import java.util.Map;

/**
 * Parent implementation of all file-like Dataset resource parsers.
 * <br>
 * Parses the common part of all file-like Dataset resources and delegates the processing of Dataset type-specific fields to subclasses.
 *
 * @param <T> class for file-like Dataset resource, that can be parsed by the particular subclass parser
 */
public abstract class FileDatasetParser<T extends FileDataset> extends AbstractDatasetParser<T> {

    private static final Logger LOGGER = new Logger(FileDatasetParser.class);

    /**
     * Map of parsers for DatasetLocation resources.
     */
    private Map<LocationType, DatasetLocationParser<? extends DatasetLocation>> locationParserMap;

    @Override
    protected T parseDatasetSpecificFields(String name,
                                           String type,
                                           IReference lsName,
                                           IParameters parameters,
                                           IDatasetSchema datasetSchema,
                                           JsonNode datasetStructureNode,
                                           JsonNode typePropertiesNode) {
        DatasetLocation fileDatasetLocation = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            JsonNode locationNode = typePropertiesNode.path("location");
            if (JsonUtil.isNotNullAndNotMissing(locationNode)) {
                LocationType locationType = parseLocationType(locationNode, name);

                DatasetLocationParser<? extends DatasetLocation> datasetLocationParser =
                        locationParserMap.get(locationType);
                fileDatasetLocation = datasetLocationParser.parseDatasetLocation(locationNode);
            }
        }
        return parseFileDatasetSpecificFields(name, lsName, parameters, datasetSchema, datasetStructureNode, fileDatasetLocation, typePropertiesNode);
    }

    /**
     * Parses specific fields for each file-like dataset type.
     *
     * @param name                 dataset name
     * @param lsName               linked service reference
     * @param parameters           dataset parameters definition
     * @param datasetSchema        dataset schema
     * @param datasetStructureNode JSON node representing the "structure" field value
     * @param location             location of the file-like dataset
     * @param typePropertiesNode   JSON node representing "typeProperties" field value
     * @return instance of a dataset
     */
    protected abstract T parseFileDatasetSpecificFields(String name,
                                                        IReference lsName,
                                                        IParameters parameters,
                                                        IDatasetSchema datasetSchema,
                                                        JsonNode datasetStructureNode,
                                                        DatasetLocation location,
                                                        JsonNode typePropertiesNode);

    /**
     * Parse the type of the Location resource.
     *
     * @param locationNode JSON node representing location field value
     * @param datasetName  name of the dataset (for logging purposes)
     * @return type of the location or null in case it is not recognizable
     */
    private LocationType parseLocationType(JsonNode locationNode, String datasetName) {
        LocationType locationType = null;
        if (JsonUtil.isNotNullAndNotMissing(locationNode)) {
            String locationTypeString = JsonUtil.pathAsText(locationNode, "type");
            try {
                locationType = LocationType.valueOfCI(locationTypeString);
            } catch (IllegalArgumentException e) {
                LOGGER.log(Categories.parsingErrors()
                        .unknownDatasetLocationType()
                        .locationType(locationTypeString)
                        .datasetName(datasetName)
                        .catching(e)
                );
            }
        }
        return locationType;
    }

    public void setLocationParserMap(Map<LocationType, DatasetLocationParser<? extends DatasetLocation>> locationParserMap) {
        this.locationParserMap = locationParserMap;
    }
}
