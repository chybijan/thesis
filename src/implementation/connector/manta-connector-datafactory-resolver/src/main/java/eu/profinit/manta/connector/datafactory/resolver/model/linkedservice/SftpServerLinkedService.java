package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.ISftpServerLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;

/**
 * Implementation of the FtpServerLinkedService resource interface.
 */
public class SftpServerLinkedService extends AbstractGeneralFtpServerLinkedService implements ISftpServerLinkedService {


    public SftpServerLinkedService(String name,
                                   String description,
                                   IParameters parameters,
                                   IAdfFieldValue host,
                                   IAdfFieldValue port,
                                   IAdfFieldValue userName) {
        super(name, description, parameters, host, port, userName);

    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.SFTP;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitSftpServerLinkedService(this);
    }
}
