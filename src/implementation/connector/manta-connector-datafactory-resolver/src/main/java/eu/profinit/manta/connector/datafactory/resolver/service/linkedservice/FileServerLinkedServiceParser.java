package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.FileServerLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for FileServerLinkedService resource.
 */
public class FileServerLinkedServiceParser extends AbstractLinkedServiceParser<FileServerLinkedService> {

    @Override
    protected FileServerLinkedService parseTypeSpecificFields(String name,
                                                              String description,
                                                              String type,
                                                              IParameters parameters,
                                                              JsonNode typePropertiesNode) {
        IAdfFieldValue host = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            host = processAdfFieldValue(typePropertiesNode.path("host"));
        }

        return new FileServerLinkedService(name, description, parameters, host);
    }
}
