package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AzureFileStorageLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for AzureFileStorageLinkedService resource.
 */
public class AzureFileStorageLinkedServiceParser extends AbstractLinkedServiceParser<AzureFileStorageLinkedService> {

    @Override
    protected AzureFileStorageLinkedService parseTypeSpecificFields(String name,
                                                                    String description,
                                                                    String type,
                                                                    IParameters parameters,
                                                                    JsonNode typePropertiesNode) {
        IAdfFieldValue connectionString = null;
        IAdfFieldValue host = null;
        IAdfFieldValue sasUri = null;
        IAdfFieldValue snapshot = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            connectionString = processAdfFieldValue(typePropertiesNode.path("connectionString"));
            host = processAdfFieldValue(typePropertiesNode.path("host"));
            sasUri = processAdfFieldValue(typePropertiesNode.path("sasUri"));
            snapshot = processAdfFieldValue(typePropertiesNode.path("snapshot"));
        }

        return new AzureFileStorageLinkedService(name, description, parameters, connectionString, host, sasUri, snapshot);
    }
}
