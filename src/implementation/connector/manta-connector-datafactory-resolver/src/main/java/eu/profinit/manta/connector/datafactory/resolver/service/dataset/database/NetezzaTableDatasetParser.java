package eu.profinit.manta.connector.datafactory.resolver.service.dataset.database;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.NetezzaTableDataset;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for NetezzaTableDataset resource.
 */
public class NetezzaTableDatasetParser extends DatabaseDatasetParser<NetezzaTableDataset> {

    @Override
    protected NetezzaTableDataset parseDatabaseDatasetSpecificFields(String name,
                                                                     IReference lsName,
                                                                     IParameters parameters,
                                                                     IDatasetSchema datasetSchema,
                                                                     JsonNode datasetStructureNode,
                                                                     JsonNode typePropertiesNode) {
        IAdfFieldValue schema = null;
        IAdfFieldValue table = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            schema = processAdfFieldValue(typePropertiesNode.path("schema"));
            table = processAdfFieldValue(typePropertiesNode.path("table"));
        }

        return new NetezzaTableDataset(name, lsName, parameters, datasetSchema, datasetStructureNode, schema, table);
    }
}
