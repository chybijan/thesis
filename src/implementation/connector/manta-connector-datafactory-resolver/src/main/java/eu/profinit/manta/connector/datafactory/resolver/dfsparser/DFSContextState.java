package eu.profinit.manta.connector.datafactory.resolver.dfsparser;

import eu.profinit.manta.ast.navigator.MantaAstNavigator;
import eu.profinit.manta.ast.parser.base.IMantaAbstractParser;
import eu.profinit.manta.ast.parser.context.ParserContextState;
import org.antlr.runtime.tree.TreeAdaptor;


public class DFSContextState extends ParserContextState {

    /**
     * @param navigator AST navigator
     * @param parser the used parser
     * @param treeAdaptor the used tree adaptor
     */
    public DFSContextState(MantaAstNavigator navigator, IMantaAbstractParser parser,
                           TreeAdaptor treeAdaptor) {

        super(navigator, parser, treeAdaptor);
    }

}
