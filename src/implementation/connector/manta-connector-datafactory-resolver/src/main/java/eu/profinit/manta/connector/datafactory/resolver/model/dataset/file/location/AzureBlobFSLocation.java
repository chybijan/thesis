package eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.IAzureBlobFSLocation;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.LocationType;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.visitor.ILocationVisitor;

/**
 * Implementation of the DatasetLocation resource of type AzureBlobFS.
 */
public class AzureBlobFSLocation extends FileDatasetLocation implements IAzureBlobFSLocation {

    /**
     * The Data Lake Storage Gen2 file system name.
     */
    private final IAdfFieldValue fileSystem;

    /**
     * @param fileSystem the Data Lake Storage Gen2 file system name
     * @param folderPath see {@link FileDatasetLocation#FileDatasetLocation(IAdfFieldValue, IAdfFieldValue)}
     * @param fileName   see {@link FileDatasetLocation#FileDatasetLocation(IAdfFieldValue, IAdfFieldValue)}
     */
    public AzureBlobFSLocation(IAdfFieldValue fileSystem, IAdfFieldValue folderPath, IAdfFieldValue fileName) {
        super(folderPath, fileName);
        this.fileSystem = fileSystem;
    }

    @Override
    public LocationType getLocationType() {
        return LocationType.AZURE_BLOB_FS_LOCATION;
    }

    @Override
    public IAdfFieldValue getFileSystem() {
        return fileSystem;
    }

    @Override
    public <T> T accept(ILocationVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
