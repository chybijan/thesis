package eu.profinit.manta.connector.datafactory.resolver.service.dataset.database;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.TeradataTableDataset;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for TeradataTableDataset resource.
 */
public class TeradataTableDatasetParser extends DatabaseDatasetParser<TeradataTableDataset> {

    @Override
    protected TeradataTableDataset parseDatabaseDatasetSpecificFields(String name,
                                                                      IReference lsName,
                                                                      IParameters parameters,
                                                                      IDatasetSchema datasetSchema,
                                                                      JsonNode datasetStructureNode,
                                                                      JsonNode typePropertiesNode) {
        IAdfFieldValue database = null;
        IAdfFieldValue table = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            database = processAdfFieldValue(typePropertiesNode.path("database"));
            table = processAdfFieldValue(typePropertiesNode.path("table"));
        }

        return new TeradataTableDataset(name, lsName, parameters, datasetSchema, datasetStructureNode, database, table);
    }
}
