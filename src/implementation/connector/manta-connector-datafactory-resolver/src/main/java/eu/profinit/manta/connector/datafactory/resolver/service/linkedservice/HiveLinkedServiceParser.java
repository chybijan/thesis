package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.HiveServerType;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.HiveLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;
import eu.profinit.manta.platform.logging.api.logging.Logger;

/**
 * Parser for HiveLinkedService resource.
 */
public class HiveLinkedServiceParser extends AbstractLinkedServiceParser<HiveLinkedService> {

    private static final Logger LOGGER = new Logger(HiveLinkedServiceParser.class);

    @Override
    protected HiveLinkedService parseTypeSpecificFields(String name,
                                                        String description,
                                                        String type,
                                                        IParameters parameters,
                                                        JsonNode typePropertiesNode) {
        IAdfFieldValue host = null;
        IAdfFieldValue httpPath = null;
        IAdfFieldValue port = null;
        HiveServerType serverType = null;
        IAdfFieldValue useNativeQuery = null;
        IAdfFieldValue username = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            host = processAdfFieldValue(typePropertiesNode.path("host"));
            httpPath = processAdfFieldValue(typePropertiesNode.path("httpPath"));
            port = processAdfFieldValue(typePropertiesNode.path("port"));
            serverType = parseServerType(typePropertiesNode.path("serverType"), name);
            useNativeQuery = processAdfFieldValue(typePropertiesNode.path("useNativeQuery"));
            username = processAdfFieldValue(typePropertiesNode.path("username"));
        }

        return new HiveLinkedService(name, description, parameters, host, httpPath, port, serverType, useNativeQuery, username);
    }

    /**
     * Parses Hive server type value.
     *
     * @param serverTypeValue   JSON node representing the value of "serverType" field
     * @param linkedServiceName name of the processed linked service
     * @return type of Hive server or null if couldn't be parsed
     */
    private HiveServerType parseServerType(JsonNode serverTypeValue, String linkedServiceName) {
        HiveServerType hiveServerType = null;
        if (JsonUtil.isNotNullAndNotMissing(serverTypeValue)) {
            try {
                hiveServerType = HiveServerType.valueOfCI(serverTypeValue.asText());
            } catch (IllegalArgumentException ex) {
                LOGGER.log(Categories.parsingErrors()
                        .unknownHiveServerType()
                        .hiveServerType(serverTypeValue.asText())
                        .linkedServiceName(linkedServiceName)
                        .catching(ex)
                );
            }
        }
        return hiveServerType;
    }
}
