package eu.profinit.manta.connector.datafactory.resolver.model.dataflow.mapping;

import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.dataflow.DataFlowType;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowEndpoint;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowScript;
import eu.profinit.manta.connector.datafactory.model.dataflow.mapping.IMappingDataFlowProperties;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.DataFlowProperties;

import java.util.Collection;
import java.util.Map;

public class MappingDataFlowProperties extends DataFlowProperties implements IMappingDataFlowProperties{

    protected final Map<String, IDataFlowEndpoint> sinks;

    protected final Collection<String> transformations;

    public MappingDataFlowProperties(IDataFlowScript script, Map<String, IDataFlowEndpoint> sources, Map<String, IDataFlowEndpoint> sinks, Collection<String> transformations) {
        super(DataFlowType.MAPPING_DATA_FLOW, script, sources);
        this.sinks = sinks;
        this.transformations = transformations;
    }

    @Override
    public Map<String, IDataFlowEndpoint> getSinks() {
        return sinks;
    }

    @Override
    public Collection<String> getTransformationNames() {
        return transformations;
    }

    @Override
    public Collection<IReference> getAllReferences() {

        // Get reference for each source as in parent
        Collection<IReference> references = super.addEndpointReferences(getSources());

        // Get reference for each sink
        references.addAll(addEndpointReferences(getSinks()));

        return references;
    }
}
