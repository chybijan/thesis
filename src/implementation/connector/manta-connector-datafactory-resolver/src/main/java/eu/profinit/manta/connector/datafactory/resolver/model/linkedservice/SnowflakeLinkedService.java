package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.ISnowflakeLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;

/**
 * Implementation of the SnowflakeLinkedService resource interface.
 */
public class SnowflakeLinkedService extends LinkedService implements ISnowflakeLinkedService {

    /**
     * The connection string of snowflake.
     */
    private final IAdfFieldValue connectionString;

    public SnowflakeLinkedService(String name,
                                  String description,
                                  IParameters parameters,
                                  IAdfFieldValue connectionString) {
        super(name, description, parameters);
        this.connectionString = connectionString;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.SNOWFLAKE;
    }

    @Override
    public IAdfFieldValue getConnectionString() {
        return connectionString;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitSnowflakeLinkedService(this);
    }
}
