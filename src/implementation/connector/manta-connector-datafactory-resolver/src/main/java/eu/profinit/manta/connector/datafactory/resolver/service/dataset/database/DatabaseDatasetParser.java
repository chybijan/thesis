package eu.profinit.manta.connector.datafactory.resolver.service.dataset.database;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.DatabaseDataset;
import eu.profinit.manta.connector.datafactory.resolver.service.dataset.AbstractDatasetParser;

/**
 * Parent implementation of all database-like Dataset resource parsers.
 *
 * @param <T> class for database-like Dataset resource, that can be parsed by the particular subclass parser
 */
public abstract class DatabaseDatasetParser<T extends DatabaseDataset> extends AbstractDatasetParser<T> {

    @Override
    protected T parseDatasetSpecificFields(String name,
                                           String type,
                                           IReference lsName,
                                           IParameters parameters,
                                           IDatasetSchema datasetSchema,
                                           JsonNode datasetStructureNode,
                                           JsonNode typePropertiesNode) {
        return parseDatabaseDatasetSpecificFields(name, lsName, parameters, datasetSchema, datasetStructureNode, typePropertiesNode);
    }

    /**
     * Parses specific fields for each database-like dataset type.
     *
     * @param name                 dataset name
     * @param lsName               linked service reference
     * @param parameters           dataset parameters definition
     * @param datasetSchema        dataset schema
     * @param datasetStructureNode JSON node representing the "structure" field value
     * @param typePropertiesNode   JSON node representing "typeProperties" field value
     * @return instance of a dataset
     */
    protected abstract T parseDatabaseDatasetSpecificFields(String name,
                                                            IReference lsName,
                                                            IParameters parameters,
                                                            IDatasetSchema datasetSchema,
                                                            JsonNode datasetStructureNode,
                                                            JsonNode typePropertiesNode);
}
