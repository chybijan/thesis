package eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.IGoogleCloudStorageLocation;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.LocationType;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.visitor.ILocationVisitor;

/**
 * Implementation of the DatasetLocation resource of type GoogleCloudStorage.
 */
public class GoogleCloudStorageLocation extends FileDatasetLocation implements IGoogleCloudStorageLocation {

    /**
     * The GCS bucket name.
     */
    private final IAdfFieldValue bucketName;

    /**
     * @param bucketName the GCS bucket name.
     * @param folderPath see {@link FileDatasetLocation#FileDatasetLocation(IAdfFieldValue, IAdfFieldValue)}
     * @param fileName   see {@link FileDatasetLocation#FileDatasetLocation(IAdfFieldValue, IAdfFieldValue)}
     */
    public GoogleCloudStorageLocation(IAdfFieldValue bucketName, IAdfFieldValue folderPath, IAdfFieldValue fileName) {
        super(folderPath, fileName);
        this.bucketName = bucketName;
    }

    @Override
    public LocationType getLocationType() {
        return LocationType.GOOGLE_CLOUD_STORAGE_LOCATION;
    }


    @Override
    public IAdfFieldValue getBucketName() {
        return bucketName;
    }

    @Override
    public <T> T accept(ILocationVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
