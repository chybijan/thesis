package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.linkedservice.IPostgreSqlLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;

/**
 * Implementation of the PostgreSqlLinkedService resource interface.
 */
public class PostgreSqlLinkedService extends LinkedService implements IPostgreSqlLinkedService {

    /**
     * The connection string.
     */
    private final IAdfFieldValue connectionString;

    // THE FOLLOWING THREE PROPERTIES (server, database, username) are part of the "previous" payload and should not be used anymore.
    // https://docs.microsoft.com/en-us/azure/data-factory/connector-postgresql#linked-service-properties

    /**
     * Name of the PostgreSQL server. It is used in legacy format.
     */
    private final IAdfFieldValue server;

    /**
     * Name of the PostgreSQL database. It is used in legacy format.
     */
    private final IAdfFieldValue database;

    /**
     * Username to login. It is used in legacy format.
     */
    private final IAdfFieldValue username;

    public PostgreSqlLinkedService(String name,
                                   String description,
                                   IParameters parameters,
                                   IAdfFieldValue connectionString,
                                   IAdfFieldValue server,
                                   IAdfFieldValue database,
                                   IAdfFieldValue username) {
        super(name, description, parameters);
        this.connectionString = connectionString;
        this.server = server;
        this.database = database;
        this.username = username;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.POSTGRESQL;
    }

    @Override
    public IAdfFieldValue getConnectionString() {
        return connectionString;
    }

    @Override
    public IAdfFieldValue getServer() {
        return server;
    }

    @Override
    public IAdfFieldValue getDatabase() {
        return database;
    }

    @Override
    public IAdfFieldValue getUsername() {
        return username;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitPostgreSqlLinkedService(this);
    }
}
