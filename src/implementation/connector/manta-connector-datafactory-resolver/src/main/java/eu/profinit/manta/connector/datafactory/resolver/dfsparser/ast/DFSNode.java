package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.ast.MantaAstNode;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowScript;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IDFSNode;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import org.antlr.runtime.Token;

import java.util.List;

public class DFSNode extends MantaAstNode<DFSContextState> implements IDFSNode {

    public DFSNode() {
        super();
    }

    public DFSNode(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public DFSNode(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    @Override
    public DFSNode dupNode() {
        DFSNode result = new DFSNode(this.getToken(), getContextState());
        result.setWhiteTokens(getWhiteTokens());
        return result;
    }

    @Override
    public DFSNode getParent() {
        return (DFSNode) super.getParent();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<? extends DFSNode> getChildren() {
        return (List<? extends DFSNode>) super.getChildren();
    }

    @Override
    public IDataFlowScript resolve(IDataFlowScript currentDFS) {
        return currentDFS;
    }
}
