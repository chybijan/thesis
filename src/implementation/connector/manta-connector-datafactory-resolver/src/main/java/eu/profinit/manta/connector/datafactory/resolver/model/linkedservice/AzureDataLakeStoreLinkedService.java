package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.linkedservice.IAzureDataLakeStoreLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;

/**
 * Implementation of the AzureDataLakeStoreLinkedService resource interface.
 */
public class AzureDataLakeStoreLinkedService extends LinkedService implements IAzureDataLakeStoreLinkedService {

    private final IAdfFieldValue accountName;

    private final IAdfFieldValue dataLakeStoreUri;

    public AzureDataLakeStoreLinkedService(String name,
                                           String description,
                                           IParameters parameters,
                                           IAdfFieldValue accountName,
                                           IAdfFieldValue dataLakeStoreUri) {
        super(name, description, parameters);
        this.accountName = accountName;
        this.dataLakeStoreUri = dataLakeStoreUri;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.AZURE_DATA_LAKE_STORE;
    }

    @Override
    public IAdfFieldValue getAccountName() {
        return accountName;
    }

    @Override
    public IAdfFieldValue getDataLakeStoreUri() {
        return dataLakeStoreUri;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitAzureDataLakeStoreLinkedService(this);
    }
}
