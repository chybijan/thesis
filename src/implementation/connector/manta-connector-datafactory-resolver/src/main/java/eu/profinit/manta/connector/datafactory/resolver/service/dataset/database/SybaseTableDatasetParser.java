package eu.profinit.manta.connector.datafactory.resolver.service.dataset.database;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.SybaseTableDataset;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for SybaseTableDataset resource.
 */
public class SybaseTableDatasetParser extends DatabaseDatasetParser<SybaseTableDataset> {

    @Override
    protected SybaseTableDataset parseDatabaseDatasetSpecificFields(String name,
                                                                    IReference lsName,
                                                                    IParameters parameters,
                                                                    IDatasetSchema datasetSchema,
                                                                    JsonNode datasetStructureNode,
                                                                    JsonNode typePropertiesNode) {
        IAdfFieldValue schema = null;
        IAdfFieldValue table = null;
        IAdfFieldValue tableName = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            schema = processAdfFieldValue(typePropertiesNode.path("schema"));
            table = processAdfFieldValue(typePropertiesNode.path("table"));
            tableName = processAdfFieldValue(typePropertiesNode.path("tableName"));
        }

        return new SybaseTableDataset(name, lsName, parameters, datasetSchema, datasetStructureNode, schema, table, tableName);
    }
}
