package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstDataTypeFormat;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import org.antlr.runtime.Token;

public class AstDataTypeFormat extends DFSNode implements IAstDataTypeFormat {
    public AstDataTypeFormat() {
    }

    public AstDataTypeFormat(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstDataTypeFormat(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }
}
