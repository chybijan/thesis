package eu.profinit.manta.connector.datafactory.resolver.service.dataflow;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowEndpoint;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowProperties;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowScript;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.DataFlowScript;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.mapping.MappingDataFlowProperties;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;
import eu.profinit.manta.connector.datafactory.resolver.service.common.ReferenceParser;
import eu.profinit.manta.platform.logging.api.logging.Logger;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public class MappingDataFlowPropertiesParser extends AbstractDataFlowPropertiesParser{

    private static final Logger LOGGER = new Logger(MappingDataFlowPropertiesParser.class);

    private static final String SCRIPT_PATH = "script";
    private static final String SCRIPT_LINES_PATH = "/scriptLines";

    private static final String SOURCES_PATH = "/sources";

    private static final String SINKS_PATH = "/sinks";

    private static final String TRANSFORMATION_PATH = "/transformations";

    private static final String TRANSFORMATION_NAME_PATH = "name";

    public MappingDataFlowPropertiesParser(ReferenceParser referenceParser, DFSParserService parserService) {
        super(referenceParser, parserService);
    }

    @Override
    public IDataFlowProperties parseProperties(JsonNode typePropertiesNode) {

        IDataFlowProperties properties = null;

        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {

            // Load sources
            JsonNode sourcesArray = typePropertiesNode.at(SOURCES_PATH);
            Map<String, IDataFlowEndpoint> sources = parseEndpoints(sourcesArray);
            if(sources.isEmpty()){
                LOGGER.log(Categories.parsingErrors().missingSourcesOfDataFlowProperties());
            }

            // Load sinks
            JsonNode sinksArray = typePropertiesNode.at(SINKS_PATH);
            Map<String, IDataFlowEndpoint> sinks = parseEndpoints(sinksArray);
            if(sinks.isEmpty()){
                LOGGER.log(Categories.parsingErrors().missingSinksOfDataFlowProperties());
            }

            // Load transformations
            JsonNode transformationsArray = typePropertiesNode.at(TRANSFORMATION_PATH);
            Collection<String> transformationNames = parseTransformations(transformationsArray);

            // Load script
            IDataFlowScript script = new DataFlowScript(Collections.emptyList(), Collections.emptyMap());

            String scriptStr = loadScript(typePropertiesNode);
            if(StringUtils.isBlank(scriptStr)){
                LOGGER.log(Categories.parsingErrors().missingDataFlowScript());
            } else{
                // Parse script
                script = parserService.processScript(scriptStr);
            }

            // Create data flow
            properties = new MappingDataFlowProperties(script, sources, sinks, transformationNames);
        }

        return properties;

    }

    /**
     * Loads script as continuous string
     * @param typePropertiesNode node with script attribute
     * @return Script as a string or null if script is not found
     */
    private String loadScript(JsonNode typePropertiesNode){
        String scriptStr = JsonUtil.pathAsText(typePropertiesNode, SCRIPT_PATH);
        if(StringUtils.isBlank(scriptStr)){
            JsonNode linesArrayNode = typePropertiesNode.at(SCRIPT_LINES_PATH);
            if(linesArrayNode.isArray())
            {
                StringBuilder scriptStringBuilder = new StringBuilder();
                for(JsonNode line : linesArrayNode){
                    String stringLine = line.textValue();
                    scriptStringBuilder.append(stringLine).append("\n");
                }
                scriptStr = scriptStringBuilder.toString();
            }
        }

        return scriptStr;
    }


    /**
     * Parse transformations node into Collection of transformation names
     * @param transformationsArray json node representing array
     * @return Collection of transformation names,
     *         or empty Collection if transformationArray is null, empty or is not array
     */
    protected Collection<String> parseTransformations(JsonNode transformationsArray) {

        Collection<String> transformations = new ArrayList<>();

        if (JsonUtil.isNotNullAndNotMissing(transformationsArray) && transformationsArray.isArray()) {

            // Go through each transformation and find its name
            transformationsArray.elements().forEachRemaining(transformationNode -> {

                // Load transformation name
                String transformationName = JsonUtil.pathAsText(transformationNode, TRANSFORMATION_NAME_PATH);
                if(transformationName != null){
                    transformations.add(transformationName);
                }

            });

        }

        return transformations;
    }
}
