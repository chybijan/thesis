package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.UnknownLinkedService;
import eu.profinit.manta.platform.logging.api.logging.Logger;

/**
 * Parser for unrecognizable or unsupported LinkedService resources by Manta.
 */
public class UnknownLinkedServiceParser extends AbstractLinkedServiceParser<UnknownLinkedService> {

    private static final Logger LOGGER = new Logger(UnknownLinkedServiceParser.class);

    @Override
    protected UnknownLinkedService parseTypeSpecificFields(String name,
                                                           String description,
                                                           String type,
                                                           IParameters parameters,
                                                           JsonNode typePropertiesNode) {
        LOGGER.log(Categories.parsingErrors()
                .unsupportedLinkedServiceType()
                .linkedServiceName(name)
                .linkedServiceType(type)
        );
        return new UnknownLinkedService(name, description, parameters);
    }
}
