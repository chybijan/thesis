package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IJoinTransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.ITable;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstExpression;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstFilterTransformation;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstJoinTransformation;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstTransformationAttributes;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.VoidExpression;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.FilterTransformation;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.JoinTransformation;
import eu.profinit.manta.platform.logging.api.logging.Logger;
import org.antlr.runtime.Token;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AstJoinTransformation extends AstConcreteTransformation implements IAstJoinTransformation {

    private static final Logger LOGGER = new Logger(AstJoinTransformation.class);

    private static String JOIN_TYPE = "joinType";

    public AstJoinTransformation() {
    }

    public AstJoinTransformation(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstJoinTransformation(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    @Override
    public IAstExpression findJoinCondition() {
        return (IAstExpression) selectSingleNode("AST_EXPRESSION");
    }

    @Override
    protected ITransformation createTransformation(
            String name, Set<String> substreams,
            Set<IADFStream> inputs, Set<String> outputs,
            ITable table) {

        IAstExpression astFilterCondition = findJoinCondition();
        IExpression expression = astFilterCondition != null ? astFilterCondition : new VoidExpression();

        IAstTransformationAttributes astAttributes = findTransformationAttributes();
        Map<String, IExpression> attributes = astAttributes != null ? astAttributes.resolveAttributes() : new HashMap<>();

        IJoinTransformation.JoinType joinType = IJoinTransformation.JoinType.UNKNOWN;
        String joinTypeValue = "";

        if(attributes.containsKey(JOIN_TYPE)){
            try{
                 joinTypeValue = attributes.get(JOIN_TYPE).asString();
                joinType = IJoinTransformation.JoinType.valueOfCI(joinTypeValue);
            }catch (IllegalArgumentException e){
                LOGGER.log(Categories.parsingErrors().invalidTransformationAttributeValue()
                        .expected("outer | inner | left | right | cross")
                        .name(JOIN_TYPE)
                        .value(joinTypeValue));
            }
        }else{
            LOGGER.log(Categories.parsingErrors().missingTransformationAttribute().expectedAttribute(JOIN_TYPE));
        }

        return new JoinTransformation(
                name, substreams,  inputs, outputs, table, attributes, expression, joinType);
    }

}
