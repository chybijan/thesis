package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.ast.token.impl.SqlMantaToken;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.parser.DFSLexer;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenStream;


public class ErrorToken extends SqlMantaToken {

    private static final long serialVersionUID = 6531256097861568427L;

    public ErrorToken(TokenStream input, Token start, Token stop, RecognitionException exception) {
        super(DFSLexer.AST_ERROR);
        this.implInput = input;
        this.implStart = start;
        this.implStop = stop;
        this.implException = exception;
    }

    private final transient TokenStream implInput;
    private final transient Token implStart;
    private final transient Token implStop;
    private final transient RecognitionException implException;

    public TokenStream getInput() {
        return implInput;
    }

    public Token getStart() {
        return implStart;
    }

    public Token getStop() {
        return implStop;
    }

    public RecognitionException getException() {
        return implException;
    }

}
