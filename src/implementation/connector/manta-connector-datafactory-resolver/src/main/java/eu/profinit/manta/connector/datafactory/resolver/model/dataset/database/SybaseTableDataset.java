package eu.profinit.manta.connector.datafactory.resolver.model.dataset.database;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.dataset.DatasetType;
import eu.profinit.manta.connector.datafactory.model.dataset.database.ISybaseTableDataset;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.model.visitor.IDatasetVisitor;

import java.util.Optional;

/**
 * Implementation of the Dataset resource of type SybaseTable.
 */
public class SybaseTableDataset extends DatabaseDataset implements ISybaseTableDataset {

    /**
     * The Sybase table name.
     */
    private final IAdfFieldValue tableName;

    public SybaseTableDataset(String name,
                              IReference linkedServiceName,
                              IParameters parameters,
                              IDatasetSchema datasetSchema,
                              JsonNode datasetStructure,
                              IAdfFieldValue schema,
                              IAdfFieldValue table,
                              IAdfFieldValue tableName) {
        super(name, linkedServiceName, parameters, datasetSchema, datasetStructure, schema, table);
        this.tableName = tableName;
    }

    @Override
    public DatasetType getType() {
        return DatasetType.SYBASE_TABLE;
    }

    @Override
    public Optional<IAdfFieldValue> getTableName() {
        return tableName != null ? Optional.of(tableName) : Optional.empty();
    }

    @Override
    public <T> T accept(IDatasetVisitor<T> visitor) {
        return visitor.visitSybaseTable(this);
    }
}
