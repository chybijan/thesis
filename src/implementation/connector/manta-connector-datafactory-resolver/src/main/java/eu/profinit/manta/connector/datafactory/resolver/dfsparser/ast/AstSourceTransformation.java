package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.DataFlowDataType;
import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumn;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.ITable;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.*;
import eu.profinit.manta.connector.datafactory.model.errors.InvalidExpressionTypeException;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.SourceTransformation;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.datarepresentation.ColumnDef;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.datarepresentation.Table;
import eu.profinit.manta.platform.logging.api.logging.Logger;
import org.antlr.runtime.Token;

import java.util.*;

public class AstSourceTransformation extends AstConcreteTransformation implements IAstSourceTransformation {

    private static final Logger LOGGER = new Logger(AstSourceTransformation.class);

    public AstSourceTransformation() {
    }

    public AstSourceTransformation(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstSourceTransformation(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    @Override
    public IAstOutputAttribute findOutputAttribute() {
        return (IAstOutputAttribute) selectSingleNode("AST_TRANSFORMATION_ATTRIBUTES/AST_OUTPUT_ATTRIBUTE");
    }

    @Override
    protected ITransformation createTransformation(
            String name, Set<String> substreams,
            Set<IADFStream> inputs, Set<String> outputs,
            ITable table) {

        boolean allowSchemaDrift = resolveAllowSchemaDrift();

        IAstTransformationAttributes astAttributes = findTransformationAttributes();
        Map<String, IExpression> attributes = astAttributes != null ? astAttributes.resolveAttributes() : new HashMap<>();

        return new SourceTransformation(
                name, substreams,  inputs, outputs, table, attributes, allowSchemaDrift);
    }


    private boolean resolveAllowSchemaDrift(){
        IAstTransformationAttributes attributes = findTransformationAttributes();

        Optional<IAstExpression> value = attributes.findValueByName("allowSchemaDrift");
        if(value.isPresent()){
            try {
                return value.get().resolveAsBoolean();
            }catch (InvalidExpressionTypeException e){
                LOGGER.log(Categories.parsingErrors().invalidExpressionType().expectedType("boolean"));
                return false;
            }

        }else{
            LOGGER.log(Categories.parsingErrors().missingTransformationAttribute().expectedAttribute("allowSchemaDrift"));
            return false;
        }
    }

    @Override
    protected ITable createTable() {

        IAstOutputAttribute outputAttribute = findOutputAttribute();

        // Check if output attribute is defined. If not table is dynamicaly generated
        if(outputAttribute == null){
            // TODO generate table
            return new Table(Collections.emptySet());
        }
        else{
            List<IColumn> tableColumns = new ArrayList<>();
            List<IAstColumnDefinition> columnDefinitions = outputAttribute.findColumns();
            for(IAstColumnDefinition definition : columnDefinitions){
                IAstIdentifier astName = definition.findColumnName();
                if(astName == null){
                    LOGGER.log(Categories.parsingErrors().missingSourceColumnName());
                }else{
                    IAstDataType astColumnType = definition.findDataType();
                    DataFlowDataType type = astColumnType.getDataType();

                    IAstDataTypeFormat astFormat = definition.findDataTypeFormat();
                    String format = "";
                    if(astFormat != null){
                        format = astFormat.toNormalizedString();
                    }
                    IColumn definitionColumn = new ColumnDef(astName.getIdentifier(), type, format);
                    tableColumns.add(definitionColumn);
                }
            }

            return new Table(tableColumns);
        }
    }

}
