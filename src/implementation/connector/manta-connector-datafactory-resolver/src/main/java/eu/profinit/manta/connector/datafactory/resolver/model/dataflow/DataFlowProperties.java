package eu.profinit.manta.connector.datafactory.resolver.model.dataflow;

import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.dataflow.DataFlowType;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowEndpoint;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowProperties;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowScript;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import java.util.*;

public abstract class DataFlowProperties implements IDataFlowProperties {

    private static final Logger LOGGER = new Logger(DataFlowProperties.class);

    private final IDataFlowScript script;

    private final Map<String, IDataFlowEndpoint> sources;

    private final DataFlowType type;

    protected DataFlowProperties(DataFlowType type, IDataFlowScript script, Map<String, IDataFlowEndpoint> sources) {
        this.script = script;
        this.sources = sources;
        this.type = type;
    }

    @Override
    public IDataFlowScript getScript() {
        return script;
    }

    @Override
    public Map<String, IDataFlowEndpoint> getSources() {
        return sources;
    }

    @Override
    public DataFlowType getType() {
        return type;
    }

    @Override
    public Collection<IReference> getAllReferences() {

        // Get reference for each source
        return addEndpointReferences(getSources());

    }

    /**
     * Returns all references of Collection of endpoints
     * @param endpoints endpoint collection
     * @return collection of references
     */
    protected Collection<IReference> addEndpointReferences(Map<String, IDataFlowEndpoint> endpoints){

        // References should not be duplicated
        Set<IReference> references = new HashSet<>();

        for(IDataFlowEndpoint endpoint : endpoints.values()){

            try {
                references.add(endpoint.getReference());
            }catch (IllegalStateException e){
                LOGGER.log(Categories.generalErrors().invalidEndpointReference());
            }

        }

        return references;
    }
}
