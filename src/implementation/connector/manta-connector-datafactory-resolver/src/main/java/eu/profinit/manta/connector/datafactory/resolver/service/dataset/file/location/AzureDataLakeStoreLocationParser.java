package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file.location;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.AzureDataLakeStoreLocation;

public class AzureDataLakeStoreLocationParser extends FileDatasetLocationParser<AzureDataLakeStoreLocation> {

    @Override
    public AzureDataLakeStoreLocation parseFileDatasetSpecificLocation(IAdfFieldValue fileName, IAdfFieldValue folderPath, JsonNode locationNode) {
        return new AzureDataLakeStoreLocation(folderPath, fileName);
    }
}
