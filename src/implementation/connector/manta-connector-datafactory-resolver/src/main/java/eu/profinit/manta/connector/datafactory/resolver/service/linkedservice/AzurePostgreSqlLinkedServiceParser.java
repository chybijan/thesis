package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AzurePostgreSqlLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for AzurePostgreSqlLinkedService resource.
 */
public class AzurePostgreSqlLinkedServiceParser extends AbstractLinkedServiceParser<AzurePostgreSqlLinkedService> {

    @Override
    protected AzurePostgreSqlLinkedService parseTypeSpecificFields(String name,
                                                                   String description,
                                                                   String type,
                                                                   IParameters parameters,
                                                                   JsonNode typePropertiesNode) {
        IAdfFieldValue connectionString = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            connectionString = processAdfFieldValue(typePropertiesNode.path("connectionString"));
        }

        return new AzurePostgreSqlLinkedService(name, description, parameters, connectionString);
    }
}
