package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstExpression;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstIdentifier;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstTransformationAttribute;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import org.antlr.runtime.Token;

public class AstTransformationAttribute extends DFSNode implements IAstTransformationAttribute {
    public AstTransformationAttribute() {
    }

    public AstTransformationAttribute(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstTransformationAttribute(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    @Override
    public IAstIdentifier findName() {
        return (IAstIdentifier) selectSingleNode("AST_ATTRIBUTE_NAME");
    }

    @Override
    public IAstExpression findValue() {
        return (IAstExpression) selectSingleNode("AST_EXPRESSION");
    }
}
