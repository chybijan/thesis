package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.linkedservice.IAzureBlobStorageLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;

/**
 * Implementation of the AzureBlobStorageLinkedService resource interface.
 */
public class AzureBlobStorageLinkedService extends LinkedService implements IAzureBlobStorageLinkedService {

    /**
     * The connection string.
     * It is mutually exclusive with sasUri, serviceEndpoint property.
     */
    private final IAdfFieldValue connectionString;

    /**
     * SAS URI of the Azure Blob Storage resource.
     * It is mutually exclusive with connectionString, serviceEndpoint property.
     */
    private final IAdfFieldValue sasUri;

    /**
     * Blob service endpoint of the Azure Blob Storage resource.
     * It is mutually exclusive with connectionString, sasUri property.
     */
    private final IAdfFieldValue serviceEndpoint;

    public AzureBlobStorageLinkedService(String name,
                                         String description,
                                         IParameters parameters,
                                         IAdfFieldValue connectionString,
                                         IAdfFieldValue sasUri,
                                         IAdfFieldValue serviceEndpoint) {
        super(name, description, parameters);
        this.connectionString = connectionString;
        this.sasUri = sasUri;
        this.serviceEndpoint = serviceEndpoint;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.AZURE_BLOB_STORAGE;
    }

    @Override
    public IAdfFieldValue getConnectionString() {
        return connectionString;
    }

    @Override
    public IAdfFieldValue getSasUri() {
        return sasUri;
    }

    @Override
    public IAdfFieldValue getServiceEndpoint() {
        return serviceEndpoint;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitAzureBlobStorageLinkedService(this);
    }
}
