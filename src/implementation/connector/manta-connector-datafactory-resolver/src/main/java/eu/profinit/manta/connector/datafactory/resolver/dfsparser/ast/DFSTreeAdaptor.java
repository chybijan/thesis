package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.ast.MantaTreeAdaptor;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.parser.DFSLexer;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenStream;

public class DFSTreeAdaptor
        extends MantaTreeAdaptor<DFSContextState, DFSNode> {

    @Override
    public Object create(Token payload) {
        if (payload instanceof ErrorToken) {
            ErrorToken et = (ErrorToken) payload;
            return new DFSErrorNode(et, et.getInput(), et.getStart(), et.getStop(), et.getException(), getContextState());
        } else {
            return new DFSNode(payload, getContextState());
        }
    }

    @Override
    public Object errorNode(TokenStream input, Token start, Token stop, RecognitionException e) {
        int errorTokenType = DFSLexer.AST_ERROR;
        DFSErrorNode result = new DFSErrorNode(
                createToken(errorTokenType, input.toString(start, stop)),
                input, start, stop, e, getContextState());
        return result;
    }

}
