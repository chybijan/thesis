package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.IAzureFileStorageLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;

/**
 * Implementation of the AmazonRedshiftLinkedService resource interface.
 */
public class AzureFileStorageLinkedService extends LinkedService implements IAzureFileStorageLinkedService {

    /**
     * The connection string. It is mutually exclusive with sasUri property.
     */
    private final IAdfFieldValue connectionString;

    /**
     * Host name of the server.
     */
    private final IAdfFieldValue host;

    /**
     * SAS URI of the Azure File resource. It is mutually exclusive with connectionString property.
     */
    private final IAdfFieldValue sasUri;

    private final IAdfFieldValue snapshot;

    public AzureFileStorageLinkedService(String name,
                                         String description,
                                         IParameters parameters,
                                         IAdfFieldValue connectionString,
                                         IAdfFieldValue host,
                                         IAdfFieldValue sasUri,
                                         IAdfFieldValue snapshot) {
        super(name, description, parameters);
        this.connectionString = connectionString;
        this.host = host;
        this.sasUri = sasUri;
        this.snapshot = snapshot;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.AZURE_FILE_STORAGE;
    }

    @Override
    public IAdfFieldValue getConnectionString() {
        return connectionString;
    }

    @Override
    public IAdfFieldValue getHost() {
        return host;
    }

    @Override
    public IAdfFieldValue getSasUri() {
        return sasUri;
    }

    @Override
    public IAdfFieldValue getSnapshot() {
        return snapshot;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitAzureFileStorageLinkedService(this);
    }
}
