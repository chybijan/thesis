package eu.profinit.manta.connector.datafactory.resolver.service.common;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;

/**
 * Interface for service responsible for parsing Azure Data Factory resource parameters.
 */
public interface ParametersParser {

    /**
     * Parses parameters specification of Azure Data Factory resource.
     *
     * @param parametersNode JSON node representing the parameters' field value
     * @return parsed parameters
     */
    IParameters parseParameters(JsonNode parametersNode);

}
