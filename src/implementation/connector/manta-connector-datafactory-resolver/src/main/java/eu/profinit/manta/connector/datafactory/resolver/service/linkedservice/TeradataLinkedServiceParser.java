package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.TeradataLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for TeradataLinkedService resource.
 */
public class TeradataLinkedServiceParser extends AbstractLinkedServiceParser<TeradataLinkedService> {

    @Override
    protected TeradataLinkedService parseTypeSpecificFields(String name,
                                                            String description,
                                                            String type,
                                                            IParameters parameters,
                                                            JsonNode typePropertiesNode) {
        IAdfFieldValue connectionString = null;
        IAdfFieldValue server = null;
        IAdfFieldValue username = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            connectionString = processAdfFieldValue(typePropertiesNode.path("connectionString"));
            server = processAdfFieldValue(typePropertiesNode.path("server"));
            username = processAdfFieldValue(typePropertiesNode.path("username"));
        }

        return new TeradataLinkedService(name, description, parameters, connectionString, server, username);
    }
}
