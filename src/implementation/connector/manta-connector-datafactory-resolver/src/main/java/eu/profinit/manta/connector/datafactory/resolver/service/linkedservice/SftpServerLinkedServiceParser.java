package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.SftpServerLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for SftpServerLinkedService resource.
 */
public class SftpServerLinkedServiceParser extends AbstractLinkedServiceParser<SftpServerLinkedService> {

    @Override
    protected SftpServerLinkedService parseTypeSpecificFields(String name,
                                                              String description,
                                                              String type,
                                                              IParameters parameters,
                                                              JsonNode typePropertiesNode) {
        IAdfFieldValue host = null;
        IAdfFieldValue port = null;
        IAdfFieldValue serviceUrl = null;
        IAdfFieldValue userName = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            host = processAdfFieldValue(typePropertiesNode.path("host"));
            port = processAdfFieldValue(typePropertiesNode.path("port"));
            userName = processAdfFieldValue(typePropertiesNode.path("userName"));
        }

        return new SftpServerLinkedService(name, description, parameters, host, port, userName);
    }
}
