package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file.location;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.AzureBlobStorageLocation;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

public class AzureBlobStorageLocationParser extends FileDatasetLocationParser<AzureBlobStorageLocation> {

    @Override
    public AzureBlobStorageLocation parseFileDatasetSpecificLocation(IAdfFieldValue fileName, IAdfFieldValue folderPath, JsonNode locationNode) {
        IAdfFieldValue container = null;
        if (JsonUtil.isNotNullAndNotMissing(locationNode)) {
            container = adfFieldValueParser.parseAdfFieldValue(locationNode.path("container"));
        }
        return new AzureBlobStorageLocation(container, folderPath, fileName);
    }
}
