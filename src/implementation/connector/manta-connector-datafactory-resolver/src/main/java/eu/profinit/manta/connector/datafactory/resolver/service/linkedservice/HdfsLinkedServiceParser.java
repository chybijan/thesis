package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.HdfsLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for HdfsLinkedService resource.
 */
public class HdfsLinkedServiceParser extends AbstractLinkedServiceParser<HdfsLinkedService> {

    @Override
    protected HdfsLinkedService parseTypeSpecificFields(String name,
                                                        String description,
                                                        String type,
                                                        IParameters parameters,
                                                        JsonNode typePropertiesNode) {
        IAdfFieldValue url = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            url = processAdfFieldValue(typePropertiesNode.path("url"));
        }

        return new HdfsLinkedService(name, description, parameters, url);
    }
}
