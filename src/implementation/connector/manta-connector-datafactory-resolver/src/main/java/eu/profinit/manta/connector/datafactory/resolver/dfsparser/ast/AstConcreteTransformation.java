package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowScript;
import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumn;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.ITable;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstConcreteTransformation;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstTransformationAttributes;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.datarepresentation.Table;
import eu.profinit.manta.platform.logging.api.logging.Logger;
import org.antlr.runtime.Token;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AstConcreteTransformation extends DFSNode implements IAstConcreteTransformation {

    private static final Logger LOGGER = new Logger(AstConcreteTransformation.class);

    protected AstConcreteTransformation() {
    }

    protected AstConcreteTransformation(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    protected AstConcreteTransformation(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    @Override
    public IAstTransformationAttributes findTransformationAttributes(){
        return (IAstTransformationAttributes) selectSingleNode("AST_TRANSFORMATION_ATTRIBUTES");
    }

    @Override
    public IDataFlowScript resolveConcreteTransformation(Set<IADFStream> inputStreams, Set<IADFStream> thisStreams, IDataFlowScript currentDFS) {

        Optional<IADFStream> streamOpt = thisStreams.stream().findFirst();
        if(streamOpt.isEmpty()){
            LOGGER.log(Categories.parsingErrors().missingTransformationName());
            return currentDFS;
        }

        String name = streamOpt.get().getName();

        Set<String> substreams = thisStreams.stream()
                .map(IADFStream::getSubstreamName)
                .collect(Collectors.toSet());

        Set<String> outputs = new HashSet<>();

        ITable table = createTable();

        ITransformation transformation = createTransformation(
                name, substreams, inputStreams, outputs, table
        );

        return currentDFS.addedTransformation(name, transformation);
    }

    protected ITable createTable() {
        // TODO general column generation
        List<IColumn> tableColumns = new ArrayList<>();
        return new Table(tableColumns);
    }

    protected abstract ITransformation createTransformation(
            String name, Set<String> substreams,
            Set<IADFStream> inputs, Set<String> outputs,
            ITable table
    );

}
