package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file.location;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.GoogleCloudStorageLocation;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

public class GoogleCloudStorageLocationParser extends FileDatasetLocationParser<GoogleCloudStorageLocation> {

    @Override
    public GoogleCloudStorageLocation parseFileDatasetSpecificLocation(IAdfFieldValue fileName, IAdfFieldValue folderPath, JsonNode locationNode) {
        IAdfFieldValue bucketName = null;
        if (JsonUtil.isNotNullAndNotMissing(locationNode)) {
            bucketName = adfFieldValueParser.parseAdfFieldValue(locationNode.path("bucketName"));
        }
        return new GoogleCloudStorageLocation(bucketName, folderPath, fileName);
    }
}
