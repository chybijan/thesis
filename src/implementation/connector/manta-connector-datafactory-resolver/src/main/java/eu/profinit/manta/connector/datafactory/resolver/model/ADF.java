package eu.profinit.manta.connector.datafactory.resolver.model;

import eu.profinit.manta.connector.datafactory.model.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

public class ADF implements IADF {

    Optional<IFactory> factory;

    Optional<IDataFlow> dataFlow;

    Optional<IPipeline> pipeline;

    Collection<IDataset> datasets;

    Collection<ILinkedService> linkedServices;

    public ADF(){
        this.pipeline = Optional.empty();
        datasets = new ArrayList<>();
        linkedServices = new ArrayList<>();
        factory = Optional.empty();
        dataFlow = Optional.empty();
    }

    public ADF(IPipeline pipeline){
        this.pipeline = Optional.of(pipeline);
        datasets = new ArrayList<>();
        linkedServices = new ArrayList<>();
        factory = Optional.empty();
        dataFlow = Optional.empty();
    }

    public ADF(IDataFlow dataset){
        this.dataFlow = Optional.of(dataset);
        datasets = new ArrayList<>();
        linkedServices = new ArrayList<>();
        factory = Optional.empty();
        pipeline = Optional.empty();
    }

    @Override
    public Optional<IFactory> getFactory() {
        return factory;
    }

    @Override
    public Optional<IDataFlow> getDataFlow() {
        return dataFlow;
    }

    @Override
    public Optional<IPipeline> getPipeline() {
        return pipeline;
    }

    @Override
    public Collection<IDataset> getDatasets() {
        return datasets;
    }

    @Override
    public Collection<ILinkedService> getLinkedServices() {
        return linkedServices;
    }

    @Override
    public void addDatasets(Collection<IDataset> datasets) {
        this.datasets.addAll(datasets);
    }

    @Override
    public void addLinkedServices(Collection<ILinkedService> linkedServices) {
        this.linkedServices.addAll(linkedServices);
    }

    @Override
    public void setFactory(IFactory factory) {
        this.factory = Optional.of(factory);
    }

    @Override
    public boolean isValid() {
        // Only one has to be not null to be valid
        return (dataFlow.isPresent()) ^ (pipeline.isPresent());
    }
}
