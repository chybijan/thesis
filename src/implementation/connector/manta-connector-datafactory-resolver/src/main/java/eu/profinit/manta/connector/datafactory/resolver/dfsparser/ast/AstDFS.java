package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowScript;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstParameter;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstDFS;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstTransformation;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import org.antlr.runtime.Token;

import java.util.List;

public class AstDFS extends DFSNode implements IAstDFS {

    public AstDFS() {
    }

    public AstDFS(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstDFS(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    @Override
    public List<IAstParameter> findParameters() {
        return selectNodes("AST_PARAMETERS/AST_PARAMETER");
    }

    @Override
    public List<IAstTransformation> findTransformations() {
        return selectNodes("AST_TRANSFORMATIONS/AST_TRANSFORMATION");
    }

    @Override
    public IDataFlowScript resolve(IDataFlowScript currentDFS) {

        // Fill DFS with parameters
        List<IAstParameter> parameters = findParameters();
        for(IAstParameter parameter : parameters){
            currentDFS = parameter.resolve(currentDFS);
        }

        // Fill DFS with transformations
        List<IAstTransformation> transformations = findTransformations();
        for(IAstTransformation transformation : transformations){
            currentDFS = transformation.resolve(currentDFS);
        }

        return currentDFS;

    }
}
