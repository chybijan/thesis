package eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IUnknownTransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.ITable;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class UnknownTransformation extends AbstractTransformation implements IUnknownTransformation {

    public UnknownTransformation(String name, Set<String> substreams, Collection<IADFStream> inputs, Collection<String> outputs, ITable table, Map<String, IExpression> attributes) {
        super(name, substreams, inputs, outputs, table, attributes);
    }

}
