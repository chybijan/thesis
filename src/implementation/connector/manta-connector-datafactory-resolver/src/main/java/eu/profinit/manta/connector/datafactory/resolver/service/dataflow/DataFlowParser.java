package eu.profinit.manta.connector.datafactory.resolver.service.dataflow;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.IDataFlow;

/**
 * Interface for parser of data flow resource
 */
public interface DataFlowParser {

    /**
     * Parses the data flow resource provided on the input.
     *
     * @param dataFlowNode JSON node representing the root of the data flow resource
     * @return parsed data flow, or null if dataFlowNode is null, is missing
     *         or it contains properties that are not parsed successfully
     */
    IDataFlow parseDataFlow(JsonNode dataFlowNode);

}
