package eu.profinit.manta.connector.datafactory.resolver.service.factory;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.resolver.model.Factory;

/**
 * Interface for parser of Factory resource.
 */
public interface FactoryParser {

    /**
     * Parses the Factory resource provided on the input.
     *
     * @param factoryNode JSON node representing the root of the Factory resource
     * @return parsed factory or null in case the provided node is null or missing
     */
    Factory parseFactory(JsonNode factoryNode);

}
