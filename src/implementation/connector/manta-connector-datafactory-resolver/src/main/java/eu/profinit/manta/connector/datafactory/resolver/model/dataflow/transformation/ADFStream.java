package eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;

import java.util.Objects;

public class ADFStream implements IADFStream {

    private final String name;
    private final String substream;

    public ADFStream(String name, String substream) {
        this.name = name;
        this.substream = substream;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSubstreamName() {
        return substream;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ADFStream adfStream = (ADFStream) o;
        return name.equals(adfStream.name) && substream.equals(adfStream.substream);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, substream);
    }
}
