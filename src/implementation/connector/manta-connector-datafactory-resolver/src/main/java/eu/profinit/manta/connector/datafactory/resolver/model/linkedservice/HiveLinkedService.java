package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.HiveServerType;
import eu.profinit.manta.connector.datafactory.model.linkedservice.IHiveLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;

/**
 * Implementation of the HiveLinkedService resource interface.
 */
public class HiveLinkedService extends LinkedService implements IHiveLinkedService {

    /**
     * IP address or host name of the Hive server, separated by ';' for multiple hosts (only when serviceDiscoveryMode is enable).
     */
    private final IAdfFieldValue host;

    /**
     * The partial URL corresponding to the Hive server.
     */
    private final IAdfFieldValue httpPath;

    /**
     * The TCP port that the Hive server uses to listen for client connections.
     */
    private final IAdfFieldValue port;

    /**
     * The type of Hive server.
     */
    private final HiveServerType serverType;

    /**
     * Specifies whether the driver uses native HiveQL queries,or converts them into an equivalent form in HiveQL.
     */
    private final IAdfFieldValue useNativeQuery;

    /**
     * The user name that you use to access Hive Server.
     */
    private final IAdfFieldValue username;

    public HiveLinkedService(String name,
                             String description,
                             IParameters parameters,
                             IAdfFieldValue host,
                             IAdfFieldValue httpPath,
                             IAdfFieldValue port,
                             HiveServerType serverType,
                             IAdfFieldValue useNativeQuery,
                             IAdfFieldValue username) {
        super(name, description, parameters);
        this.host = host;
        this.httpPath = httpPath;
        this.port = port;
        this.serverType = serverType;
        this.useNativeQuery = useNativeQuery;
        this.username = username;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.HIVE;
    }

    @Override
    public IAdfFieldValue getHost() {
        return host;
    }

    @Override
    public IAdfFieldValue getHttpPath() {
        return httpPath;
    }

    @Override
    public IAdfFieldValue getPort() {
        return port;
    }

    @Override
    public HiveServerType getServerType() {
        return serverType;
    }

    @Override
    public IAdfFieldValue getUseNativeQuery() {
        return useNativeQuery;
    }

    @Override
    public IAdfFieldValue getUsername() {
        return username;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitHiveLinkedService(this);
    }
}
