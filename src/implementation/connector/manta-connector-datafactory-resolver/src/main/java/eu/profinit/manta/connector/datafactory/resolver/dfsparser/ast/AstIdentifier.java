package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstIdentifier;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import org.antlr.runtime.Token;

import java.util.stream.Collectors;

public class AstIdentifier extends DFSNode implements IAstIdentifier {
    public AstIdentifier() {
    }

    public AstIdentifier(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstIdentifier(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    @Override
    public String getIdentifier() {

        if(getChildren().isEmpty())
        {
            return "";
        }
        else{
            return toNormalizedString();
        }
    }
}
