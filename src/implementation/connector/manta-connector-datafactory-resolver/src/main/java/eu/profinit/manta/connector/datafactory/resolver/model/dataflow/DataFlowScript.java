package eu.profinit.manta.connector.datafactory.resolver.model.dataflow;

import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowScript;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataflowParameter;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;

import java.util.*;

public class DataFlowScript implements IDataFlowScript {

    private final Collection<IDataflowParameter> parameters;

    private final Map<String, ITransformation> transformations;

    /**
     * Inputs waiting to be connected. Entry is added to this map if some transformation did not find it's input transformation in this script
     * Key: Input transformation name
     * Value: Output transformation names
     */
    private final Map<String, Set<String>> unprocessedInputs;

    /**
     * If during parsing were any errors.
     */
    private final boolean parsedWithoutErrors;

    public DataFlowScript(Collection<IDataflowParameter> parameters, Map<String, ITransformation> transformations, Map<String, Set<String>> unprocessedInputs, Boolean parsedWithoutErrors) {
        this.parameters = parameters;
        this.transformations = transformations;
        this.unprocessedInputs = unprocessedInputs;
        this.parsedWithoutErrors = parsedWithoutErrors;
    }

    public DataFlowScript(Collection<IDataflowParameter> parameters, Map<String, ITransformation> transformations) {
        this.parameters = parameters;
        this.transformations = transformations;
        unprocessedInputs = Collections.emptyMap();
        this.parsedWithoutErrors = true;
    }

    @Override
    public Collection<IDataflowParameter> getParameters() {
        return parameters;
    }

    @Override
    public Map<String, ITransformation> getTransformations() {
        return transformations;
    }

    @Override
    public Optional<ITransformation> getTransformation(String name) {
        ITransformation transformation = transformations.get(name);
        return (transformation != null) ? Optional.of(transformation) : Optional.empty();
    }

    @Override
    public Optional<IDataflowParameter> getParameter(String name) {
        for(IDataflowParameter parameter : parameters){
            if(parameter.getName().equals(name)){
                return Optional.of(parameter);
            }
        }

        return Optional.empty();
    }

    @Override
    public IDataFlowScript updatedParameters(Collection<IDataflowParameter> newParameters) {
        return new DataFlowScript(newParameters, transformations, unprocessedInputs, parsedWithoutErrors);
    }

    @Override
    public IDataFlowScript updatedTransformations(Map<String, ITransformation> newTransformations) {

        IDataFlowScript newDFS = new DataFlowScript(parameters, Collections.emptyMap(), Collections.emptyMap(), parsedWithoutErrors);

        for(Map.Entry<String, ITransformation> newTransformation : newTransformations.entrySet()){
            newDFS = newDFS.addedTransformation(newTransformation.getKey(), newTransformation.getValue());
        }

        return newDFS;
    }

    /**
     * Updates transformations but does not check for DFS invariant
     * @param newTransformations transformations to set
     * @return updated data flow script
     */
    private IDataFlowScript updatedTransformationsUnchecked(Map<String, ITransformation> newTransformations,
                                                            Map<String, Set<String>> newUnprocessed) {
        return new DataFlowScript(parameters, newTransformations, newUnprocessed, parsedWithoutErrors);
    }

    @Override
    public IDataFlowScript addedParameter(IDataflowParameter newParameter) {
        List<IDataflowParameter> newParameters = new ArrayList<>(getParameters());
        newParameters.add(newParameter);
        return updatedParameters(newParameters);
    }

    @Override
    public IDataFlowScript addedTransformation(String transformationId, ITransformation newTransformation) {

        // Check if some previous transformation had this transformation as it's input
        Map<String, Set<String>> newUnprocessed = new HashMap<>(unprocessedInputs);
        if(newUnprocessed.containsKey(transformationId)){
            Set<String> outputs = newUnprocessed.get(transformationId);
            for(String output: outputs){
                newTransformation.addOutput(output);
            }
            newUnprocessed.remove(transformationId);
            // TODO test if this works
        }

        Map<String, ITransformation> newTransformations = new HashMap<>(getTransformations());
        newTransformations.put(transformationId, newTransformation);

        // Find previous transformations
        for(IADFStream inputStream : newTransformation.getInputs()) {
            ITransformation inputTransformation = newTransformations.get(inputStream.getName());

            if(inputTransformation != null){
                // Input is already processed
                inputTransformation.addOutput(transformationId);}
            else {
                // Input is not processed yet.
                newUnprocessed = addNotProcessedInput(newUnprocessed, inputStream.getName(), transformationId);
            }
            // TODO check if this works
        }

        return updatedTransformationsUnchecked(newTransformations, newUnprocessed);
    }

    /**
     * Adds new unprocessed input name for it's processing
     * @param initialUnprocessed initial unprocessed inputs map
     * @param inputName
     * @param transformationId
     * @return updated UnprocessedInput map
     */
    private Map<String, Set<String>> addNotProcessedInput(Map<String, Set<String>> initialUnprocessed, String inputName, String transformationId) {

        Map<String, Set<String>> newUnprocessed = new HashMap<>(initialUnprocessed);

        if(newUnprocessed.containsKey(inputName)){
            newUnprocessed.get(inputName).add(transformationId);
        }else{
            Set<String> outputs = new HashSet<>();
            outputs.add(transformationId);
            newUnprocessed.put(inputName, outputs);
        }

        return newUnprocessed;

    }

    @Override
    public boolean isParsedWithoutErrors() {
        return parsedWithoutErrors;
    }

    @Override
    public IDataFlowScript updatedParsedWithoutErrors(boolean parsedWithoutErrors) {
        return new DataFlowScript(parameters, transformations, unprocessedInputs, parsedWithoutErrors);
    }

}
