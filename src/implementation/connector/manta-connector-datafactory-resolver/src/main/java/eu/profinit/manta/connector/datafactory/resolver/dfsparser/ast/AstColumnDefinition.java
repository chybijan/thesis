package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstColumnDefinition;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstDataType;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstDataTypeFormat;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstIdentifier;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import org.antlr.runtime.Token;

public class AstColumnDefinition extends DFSNode implements IAstColumnDefinition {

    public AstColumnDefinition() {
    }

    public AstColumnDefinition(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstColumnDefinition(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    @Override
    public IAstIdentifier findColumnName() {
        return (IAstIdentifier) selectSingleNode("AST_COLUMN_NAME");
    }

    @Override
    public IAstDataType findDataType() {
        return (IAstDataType) selectSingleNode("AST_DATA_TYPE");
    }

    @Override
    public IAstDataTypeFormat findDataTypeFormat() {
        return (IAstDataTypeFormat) selectSingleNode("AST_DATA_TYPE_FORMAT");
    }
}
