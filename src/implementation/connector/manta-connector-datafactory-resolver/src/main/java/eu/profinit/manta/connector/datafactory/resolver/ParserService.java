package eu.profinit.manta.connector.datafactory.resolver;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.IDataFlow;
import eu.profinit.manta.connector.datafactory.model.IDataset;
import eu.profinit.manta.connector.datafactory.model.IFactory;
import eu.profinit.manta.connector.datafactory.model.ILinkedService;

/**
 * Interface for parser service for Azure Data Factory resources.
 */
public interface ParserService {

    /**
     * Parses the linked service resource.
     * <br>
     * The provided JSON node represents the root of the linked service resource.
     *
     * @param linkedServiceNode JSON node representing the root of the linked service resource
     * @return instance of the linked service
     */
    ILinkedService parseLinkedService(JsonNode linkedServiceNode);

    /**
     * Parses the factory resource.
     * <br>
     * The provided JSON node represents the root of the factory resource.
     *
     * @param factoryNode JSON node representing the root of the factory resource
     * @return instance of the factory
     */
    IFactory parseFactory(JsonNode factoryNode);

    /**
     * Parses the data flow resource
     * <br>
     * The provided JSON node represents the root of the data flow resource
     *
     * @param dataFlowNode JSON node representing the root of the data flow resource
     * @return instance of the data flow, or null if dataFlowNode is not parsed correctly
     */
    IDataFlow parseDataFlow(JsonNode dataFlowNode);

    /**
     * Parses the dataset resource.
     * <br>
     * The provided JSON node represents the root of the dataset resource.
     *
     * @param datasetNode JSON node representing the root of the dataset resource
     * @return instance of the dataset
     */
    IDataset parseDataset(JsonNode datasetNode);

}
