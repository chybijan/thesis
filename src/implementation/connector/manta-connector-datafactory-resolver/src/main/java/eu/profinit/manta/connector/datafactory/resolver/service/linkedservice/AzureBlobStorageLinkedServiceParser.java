package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AzureBlobStorageLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for AmazonRedshiftLinkedService resource.
 */
public class AzureBlobStorageLinkedServiceParser extends AbstractLinkedServiceParser<AzureBlobStorageLinkedService> {

    @Override
    protected AzureBlobStorageLinkedService parseTypeSpecificFields(String name,
                                                                    String description,
                                                                    String type,
                                                                    IParameters parameters,
                                                                    JsonNode typePropertiesNode) {
        IAdfFieldValue connectionString = null;
        IAdfFieldValue sasUri = null;
        IAdfFieldValue serviceEndpoint = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            connectionString = processAdfFieldValue(typePropertiesNode.path("connectionString"));
            sasUri = processAdfFieldValue(typePropertiesNode.path("sasUri"));
            serviceEndpoint = processAdfFieldValue(typePropertiesNode.path("serviceEndpoint"));
        }

        return new AzureBlobStorageLinkedService(name, description, parameters, connectionString, sasUri, serviceEndpoint);
    }
}
