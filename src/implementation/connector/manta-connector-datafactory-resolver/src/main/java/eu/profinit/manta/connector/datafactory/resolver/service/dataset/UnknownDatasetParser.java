package eu.profinit.manta.connector.datafactory.resolver.service.dataset;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.UnknownDataset;
import eu.profinit.manta.platform.logging.api.logging.Logger;

/**
 * Parser for unrecognizable or unsupported Dataset resources by Manta.
 */
public class UnknownDatasetParser extends AbstractDatasetParser<UnknownDataset> {

    private static final Logger LOGGER = new Logger(UnknownDatasetParser.class);

    @Override
    protected UnknownDataset parseDatasetSpecificFields(String name,
                                                        String type,
                                                        IReference lsName,
                                                        IParameters parameters,
                                                        IDatasetSchema datasetSchema,
                                                        JsonNode datasetStructureNode,
                                                        JsonNode typePropertiesNode) {
        LOGGER.log(Categories.parsingErrors()
                .unsupportedDatasetType()
                .datasetName(name)
                .datasetType(type)
        );
        return new UnknownDataset(name, lsName, parameters, datasetSchema, datasetStructureNode);
    }
}
