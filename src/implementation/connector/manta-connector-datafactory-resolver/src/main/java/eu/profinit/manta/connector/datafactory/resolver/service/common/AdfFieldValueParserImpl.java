package eu.profinit.manta.connector.datafactory.resolver.service.common;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.AdfFieldType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.model.common.AdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;


public class AdfFieldValueParserImpl implements AdfFieldValueParser {

    protected static final String TYPE_PATH = "type";

    protected static final String VALUE_PATH = "value";


    protected static final String EXPRESSION_TYPE = "Expression";

    @Override
    public IAdfFieldValue parseAdfFieldValue(JsonNode adfFieldValueNode) {


        IAdfFieldValue adfFieldValue = null;
        if (JsonUtil.isNotNullAndNotMissing(adfFieldValueNode)) {

            // Set default values
            AdfFieldType type = AdfFieldType.STRING;
            String rawValue = adfFieldValueNode.toString();

            // Find it's type
            if(adfFieldValueNode.isObject()){

                type = getObjectType(adfFieldValueNode);

                if(type.equals(AdfFieldType.EXPRESSION)){
                    rawValue = loadExpressionFromNode(adfFieldValueNode, rawValue);
                }

            }
            else if(adfFieldValueNode.isArray()){

                type = AdfFieldType.ARRAY;

            }
            else if(adfFieldValueNode.isInt()){

                type = AdfFieldType.INT;

            }
            else if(adfFieldValueNode.isFloatingPointNumber()){

                type = AdfFieldType.FLOAT;

            }
            else{
                String realStringValue = adfFieldValueNode.textValue();
                if(realStringValue != null){
                    rawValue = realStringValue;
                }
            }

            adfFieldValue = new AdfFieldValue(type, rawValue);

        }
        return adfFieldValue;

    }

    AdfFieldType getObjectType(JsonNode objectNode){

        String strType = JsonUtil.pathAsText(objectNode, TYPE_PATH);
        if(strType != null && strType.equals(EXPRESSION_TYPE)){

            return AdfFieldType.EXPRESSION;

        }
        else{
            return AdfFieldType.OBJECT;
        }

    }

    String loadExpressionFromNode(JsonNode expressionNode, String defaultValue){

        // Load expression string
        String optStringValue = JsonUtil.pathAsText(expressionNode, VALUE_PATH);

        if(optStringValue != null){
            return optStringValue;
        }

        return defaultValue;
    }

}
