package eu.profinit.manta.connector.datafactory.resolver.model.dataflow;

import eu.profinit.manta.connector.datafactory.model.dataflow.DataFlowDataType;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataflowParameter;
import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;

public class DataflowParameter implements IDataflowParameter {

    private final String name;
    private final DataFlowDataType type;
    private final IExpression defaultValue;

    public DataflowParameter(String name, DataFlowDataType type, IExpression defaultValue) {
        this.name = name;
        this.type = type;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public DataFlowDataType getDataType() {
        return type;
    }

    @Override
    public IExpression getDefaultValue() {
        return defaultValue;
    }
}
