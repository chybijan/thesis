package eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.datarepresentation;

import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumn;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.ITable;

import java.util.Collection;

public class Table implements ITable {

    private final Collection<IColumn> columns;

    public Table(Collection<IColumn> columns) {
        this.columns = columns;
    }

    @Override
    public Collection<IColumn> getColumns() {
        return columns;
    }
}
