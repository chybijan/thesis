package eu.profinit.manta.connector.datafactory.resolver.service.dataset;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.resolver.model.Dataset;

/**
 * Interface for all parsers of the Dataset resource.
 *
 * @param <T> class for Dataset resource, that can be parsed by the particular parser implementation
 */
public interface DatasetParser<T extends Dataset> {

    T parseDataset(JsonNode datasetNode);

}
