package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.PostgreSqlLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for PostgreSqlLinkedService resource.
 */
public class PostgreSqlLinkedServiceParser extends AbstractLinkedServiceParser<PostgreSqlLinkedService> {

    @Override
    protected PostgreSqlLinkedService parseTypeSpecificFields(String name,
                                                              String description,
                                                              String type,
                                                              IParameters parameters,
                                                              JsonNode typePropertiesNode) {
        IAdfFieldValue connectionString = null;
        IAdfFieldValue server = null;
        IAdfFieldValue database = null;
        IAdfFieldValue username = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            connectionString = processAdfFieldValue(typePropertiesNode.path("connectionString"));
            server = processAdfFieldValue(typePropertiesNode.path("server"));
            database = processAdfFieldValue(typePropertiesNode.path("database"));
            username = processAdfFieldValue(typePropertiesNode.path("username"));
        }

        return new PostgreSqlLinkedService(name, description, parameters, connectionString, server, database, username);
    }
}
