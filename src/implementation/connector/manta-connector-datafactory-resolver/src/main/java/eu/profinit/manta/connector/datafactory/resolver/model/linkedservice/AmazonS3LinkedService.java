package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.linkedservice.IAmazonS3LinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;

/**
 * Implementation of the AmazonS3LinkedService resource interface.
 */
public class AmazonS3LinkedService extends LinkedService implements IAmazonS3LinkedService {

    private final IAdfFieldValue serviceUrl;

    public AmazonS3LinkedService(String name,
                                 String description,
                                 IParameters parameters,
                                 IAdfFieldValue serviceUrl) {
        super(name, description, parameters);
        this.serviceUrl = serviceUrl;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.AMAZON_S3;
    }

    @Override
    public IAdfFieldValue getServiceUrl() {
        return serviceUrl;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitAmazonS3LinkedService(this);
    }
}
