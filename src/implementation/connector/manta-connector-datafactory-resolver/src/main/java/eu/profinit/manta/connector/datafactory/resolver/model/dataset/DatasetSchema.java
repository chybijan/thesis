package eu.profinit.manta.connector.datafactory.resolver.model.dataset;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;

import java.util.List;

/**
 * Implementation of the physical type schema of the dataset.
 */
public class DatasetSchema implements IDatasetSchema {

    /**
     * Array of data elements representing the dataset schema.
     * <br>
     * This is field is mutual exclusive with {@link IDatasetSchema#expression}.
     */
    private final List<IDataElement> dataElements;

    /**
     * Expression with resultType array.
     * <br>
     * This is field is mutual exclusive with {@link IDatasetSchema#dataElements}.
     */
    private final IAdfFieldValue expression;

    public DatasetSchema(List<IDataElement> dataElements, IAdfFieldValue expression) {
        this.dataElements = dataElements;
        this.expression = expression;
    }

    @Override
    public List<IDataElement> getDataElements() {
        return dataElements;
    }

    @Override
    public IAdfFieldValue getExpression() {
        return expression;
    }

    /**
     * Implementation of the single element in the dataset schema representation.
     */
    public static class DataElement implements IDataElement{

        /**
         * Name of the data element.
         */
        private final IAdfFieldValue name;

        /**
         * Type of the data element.
         */
        private final IAdfFieldValue type;

        public DataElement(IAdfFieldValue name, IAdfFieldValue type) {
            this.name = name;
            this.type = type;
        }

        @Override
        public IAdfFieldValue getName() {
            return name;
        }

        @Override
        public IAdfFieldValue getType() {
            return type;
        }
    }
}
