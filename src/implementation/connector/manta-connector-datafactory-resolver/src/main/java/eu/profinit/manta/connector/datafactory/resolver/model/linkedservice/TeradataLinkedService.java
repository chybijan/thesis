package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.linkedservice.ITeradataLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;

/**
 * Implementation of the TeradataLinkedService resource interface.
 */
public class TeradataLinkedService extends LinkedService implements ITeradataLinkedService {

    /**
     * Teradata ODBC connection string.
     */
    private final IAdfFieldValue connectionString;

    /**
     * Server name for connection.
     */
    private final IAdfFieldValue server;

    /**
     * Username for authentication.
     */
    private final IAdfFieldValue username;

    public TeradataLinkedService(String name,
                                 String description,
                                 IParameters parameters,
                                 IAdfFieldValue connectionString,
                                 IAdfFieldValue server,
                                 IAdfFieldValue username) {
        super(name, description, parameters);
        this.connectionString = connectionString;
        this.server = server;
        this.username = username;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.TERADATA;
    }

    @Override
    public IAdfFieldValue getConnectionString() {
        return connectionString;
    }

    @Override
    public IAdfFieldValue getServer() {
        return server;
    }

    @Override
    public IAdfFieldValue getUsername() {
        return username;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitTeradataLinkedService(this);
    }
}
