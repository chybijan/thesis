package eu.profinit.manta.connector.datafactory.resolver.model;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.IDataset;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.dataset.DatasetType;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Implementation of the Dataset resource interface.
 */
public abstract class Dataset implements IDataset {

    /**
     * The dataset resource name.
     */
    private final String name;

    /**
     * Linked service reference.
     */
    private final IReference linkedServiceName;

    /**
     * IParameters for dataset.
     */
    private final IParameters parameters;

    /**
     * Columns that define the physical type schema of the dataset.
     */
    private final IDatasetSchema datasetSchema;

    /**
     * Columns that define the structure of the dataset.
     */
    private final JsonNode datasetStructure;

    /**
     * @param name              the dataset resource name
     * @param linkedServiceName linked service reference
     * @param parameters        parameters for dataset
     * @param datasetSchema     columns that define the physical type schema of the dataset
     * @param datasetStructure  columns that define the structure of the dataset
     */
    protected Dataset(String name,
                   IReference linkedServiceName,
                   IParameters parameters,
                   IDatasetSchema datasetSchema,
                   JsonNode datasetStructure) {
        this.name = name;
        this.linkedServiceName = linkedServiceName;
        this.parameters = parameters;
        this.datasetSchema = datasetSchema;
        this.datasetStructure = datasetStructure;
    }

    @Override
    public String getName() {
        return name;
    }

    public IReference getLinkedServiceReference() {
        return linkedServiceName;
    }

    public IParameters getParameters() {
        return parameters;
    }

    public IDatasetSchema getDatasetSchema() {
        return datasetSchema;
    }

    public JsonNode getDatasetStructure() {
        return datasetStructure;
    }

    @Override
    public Collection<IReference> getAllReferences() {

        Collection<IReference> references = new ArrayList<>();

        if(linkedServiceName != null){
            references.add(linkedServiceName);
        }

        return references;

    }
}
