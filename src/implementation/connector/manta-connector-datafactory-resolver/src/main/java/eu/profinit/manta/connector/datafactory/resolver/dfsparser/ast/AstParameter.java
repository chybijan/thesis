package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.DataFlowDataType;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowScript;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataflowParameter;
import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstDataType;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstExpression;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstParameter;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstIdentifier;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.DataflowParameter;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.VoidExpression;
import eu.profinit.manta.platform.logging.api.logging.Logger;
import org.antlr.runtime.Token;

import java.util.ArrayList;
import java.util.List;

public class AstParameter extends DFSNode implements IAstParameter {

    private static final Logger LOGGER = new Logger(AstParameter.class);

    public AstParameter() {
    }

    public AstParameter(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstParameter(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    @Override
    public IAstIdentifier findName() {
        return (IAstIdentifier) selectSingleNode("AST_PARAMETER_NAME");
    }

    @Override
    public IAstDataType findType() {
        return (IAstDataType) selectSingleNode("AST_DATA_TYPE");
    }

    @Override
    public IAstExpression findDefaultValue() {
        return (IAstExpression) selectSingleNode("AST_PARAMETER_DEFAULT_VALUE/AST_EXPRESSION");
    }

    @Override
    public IDataFlowScript resolve(IDataFlowScript currentDFS) {
        IAstIdentifier astName = findName();
        IAstDataType astType = findType();
        IAstExpression astExpression = findDefaultValue();

        List<IDataflowParameter> newParameters = new ArrayList<>(currentDFS.getParameters());

        String name = astName.getIdentifier();

        DataFlowDataType type = DataFlowDataType.UNKNOWN_TYPE;
        try {
            type = astType.getDataType();
        }catch (IllegalArgumentException e){
            LOGGER.log(Categories.parsingErrors()
                .unknownDataFlowParameterType()
                .parameterType(astType.toNormalizedString())
                .parameterName(name));
        }

        IExpression expression = astExpression != null ? astExpression : new VoidExpression();

        IDataflowParameter parameter = new DataflowParameter(name, type, expression);

        newParameters.add(parameter);

        return currentDFS.updatedParameters(newParameters);
    }
}
