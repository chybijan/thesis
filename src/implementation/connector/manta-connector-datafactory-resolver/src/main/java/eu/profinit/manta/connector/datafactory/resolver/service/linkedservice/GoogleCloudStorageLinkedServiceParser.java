package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.GoogleCloudStorageLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for GoogleCloudStorageLinkedService resource.
 */
public class GoogleCloudStorageLinkedServiceParser extends AbstractLinkedServiceParser<GoogleCloudStorageLinkedService> {

    @Override
    protected GoogleCloudStorageLinkedService parseTypeSpecificFields(String name,
                                                                      String description,
                                                                      String type,
                                                                      IParameters parameters,
                                                                      JsonNode typePropertiesNode) {
        IAdfFieldValue serviceUrl = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            serviceUrl = processAdfFieldValue(typePropertiesNode.path("serviceUrl"));
        }

        return new GoogleCloudStorageLinkedService(name, description, parameters, serviceUrl);
    }
}
