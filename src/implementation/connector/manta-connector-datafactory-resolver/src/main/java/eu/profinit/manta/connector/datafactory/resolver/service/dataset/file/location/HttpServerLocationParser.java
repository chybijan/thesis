package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file.location;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.HttpServerLocation;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

public class HttpServerLocationParser extends DatasetLocationParser<HttpServerLocation> {

    @Override
    public HttpServerLocation parseDatasetLocation(JsonNode locationNode) {
        IAdfFieldValue relativeUrl = null;
        if (JsonUtil.isNotNullAndNotMissing(locationNode)) {
            relativeUrl = adfFieldValueParser.parseAdfFieldValue(locationNode.path("relativeUrl"));
        }
        return new HttpServerLocation(relativeUrl);
    }
}
