package eu.profinit.manta.connector.datafactory.resolver.model.factory;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.factory.GlobalParameterType;
import eu.profinit.manta.connector.datafactory.model.factory.IGlobalParameterSpecification;

/**
 * Implementation of GlobalParameterSpecification resource interface.
 */
public class GlobalParameterSpecification implements IGlobalParameterSpecification {

    /**
     * Value of the global parameter.
     * <br>
     * It can be atomic value of any data type, array, object.
     */
    private final JsonNode value;

    /**
     * Type of the global parameter.
     */
    private final GlobalParameterType type;

    /**
     * @param value value of the global parameter
     * @param type  type of the global parameter
     */
    public GlobalParameterSpecification(JsonNode value, GlobalParameterType type) {
        this.value = value;
        this.type = type;
    }

    @Override
    public JsonNode getValue() {
        return value;
    }

    @Override
    public GlobalParameterType getType() {
        return type;
    }
}
