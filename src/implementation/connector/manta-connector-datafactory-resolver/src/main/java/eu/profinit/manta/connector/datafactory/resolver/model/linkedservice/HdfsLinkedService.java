package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.IHdfsLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;

/**
 * Implementation of the HdfsLinkedService resource interface.
 */
public class HdfsLinkedService extends LinkedService implements IHdfsLinkedService {

    /**
     * The URL of the HDFS service endpoint, e.g. http://myhostname:50070/webhdfs/v1 .
     */
    private final IAdfFieldValue url;

    public HdfsLinkedService(String name,
                             String description,
                             IParameters parameters,
                             IAdfFieldValue url) {
        super(name, description, parameters);
        this.url = url;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.HDFS;
    }

    @Override
    public IAdfFieldValue getUrl() {
        return url;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitHdfsLinkedService(this);
    }
}
