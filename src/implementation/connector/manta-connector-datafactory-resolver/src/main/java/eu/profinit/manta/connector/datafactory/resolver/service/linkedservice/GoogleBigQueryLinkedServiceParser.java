package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.GoogleBigQueryLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for GoogleBigQueryLinkedService resource.
 */
public class GoogleBigQueryLinkedServiceParser extends AbstractLinkedServiceParser<GoogleBigQueryLinkedService> {

    @Override
    protected GoogleBigQueryLinkedService parseTypeSpecificFields(String name,
                                                                  String description,
                                                                  String type,
                                                                  IParameters parameters,
                                                                  JsonNode typePropertiesNode) {
        IAdfFieldValue project = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            project = processAdfFieldValue(typePropertiesNode.path("project"));

        }
        return new GoogleBigQueryLinkedService(name, description, parameters, project);
    }
}
