package eu.profinit.manta.connector.datafactory.resolver.service;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Util class for working with com.fasterxml.jackson API.
 */
public class JsonUtil {

    private JsonUtil(){}

    /**
     * Combines {@link JsonNode#path} and {@link JsonNode#asText()} into one.
     * <br>
     * Applies {@link JsonNode#path} method to the provided JSON node with the provided path value.
     * After that applies {@link JsonNode#asText()} to the found JSON node.
     *
     * @param node node to apply {@link JsonNode#path} method to
     * @param path path to use in {@link JsonNode#path} method
     * @return result of {@link JsonNode#asText()} to the found JSON node, or null in case the provided JSON node is null or missing
     */
    public static String pathAsText(JsonNode node, String path) {
        if (node != null && path != null) {
            return node.path(path).asText();
        }
        return null;
    }

    /**
     * Checks that the provided node is not {@code null} and is not missing node with {@link JsonNode#isMissingNode()}.
     *
     * @param node node to check
     * @return true if not null and not missing node, false otherwise
     */
    public static boolean isNotNullAndNotMissing(JsonNode node) {
        return node != null && !node.isMissingNode();
    }

}
