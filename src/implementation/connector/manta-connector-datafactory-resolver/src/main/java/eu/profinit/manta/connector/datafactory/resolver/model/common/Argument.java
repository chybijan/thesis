package eu.profinit.manta.connector.datafactory.resolver.model.common;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IArgument;

/**
 * Implementation of argument of the Azure Data Factory resource.
 * <br>
 * Consists of formal name of the parameter specified by the target resource, and actual value.
 */
public class Argument implements IArgument {

    /**
     * Formal name of the parameter specified by the target resource.
     */
    private final String name;

    /**
     * Adf field with
     */
    private final IAdfFieldValue value;

    /**
     * @param name  formal name of the parameter specified by the target resource
     * @param value JSON node representing value of the argument
     */
    public Argument(String name, IAdfFieldValue value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public IAdfFieldValue getFieldValue() {
        return value;
    }
}
