package eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.IAzureBlobStorageLocation;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.LocationType;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.visitor.ILocationVisitor;

/**
 * Implementation of the DatasetLocation resource of type AzureBlobStorage.
 */
public class AzureBlobStorageLocation extends FileDatasetLocation implements IAzureBlobStorageLocation {

    /**
     * The blob container.
     */
    private final IAdfFieldValue container;

    /**
     * @param container  the blob container
     * @param folderPath see {@link FileDatasetLocation#FileDatasetLocation(IAdfFieldValue, IAdfFieldValue)}
     * @param fileName   see {@link FileDatasetLocation#FileDatasetLocation(IAdfFieldValue, IAdfFieldValue)}
     */
    public AzureBlobStorageLocation(IAdfFieldValue container, IAdfFieldValue folderPath, IAdfFieldValue fileName) {
        super(folderPath, fileName);
        this.container = container;
    }

    @Override
    public LocationType getLocationType() {
        return LocationType.AZURE_BLOB_STORAGE_LOCATION;
    }

    @Override
    public IAdfFieldValue getContainer() {
        return container;
    }

    @Override
    public <T> T accept(ILocationVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
