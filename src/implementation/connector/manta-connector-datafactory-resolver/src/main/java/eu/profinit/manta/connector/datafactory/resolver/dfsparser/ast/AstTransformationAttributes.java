package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstExpression;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstIdentifier;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstTransformationAttribute;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstTransformationAttributes;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import org.antlr.runtime.Token;

import java.util.*;

public class AstTransformationAttributes extends DFSNode implements IAstTransformationAttributes {

    public AstTransformationAttributes() {
    }

    public AstTransformationAttributes(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstTransformationAttributes(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    private List<IAstTransformationAttribute> findTransformationAttributes(){
        return selectNodes("AST_TRANSFORMATION_ATTRIBUTE");
    }

    @Override
    public Optional<IAstExpression> findValueByName(String attributeName) {
        Optional<IAstTransformationAttribute> optAttribute = findAttributeByName(attributeName);
        return optAttribute.map(IAstTransformationAttribute::findValue);
    }

    @Override
    public Optional<IAstTransformationAttribute> findAttributeByName(String attributeName) {
        List<IAstTransformationAttribute> attributes = findTransformationAttributes();

        for(IAstTransformationAttribute attribute : attributes){
            IAstIdentifier astName = attribute.findName();
            if (astName != null && Objects.equals(attributeName, astName.getIdentifier()))
                return Optional.of(attribute);
        }

        return Optional.empty();
    }

    @Override
    public Map<String, IExpression> resolveAttributes() {

        Map<String, IExpression> attributes = new HashMap<>();

        List<IAstTransformationAttribute> astAttributes = findTransformationAttributes();

        for(IAstTransformationAttribute astAttribute : astAttributes){
            String name = astAttribute.findName().getIdentifier();
            IExpression value = astAttribute.findValue();

            attributes.put(name, value);
        }

        return attributes;
    }
}
