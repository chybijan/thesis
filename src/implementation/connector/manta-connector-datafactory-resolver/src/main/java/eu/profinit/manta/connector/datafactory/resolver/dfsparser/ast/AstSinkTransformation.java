package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.ITable;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstSinkTransformation;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstTransformationAttributes;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.SinkTransformation;
import org.antlr.runtime.Token;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AstSinkTransformation extends AstConcreteTransformation implements IAstSinkTransformation{

    public AstSinkTransformation() {
    }

    public AstSinkTransformation(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstSinkTransformation(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    @Override
    protected ITransformation createTransformation(
            String name, Set<String> substreams,
            Set<IADFStream> inputs, Set<String> outputs,
            ITable table) {

        IAstTransformationAttributes astAttributes = findTransformationAttributes();
        Map<String, IExpression> attributes = astAttributes != null ? astAttributes.resolveAttributes() : new HashMap<>();

        return new SinkTransformation(
                name, substreams,  inputs, outputs, table, attributes);
    }

}
