package eu.profinit.manta.connector.datafactory.resolver.model.common;

import eu.profinit.manta.connector.datafactory.model.common.AdfFieldType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

/**
 * Implementation of {@link IAdfFieldValue} represents the value of the Azure Data Factory resource field.
 * <br>
 * Value of the Azure Data Factory resource field can be one of the following:
 * <ul>
 *     <li>
 *         atomic value of any data type,
 *     </li>
 *     <li>
 *         expression resulting in the atomic value of the corresponding type.
 *     </li>
 * </ul>
 */
public class AdfFieldValue implements IAdfFieldValue {

    /**
     * JSON node representing value of the field.
     */
    private final String rawValue;

    /**
     * Type off field
     */
    private final AdfFieldType type;

    public AdfFieldValue(AdfFieldType type, String rawValue) {
        this.rawValue = rawValue;
        this.type = type;
    }

    /**
     * Alternative to toString method
     *
     * @return string representation of field
     */
    @Override
    public String getStringValue() {
        return rawValue;
    }

    // TODO Value could be later expanded to contain all data types instead of string representation of the value

    @Override
    public AdfFieldType getType() {
        return type;
    }

}
