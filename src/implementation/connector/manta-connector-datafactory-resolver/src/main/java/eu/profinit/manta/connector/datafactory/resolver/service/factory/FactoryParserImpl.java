package eu.profinit.manta.connector.datafactory.resolver.service.factory;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.factory.GlobalParameterType;
import eu.profinit.manta.connector.datafactory.model.factory.IGlobalParameterSpecification;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.resolver.model.Factory;
import eu.profinit.manta.connector.datafactory.resolver.model.factory.GlobalParameterSpecification;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of parsing logic for Factory resource.
 */
public class FactoryParserImpl implements FactoryParser {

    private static final Logger LOGGER = new Logger(FactoryParserImpl.class);

    @Override
    public Factory parseFactory(JsonNode factoryNode) {
        Factory factory = null;
        if (JsonUtil.isNotNullAndNotMissing(factoryNode)) {
            factory = new Factory(
                    JsonUtil.pathAsText(factoryNode, "name"),
                    parseGlobalParameters(factoryNode.at("/properties/globalParameters"))
            );
        }
        return factory;
    }

    /**
     * Parses global parameters of the Factory resource.
     *
     * @param globalParametersNode JSON node representing global parameters field value
     * @return map of global parameters, where the key is the name of the parameter and value is global parameter specification
     */
    private Map<String, IGlobalParameterSpecification> parseGlobalParameters(JsonNode globalParametersNode) {
        Map<String, IGlobalParameterSpecification> globalParameters = new HashMap<>();
        if (JsonUtil.isNotNullAndNotMissing(globalParametersNode)) {
            globalParametersNode.fieldNames().forEachRemaining((paramName) -> {
                JsonNode paramNode = globalParametersNode.get(paramName);
                globalParameters.put(paramName, parseGlobalParameterSpecification(paramName, paramNode));
            });
        }
        return globalParameters;
    }

    /**
     * Parses single instance of global parameter specification.
     * <br>
     * Error is logged in case parameter's type cannot be parsed.
     *
     * @param paramName name of the param
     * @param paramNode JSON node representing the parameter's value
     * @return global parameter specification
     */
    private GlobalParameterSpecification parseGlobalParameterSpecification(String paramName, JsonNode paramNode) {
        GlobalParameterType type = GlobalParameterType.OBJECT;
        String typeString = JsonUtil.pathAsText(paramNode, "type");
        try {
            type = GlobalParameterType.valueOfCI(typeString);
        } catch (IllegalArgumentException ex) {
            LOGGER.log(Categories.parsingErrors()
                    .unknownGlobalParameterType()
                    .parameterType(typeString)
                    .parameterName(paramName)
                    .usedParameterType(GlobalParameterType.OBJECT)
                    .catching(ex)
            );
        }
        JsonNode value = paramNode.get("value");
        return new GlobalParameterSpecification(value, type);
    }
}
