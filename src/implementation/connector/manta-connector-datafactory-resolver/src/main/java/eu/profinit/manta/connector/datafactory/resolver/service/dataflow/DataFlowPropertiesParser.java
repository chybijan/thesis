package eu.profinit.manta.connector.datafactory.resolver.service.dataflow;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowProperties;

/**
 * Interface for type property parsers
 */
public interface DataFlowPropertiesParser {

    /**
     * Parse node into type properties
     * @param typePropertiesNode json node with properties of concrete type
     * @return Concrete properties object,
     *         or null if typePropertiesNode is null or empty
     */
    IDataFlowProperties parseProperties(JsonNode typePropertiesNode);

}
