package eu.profinit.manta.connector.datafactory.resolver.model;

import eu.profinit.manta.connector.datafactory.model.IDataFlow;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowProperties;

import java.util.Collection;

/**
 * Implementation of the Data flow resource interface.
 */
public class DataFlow implements IDataFlow {

    protected final String name;

    protected final IDataFlowProperties properties;

    public DataFlow(String name, IDataFlowProperties properties) {
        this.name = name;
        this.properties = properties;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public IDataFlowProperties getProperties() {
        return properties;
    }

    @Override
    public Collection<IReference> getAllReferences() {
        return properties.getAllReferences();
    }
}
