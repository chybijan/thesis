package eu.profinit.manta.connector.datafactory.resolver.service.common;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IParameterSpecification;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.common.ParameterType;
import eu.profinit.manta.connector.datafactory.model.factory.GlobalParameterType;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.resolver.model.common.ParameterSpecification;
import eu.profinit.manta.connector.datafactory.resolver.model.common.Parameters;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Service responsible for parsing Azure Data Factory resource parameters.
 */
public class ParametersParserImpl implements ParametersParser {

    private static final Logger LOGGER = new Logger(ParametersParserImpl.class);

    @Override
    public IParameters parseParameters(JsonNode parametersNode) {
        Map<String, IParameterSpecification> parameters = new HashMap<>();
        if (JsonUtil.isNotNullAndNotMissing(parametersNode)) {
            parametersNode.fieldNames().forEachRemaining((paramName) -> {
                JsonNode paramNode = parametersNode.get(paramName);
                parameters.put(paramName, parseParameterSpecification(paramName, paramNode));
            });
        }
        return new Parameters(parameters);
    }


    /**
     * Parses single instance of parameter specification.
     * <br>
     * Error is logged in case parameter's type cannot be parsed.
     *
     * @param paramName name of the param
     * @param paramNode JSON node representing the parameter's value
     * @return parameter specification
     */
    private IParameterSpecification parseParameterSpecification(String paramName, JsonNode paramNode) {
        ParameterType type = ParameterType.OBJECT;
        String typeString = JsonUtil.pathAsText(paramNode, "type");
        try {
            type = ParameterType.valueOfCI(typeString);
        } catch (IllegalArgumentException ex) {
            LOGGER.log(Categories.parsingErrors()
                    .unknownParameterType()
                    .parameterType(typeString)
                    .parameterName(paramName)
                    .usedParameterType(GlobalParameterType.OBJECT)
                    .catching(ex)
            );
        }
        JsonNode value = paramNode.path("defaultValue");
        return new ParameterSpecification(value, type);
    }

}
