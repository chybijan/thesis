package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Error;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.common.errordefinitions.categories.ParsingFailureBuilder;
import eu.profinit.manta.platform.logging.common.messages.CommonMessages;

public class ParsingErrors extends BaseCategory {

    @Error(userMessage = "Unknown type %{parameterType} for global parameter \"%{parameterName}\". %{usedParameterType} is used instead.",
            technicalMessage = "Unknown type %{parameterType} for global parameter \"%{parameterName}\". %{usedParameterType} is used instead.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnknownGlobalParameterTypeBuilder unknownGlobalParameterType() {
        return new UnknownGlobalParameterTypeBuilder(this);
    }

    @Error(userMessage = "Unknown type %{parameterType} for parameter \"%{parameterName}\". %{usedParameterType} is used instead.",
            technicalMessage = "Unknown type %{parameterType} for parameter \"%{parameterName}\". %{usedParameterType} is used instead.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnknownParameterTypeBuilder unknownParameterType() {
        return new UnknownParameterTypeBuilder(this);
    }

    @Error(userMessage = "Unknown type %{columnType} for column \"%{columnName}\". %{usedColumnType} is used instead.",
            technicalMessage = "Unknown type %{columnType} for column \"%{columnName}\". %{usedColumnType} is used instead.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnknownColumnTypeBuilder unknownColumnType() {
        return new UnknownColumnTypeBuilder(this);
    }

    @Error(userMessage = "Unknown type '%{type}'. %{usedType} is used instead.",
            technicalMessage = "Unknown type '%{type}'. %{usedType} is used instead.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnknownDataTypeBuilder unknownDataType() {
        return new UnknownDataTypeBuilder(this);
    }

    @Error(userMessage = "Unknown type \"%{parameterType}\" for parameter \"%{parameterName}\". Parameter type is not used during analysis",
            technicalMessage = "Unknown type %{parameterType} for parameter \"%{parameterName}\". Parameter type is not used during analysis",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnknownDataFlowParameterTypeBuilder unknownDataFlowParameterType() {
        return new UnknownDataFlowParameterTypeBuilder(this);
    }

    @Error(userMessage = "Unknown type %{datasetType} for dataset \"%{datasetName}\". %{defaultDatasetType} is used instead.",
            technicalMessage = "Unknown type %{datasetType} for dataset \"%{datasetName}\". %{defaultDatasetType} is used instead.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnknownDatasetTypeBuilder unknownDatasetType() {
        return new UnknownDatasetTypeBuilder(this);
    }

    @Error(userMessage = "Unknown type %{linkedServiceType} for linked service \"%{linkedServiceName}\". %{defaultLinkedServiceType} is used instead.",
            technicalMessage = "Unknown type %{linkedServiceType} for linked service \"%{linkedServiceName}\". %{defaultLinkedServiceType} is used instead.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnknownLinkedServiceTypeBuilder unknownLinkedServiceType() {
        return new UnknownLinkedServiceTypeBuilder(this);
    }

    @Error(userMessage = "Unknown Hive server type %{hiveServerType} for linked service \"%{linkedServiceName}\".",
            technicalMessage = "Unknown Hive server type %{hiveServerType} for linked service \"%{linkedServiceName}\".",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnknownHiveServerTypeBuilder unknownHiveServerType() {
        return new UnknownHiveServerTypeBuilder(this);
    }

    @Error(userMessage = "Unknown location type %{locationType} for dataset \"%{datasetName}\". The processing of the location is skipped.",
            technicalMessage = "Unknown location type %{locationType} for dataset \"%{datasetName}\". The processing of the location is skipped.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnknownDatasetLocationTypeBuilder unknownDatasetLocationType() {
        return new UnknownDatasetLocationTypeBuilder(this);
    }

    @Error(userMessage = "Type %{datasetType} of the dataset \"%{datasetName}\" is not supported by Manta. The processing of type specific fields is skipped.",
            technicalMessage = "Type %{datasetType} of the dataset \"%{datasetName}\" is not supported by Manta. The processing of type specific fields is skipped.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.WARN,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnsupportedDatasetTypeBuilder unsupportedDatasetType() {
        return new UnsupportedDatasetTypeBuilder(this);
    }

    @Error(userMessage = "Type %{linkedServiceType} of the linked service \"%{linkedServiceName}\" is not supported by Manta. The processing of type specific fields is skipped.",
            technicalMessage = "Type %{linkedServiceType} of the linked service \"%{linkedServiceName}\" is not supported by Manta. The processing of type specific fields is skipped.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.WARN,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnsupportedLinkedServiceTypeBuilder unsupportedLinkedServiceType() {
        return new UnsupportedLinkedServiceTypeBuilder(this);
    }

    @Error(userMessage = "Unexpected element structure \n\"%{value}\"\n. The provided part of input cannot be parsed.",
            technicalMessage = "Unexpected element structure \n\"%{value}\"\n. The provided part of input cannot be parsed.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnexpectedElementStructureBuilder unexpectedElementStructure() {
        return new UnexpectedElementStructureBuilder(this);
    }

    @Error(userMessage = "Data flow resource is missing name property.",
            technicalMessage = "Data flow resource is missing name property in JSON: \n\"%{jsonString}\"\n.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public MissingDataFlowNameBuilder missingDataFlowName() {
        return new MissingDataFlowNameBuilder(this);
    }

    @Error(userMessage = "Data flow properties of resource named \"%{resourceName}\" couldn't be loaded. Data flow resource cannot be parsed.",
            technicalMessage = "Data flow properties of resource named \"%{resourceName}\" couldn't be loaded. Data flow resource cannot be parsed.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnknownDataFlowPropertiesBuilder unknownDataFlowProperties() {
        return new UnknownDataFlowPropertiesBuilder(this);
    }

    @Error(userMessage = "Unknown data flow type \"%{typeName}\". %{defaultType} is used instead.",
            technicalMessage = "Unknown data flow type \"%{typeName}\". %{defaultType} is used instead.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnknownDataFlowTypeBuilder unknownDataFlowType() {
        return new UnknownDataFlowTypeBuilder(this);
    }

    @Error(userMessage = "Unknown reference type \"%{typeName}\". %{defaultType} is used instead.",
            technicalMessage = "Unknown reference type \"%{typeName}\". %{defaultType} is used instead.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnknownReferenceTypeBuilder unknownReferenceType() {
        return new UnknownReferenceTypeBuilder(this);
    }

    @Error(userMessage = "Data flow type \"%{type}\" is not supported by Manta. Data flow resource cannot be parsed",
            technicalMessage = "Data flow type \"%{type}\" is not supported by Manta. Data flow resource cannot be parsed",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnsupportedDataFlowTypeBuilder unsupportedDataFlowType() {
        return new UnsupportedDataFlowTypeBuilder(this);
    }


    @Error(userMessage = "Dataset reference is missing name property.",
            technicalMessage = "Dataset reference is missing name property in JSON: \n\"%{referenceNode}\"\n.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public MissingDatasetReferenceNameBuilder missingDatasetReferenceName() {
        return new MissingDatasetReferenceNameBuilder(this);
    }

    @Error(userMessage = "Endpoint is missing name property.",
            technicalMessage = "Endpoint is missing name property in JSON: \n\"%{endpointNode}\"\n.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public MissingEndpointNameBuilder missingEndpointName() {
        return new MissingEndpointNameBuilder(this);
    }


    @Error(userMessage = "Endpoint \"%{endpointName}\" is missing dataset reference.",
            technicalMessage = "Endpoint \"%{endpointName}\" is missing dataset reference.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.WARN,
            lineageImpact = Impact.SINGLE_INPUT)
    public MissingDatasetReferenceOfEndpointBuilder missingDatasetReferenceOfEndpoint() {
        return new MissingDatasetReferenceOfEndpointBuilder(this);
    }

    @Error(userMessage = "Endpoint \"%{endpointName}\" is missing linked service reference.",
            technicalMessage = "Endpoint \"%{endpointName}\" is missing linked service reference.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.WARN,
            lineageImpact = Impact.SINGLE_INPUT)
    public MissingLinkedServiceReferenceOfEndpointBuilder missingLinkedServiceReferenceOfEndpoint() {
        return new MissingLinkedServiceReferenceOfEndpointBuilder(this);
    }

    @Error(userMessage = "Endpoint \"%{endpointName}\" is missing schema linked service reference.",
            technicalMessage = "Endpoint \"%{endpointName}\" is missing schema linked service reference.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.WARN,
            lineageImpact = Impact.SINGLE_INPUT)
    public MissingSchemaLinkedServiceReferenceOfEndpointBuilder missingSchemaLinkedServiceReferenceOfEndpoint() {
        return new MissingSchemaLinkedServiceReferenceOfEndpointBuilder(this);
    }

    @Error(userMessage = "Dataset \"%{datasetName}\" is missing linked service reference.",
            technicalMessage = "Dataset \"%{datasetName}\" is missing linked service reference.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public MissingLinkedServiceReferenceOfDatasetBuilder missingLinkedServiceReferenceOfDataset() {
        return new MissingLinkedServiceReferenceOfDatasetBuilder(this);
    }

    @Error(userMessage = "Dataflow property is missing sources. Lineage could be incomplete.",
            technicalMessage = "Dataflow property is missing sources. Lineage could be incomplete.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.WARN,
            lineageImpact = Impact.SINGLE_INPUT)
    public MissingSourcesOfDataFlowPropertiesBuilder missingSourcesOfDataFlowProperties() {
        return new MissingSourcesOfDataFlowPropertiesBuilder(this);
    }

    @Error(userMessage = "Mapping dataflow property is missing sinks. Lineage could be incomplete.",
            technicalMessage = "Mapping dataflow property is missing sinks. Lineage could be incomplete.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.WARN,
            lineageImpact = Impact.SINGLE_INPUT)
    public MissingSinksOfDataFlowPropertiesBuilder missingSinksOfDataFlowProperties() {
        return new MissingSinksOfDataFlowPropertiesBuilder(this);
    }

    @Error(userMessage = "Dataflow is missing script.",
            technicalMessage = "Dataflow is missing script.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public MissingDataFlowScriptBuilder missingDataFlowScript() {
        return new MissingDataFlowScriptBuilder(this);
    }

    @Error(
            userMessage = "%{message}. This error most likely happened because of previous errors.",
            technicalMessage = "%{message}. This error most likely happened because of previous errors.",
            solution = "Check the log for parsing errors and check that the input is correct. Otherwise please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.",
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public MalformedAstBuilder malformedAst() {
        return new MalformedAstBuilder(this);
    }

    @Error(
            userMessage = "Failed to parse the input '%{input}'.",
            technicalMessage = "Failed to parse the input '%{input}'.",
            solution = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.",
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT
    )
    public DFSParsingFailureBuilder DFSParsingFailure() {
        return new DFSParsingFailureBuilder(this);
    }

    @Error(userMessage = "Data flow transformation is missing name.",
            technicalMessage = "Data flow transformation is missing name in JSON.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public MissingDataFlowNameBuilder missingTransformationName() {
        return new MissingDataFlowNameBuilder(this);
    }

    @Error(userMessage = "Data flow transformation attribute named '%{expectedAttribute}' not found.",
            technicalMessage = "Data flow transformation attribute named '%{expectedAttribute}' not found.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public MissingTransformationAttributeBuilder missingTransformationAttribute() {
        return new MissingTransformationAttributeBuilder(this);
    }

    @Error(userMessage = "Invalid expression type encountered, expected: '%{expectedType}'",
            technicalMessage = "Invalid expression type encountered, expected: '%{expectedType}'",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public InvalidExpressionTypeBuilder invalidExpressionType() {
        return new InvalidExpressionTypeBuilder(this);
    }

    @Error(userMessage = "Data flow transformation attribute named: '%{name}' has invalid value: '%{value}'." +
            "Expected format: '%{expected}'",
            technicalMessage = "Data flow transformation attribute named: '%{name}' has invalid value: '%{value}'." +
                    "Expected format: '%{expected}'",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public InvalidTransformationAttributeValueBuilder invalidTransformationAttributeValue() {
        return new InvalidTransformationAttributeValueBuilder(this);
    }


    @Error(userMessage = "Data flow source transformation column name is missing.",
            technicalMessage = "Data flow source transformation column name is missing.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public MissingSourceColumnNameBuilder missingSourceColumnName() {
        return new MissingSourceColumnNameBuilder(this);
    }

    @Error(userMessage = "Data flow source transformation column data type is missing.",
            technicalMessage = "Data flow source transformation column data type is missing.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public MissingSourceColumnDataTypeBuilder missingSourceColumnDataType() {
        return new MissingSourceColumnDataTypeBuilder(this);
    }


}
