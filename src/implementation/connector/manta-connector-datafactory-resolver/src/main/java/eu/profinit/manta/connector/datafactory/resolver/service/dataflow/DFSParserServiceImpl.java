package eu.profinit.manta.connector.datafactory.resolver.service.dataflow;

import eu.profinit.manta.ast.MantaAstNode;
import eu.profinit.manta.ast.MantaTreeAdaptor;
import eu.profinit.manta.ast.navigator.MantaAstNavigator;
import eu.profinit.manta.ast.parser.context.ParserContextState;
import eu.profinit.manta.ast.parser.stream.ANTLRIgnoreCaseMemoryStream;
import eu.profinit.manta.ast.token.impl.MantaTokenSourceAdaptor;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowScript;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstDFS;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IDFSNode;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.parser.DFSLexer;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.parser.DFSParser;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast.DFSTreeAdaptor;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.DataFlowScript;
import eu.profinit.manta.platform.logging.api.logging.Logger;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.Lexer;

import org.antlr.runtime.RecognitionException;

import java.util.Collections;

public class DFSParserServiceImpl implements DFSParserService {

    private static final Logger LOGGER = new Logger(DFSParserServiceImpl.class);

    @Override
    public IDataFlowScript processScript(String script) {

        DFSParser dfsParser = parseDFS(script);

        DFSParser.dfs_return dfsReturn = null;

        try {
            dfsReturn = dfsParser.dfs();
        } catch (RecognitionException e) {
            LOGGER.log(Categories.parsingErrors().DFSParsingFailure()
                    .input(script)
                    .catching(e));
            return createFailedDFS();
        }

        Object astRoot = dfsReturn.getTree();

        if (!(astRoot instanceof IAstDFS)) {

            LOGGER.log(Categories.parsingErrors().malformedAst()
                    .message("Parsing output was not a DFS"));

            return createFailedDFS();

        }else{
            IAstDFS rootNode = (IAstDFS) astRoot;

            IDataFlowScript dfs = rootNode.resolve(new DataFlowScript(Collections.emptyList(), Collections.emptyMap()));

            if (dfsParser.getNumberOfSyntaxErrors() > 0) {
                dfs = dfs.updatedParsedWithoutErrors(false);
            }

            return dfs;
        }

    }

    /**
     * @return empty DFS with error flag
     */
    private IDataFlowScript createFailedDFS(){
        IDataFlowScript dfs = new DataFlowScript(Collections.emptyList(), Collections.emptyMap());
        dfs = dfs.updatedParsedWithoutErrors(false);
        return dfs;
    }

    /**
     * Common parser initialization method.
     *
     * @param script input DFS
     * @return executed parser
     */
    private DFSParser parseDFS(String script) {

        ANTLRIgnoreCaseMemoryStream inputStream = new ANTLRIgnoreCaseMemoryStream(script);
        Lexer lexer = new DFSLexer(inputStream);

        // Custom manta tokens source creating our own SqlMantaTokens instead of general antlr Token
        MantaTokenSourceAdaptor tokenSource = new MantaTokenSourceAdaptor(lexer);
        CommonTokenStream tokens = new CommonTokenStream(tokenSource);
        DFSParser parser = new DFSParser(tokens);

        // Create tree adapter for extending functionality of each AST node.
        DFSTreeAdaptor adaptor = new DFSTreeAdaptor();

        // Create navigator to navigate created AST tree via XPath.
        MantaAstNavigator navigator = new MantaAstNavigator();

        // Configure navigator for nodes, which are explicitly created in grammar.
        DFSContextState contextState = new DFSContextState(navigator, parser, adaptor);
        parser.setContextState(contextState);
        adaptor.setContextState(contextState);
        parser.setTreeAdaptor(adaptor);

        return parser;
    }

}
