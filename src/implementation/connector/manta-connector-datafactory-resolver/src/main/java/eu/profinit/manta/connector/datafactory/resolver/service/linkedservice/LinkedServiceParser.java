package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;

public interface LinkedServiceParser<T> {

    T parseLinkedService(JsonNode linkedServiceNode);
}
