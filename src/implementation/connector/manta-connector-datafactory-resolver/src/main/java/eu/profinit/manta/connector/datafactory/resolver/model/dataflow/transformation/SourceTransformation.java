package eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ISourceTransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.ITable;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class SourceTransformation extends AbstractTransformation implements ISourceTransformation {

    private final boolean allowSchemaDrift;

    public SourceTransformation(String name, Set<String> substreams,
                                Collection<IADFStream> inputs, Collection<String> outputs, ITable table,
                                Map<String, IExpression> attributes,
                                boolean allowSchemaDrift) {
        super(name, substreams, inputs, outputs, table, attributes);
        this.allowSchemaDrift = allowSchemaDrift;
    }

    @Override
    public boolean getAllowSchemaDrift() {
        return allowSchemaDrift;
    }

}
