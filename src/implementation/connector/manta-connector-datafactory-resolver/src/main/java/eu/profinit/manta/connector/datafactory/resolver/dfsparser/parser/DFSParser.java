// $ANTLR 3.5-rc-2 DFSParser.g 2022-04-24 18:44:27

    package eu.profinit.manta.connector.datafactory.resolver.dfsparser.parser;

    import eu.profinit.manta.platform.logging.api.logging.Logger;

    import eu.profinit.manta.ast.parser.base.MantaAbstractParser;
    import eu.profinit.manta.ast.token.impl.SqlMantaToken;

    import eu.profinit.manta.connector.datafactory.resolver.dfsparser.AbstractDFSParser;
    import eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast.*;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.function.Function;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class DFSParser extends AbstractDFSParser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AMPERSAND", "ANY_CHAR", "APOSTROPHE", 
		"ARRAY_ANY", "ARRAY_BOOLEAN", "ARRAY_DATE", "ARRAY_DECIMAL", "ARRAY_DOUBLE", 
		"ARRAY_FLOAT", "ARRAY_INTEGER", "ARRAY_LONG", "ARRAY_SHORT", "ARRAY_STRING", 
		"ARRAY_TIMESTAMP", "ASTERISK", "AST_ADDITION_EXPRESSION", "AST_ADDITION_OPERATOR", 
		"AST_AND_EXPRESSION", "AST_ARRAY_ELEMENT", "AST_ARRAY_LITERAL", "AST_ATTRIBUTE_NAME", 
		"AST_COLUMN_DEFINITION", "AST_COLUMN_DEFINITIONS", "AST_COLUMN_NAME", 
		"AST_COLUMN_REFERENCE", "AST_COMPARE_EXPRESSION", "AST_COMPARE_OPERATOR", 
		"AST_CONCRETE_TRANSFORMATION", "AST_DATA_TYPE", "AST_DATA_TYPE_FORMAT", 
		"AST_DFS", "AST_ERROR", "AST_EXPRESSION", "AST_EXPRESSION_FUNCTION", "AST_EXPRESSION_LITERAL", 
		"AST_FLOATING_POINT_LITERAL", "AST_INPUT_STREAMS", "AST_INTEGER_LITERAL", 
		"AST_LEFT_OPERAND", "AST_MAP_DATA_TYPE", "AST_MAP_KEY_TYPE", "AST_MAP_VALUE_TYPE", 
		"AST_MULT_EXPRESSION", "AST_MULT_OPERATOR", "AST_NOT_EXPRESSION", "AST_OPERAND", 
		"AST_OR_EXPRESSION", "AST_OUTPUT_ATTRIBUTE", "AST_PARAMETER", "AST_PARAMETERS", 
		"AST_PARAMETER_DEFAULT_VALUE", "AST_PARAMETER_INVOCATION", "AST_PARAMETER_NAME", 
		"AST_POW_EXPRESSION", "AST_RIGHT_OPERAND", "AST_STREAM_NAME", "AST_STRING_LITERAL", 
		"AST_SUBSTREAM_NAME", "AST_TRANSFORMATION", "AST_TRANSFORMATIONS", "AST_TRANSFORMATION_ATTRIBUTE", 
		"AST_TRANSFORMATION_ATTRIBUTES", "AST_TRANSFORMATION_NAME", "AST_UNARY_EXPRESSION", 
		"AST_UNARY_OPERATOR", "AT_SIGN", "BACK_SLASH", "BAR", "BLANK", "CARET", 
		"COLON", "COMMA", "CR", "CROSSHATCH", "DECIMAL_TYPE", "DIGIT", "DOLLAR_SIGN", 
		"EQUALS", "EXCLAMATION_MARK", "FLOATING_POINT_LITERAL", "GREATER_THAN", 
		"INTEGER_LITERAL", "KW_ANY", "KW_AS", "KW_BOOLEAN", "KW_DATE", "KW_DECIMAL", 
		"KW_DOUBLE", "KW_FALSE", "KW_FILTER", "KW_FLOAT", "KW_INTEGER", "KW_JOIN", 
		"KW_LONG", "KW_OUTPUT", "KW_PARAMETERS", "KW_SHORT", "KW_SINK", "KW_SOURCE", 
		"KW_STRING", "KW_TIMESTAMP", "KW_TRUE", "LEFT_BRACE", "LEFT_BRACKET", 
		"LEFT_PARENT", "LESS_THAN", "LETTER", "LF", "MINUS_SIGN", "NEWLINE", "OUTPUT_STREAM", 
		"PERCENT_SIGN", "PERIOD", "PLUS_SIGN", "QUESTION_MARK", "QUOTATION_MARK", 
		"RIGHT_BRACE", "RIGHT_BRACKET", "RIGHT_PARENT", "SEMICOLON", "SIMPLE_ID", 
		"SLASH", "STRING_LITERAL", "TAB", "UNDERSCORE", "UNICODE_BOM", "WHITESPACE", 
		"131", "132", "133", "134", "135", "136", "137", "138", "139", "140", 
		"141", "142", "143", "144", "145", "146", "147", "148", "149", "150", 
		"151"
	};
	public static final int EOF=-1;
	public static final int AMPERSAND=4;
	public static final int ANY_CHAR=5;
	public static final int APOSTROPHE=6;
	public static final int ARRAY_ANY=7;
	public static final int ARRAY_BOOLEAN=8;
	public static final int ARRAY_DATE=9;
	public static final int ARRAY_DECIMAL=10;
	public static final int ARRAY_DOUBLE=11;
	public static final int ARRAY_FLOAT=12;
	public static final int ARRAY_INTEGER=13;
	public static final int ARRAY_LONG=14;
	public static final int ARRAY_SHORT=15;
	public static final int ARRAY_STRING=16;
	public static final int ARRAY_TIMESTAMP=17;
	public static final int ASTERISK=18;
	public static final int AST_ADDITION_EXPRESSION=19;
	public static final int AST_ADDITION_OPERATOR=20;
	public static final int AST_AND_EXPRESSION=21;
	public static final int AST_ARRAY_ELEMENT=22;
	public static final int AST_ARRAY_LITERAL=23;
	public static final int AST_ATTRIBUTE_NAME=24;
	public static final int AST_COLUMN_DEFINITION=25;
	public static final int AST_COLUMN_DEFINITIONS=26;
	public static final int AST_COLUMN_NAME=27;
	public static final int AST_COLUMN_REFERENCE=28;
	public static final int AST_COMPARE_EXPRESSION=29;
	public static final int AST_COMPARE_OPERATOR=30;
	public static final int AST_CONCRETE_TRANSFORMATION=31;
	public static final int AST_DATA_TYPE=32;
	public static final int AST_DATA_TYPE_FORMAT=33;
	public static final int AST_DFS=34;
	public static final int AST_ERROR=35;
	public static final int AST_EXPRESSION=36;
	public static final int AST_EXPRESSION_FUNCTION=37;
	public static final int AST_EXPRESSION_LITERAL=38;
	public static final int AST_FLOATING_POINT_LITERAL=39;
	public static final int AST_INPUT_STREAMS=40;
	public static final int AST_INTEGER_LITERAL=41;
	public static final int AST_LEFT_OPERAND=42;
	public static final int AST_MAP_DATA_TYPE=43;
	public static final int AST_MAP_KEY_TYPE=44;
	public static final int AST_MAP_VALUE_TYPE=45;
	public static final int AST_MULT_EXPRESSION=46;
	public static final int AST_MULT_OPERATOR=47;
	public static final int AST_NOT_EXPRESSION=48;
	public static final int AST_OPERAND=49;
	public static final int AST_OR_EXPRESSION=50;
	public static final int AST_OUTPUT_ATTRIBUTE=51;
	public static final int AST_PARAMETER=52;
	public static final int AST_PARAMETERS=53;
	public static final int AST_PARAMETER_DEFAULT_VALUE=54;
	public static final int AST_PARAMETER_INVOCATION=55;
	public static final int AST_PARAMETER_NAME=56;
	public static final int AST_POW_EXPRESSION=57;
	public static final int AST_RIGHT_OPERAND=58;
	public static final int AST_STREAM_NAME=59;
	public static final int AST_STRING_LITERAL=60;
	public static final int AST_SUBSTREAM_NAME=61;
	public static final int AST_TRANSFORMATION=62;
	public static final int AST_TRANSFORMATIONS=63;
	public static final int AST_TRANSFORMATION_ATTRIBUTE=64;
	public static final int AST_TRANSFORMATION_ATTRIBUTES=65;
	public static final int AST_TRANSFORMATION_NAME=66;
	public static final int AST_UNARY_EXPRESSION=67;
	public static final int AST_UNARY_OPERATOR=68;
	public static final int AT_SIGN=69;
	public static final int BACK_SLASH=70;
	public static final int BAR=71;
	public static final int BLANK=72;
	public static final int CARET=73;
	public static final int COLON=74;
	public static final int COMMA=75;
	public static final int CR=76;
	public static final int CROSSHATCH=77;
	public static final int DECIMAL_TYPE=78;
	public static final int DIGIT=79;
	public static final int DOLLAR_SIGN=80;
	public static final int EQUALS=81;
	public static final int EXCLAMATION_MARK=82;
	public static final int FLOATING_POINT_LITERAL=83;
	public static final int GREATER_THAN=84;
	public static final int INTEGER_LITERAL=85;
	public static final int KW_ANY=86;
	public static final int KW_AS=87;
	public static final int KW_BOOLEAN=88;
	public static final int KW_DATE=89;
	public static final int KW_DECIMAL=90;
	public static final int KW_DOUBLE=91;
	public static final int KW_FALSE=92;
	public static final int KW_FILTER=93;
	public static final int KW_FLOAT=94;
	public static final int KW_INTEGER=95;
	public static final int KW_JOIN=96;
	public static final int KW_LONG=97;
	public static final int KW_OUTPUT=98;
	public static final int KW_PARAMETERS=99;
	public static final int KW_SHORT=100;
	public static final int KW_SINK=101;
	public static final int KW_SOURCE=102;
	public static final int KW_STRING=103;
	public static final int KW_TIMESTAMP=104;
	public static final int KW_TRUE=105;
	public static final int LEFT_BRACE=106;
	public static final int LEFT_BRACKET=107;
	public static final int LEFT_PARENT=108;
	public static final int LESS_THAN=109;
	public static final int LETTER=110;
	public static final int LF=111;
	public static final int MINUS_SIGN=112;
	public static final int NEWLINE=113;
	public static final int OUTPUT_STREAM=114;
	public static final int PERCENT_SIGN=115;
	public static final int PERIOD=116;
	public static final int PLUS_SIGN=117;
	public static final int QUESTION_MARK=118;
	public static final int QUOTATION_MARK=119;
	public static final int RIGHT_BRACE=120;
	public static final int RIGHT_BRACKET=121;
	public static final int RIGHT_PARENT=122;
	public static final int SEMICOLON=123;
	public static final int SIMPLE_ID=124;
	public static final int SLASH=125;
	public static final int STRING_LITERAL=126;
	public static final int TAB=127;
	public static final int UNDERSCORE=128;
	public static final int UNICODE_BOM=129;
	public static final int WHITESPACE=130;

	// delegates
	public DFSParser_DFSNonReserveredKW gDFSNonReserveredKW;
	public AbstractDFSParser[] getDelegates() {
		return new AbstractDFSParser[] {gDFSNonReserveredKW};
	}

	// delegators


	public DFSParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public DFSParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
		this.state.ruleMemo = new HashMap[40+4+1];


		        
				gDFSNonReserveredKW = new DFSParser_DFSNonReserveredKW(input, state, this);
                    
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
		gDFSNonReserveredKW.setTreeAdaptor(this.adaptor);
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return DFSParser.tokenNames; }
	@Override public String getGrammarFileName() { return "DFSParser.g"; }



	    private DFSParser gDFSParser = this;



	public static class dfs_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "dfs"
	// DFSParser.g:45:1: dfs : (param= parameters )? (trans= transformations )? -> ^( AST_DFS[contextState] ( $param)? ( $trans)? ) ;
	public final DFSParser.dfs_return dfs() throws RecognitionException {
		DFSParser.dfs_return retval = new DFSParser.dfs_return();
		retval.start = input.LT(1);
		int dfs_StartIndex = input.index();

		Object root_0 = null;

		ParserRuleReturnScope param =null;
		ParserRuleReturnScope trans =null;

		RewriteRuleSubtreeStream stream_transformations=new RewriteRuleSubtreeStream(adaptor,"rule transformations");
		RewriteRuleSubtreeStream stream_parameters=new RewriteRuleSubtreeStream(adaptor,"rule parameters");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return retval; }

			// DFSParser.g:46:5: ( (param= parameters )? (trans= transformations )? -> ^( AST_DFS[contextState] ( $param)? ( $trans)? ) )
			// DFSParser.g:46:9: (param= parameters )? (trans= transformations )?
			{
			// DFSParser.g:46:14: (param= parameters )?
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==KW_PARAMETERS) ) {
				int LA1_1 = input.LA(2);
				if ( (LA1_1==LEFT_BRACE) ) {
					alt1=1;
				}
			}
			switch (alt1) {
				case 1 :
					// DFSParser.g:46:14: param= parameters
					{
					pushFollow(FOLLOW_parameters_in_dfs101);
					param=parameters();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_parameters.add(param.getTree());
					}
					break;

			}

			// DFSParser.g:46:32: (trans= transformations )?
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0==AT_SIGN||LA2_0==COMMA||(LA2_0 >= KW_ANY && LA2_0 <= KW_TRUE)||LA2_0==SIMPLE_ID) ) {
				alt2=1;
			}
			switch (alt2) {
				case 1 :
					// DFSParser.g:46:32: trans= transformations
					{
					pushFollow(FOLLOW_transformations_in_dfs106);
					trans=transformations();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_transformations.add(trans.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: trans, param
			// token labels: 
			// rule labels: param, retval, trans
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_param=new RewriteRuleSubtreeStream(adaptor,"rule param",param!=null?param.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_trans=new RewriteRuleSubtreeStream(adaptor,"rule trans",trans!=null?trans.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 47:9: -> ^( AST_DFS[contextState] ( $param)? ( $trans)? )
			{
				// DFSParser.g:47:13: ^( AST_DFS[contextState] ( $param)? ( $trans)? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(new AstDFS(AST_DFS, contextState), root_1);
				// DFSParser.g:47:46: ( $param)?
				if ( stream_param.hasNext() ) {
					adaptor.addChild(root_1, stream_param.nextTree());
				}
				stream_param.reset();

				// DFSParser.g:47:54: ( $trans)?
				if ( stream_trans.hasNext() ) {
					adaptor.addChild(root_1, stream_trans.nextTree());
				}
				stream_trans.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 5, dfs_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "dfs"


	public static class parameters_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "parameters"
	// DFSParser.g:50:1: parameters : KW_PARAMETERS LEFT_BRACE (par1= parameter ( COMMA par2+= parameter )* )? RIGHT_BRACE -> ^( AST_PARAMETERS KW_PARAMETERS LEFT_BRACE ( $par1 ( COMMA $par2)* )? RIGHT_BRACE ) ;
	public final DFSParser.parameters_return parameters() throws RecognitionException {
		DFSParser.parameters_return retval = new DFSParser.parameters_return();
		retval.start = input.LT(1);
		int parameters_StartIndex = input.index();

		Object root_0 = null;

		Token KW_PARAMETERS1=null;
		Token LEFT_BRACE2=null;
		Token COMMA3=null;
		Token RIGHT_BRACE4=null;
		List<Object> list_par2=null;
		ParserRuleReturnScope par1 =null;
		RuleReturnScope par2 = null;
		Object KW_PARAMETERS1_tree=null;
		Object LEFT_BRACE2_tree=null;
		Object COMMA3_tree=null;
		Object RIGHT_BRACE4_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleTokenStream stream_RIGHT_BRACE=new RewriteRuleTokenStream(adaptor,"token RIGHT_BRACE");
		RewriteRuleTokenStream stream_LEFT_BRACE=new RewriteRuleTokenStream(adaptor,"token LEFT_BRACE");
		RewriteRuleTokenStream stream_KW_PARAMETERS=new RewriteRuleTokenStream(adaptor,"token KW_PARAMETERS");
		RewriteRuleSubtreeStream stream_parameter=new RewriteRuleSubtreeStream(adaptor,"rule parameter");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return retval; }

			// DFSParser.g:51:5: ( KW_PARAMETERS LEFT_BRACE (par1= parameter ( COMMA par2+= parameter )* )? RIGHT_BRACE -> ^( AST_PARAMETERS KW_PARAMETERS LEFT_BRACE ( $par1 ( COMMA $par2)* )? RIGHT_BRACE ) )
			// DFSParser.g:51:9: KW_PARAMETERS LEFT_BRACE (par1= parameter ( COMMA par2+= parameter )* )? RIGHT_BRACE
			{
			KW_PARAMETERS1=(Token)match(input,KW_PARAMETERS,FOLLOW_KW_PARAMETERS_in_parameters153); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_KW_PARAMETERS.add(KW_PARAMETERS1);

			LEFT_BRACE2=(Token)match(input,LEFT_BRACE,FOLLOW_LEFT_BRACE_in_parameters155); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LEFT_BRACE.add(LEFT_BRACE2);

			// DFSParser.g:51:34: (par1= parameter ( COMMA par2+= parameter )* )?
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0==EOF||(LA4_0 >= KW_ANY && LA4_0 <= KW_TRUE)||LA4_0==SIMPLE_ID) ) {
				alt4=1;
			}
			switch (alt4) {
				case 1 :
					// DFSParser.g:51:35: par1= parameter ( COMMA par2+= parameter )*
					{
					pushFollow(FOLLOW_parameter_in_parameters160);
					par1=parameter();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_parameter.add(par1.getTree());
					// DFSParser.g:51:50: ( COMMA par2+= parameter )*
					loop3:
					while (true) {
						int alt3=2;
						int LA3_0 = input.LA(1);
						if ( (LA3_0==COMMA) ) {
							alt3=1;
						}

						switch (alt3) {
						case 1 :
							// DFSParser.g:51:51: COMMA par2+= parameter
							{
							COMMA3=(Token)match(input,COMMA,FOLLOW_COMMA_in_parameters163); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_COMMA.add(COMMA3);

							pushFollow(FOLLOW_parameter_in_parameters167);
							par2=parameter();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_parameter.add(par2.getTree());
							if (list_par2==null) list_par2=new ArrayList<Object>();
							list_par2.add(par2.getTree());
							}
							break;

						default :
							break loop3;
						}
					}

					}
					break;

			}

			RIGHT_BRACE4=(Token)match(input,RIGHT_BRACE,FOLLOW_RIGHT_BRACE_in_parameters173); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RIGHT_BRACE.add(RIGHT_BRACE4);

			// AST REWRITE
			// elements: par1, LEFT_BRACE, KW_PARAMETERS, COMMA, par2, RIGHT_BRACE
			// token labels: 
			// rule labels: par1, retval
			// token list labels: 
			// rule list labels: par2
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_par1=new RewriteRuleSubtreeStream(adaptor,"rule par1",par1!=null?par1.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_par2=new RewriteRuleSubtreeStream(adaptor,"token par2",list_par2);
			root_0 = (Object)adaptor.nil();
			// 52:9: -> ^( AST_PARAMETERS KW_PARAMETERS LEFT_BRACE ( $par1 ( COMMA $par2)* )? RIGHT_BRACE )
			{
				// DFSParser.g:52:13: ^( AST_PARAMETERS KW_PARAMETERS LEFT_BRACE ( $par1 ( COMMA $par2)* )? RIGHT_BRACE )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_PARAMETERS, "AST_PARAMETERS"), root_1);
				adaptor.addChild(root_1, stream_KW_PARAMETERS.nextNode());
				adaptor.addChild(root_1, stream_LEFT_BRACE.nextNode());
				// DFSParser.g:52:55: ( $par1 ( COMMA $par2)* )?
				if ( stream_par1.hasNext()||stream_COMMA.hasNext()||stream_par2.hasNext() ) {
					adaptor.addChild(root_1, stream_par1.nextTree());
					// DFSParser.g:52:62: ( COMMA $par2)*
					while ( stream_COMMA.hasNext()||stream_par2.hasNext() ) {
						adaptor.addChild(root_1, stream_COMMA.nextNode());
						adaptor.addChild(root_1, stream_par2.nextTree());
					}
					stream_COMMA.reset();
					stream_par2.reset();

				}
				stream_par1.reset();
				stream_COMMA.reset();
				stream_par2.reset();

				adaptor.addChild(root_1, stream_RIGHT_BRACE.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 6, parameters_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "parameters"


	public static class parameter_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "parameter"
	// DFSParser.g:55:1: parameter : id= all_identifier KW_AS dt= data_type (dfe= df_expression )? -> ^( AST_PARAMETER[contextState] ^( AST_PARAMETER_NAME[contextState] $id) KW_AS ^( AST_DATA_TYPE[contextState] $dt) ^( AST_PARAMETER_DEFAULT_VALUE ( $dfe)? ) ) ;
	public final DFSParser.parameter_return parameter() throws RecognitionException {
		DFSParser.parameter_return retval = new DFSParser.parameter_return();
		retval.start = input.LT(1);
		int parameter_StartIndex = input.index();

		Object root_0 = null;

		Token KW_AS5=null;
		ParserRuleReturnScope id =null;
		ParserRuleReturnScope dt =null;
		ParserRuleReturnScope dfe =null;

		Object KW_AS5_tree=null;
		RewriteRuleTokenStream stream_KW_AS=new RewriteRuleTokenStream(adaptor,"token KW_AS");
		RewriteRuleSubtreeStream stream_df_expression=new RewriteRuleSubtreeStream(adaptor,"rule df_expression");
		RewriteRuleSubtreeStream stream_data_type=new RewriteRuleSubtreeStream(adaptor,"rule data_type");
		RewriteRuleSubtreeStream stream_all_identifier=new RewriteRuleSubtreeStream(adaptor,"rule all_identifier");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return retval; }

			// DFSParser.g:56:5: (id= all_identifier KW_AS dt= data_type (dfe= df_expression )? -> ^( AST_PARAMETER[contextState] ^( AST_PARAMETER_NAME[contextState] $id) KW_AS ^( AST_DATA_TYPE[contextState] $dt) ^( AST_PARAMETER_DEFAULT_VALUE ( $dfe)? ) ) )
			// DFSParser.g:56:9: id= all_identifier KW_AS dt= data_type (dfe= df_expression )?
			{
			pushFollow(FOLLOW_all_identifier_in_parameter229);
			id=all_identifier();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_all_identifier.add(id.getTree());
			KW_AS5=(Token)match(input,KW_AS,FOLLOW_KW_AS_in_parameter231); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_KW_AS.add(KW_AS5);

			pushFollow(FOLLOW_data_type_in_parameter235);
			dt=data_type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_data_type.add(dt.getTree());
			// DFSParser.g:56:49: (dfe= df_expression )?
			int alt5=2;
			int LA5_0 = input.LA(1);
			if ( (LA5_0==DOLLAR_SIGN||(LA5_0 >= EXCLAMATION_MARK && LA5_0 <= FLOATING_POINT_LITERAL)||(LA5_0 >= INTEGER_LITERAL && LA5_0 <= KW_TRUE)||(LA5_0 >= LEFT_BRACKET && LA5_0 <= LEFT_PARENT)||LA5_0==MINUS_SIGN||LA5_0==PLUS_SIGN||LA5_0==SIMPLE_ID||LA5_0==STRING_LITERAL) ) {
				alt5=1;
			}
			switch (alt5) {
				case 1 :
					// DFSParser.g:56:49: dfe= df_expression
					{
					pushFollow(FOLLOW_df_expression_in_parameter239);
					dfe=df_expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_df_expression.add(dfe.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: dt, id, KW_AS, dfe
			// token labels: 
			// rule labels: dt, dfe, id, retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_dt=new RewriteRuleSubtreeStream(adaptor,"rule dt",dt!=null?dt.getTree():null);
			RewriteRuleSubtreeStream stream_dfe=new RewriteRuleSubtreeStream(adaptor,"rule dfe",dfe!=null?dfe.getTree():null);
			RewriteRuleSubtreeStream stream_id=new RewriteRuleSubtreeStream(adaptor,"rule id",id!=null?id.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 57:9: -> ^( AST_PARAMETER[contextState] ^( AST_PARAMETER_NAME[contextState] $id) KW_AS ^( AST_DATA_TYPE[contextState] $dt) ^( AST_PARAMETER_DEFAULT_VALUE ( $dfe)? ) )
			{
				// DFSParser.g:57:13: ^( AST_PARAMETER[contextState] ^( AST_PARAMETER_NAME[contextState] $id) KW_AS ^( AST_DATA_TYPE[contextState] $dt) ^( AST_PARAMETER_DEFAULT_VALUE ( $dfe)? ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(new AstParameter(AST_PARAMETER, contextState), root_1);
				// DFSParser.g:57:57: ^( AST_PARAMETER_NAME[contextState] $id)
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot(new AstIdentifier(AST_PARAMETER_NAME, contextState), root_2);
				adaptor.addChild(root_2, stream_id.nextTree());
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_1, stream_KW_AS.nextNode());
				// DFSParser.g:57:118: ^( AST_DATA_TYPE[contextState] $dt)
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot(new AstDataType(AST_DATA_TYPE, contextState), root_2);
				adaptor.addChild(root_2, stream_dt.nextTree());
				adaptor.addChild(root_1, root_2);
				}

				// DFSParser.g:57:166: ^( AST_PARAMETER_DEFAULT_VALUE ( $dfe)? )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_PARAMETER_DEFAULT_VALUE, "AST_PARAMETER_DEFAULT_VALUE"), root_2);
				// DFSParser.g:57:197: ( $dfe)?
				if ( stream_dfe.hasNext() ) {
					adaptor.addChild(root_2, stream_dfe.nextTree());
				}
				stream_dfe.reset();

				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 7, parameter_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "parameter"


	public static class data_type_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "data_type"
	// DFSParser.g:60:1: data_type : ( KW_INTEGER | KW_BOOLEAN | KW_DATE | KW_TIMESTAMP | KW_ANY | KW_SHORT | KW_DOUBLE | KW_FLOAT | KW_LONG | DECIMAL_TYPE | KW_STRING | ARRAY_INTEGER | ARRAY_BOOLEAN | ARRAY_DATE | ARRAY_TIMESTAMP | ARRAY_ANY | ARRAY_SHORT | ARRAY_DOUBLE | ARRAY_FLOAT | ARRAY_LONG | ARRAY_DECIMAL | ARRAY_STRING | param_map_data_type | LEFT_PARENT RIGHT_PARENT );
	public final DFSParser.data_type_return data_type() throws RecognitionException {
		DFSParser.data_type_return retval = new DFSParser.data_type_return();
		retval.start = input.LT(1);
		int data_type_StartIndex = input.index();

		Object root_0 = null;

		Token KW_INTEGER6=null;
		Token KW_BOOLEAN7=null;
		Token KW_DATE8=null;
		Token KW_TIMESTAMP9=null;
		Token KW_ANY10=null;
		Token KW_SHORT11=null;
		Token KW_DOUBLE12=null;
		Token KW_FLOAT13=null;
		Token KW_LONG14=null;
		Token DECIMAL_TYPE15=null;
		Token KW_STRING16=null;
		Token ARRAY_INTEGER17=null;
		Token ARRAY_BOOLEAN18=null;
		Token ARRAY_DATE19=null;
		Token ARRAY_TIMESTAMP20=null;
		Token ARRAY_ANY21=null;
		Token ARRAY_SHORT22=null;
		Token ARRAY_DOUBLE23=null;
		Token ARRAY_FLOAT24=null;
		Token ARRAY_LONG25=null;
		Token ARRAY_DECIMAL26=null;
		Token ARRAY_STRING27=null;
		Token LEFT_PARENT29=null;
		Token RIGHT_PARENT30=null;
		ParserRuleReturnScope param_map_data_type28 =null;

		Object KW_INTEGER6_tree=null;
		Object KW_BOOLEAN7_tree=null;
		Object KW_DATE8_tree=null;
		Object KW_TIMESTAMP9_tree=null;
		Object KW_ANY10_tree=null;
		Object KW_SHORT11_tree=null;
		Object KW_DOUBLE12_tree=null;
		Object KW_FLOAT13_tree=null;
		Object KW_LONG14_tree=null;
		Object DECIMAL_TYPE15_tree=null;
		Object KW_STRING16_tree=null;
		Object ARRAY_INTEGER17_tree=null;
		Object ARRAY_BOOLEAN18_tree=null;
		Object ARRAY_DATE19_tree=null;
		Object ARRAY_TIMESTAMP20_tree=null;
		Object ARRAY_ANY21_tree=null;
		Object ARRAY_SHORT22_tree=null;
		Object ARRAY_DOUBLE23_tree=null;
		Object ARRAY_FLOAT24_tree=null;
		Object ARRAY_LONG25_tree=null;
		Object ARRAY_DECIMAL26_tree=null;
		Object ARRAY_STRING27_tree=null;
		Object LEFT_PARENT29_tree=null;
		Object RIGHT_PARENT30_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return retval; }

			// DFSParser.g:61:5: ( KW_INTEGER | KW_BOOLEAN | KW_DATE | KW_TIMESTAMP | KW_ANY | KW_SHORT | KW_DOUBLE | KW_FLOAT | KW_LONG | DECIMAL_TYPE | KW_STRING | ARRAY_INTEGER | ARRAY_BOOLEAN | ARRAY_DATE | ARRAY_TIMESTAMP | ARRAY_ANY | ARRAY_SHORT | ARRAY_DOUBLE | ARRAY_FLOAT | ARRAY_LONG | ARRAY_DECIMAL | ARRAY_STRING | param_map_data_type | LEFT_PARENT RIGHT_PARENT )
			int alt6=24;
			switch ( input.LA(1) ) {
			case KW_INTEGER:
				{
				alt6=1;
				}
				break;
			case KW_BOOLEAN:
				{
				alt6=2;
				}
				break;
			case KW_DATE:
				{
				alt6=3;
				}
				break;
			case KW_TIMESTAMP:
				{
				alt6=4;
				}
				break;
			case KW_ANY:
				{
				alt6=5;
				}
				break;
			case KW_SHORT:
				{
				alt6=6;
				}
				break;
			case KW_DOUBLE:
				{
				alt6=7;
				}
				break;
			case KW_FLOAT:
				{
				alt6=8;
				}
				break;
			case KW_LONG:
				{
				alt6=9;
				}
				break;
			case DECIMAL_TYPE:
				{
				alt6=10;
				}
				break;
			case KW_STRING:
				{
				alt6=11;
				}
				break;
			case ARRAY_INTEGER:
				{
				alt6=12;
				}
				break;
			case ARRAY_BOOLEAN:
				{
				alt6=13;
				}
				break;
			case ARRAY_DATE:
				{
				alt6=14;
				}
				break;
			case ARRAY_TIMESTAMP:
				{
				alt6=15;
				}
				break;
			case ARRAY_ANY:
				{
				alt6=16;
				}
				break;
			case ARRAY_SHORT:
				{
				alt6=17;
				}
				break;
			case ARRAY_DOUBLE:
				{
				alt6=18;
				}
				break;
			case ARRAY_FLOAT:
				{
				alt6=19;
				}
				break;
			case ARRAY_LONG:
				{
				alt6=20;
				}
				break;
			case ARRAY_DECIMAL:
				{
				alt6=21;
				}
				break;
			case ARRAY_STRING:
				{
				alt6=22;
				}
				break;
			case LEFT_BRACKET:
				{
				alt6=23;
				}
				break;
			case LEFT_PARENT:
				{
				alt6=24;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 6, 0, input);
				throw nvae;
			}
			switch (alt6) {
				case 1 :
					// DFSParser.g:61:9: KW_INTEGER
					{
					root_0 = (Object)adaptor.nil();


					KW_INTEGER6=(Token)match(input,KW_INTEGER,FOLLOW_KW_INTEGER_in_data_type310); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					KW_INTEGER6_tree = (Object)adaptor.create(KW_INTEGER6);
					adaptor.addChild(root_0, KW_INTEGER6_tree);
					}

					}
					break;
				case 2 :
					// DFSParser.g:62:9: KW_BOOLEAN
					{
					root_0 = (Object)adaptor.nil();


					KW_BOOLEAN7=(Token)match(input,KW_BOOLEAN,FOLLOW_KW_BOOLEAN_in_data_type320); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					KW_BOOLEAN7_tree = (Object)adaptor.create(KW_BOOLEAN7);
					adaptor.addChild(root_0, KW_BOOLEAN7_tree);
					}

					}
					break;
				case 3 :
					// DFSParser.g:63:9: KW_DATE
					{
					root_0 = (Object)adaptor.nil();


					KW_DATE8=(Token)match(input,KW_DATE,FOLLOW_KW_DATE_in_data_type330); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					KW_DATE8_tree = (Object)adaptor.create(KW_DATE8);
					adaptor.addChild(root_0, KW_DATE8_tree);
					}

					}
					break;
				case 4 :
					// DFSParser.g:64:9: KW_TIMESTAMP
					{
					root_0 = (Object)adaptor.nil();


					KW_TIMESTAMP9=(Token)match(input,KW_TIMESTAMP,FOLLOW_KW_TIMESTAMP_in_data_type340); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					KW_TIMESTAMP9_tree = (Object)adaptor.create(KW_TIMESTAMP9);
					adaptor.addChild(root_0, KW_TIMESTAMP9_tree);
					}

					}
					break;
				case 5 :
					// DFSParser.g:65:9: KW_ANY
					{
					root_0 = (Object)adaptor.nil();


					KW_ANY10=(Token)match(input,KW_ANY,FOLLOW_KW_ANY_in_data_type350); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					KW_ANY10_tree = (Object)adaptor.create(KW_ANY10);
					adaptor.addChild(root_0, KW_ANY10_tree);
					}

					}
					break;
				case 6 :
					// DFSParser.g:66:9: KW_SHORT
					{
					root_0 = (Object)adaptor.nil();


					KW_SHORT11=(Token)match(input,KW_SHORT,FOLLOW_KW_SHORT_in_data_type360); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					KW_SHORT11_tree = (Object)adaptor.create(KW_SHORT11);
					adaptor.addChild(root_0, KW_SHORT11_tree);
					}

					}
					break;
				case 7 :
					// DFSParser.g:67:9: KW_DOUBLE
					{
					root_0 = (Object)adaptor.nil();


					KW_DOUBLE12=(Token)match(input,KW_DOUBLE,FOLLOW_KW_DOUBLE_in_data_type370); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					KW_DOUBLE12_tree = (Object)adaptor.create(KW_DOUBLE12);
					adaptor.addChild(root_0, KW_DOUBLE12_tree);
					}

					}
					break;
				case 8 :
					// DFSParser.g:68:9: KW_FLOAT
					{
					root_0 = (Object)adaptor.nil();


					KW_FLOAT13=(Token)match(input,KW_FLOAT,FOLLOW_KW_FLOAT_in_data_type380); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					KW_FLOAT13_tree = (Object)adaptor.create(KW_FLOAT13);
					adaptor.addChild(root_0, KW_FLOAT13_tree);
					}

					}
					break;
				case 9 :
					// DFSParser.g:69:9: KW_LONG
					{
					root_0 = (Object)adaptor.nil();


					KW_LONG14=(Token)match(input,KW_LONG,FOLLOW_KW_LONG_in_data_type390); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					KW_LONG14_tree = (Object)adaptor.create(KW_LONG14);
					adaptor.addChild(root_0, KW_LONG14_tree);
					}

					}
					break;
				case 10 :
					// DFSParser.g:70:9: DECIMAL_TYPE
					{
					root_0 = (Object)adaptor.nil();


					DECIMAL_TYPE15=(Token)match(input,DECIMAL_TYPE,FOLLOW_DECIMAL_TYPE_in_data_type400); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					DECIMAL_TYPE15_tree = (Object)adaptor.create(DECIMAL_TYPE15);
					adaptor.addChild(root_0, DECIMAL_TYPE15_tree);
					}

					}
					break;
				case 11 :
					// DFSParser.g:71:9: KW_STRING
					{
					root_0 = (Object)adaptor.nil();


					KW_STRING16=(Token)match(input,KW_STRING,FOLLOW_KW_STRING_in_data_type410); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					KW_STRING16_tree = (Object)adaptor.create(KW_STRING16);
					adaptor.addChild(root_0, KW_STRING16_tree);
					}

					}
					break;
				case 12 :
					// DFSParser.g:72:9: ARRAY_INTEGER
					{
					root_0 = (Object)adaptor.nil();


					ARRAY_INTEGER17=(Token)match(input,ARRAY_INTEGER,FOLLOW_ARRAY_INTEGER_in_data_type420); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ARRAY_INTEGER17_tree = (Object)adaptor.create(ARRAY_INTEGER17);
					adaptor.addChild(root_0, ARRAY_INTEGER17_tree);
					}

					}
					break;
				case 13 :
					// DFSParser.g:73:9: ARRAY_BOOLEAN
					{
					root_0 = (Object)adaptor.nil();


					ARRAY_BOOLEAN18=(Token)match(input,ARRAY_BOOLEAN,FOLLOW_ARRAY_BOOLEAN_in_data_type430); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ARRAY_BOOLEAN18_tree = (Object)adaptor.create(ARRAY_BOOLEAN18);
					adaptor.addChild(root_0, ARRAY_BOOLEAN18_tree);
					}

					}
					break;
				case 14 :
					// DFSParser.g:74:9: ARRAY_DATE
					{
					root_0 = (Object)adaptor.nil();


					ARRAY_DATE19=(Token)match(input,ARRAY_DATE,FOLLOW_ARRAY_DATE_in_data_type440); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ARRAY_DATE19_tree = (Object)adaptor.create(ARRAY_DATE19);
					adaptor.addChild(root_0, ARRAY_DATE19_tree);
					}

					}
					break;
				case 15 :
					// DFSParser.g:75:9: ARRAY_TIMESTAMP
					{
					root_0 = (Object)adaptor.nil();


					ARRAY_TIMESTAMP20=(Token)match(input,ARRAY_TIMESTAMP,FOLLOW_ARRAY_TIMESTAMP_in_data_type450); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ARRAY_TIMESTAMP20_tree = (Object)adaptor.create(ARRAY_TIMESTAMP20);
					adaptor.addChild(root_0, ARRAY_TIMESTAMP20_tree);
					}

					}
					break;
				case 16 :
					// DFSParser.g:76:9: ARRAY_ANY
					{
					root_0 = (Object)adaptor.nil();


					ARRAY_ANY21=(Token)match(input,ARRAY_ANY,FOLLOW_ARRAY_ANY_in_data_type460); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ARRAY_ANY21_tree = (Object)adaptor.create(ARRAY_ANY21);
					adaptor.addChild(root_0, ARRAY_ANY21_tree);
					}

					}
					break;
				case 17 :
					// DFSParser.g:77:9: ARRAY_SHORT
					{
					root_0 = (Object)adaptor.nil();


					ARRAY_SHORT22=(Token)match(input,ARRAY_SHORT,FOLLOW_ARRAY_SHORT_in_data_type470); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ARRAY_SHORT22_tree = (Object)adaptor.create(ARRAY_SHORT22);
					adaptor.addChild(root_0, ARRAY_SHORT22_tree);
					}

					}
					break;
				case 18 :
					// DFSParser.g:78:9: ARRAY_DOUBLE
					{
					root_0 = (Object)adaptor.nil();


					ARRAY_DOUBLE23=(Token)match(input,ARRAY_DOUBLE,FOLLOW_ARRAY_DOUBLE_in_data_type480); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ARRAY_DOUBLE23_tree = (Object)adaptor.create(ARRAY_DOUBLE23);
					adaptor.addChild(root_0, ARRAY_DOUBLE23_tree);
					}

					}
					break;
				case 19 :
					// DFSParser.g:79:9: ARRAY_FLOAT
					{
					root_0 = (Object)adaptor.nil();


					ARRAY_FLOAT24=(Token)match(input,ARRAY_FLOAT,FOLLOW_ARRAY_FLOAT_in_data_type490); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ARRAY_FLOAT24_tree = (Object)adaptor.create(ARRAY_FLOAT24);
					adaptor.addChild(root_0, ARRAY_FLOAT24_tree);
					}

					}
					break;
				case 20 :
					// DFSParser.g:80:9: ARRAY_LONG
					{
					root_0 = (Object)adaptor.nil();


					ARRAY_LONG25=(Token)match(input,ARRAY_LONG,FOLLOW_ARRAY_LONG_in_data_type500); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ARRAY_LONG25_tree = (Object)adaptor.create(ARRAY_LONG25);
					adaptor.addChild(root_0, ARRAY_LONG25_tree);
					}

					}
					break;
				case 21 :
					// DFSParser.g:81:9: ARRAY_DECIMAL
					{
					root_0 = (Object)adaptor.nil();


					ARRAY_DECIMAL26=(Token)match(input,ARRAY_DECIMAL,FOLLOW_ARRAY_DECIMAL_in_data_type510); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ARRAY_DECIMAL26_tree = (Object)adaptor.create(ARRAY_DECIMAL26);
					adaptor.addChild(root_0, ARRAY_DECIMAL26_tree);
					}

					}
					break;
				case 22 :
					// DFSParser.g:82:9: ARRAY_STRING
					{
					root_0 = (Object)adaptor.nil();


					ARRAY_STRING27=(Token)match(input,ARRAY_STRING,FOLLOW_ARRAY_STRING_in_data_type520); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ARRAY_STRING27_tree = (Object)adaptor.create(ARRAY_STRING27);
					adaptor.addChild(root_0, ARRAY_STRING27_tree);
					}

					}
					break;
				case 23 :
					// DFSParser.g:83:9: param_map_data_type
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_param_map_data_type_in_data_type530);
					param_map_data_type28=param_map_data_type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, param_map_data_type28.getTree());

					}
					break;
				case 24 :
					// DFSParser.g:84:9: LEFT_PARENT RIGHT_PARENT
					{
					root_0 = (Object)adaptor.nil();


					LEFT_PARENT29=(Token)match(input,LEFT_PARENT,FOLLOW_LEFT_PARENT_in_data_type540); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					LEFT_PARENT29_tree = (Object)adaptor.create(LEFT_PARENT29);
					adaptor.addChild(root_0, LEFT_PARENT29_tree);
					}

					RIGHT_PARENT30=(Token)match(input,RIGHT_PARENT,FOLLOW_RIGHT_PARENT_in_data_type542); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					RIGHT_PARENT30_tree = (Object)adaptor.create(RIGHT_PARENT30);
					adaptor.addChild(root_0, RIGHT_PARENT30_tree);
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 8, data_type_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "data_type"


	public static class param_map_data_type_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "param_map_data_type"
	// DFSParser.g:87:1: param_map_data_type : LEFT_BRACKET key= data_type COMMA value= data_type RIGHT_BRACKET -> ^( AST_MAP_DATA_TYPE LEFT_BRACKET ^( AST_MAP_KEY_TYPE $key) COMMA ^( AST_MAP_VALUE_TYPE $value) RIGHT_BRACKET ) ;
	public final DFSParser.param_map_data_type_return param_map_data_type() throws RecognitionException {
		DFSParser.param_map_data_type_return retval = new DFSParser.param_map_data_type_return();
		retval.start = input.LT(1);
		int param_map_data_type_StartIndex = input.index();

		Object root_0 = null;

		Token LEFT_BRACKET31=null;
		Token COMMA32=null;
		Token RIGHT_BRACKET33=null;
		ParserRuleReturnScope key =null;
		ParserRuleReturnScope value =null;

		Object LEFT_BRACKET31_tree=null;
		Object COMMA32_tree=null;
		Object RIGHT_BRACKET33_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleTokenStream stream_RIGHT_BRACKET=new RewriteRuleTokenStream(adaptor,"token RIGHT_BRACKET");
		RewriteRuleTokenStream stream_LEFT_BRACKET=new RewriteRuleTokenStream(adaptor,"token LEFT_BRACKET");
		RewriteRuleSubtreeStream stream_data_type=new RewriteRuleSubtreeStream(adaptor,"rule data_type");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return retval; }

			// DFSParser.g:88:5: ( LEFT_BRACKET key= data_type COMMA value= data_type RIGHT_BRACKET -> ^( AST_MAP_DATA_TYPE LEFT_BRACKET ^( AST_MAP_KEY_TYPE $key) COMMA ^( AST_MAP_VALUE_TYPE $value) RIGHT_BRACKET ) )
			// DFSParser.g:88:9: LEFT_BRACKET key= data_type COMMA value= data_type RIGHT_BRACKET
			{
			LEFT_BRACKET31=(Token)match(input,LEFT_BRACKET,FOLLOW_LEFT_BRACKET_in_param_map_data_type561); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LEFT_BRACKET.add(LEFT_BRACKET31);

			pushFollow(FOLLOW_data_type_in_param_map_data_type565);
			key=data_type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_data_type.add(key.getTree());
			COMMA32=(Token)match(input,COMMA,FOLLOW_COMMA_in_param_map_data_type567); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_COMMA.add(COMMA32);

			pushFollow(FOLLOW_data_type_in_param_map_data_type571);
			value=data_type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_data_type.add(value.getTree());
			RIGHT_BRACKET33=(Token)match(input,RIGHT_BRACKET,FOLLOW_RIGHT_BRACKET_in_param_map_data_type573); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RIGHT_BRACKET.add(RIGHT_BRACKET33);

			// AST REWRITE
			// elements: value, COMMA, key, LEFT_BRACKET, RIGHT_BRACKET
			// token labels: 
			// rule labels: value, key, retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_value=new RewriteRuleSubtreeStream(adaptor,"rule value",value!=null?value.getTree():null);
			RewriteRuleSubtreeStream stream_key=new RewriteRuleSubtreeStream(adaptor,"rule key",key!=null?key.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 89:9: -> ^( AST_MAP_DATA_TYPE LEFT_BRACKET ^( AST_MAP_KEY_TYPE $key) COMMA ^( AST_MAP_VALUE_TYPE $value) RIGHT_BRACKET )
			{
				// DFSParser.g:89:12: ^( AST_MAP_DATA_TYPE LEFT_BRACKET ^( AST_MAP_KEY_TYPE $key) COMMA ^( AST_MAP_VALUE_TYPE $value) RIGHT_BRACKET )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_MAP_DATA_TYPE, "AST_MAP_DATA_TYPE"), root_1);
				adaptor.addChild(root_1, stream_LEFT_BRACKET.nextNode());
				// DFSParser.g:89:45: ^( AST_MAP_KEY_TYPE $key)
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_MAP_KEY_TYPE, "AST_MAP_KEY_TYPE"), root_2);
				adaptor.addChild(root_2, stream_key.nextTree());
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_1, stream_COMMA.nextNode());
				// DFSParser.g:89:76: ^( AST_MAP_VALUE_TYPE $value)
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_MAP_VALUE_TYPE, "AST_MAP_VALUE_TYPE"), root_2);
				adaptor.addChild(root_2, stream_value.nextTree());
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_1, stream_RIGHT_BRACKET.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 9, param_map_data_type_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "param_map_data_type"


	public static class transformations_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "transformations"
	// DFSParser.g:92:1: transformations : (tran+= transformation OUTPUT_STREAM sn+= stream_name )+ -> ^( AST_TRANSFORMATIONS ( ^( AST_TRANSFORMATION[contextState] $tran OUTPUT_STREAM $sn) )+ ) ;
	public final DFSParser.transformations_return transformations() throws RecognitionException {
		DFSParser.transformations_return retval = new DFSParser.transformations_return();
		retval.start = input.LT(1);
		int transformations_StartIndex = input.index();

		Object root_0 = null;

		Token OUTPUT_STREAM34=null;
		List<Object> list_tran=null;
		List<Object> list_sn=null;
		RuleReturnScope tran = null;
		RuleReturnScope sn = null;
		Object OUTPUT_STREAM34_tree=null;
		RewriteRuleTokenStream stream_OUTPUT_STREAM=new RewriteRuleTokenStream(adaptor,"token OUTPUT_STREAM");
		RewriteRuleSubtreeStream stream_stream_name=new RewriteRuleSubtreeStream(adaptor,"rule stream_name");
		RewriteRuleSubtreeStream stream_transformation=new RewriteRuleSubtreeStream(adaptor,"rule transformation");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return retval; }

			// DFSParser.g:93:5: ( (tran+= transformation OUTPUT_STREAM sn+= stream_name )+ -> ^( AST_TRANSFORMATIONS ( ^( AST_TRANSFORMATION[contextState] $tran OUTPUT_STREAM $sn) )+ ) )
			// DFSParser.g:93:9: (tran+= transformation OUTPUT_STREAM sn+= stream_name )+
			{
			// DFSParser.g:93:9: (tran+= transformation OUTPUT_STREAM sn+= stream_name )+
			int cnt7=0;
			loop7:
			while (true) {
				int alt7=2;
				int LA7_0 = input.LA(1);
				if ( (LA7_0==AT_SIGN||LA7_0==COMMA||(LA7_0 >= KW_ANY && LA7_0 <= KW_TRUE)||LA7_0==SIMPLE_ID) ) {
					alt7=1;
				}

				switch (alt7) {
				case 1 :
					// DFSParser.g:93:10: tran+= transformation OUTPUT_STREAM sn+= stream_name
					{
					pushFollow(FOLLOW_transformation_in_transformations629);
					tran=transformation();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_transformation.add(tran.getTree());
					if (list_tran==null) list_tran=new ArrayList<Object>();
					list_tran.add(tran.getTree());
					OUTPUT_STREAM34=(Token)match(input,OUTPUT_STREAM,FOLLOW_OUTPUT_STREAM_in_transformations631); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_OUTPUT_STREAM.add(OUTPUT_STREAM34);

					pushFollow(FOLLOW_stream_name_in_transformations635);
					sn=stream_name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_stream_name.add(sn.getTree());
					if (list_sn==null) list_sn=new ArrayList<Object>();
					list_sn.add(sn.getTree());
					}
					break;

				default :
					if ( cnt7 >= 1 ) break loop7;
					if (state.backtracking>0) {state.failed=true; return retval;}
					EarlyExitException eee = new EarlyExitException(7, input);
					throw eee;
				}
				cnt7++;
			}

			// AST REWRITE
			// elements: sn, OUTPUT_STREAM, tran
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: sn, tran
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_sn=new RewriteRuleSubtreeStream(adaptor,"token sn",list_sn);
			RewriteRuleSubtreeStream stream_tran=new RewriteRuleSubtreeStream(adaptor,"token tran",list_tran);
			root_0 = (Object)adaptor.nil();
			// 94:9: -> ^( AST_TRANSFORMATIONS ( ^( AST_TRANSFORMATION[contextState] $tran OUTPUT_STREAM $sn) )+ )
			{
				// DFSParser.g:94:12: ^( AST_TRANSFORMATIONS ( ^( AST_TRANSFORMATION[contextState] $tran OUTPUT_STREAM $sn) )+ )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_TRANSFORMATIONS, "AST_TRANSFORMATIONS"), root_1);
				if ( !(stream_sn.hasNext()||stream_OUTPUT_STREAM.hasNext()||stream_tran.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_sn.hasNext()||stream_OUTPUT_STREAM.hasNext()||stream_tran.hasNext() ) {
					// DFSParser.g:94:34: ^( AST_TRANSFORMATION[contextState] $tran OUTPUT_STREAM $sn)
					{
					Object root_2 = (Object)adaptor.nil();
					root_2 = (Object)adaptor.becomeRoot(new AstTransformation(AST_TRANSFORMATION, contextState), root_2);
					adaptor.addChild(root_2, stream_tran.nextTree());
					adaptor.addChild(root_2, stream_OUTPUT_STREAM.nextNode());
					adaptor.addChild(root_2, stream_sn.nextTree());
					adaptor.addChild(root_1, root_2);
					}

				}
				stream_sn.reset();
				stream_OUTPUT_STREAM.reset();
				stream_tran.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 10, transformations_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "transformations"


	public static class inputStreams_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "inputStreams"
	// DFSParser.g:97:1: inputStreams : sn1= stream_name ( COMMA sn2+= stream_name )* -> ^( AST_INPUT_STREAMS $sn1 ( COMMA $sn2)* ) ;
	public final DFSParser.inputStreams_return inputStreams() throws RecognitionException {
		DFSParser.inputStreams_return retval = new DFSParser.inputStreams_return();
		retval.start = input.LT(1);
		int inputStreams_StartIndex = input.index();

		Object root_0 = null;

		Token COMMA35=null;
		List<Object> list_sn2=null;
		ParserRuleReturnScope sn1 =null;
		RuleReturnScope sn2 = null;
		Object COMMA35_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleSubtreeStream stream_stream_name=new RewriteRuleSubtreeStream(adaptor,"rule stream_name");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return retval; }

			// DFSParser.g:98:5: (sn1= stream_name ( COMMA sn2+= stream_name )* -> ^( AST_INPUT_STREAMS $sn1 ( COMMA $sn2)* ) )
			// DFSParser.g:98:9: sn1= stream_name ( COMMA sn2+= stream_name )*
			{
			pushFollow(FOLLOW_stream_name_in_inputStreams690);
			sn1=stream_name();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_stream_name.add(sn1.getTree());
			// DFSParser.g:98:25: ( COMMA sn2+= stream_name )*
			loop8:
			while (true) {
				int alt8=2;
				int LA8_0 = input.LA(1);
				if ( (LA8_0==COMMA) ) {
					alt8=1;
				}

				switch (alt8) {
				case 1 :
					// DFSParser.g:98:26: COMMA sn2+= stream_name
					{
					COMMA35=(Token)match(input,COMMA,FOLLOW_COMMA_in_inputStreams693); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(COMMA35);

					pushFollow(FOLLOW_stream_name_in_inputStreams697);
					sn2=stream_name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_stream_name.add(sn2.getTree());
					if (list_sn2==null) list_sn2=new ArrayList<Object>();
					list_sn2.add(sn2.getTree());
					}
					break;

				default :
					break loop8;
				}
			}

			// AST REWRITE
			// elements: sn2, sn1, COMMA
			// token labels: 
			// rule labels: sn1, retval
			// token list labels: 
			// rule list labels: sn2
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_sn1=new RewriteRuleSubtreeStream(adaptor,"rule sn1",sn1!=null?sn1.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_sn2=new RewriteRuleSubtreeStream(adaptor,"token sn2",list_sn2);
			root_0 = (Object)adaptor.nil();
			// 99:9: -> ^( AST_INPUT_STREAMS $sn1 ( COMMA $sn2)* )
			{
				// DFSParser.g:99:12: ^( AST_INPUT_STREAMS $sn1 ( COMMA $sn2)* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_INPUT_STREAMS, "AST_INPUT_STREAMS"), root_1);
				adaptor.addChild(root_1, stream_sn1.nextTree());
				// DFSParser.g:99:37: ( COMMA $sn2)*
				while ( stream_sn2.hasNext()||stream_COMMA.hasNext() ) {
					adaptor.addChild(root_1, stream_COMMA.nextNode());
					adaptor.addChild(root_1, stream_sn2.nextTree());
				}
				stream_sn2.reset();
				stream_COMMA.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 11, inputStreams_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "inputStreams"


	public static class stream_name_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "stream_name"
	// DFSParser.g:102:1: stream_name : id1= all_identifier ( AT_SIGN ssn= second_stream_name )? -> ^( AST_STREAM_NAME[contextState] ^( AST_TRANSFORMATION_NAME[contextState] $id1) ( AT_SIGN $ssn)? ) ;
	public final DFSParser.stream_name_return stream_name() throws RecognitionException {
		DFSParser.stream_name_return retval = new DFSParser.stream_name_return();
		retval.start = input.LT(1);
		int stream_name_StartIndex = input.index();

		Object root_0 = null;

		Token AT_SIGN36=null;
		ParserRuleReturnScope id1 =null;
		ParserRuleReturnScope ssn =null;

		Object AT_SIGN36_tree=null;
		RewriteRuleTokenStream stream_AT_SIGN=new RewriteRuleTokenStream(adaptor,"token AT_SIGN");
		RewriteRuleSubtreeStream stream_second_stream_name=new RewriteRuleSubtreeStream(adaptor,"rule second_stream_name");
		RewriteRuleSubtreeStream stream_all_identifier=new RewriteRuleSubtreeStream(adaptor,"rule all_identifier");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 12) ) { return retval; }

			// DFSParser.g:103:5: (id1= all_identifier ( AT_SIGN ssn= second_stream_name )? -> ^( AST_STREAM_NAME[contextState] ^( AST_TRANSFORMATION_NAME[contextState] $id1) ( AT_SIGN $ssn)? ) )
			// DFSParser.g:103:9: id1= all_identifier ( AT_SIGN ssn= second_stream_name )?
			{
			pushFollow(FOLLOW_all_identifier_in_stream_name745);
			id1=all_identifier();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_all_identifier.add(id1.getTree());
			// DFSParser.g:103:28: ( AT_SIGN ssn= second_stream_name )?
			int alt9=2;
			int LA9_0 = input.LA(1);
			if ( (LA9_0==AT_SIGN) ) {
				alt9=1;
			}
			switch (alt9) {
				case 1 :
					// DFSParser.g:103:29: AT_SIGN ssn= second_stream_name
					{
					AT_SIGN36=(Token)match(input,AT_SIGN,FOLLOW_AT_SIGN_in_stream_name748); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_AT_SIGN.add(AT_SIGN36);

					pushFollow(FOLLOW_second_stream_name_in_stream_name752);
					ssn=second_stream_name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_second_stream_name.add(ssn.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: id1, ssn, AT_SIGN
			// token labels: 
			// rule labels: id1, retval, ssn
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_id1=new RewriteRuleSubtreeStream(adaptor,"rule id1",id1!=null?id1.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_ssn=new RewriteRuleSubtreeStream(adaptor,"rule ssn",ssn!=null?ssn.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 104:9: -> ^( AST_STREAM_NAME[contextState] ^( AST_TRANSFORMATION_NAME[contextState] $id1) ( AT_SIGN $ssn)? )
			{
				// DFSParser.g:104:12: ^( AST_STREAM_NAME[contextState] ^( AST_TRANSFORMATION_NAME[contextState] $id1) ( AT_SIGN $ssn)? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(new AstADFStream(AST_STREAM_NAME, contextState), root_1);
				// DFSParser.g:104:58: ^( AST_TRANSFORMATION_NAME[contextState] $id1)
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot(new AstIdentifier(AST_TRANSFORMATION_NAME, contextState), root_2);
				adaptor.addChild(root_2, stream_id1.nextTree());
				adaptor.addChild(root_1, root_2);
				}

				// DFSParser.g:104:119: ( AT_SIGN $ssn)?
				if ( stream_ssn.hasNext()||stream_AT_SIGN.hasNext() ) {
					adaptor.addChild(root_1, stream_AT_SIGN.nextNode());
					adaptor.addChild(root_1, stream_ssn.nextTree());
				}
				stream_ssn.reset();
				stream_AT_SIGN.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 12, stream_name_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "stream_name"


	public static class second_stream_name_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "second_stream_name"
	// DFSParser.g:107:1: second_stream_name : (id= all_identifier -> ^( AST_SUBSTREAM_NAME[contextState] $id) | LEFT_PARENT id1= all_identifier ( COMMA id2+= all_identifier )* RIGHT_PARENT -> LEFT_PARENT ^( AST_SUBSTREAM_NAME[contextState] $id1) ( COMMA ^( AST_SUBSTREAM_NAME[contextState] $id2) )* RIGHT_PARENT );
	public final DFSParser.second_stream_name_return second_stream_name() throws RecognitionException {
		DFSParser.second_stream_name_return retval = new DFSParser.second_stream_name_return();
		retval.start = input.LT(1);
		int second_stream_name_StartIndex = input.index();

		Object root_0 = null;

		Token LEFT_PARENT37=null;
		Token COMMA38=null;
		Token RIGHT_PARENT39=null;
		List<Object> list_id2=null;
		ParserRuleReturnScope id =null;
		ParserRuleReturnScope id1 =null;
		RuleReturnScope id2 = null;
		Object LEFT_PARENT37_tree=null;
		Object COMMA38_tree=null;
		Object RIGHT_PARENT39_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleTokenStream stream_RIGHT_PARENT=new RewriteRuleTokenStream(adaptor,"token RIGHT_PARENT");
		RewriteRuleTokenStream stream_LEFT_PARENT=new RewriteRuleTokenStream(adaptor,"token LEFT_PARENT");
		RewriteRuleSubtreeStream stream_all_identifier=new RewriteRuleSubtreeStream(adaptor,"rule all_identifier");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 13) ) { return retval; }

			// DFSParser.g:108:5: (id= all_identifier -> ^( AST_SUBSTREAM_NAME[contextState] $id) | LEFT_PARENT id1= all_identifier ( COMMA id2+= all_identifier )* RIGHT_PARENT -> LEFT_PARENT ^( AST_SUBSTREAM_NAME[contextState] $id1) ( COMMA ^( AST_SUBSTREAM_NAME[contextState] $id2) )* RIGHT_PARENT )
			int alt11=2;
			int LA11_0 = input.LA(1);
			if ( (LA11_0==EOF||(LA11_0 >= KW_ANY && LA11_0 <= KW_TRUE)||LA11_0==SIMPLE_ID) ) {
				alt11=1;
			}
			else if ( (LA11_0==LEFT_PARENT) ) {
				alt11=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 11, 0, input);
				throw nvae;
			}

			switch (alt11) {
				case 1 :
					// DFSParser.g:108:9: id= all_identifier
					{
					pushFollow(FOLLOW_all_identifier_in_second_stream_name813);
					id=all_identifier();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_all_identifier.add(id.getTree());
					// AST REWRITE
					// elements: id
					// token labels: 
					// rule labels: id, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_id=new RewriteRuleSubtreeStream(adaptor,"rule id",id!=null?id.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 109:9: -> ^( AST_SUBSTREAM_NAME[contextState] $id)
					{
						// DFSParser.g:109:12: ^( AST_SUBSTREAM_NAME[contextState] $id)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(new AstIdentifier(AST_SUBSTREAM_NAME, contextState), root_1);
						adaptor.addChild(root_1, stream_id.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// DFSParser.g:110:9: LEFT_PARENT id1= all_identifier ( COMMA id2+= all_identifier )* RIGHT_PARENT
					{
					LEFT_PARENT37=(Token)match(input,LEFT_PARENT,FOLLOW_LEFT_PARENT_in_second_stream_name844); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LEFT_PARENT.add(LEFT_PARENT37);

					pushFollow(FOLLOW_all_identifier_in_second_stream_name848);
					id1=all_identifier();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_all_identifier.add(id1.getTree());
					// DFSParser.g:110:40: ( COMMA id2+= all_identifier )*
					loop10:
					while (true) {
						int alt10=2;
						int LA10_0 = input.LA(1);
						if ( (LA10_0==COMMA) ) {
							alt10=1;
						}

						switch (alt10) {
						case 1 :
							// DFSParser.g:110:41: COMMA id2+= all_identifier
							{
							COMMA38=(Token)match(input,COMMA,FOLLOW_COMMA_in_second_stream_name851); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_COMMA.add(COMMA38);

							pushFollow(FOLLOW_all_identifier_in_second_stream_name855);
							id2=all_identifier();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_all_identifier.add(id2.getTree());
							if (list_id2==null) list_id2=new ArrayList<Object>();
							list_id2.add(id2.getTree());
							}
							break;

						default :
							break loop10;
						}
					}

					RIGHT_PARENT39=(Token)match(input,RIGHT_PARENT,FOLLOW_RIGHT_PARENT_in_second_stream_name859); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RIGHT_PARENT.add(RIGHT_PARENT39);

					// AST REWRITE
					// elements: COMMA, RIGHT_PARENT, LEFT_PARENT, id2, id1
					// token labels: 
					// rule labels: id1, retval
					// token list labels: 
					// rule list labels: id2
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_id1=new RewriteRuleSubtreeStream(adaptor,"rule id1",id1!=null?id1.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_id2=new RewriteRuleSubtreeStream(adaptor,"token id2",list_id2);
					root_0 = (Object)adaptor.nil();
					// 111:9: -> LEFT_PARENT ^( AST_SUBSTREAM_NAME[contextState] $id1) ( COMMA ^( AST_SUBSTREAM_NAME[contextState] $id2) )* RIGHT_PARENT
					{
						adaptor.addChild(root_0, stream_LEFT_PARENT.nextNode());
						// DFSParser.g:111:24: ^( AST_SUBSTREAM_NAME[contextState] $id1)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(new AstIdentifier(AST_SUBSTREAM_NAME, contextState), root_1);
						adaptor.addChild(root_1, stream_id1.nextTree());
						adaptor.addChild(root_0, root_1);
						}

						// DFSParser.g:111:81: ( COMMA ^( AST_SUBSTREAM_NAME[contextState] $id2) )*
						while ( stream_COMMA.hasNext()||stream_id2.hasNext() ) {
							adaptor.addChild(root_0, stream_COMMA.nextNode());
							// DFSParser.g:111:88: ^( AST_SUBSTREAM_NAME[contextState] $id2)
							{
							Object root_1 = (Object)adaptor.nil();
							root_1 = (Object)adaptor.becomeRoot(new AstIdentifier(AST_SUBSTREAM_NAME, contextState), root_1);
							adaptor.addChild(root_1, stream_id2.nextTree());
							adaptor.addChild(root_0, root_1);
							}

						}
						stream_COMMA.reset();
						stream_id2.reset();

						adaptor.addChild(root_0, stream_RIGHT_PARENT.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 13, second_stream_name_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "second_stream_name"


	public static class transformation_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "transformation"
	// DFSParser.g:114:1: transformation : (st= source_transformation -> $st|is= inputStreams twi= transformation_with_inputs -> $is $twi);
	public final DFSParser.transformation_return transformation() throws RecognitionException {
		DFSParser.transformation_return retval = new DFSParser.transformation_return();
		retval.start = input.LT(1);
		int transformation_StartIndex = input.index();

		Object root_0 = null;

		ParserRuleReturnScope st =null;
		ParserRuleReturnScope is =null;
		ParserRuleReturnScope twi =null;

		RewriteRuleSubtreeStream stream_inputStreams=new RewriteRuleSubtreeStream(adaptor,"rule inputStreams");
		RewriteRuleSubtreeStream stream_source_transformation=new RewriteRuleSubtreeStream(adaptor,"rule source_transformation");
		RewriteRuleSubtreeStream stream_transformation_with_inputs=new RewriteRuleSubtreeStream(adaptor,"rule transformation_with_inputs");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 14) ) { return retval; }

			// DFSParser.g:115:5: (st= source_transformation -> $st|is= inputStreams twi= transformation_with_inputs -> $is $twi)
			int alt12=2;
			int LA12_0 = input.LA(1);
			if ( (LA12_0==KW_SOURCE) ) {
				int LA12_1 = input.LA(2);
				if ( (LA12_1==LEFT_PARENT) ) {
					alt12=1;
				}
				else if ( (LA12_1==AT_SIGN||LA12_1==COMMA||LA12_1==KW_FILTER||LA12_1==KW_JOIN||LA12_1==KW_SINK||LA12_1==SIMPLE_ID) ) {
					alt12=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 12, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA12_0==AT_SIGN||LA12_0==COMMA||(LA12_0 >= KW_ANY && LA12_0 <= KW_SINK)||(LA12_0 >= KW_STRING && LA12_0 <= KW_TRUE)||LA12_0==SIMPLE_ID) ) {
				alt12=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 12, 0, input);
				throw nvae;
			}

			switch (alt12) {
				case 1 :
					// DFSParser.g:115:9: st= source_transformation
					{
					pushFollow(FOLLOW_source_transformation_in_transformation922);
					st=source_transformation();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_source_transformation.add(st.getTree());
					// AST REWRITE
					// elements: st
					// token labels: 
					// rule labels: st, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_st=new RewriteRuleSubtreeStream(adaptor,"rule st",st!=null?st.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 116:9: -> $st
					{
						adaptor.addChild(root_0, stream_st.nextTree());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// DFSParser.g:117:9: is= inputStreams twi= transformation_with_inputs
					{
					pushFollow(FOLLOW_inputStreams_in_transformation947);
					is=inputStreams();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_inputStreams.add(is.getTree());
					pushFollow(FOLLOW_transformation_with_inputs_in_transformation951);
					twi=transformation_with_inputs();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_transformation_with_inputs.add(twi.getTree());
					// AST REWRITE
					// elements: is, twi
					// token labels: 
					// rule labels: twi, is, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_twi=new RewriteRuleSubtreeStream(adaptor,"rule twi",twi!=null?twi.getTree():null);
					RewriteRuleSubtreeStream stream_is=new RewriteRuleSubtreeStream(adaptor,"rule is",is!=null?is.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 118:9: -> $is $twi
					{
						adaptor.addChild(root_0, stream_is.nextTree());
						adaptor.addChild(root_0, stream_twi.nextTree());
					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 14, transformation_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "transformation"


	public static class source_transformation_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "source_transformation"
	// DFSParser.g:121:1: source_transformation : KW_SOURCE ta= transformation_attributes -> ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_SOURCE $ta) ;
	public final DFSParser.source_transformation_return source_transformation() throws RecognitionException {
		DFSParser.source_transformation_return retval = new DFSParser.source_transformation_return();
		retval.start = input.LT(1);
		int source_transformation_StartIndex = input.index();

		Object root_0 = null;

		Token KW_SOURCE40=null;
		ParserRuleReturnScope ta =null;

		Object KW_SOURCE40_tree=null;
		RewriteRuleTokenStream stream_KW_SOURCE=new RewriteRuleTokenStream(adaptor,"token KW_SOURCE");
		RewriteRuleSubtreeStream stream_transformation_attributes=new RewriteRuleSubtreeStream(adaptor,"rule transformation_attributes");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 15) ) { return retval; }

			// DFSParser.g:122:5: ( KW_SOURCE ta= transformation_attributes -> ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_SOURCE $ta) )
			// DFSParser.g:122:9: KW_SOURCE ta= transformation_attributes
			{
			KW_SOURCE40=(Token)match(input,KW_SOURCE,FOLLOW_KW_SOURCE_in_source_transformation986); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_KW_SOURCE.add(KW_SOURCE40);

			pushFollow(FOLLOW_transformation_attributes_in_source_transformation991);
			ta=transformation_attributes();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_transformation_attributes.add(ta.getTree());
			// AST REWRITE
			// elements: KW_SOURCE, ta
			// token labels: 
			// rule labels: ta, retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_ta=new RewriteRuleSubtreeStream(adaptor,"rule ta",ta!=null?ta.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 123:9: -> ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_SOURCE $ta)
			{
				// DFSParser.g:123:12: ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_SOURCE $ta)
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(new AstSourceTransformation(AST_CONCRETE_TRANSFORMATION, contextState), root_1);
				adaptor.addChild(root_1, stream_KW_SOURCE.nextNode());
				adaptor.addChild(root_1, stream_ta.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 15, source_transformation_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "source_transformation"


	public static class transformation_with_inputs_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "transformation_with_inputs"
	// DFSParser.g:126:1: transformation_with_inputs : (ft= filter_transformation -> $ft|jt= join_transformation -> $jt|st= sink_transformation -> $st|gt= general_transformation -> $gt);
	public final DFSParser.transformation_with_inputs_return transformation_with_inputs() throws RecognitionException {
		DFSParser.transformation_with_inputs_return retval = new DFSParser.transformation_with_inputs_return();
		retval.start = input.LT(1);
		int transformation_with_inputs_StartIndex = input.index();

		Object root_0 = null;

		ParserRuleReturnScope ft =null;
		ParserRuleReturnScope jt =null;
		ParserRuleReturnScope st =null;
		ParserRuleReturnScope gt =null;

		RewriteRuleSubtreeStream stream_sink_transformation=new RewriteRuleSubtreeStream(adaptor,"rule sink_transformation");
		RewriteRuleSubtreeStream stream_join_transformation=new RewriteRuleSubtreeStream(adaptor,"rule join_transformation");
		RewriteRuleSubtreeStream stream_general_transformation=new RewriteRuleSubtreeStream(adaptor,"rule general_transformation");
		RewriteRuleSubtreeStream stream_filter_transformation=new RewriteRuleSubtreeStream(adaptor,"rule filter_transformation");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 16) ) { return retval; }

			// DFSParser.g:127:5: (ft= filter_transformation -> $ft|jt= join_transformation -> $jt|st= sink_transformation -> $st|gt= general_transformation -> $gt)
			int alt13=4;
			switch ( input.LA(1) ) {
			case KW_FILTER:
				{
				alt13=1;
				}
				break;
			case KW_JOIN:
				{
				alt13=2;
				}
				break;
			case KW_SINK:
				{
				alt13=3;
				}
				break;
			case SIMPLE_ID:
				{
				alt13=4;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 13, 0, input);
				throw nvae;
			}
			switch (alt13) {
				case 1 :
					// DFSParser.g:127:9: ft= filter_transformation
					{
					pushFollow(FOLLOW_filter_transformation_in_transformation_with_inputs1035);
					ft=filter_transformation();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_filter_transformation.add(ft.getTree());
					// AST REWRITE
					// elements: ft
					// token labels: 
					// rule labels: ft, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_ft=new RewriteRuleSubtreeStream(adaptor,"rule ft",ft!=null?ft.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 128:9: -> $ft
					{
						adaptor.addChild(root_0, stream_ft.nextTree());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// DFSParser.g:129:9: jt= join_transformation
					{
					pushFollow(FOLLOW_join_transformation_in_transformation_with_inputs1060);
					jt=join_transformation();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_join_transformation.add(jt.getTree());
					// AST REWRITE
					// elements: jt
					// token labels: 
					// rule labels: jt, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_jt=new RewriteRuleSubtreeStream(adaptor,"rule jt",jt!=null?jt.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 130:9: -> $jt
					{
						adaptor.addChild(root_0, stream_jt.nextTree());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// DFSParser.g:131:9: st= sink_transformation
					{
					pushFollow(FOLLOW_sink_transformation_in_transformation_with_inputs1085);
					st=sink_transformation();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_sink_transformation.add(st.getTree());
					// AST REWRITE
					// elements: st
					// token labels: 
					// rule labels: st, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_st=new RewriteRuleSubtreeStream(adaptor,"rule st",st!=null?st.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 132:9: -> $st
					{
						adaptor.addChild(root_0, stream_st.nextTree());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 4 :
					// DFSParser.g:133:9: gt= general_transformation
					{
					pushFollow(FOLLOW_general_transformation_in_transformation_with_inputs1110);
					gt=general_transformation();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_general_transformation.add(gt.getTree());
					// AST REWRITE
					// elements: gt
					// token labels: 
					// rule labels: gt, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_gt=new RewriteRuleSubtreeStream(adaptor,"rule gt",gt!=null?gt.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 134:9: -> $gt
					{
						adaptor.addChild(root_0, stream_gt.nextTree());
					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 16, transformation_with_inputs_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "transformation_with_inputs"


	public static class filter_transformation_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "filter_transformation"
	// DFSParser.g:137:1: filter_transformation : KW_FILTER LEFT_PARENT fc= df_expression ( COMMA cta= common_transformation_attributes )? RIGHT_PARENT -> ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_FILTER LEFT_PARENT $fc ( COMMA $cta)? RIGHT_PARENT ) ;
	public final DFSParser.filter_transformation_return filter_transformation() throws RecognitionException {
		DFSParser.filter_transformation_return retval = new DFSParser.filter_transformation_return();
		retval.start = input.LT(1);
		int filter_transformation_StartIndex = input.index();

		Object root_0 = null;

		Token KW_FILTER41=null;
		Token LEFT_PARENT42=null;
		Token COMMA43=null;
		Token RIGHT_PARENT44=null;
		ParserRuleReturnScope fc =null;
		ParserRuleReturnScope cta =null;

		Object KW_FILTER41_tree=null;
		Object LEFT_PARENT42_tree=null;
		Object COMMA43_tree=null;
		Object RIGHT_PARENT44_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleTokenStream stream_RIGHT_PARENT=new RewriteRuleTokenStream(adaptor,"token RIGHT_PARENT");
		RewriteRuleTokenStream stream_KW_FILTER=new RewriteRuleTokenStream(adaptor,"token KW_FILTER");
		RewriteRuleTokenStream stream_LEFT_PARENT=new RewriteRuleTokenStream(adaptor,"token LEFT_PARENT");
		RewriteRuleSubtreeStream stream_common_transformation_attributes=new RewriteRuleSubtreeStream(adaptor,"rule common_transformation_attributes");
		RewriteRuleSubtreeStream stream_df_expression=new RewriteRuleSubtreeStream(adaptor,"rule df_expression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 17) ) { return retval; }

			// DFSParser.g:138:5: ( KW_FILTER LEFT_PARENT fc= df_expression ( COMMA cta= common_transformation_attributes )? RIGHT_PARENT -> ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_FILTER LEFT_PARENT $fc ( COMMA $cta)? RIGHT_PARENT ) )
			// DFSParser.g:138:9: KW_FILTER LEFT_PARENT fc= df_expression ( COMMA cta= common_transformation_attributes )? RIGHT_PARENT
			{
			KW_FILTER41=(Token)match(input,KW_FILTER,FOLLOW_KW_FILTER_in_filter_transformation1142); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_KW_FILTER.add(KW_FILTER41);

			LEFT_PARENT42=(Token)match(input,LEFT_PARENT,FOLLOW_LEFT_PARENT_in_filter_transformation1144); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LEFT_PARENT.add(LEFT_PARENT42);

			pushFollow(FOLLOW_df_expression_in_filter_transformation1148);
			fc=df_expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_df_expression.add(fc.getTree());
			// DFSParser.g:138:48: ( COMMA cta= common_transformation_attributes )?
			int alt14=2;
			int LA14_0 = input.LA(1);
			if ( (LA14_0==COMMA) ) {
				alt14=1;
			}
			switch (alt14) {
				case 1 :
					// DFSParser.g:138:49: COMMA cta= common_transformation_attributes
					{
					COMMA43=(Token)match(input,COMMA,FOLLOW_COMMA_in_filter_transformation1151); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(COMMA43);

					pushFollow(FOLLOW_common_transformation_attributes_in_filter_transformation1155);
					cta=common_transformation_attributes();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_common_transformation_attributes.add(cta.getTree());
					}
					break;

			}

			RIGHT_PARENT44=(Token)match(input,RIGHT_PARENT,FOLLOW_RIGHT_PARENT_in_filter_transformation1159); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RIGHT_PARENT.add(RIGHT_PARENT44);

			// AST REWRITE
			// elements: RIGHT_PARENT, fc, KW_FILTER, COMMA, LEFT_PARENT, cta
			// token labels: 
			// rule labels: cta, fc, retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_cta=new RewriteRuleSubtreeStream(adaptor,"rule cta",cta!=null?cta.getTree():null);
			RewriteRuleSubtreeStream stream_fc=new RewriteRuleSubtreeStream(adaptor,"rule fc",fc!=null?fc.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 139:9: -> ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_FILTER LEFT_PARENT $fc ( COMMA $cta)? RIGHT_PARENT )
			{
				// DFSParser.g:139:12: ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_FILTER LEFT_PARENT $fc ( COMMA $cta)? RIGHT_PARENT )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(new AstFilterTransformation(AST_CONCRETE_TRANSFORMATION, contextState), root_1);
				adaptor.addChild(root_1, stream_KW_FILTER.nextNode());
				adaptor.addChild(root_1, stream_LEFT_PARENT.nextNode());
				adaptor.addChild(root_1, stream_fc.nextTree());
				// DFSParser.g:139:107: ( COMMA $cta)?
				if ( stream_COMMA.hasNext()||stream_cta.hasNext() ) {
					adaptor.addChild(root_1, stream_COMMA.nextNode());
					adaptor.addChild(root_1, stream_cta.nextTree());
				}
				stream_COMMA.reset();
				stream_cta.reset();

				adaptor.addChild(root_1, stream_RIGHT_PARENT.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 17, filter_transformation_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "filter_transformation"


	public static class join_transformation_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "join_transformation"
	// DFSParser.g:142:1: join_transformation : KW_JOIN LEFT_PARENT fc= df_expression ( COMMA cta= common_transformation_attributes )? RIGHT_PARENT -> ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_JOIN LEFT_PARENT $fc ( COMMA $cta)? RIGHT_PARENT ) ;
	public final DFSParser.join_transformation_return join_transformation() throws RecognitionException {
		DFSParser.join_transformation_return retval = new DFSParser.join_transformation_return();
		retval.start = input.LT(1);
		int join_transformation_StartIndex = input.index();

		Object root_0 = null;

		Token KW_JOIN45=null;
		Token LEFT_PARENT46=null;
		Token COMMA47=null;
		Token RIGHT_PARENT48=null;
		ParserRuleReturnScope fc =null;
		ParserRuleReturnScope cta =null;

		Object KW_JOIN45_tree=null;
		Object LEFT_PARENT46_tree=null;
		Object COMMA47_tree=null;
		Object RIGHT_PARENT48_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleTokenStream stream_RIGHT_PARENT=new RewriteRuleTokenStream(adaptor,"token RIGHT_PARENT");
		RewriteRuleTokenStream stream_LEFT_PARENT=new RewriteRuleTokenStream(adaptor,"token LEFT_PARENT");
		RewriteRuleTokenStream stream_KW_JOIN=new RewriteRuleTokenStream(adaptor,"token KW_JOIN");
		RewriteRuleSubtreeStream stream_common_transformation_attributes=new RewriteRuleSubtreeStream(adaptor,"rule common_transformation_attributes");
		RewriteRuleSubtreeStream stream_df_expression=new RewriteRuleSubtreeStream(adaptor,"rule df_expression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 18) ) { return retval; }

			// DFSParser.g:143:5: ( KW_JOIN LEFT_PARENT fc= df_expression ( COMMA cta= common_transformation_attributes )? RIGHT_PARENT -> ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_JOIN LEFT_PARENT $fc ( COMMA $cta)? RIGHT_PARENT ) )
			// DFSParser.g:143:9: KW_JOIN LEFT_PARENT fc= df_expression ( COMMA cta= common_transformation_attributes )? RIGHT_PARENT
			{
			KW_JOIN45=(Token)match(input,KW_JOIN,FOLLOW_KW_JOIN_in_join_transformation1213); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_KW_JOIN.add(KW_JOIN45);

			LEFT_PARENT46=(Token)match(input,LEFT_PARENT,FOLLOW_LEFT_PARENT_in_join_transformation1215); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LEFT_PARENT.add(LEFT_PARENT46);

			pushFollow(FOLLOW_df_expression_in_join_transformation1219);
			fc=df_expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_df_expression.add(fc.getTree());
			// DFSParser.g:143:46: ( COMMA cta= common_transformation_attributes )?
			int alt15=2;
			int LA15_0 = input.LA(1);
			if ( (LA15_0==COMMA) ) {
				alt15=1;
			}
			switch (alt15) {
				case 1 :
					// DFSParser.g:143:47: COMMA cta= common_transformation_attributes
					{
					COMMA47=(Token)match(input,COMMA,FOLLOW_COMMA_in_join_transformation1222); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(COMMA47);

					pushFollow(FOLLOW_common_transformation_attributes_in_join_transformation1226);
					cta=common_transformation_attributes();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_common_transformation_attributes.add(cta.getTree());
					}
					break;

			}

			RIGHT_PARENT48=(Token)match(input,RIGHT_PARENT,FOLLOW_RIGHT_PARENT_in_join_transformation1230); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RIGHT_PARENT.add(RIGHT_PARENT48);

			// AST REWRITE
			// elements: COMMA, LEFT_PARENT, fc, KW_JOIN, RIGHT_PARENT, cta
			// token labels: 
			// rule labels: cta, fc, retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_cta=new RewriteRuleSubtreeStream(adaptor,"rule cta",cta!=null?cta.getTree():null);
			RewriteRuleSubtreeStream stream_fc=new RewriteRuleSubtreeStream(adaptor,"rule fc",fc!=null?fc.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 144:9: -> ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_JOIN LEFT_PARENT $fc ( COMMA $cta)? RIGHT_PARENT )
			{
				// DFSParser.g:144:12: ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_JOIN LEFT_PARENT $fc ( COMMA $cta)? RIGHT_PARENT )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(new AstJoinTransformation(AST_CONCRETE_TRANSFORMATION, contextState), root_1);
				adaptor.addChild(root_1, stream_KW_JOIN.nextNode());
				adaptor.addChild(root_1, stream_LEFT_PARENT.nextNode());
				adaptor.addChild(root_1, stream_fc.nextTree());
				// DFSParser.g:144:103: ( COMMA $cta)?
				if ( stream_COMMA.hasNext()||stream_cta.hasNext() ) {
					adaptor.addChild(root_1, stream_COMMA.nextNode());
					adaptor.addChild(root_1, stream_cta.nextTree());
				}
				stream_COMMA.reset();
				stream_cta.reset();

				adaptor.addChild(root_1, stream_RIGHT_PARENT.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 18, join_transformation_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "join_transformation"


	public static class sink_transformation_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "sink_transformation"
	// DFSParser.g:147:1: sink_transformation : KW_SINK ta= transformation_attributes -> ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_SINK $ta) ;
	public final DFSParser.sink_transformation_return sink_transformation() throws RecognitionException {
		DFSParser.sink_transformation_return retval = new DFSParser.sink_transformation_return();
		retval.start = input.LT(1);
		int sink_transformation_StartIndex = input.index();

		Object root_0 = null;

		Token KW_SINK49=null;
		ParserRuleReturnScope ta =null;

		Object KW_SINK49_tree=null;
		RewriteRuleTokenStream stream_KW_SINK=new RewriteRuleTokenStream(adaptor,"token KW_SINK");
		RewriteRuleSubtreeStream stream_transformation_attributes=new RewriteRuleSubtreeStream(adaptor,"rule transformation_attributes");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 19) ) { return retval; }

			// DFSParser.g:148:5: ( KW_SINK ta= transformation_attributes -> ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_SINK $ta) )
			// DFSParser.g:148:9: KW_SINK ta= transformation_attributes
			{
			KW_SINK49=(Token)match(input,KW_SINK,FOLLOW_KW_SINK_in_sink_transformation1284); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_KW_SINK.add(KW_SINK49);

			pushFollow(FOLLOW_transformation_attributes_in_sink_transformation1288);
			ta=transformation_attributes();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_transformation_attributes.add(ta.getTree());
			// AST REWRITE
			// elements: KW_SINK, ta
			// token labels: 
			// rule labels: ta, retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_ta=new RewriteRuleSubtreeStream(adaptor,"rule ta",ta!=null?ta.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 149:9: -> ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_SINK $ta)
			{
				// DFSParser.g:149:12: ^( AST_CONCRETE_TRANSFORMATION[contextState] KW_SINK $ta)
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(new AstSinkTransformation(AST_CONCRETE_TRANSFORMATION, contextState), root_1);
				adaptor.addChild(root_1, stream_KW_SINK.nextNode());
				adaptor.addChild(root_1, stream_ta.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 19, sink_transformation_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "sink_transformation"


	public static class general_transformation_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "general_transformation"
	// DFSParser.g:152:1: general_transformation : SIMPLE_ID ta= transformation_attributes -> ^( AST_CONCRETE_TRANSFORMATION[contextState] SIMPLE_ID $ta) ;
	public final DFSParser.general_transformation_return general_transformation() throws RecognitionException {
		DFSParser.general_transformation_return retval = new DFSParser.general_transformation_return();
		retval.start = input.LT(1);
		int general_transformation_StartIndex = input.index();

		Object root_0 = null;

		Token SIMPLE_ID50=null;
		ParserRuleReturnScope ta =null;

		Object SIMPLE_ID50_tree=null;
		RewriteRuleTokenStream stream_SIMPLE_ID=new RewriteRuleTokenStream(adaptor,"token SIMPLE_ID");
		RewriteRuleSubtreeStream stream_transformation_attributes=new RewriteRuleSubtreeStream(adaptor,"rule transformation_attributes");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 20) ) { return retval; }

			// DFSParser.g:153:5: ( SIMPLE_ID ta= transformation_attributes -> ^( AST_CONCRETE_TRANSFORMATION[contextState] SIMPLE_ID $ta) )
			// DFSParser.g:153:9: SIMPLE_ID ta= transformation_attributes
			{
			SIMPLE_ID50=(Token)match(input,SIMPLE_ID,FOLLOW_SIMPLE_ID_in_general_transformation1330); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_SIMPLE_ID.add(SIMPLE_ID50);

			pushFollow(FOLLOW_transformation_attributes_in_general_transformation1334);
			ta=transformation_attributes();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_transformation_attributes.add(ta.getTree());
			// AST REWRITE
			// elements: SIMPLE_ID, ta
			// token labels: 
			// rule labels: ta, retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_ta=new RewriteRuleSubtreeStream(adaptor,"rule ta",ta!=null?ta.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 154:9: -> ^( AST_CONCRETE_TRANSFORMATION[contextState] SIMPLE_ID $ta)
			{
				// DFSParser.g:154:12: ^( AST_CONCRETE_TRANSFORMATION[contextState] SIMPLE_ID $ta)
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(new AstGeneralTransformation(AST_CONCRETE_TRANSFORMATION, contextState), root_1);
				adaptor.addChild(root_1, stream_SIMPLE_ID.nextNode());
				adaptor.addChild(root_1, stream_ta.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 20, general_transformation_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "general_transformation"


	public static class transformation_attributes_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "transformation_attributes"
	// DFSParser.g:157:1: transformation_attributes : LEFT_PARENT (cta= common_transformation_attributes )? RIGHT_PARENT -> LEFT_PARENT ( $cta)? RIGHT_PARENT ;
	public final DFSParser.transformation_attributes_return transformation_attributes() throws RecognitionException {
		DFSParser.transformation_attributes_return retval = new DFSParser.transformation_attributes_return();
		retval.start = input.LT(1);
		int transformation_attributes_StartIndex = input.index();

		Object root_0 = null;

		Token LEFT_PARENT51=null;
		Token RIGHT_PARENT52=null;
		ParserRuleReturnScope cta =null;

		Object LEFT_PARENT51_tree=null;
		Object RIGHT_PARENT52_tree=null;
		RewriteRuleTokenStream stream_RIGHT_PARENT=new RewriteRuleTokenStream(adaptor,"token RIGHT_PARENT");
		RewriteRuleTokenStream stream_LEFT_PARENT=new RewriteRuleTokenStream(adaptor,"token LEFT_PARENT");
		RewriteRuleSubtreeStream stream_common_transformation_attributes=new RewriteRuleSubtreeStream(adaptor,"rule common_transformation_attributes");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 21) ) { return retval; }

			// DFSParser.g:158:5: ( LEFT_PARENT (cta= common_transformation_attributes )? RIGHT_PARENT -> LEFT_PARENT ( $cta)? RIGHT_PARENT )
			// DFSParser.g:158:9: LEFT_PARENT (cta= common_transformation_attributes )? RIGHT_PARENT
			{
			LEFT_PARENT51=(Token)match(input,LEFT_PARENT,FOLLOW_LEFT_PARENT_in_transformation_attributes1376); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LEFT_PARENT.add(LEFT_PARENT51);

			// DFSParser.g:158:24: (cta= common_transformation_attributes )?
			int alt16=2;
			int LA16_0 = input.LA(1);
			if ( (LA16_0==EOF||LA16_0==DOLLAR_SIGN||(LA16_0 >= EXCLAMATION_MARK && LA16_0 <= FLOATING_POINT_LITERAL)||(LA16_0 >= INTEGER_LITERAL && LA16_0 <= KW_TRUE)||(LA16_0 >= LEFT_BRACKET && LA16_0 <= LEFT_PARENT)||LA16_0==MINUS_SIGN||LA16_0==PLUS_SIGN||LA16_0==SIMPLE_ID||LA16_0==STRING_LITERAL) ) {
				alt16=1;
			}
			switch (alt16) {
				case 1 :
					// DFSParser.g:158:24: cta= common_transformation_attributes
					{
					pushFollow(FOLLOW_common_transformation_attributes_in_transformation_attributes1380);
					cta=common_transformation_attributes();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_common_transformation_attributes.add(cta.getTree());
					}
					break;

			}

			RIGHT_PARENT52=(Token)match(input,RIGHT_PARENT,FOLLOW_RIGHT_PARENT_in_transformation_attributes1383); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RIGHT_PARENT.add(RIGHT_PARENT52);

			// AST REWRITE
			// elements: RIGHT_PARENT, cta, LEFT_PARENT
			// token labels: 
			// rule labels: cta, retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_cta=new RewriteRuleSubtreeStream(adaptor,"rule cta",cta!=null?cta.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 159:9: -> LEFT_PARENT ( $cta)? RIGHT_PARENT
			{
				adaptor.addChild(root_0, stream_LEFT_PARENT.nextNode());
				// DFSParser.g:159:25: ( $cta)?
				if ( stream_cta.hasNext() ) {
					adaptor.addChild(root_0, stream_cta.nextTree());
				}
				stream_cta.reset();

				adaptor.addChild(root_0, stream_RIGHT_PARENT.nextNode());
			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 21, transformation_attributes_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "transformation_attributes"


	public static class common_transformation_attributes_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "common_transformation_attributes"
	// DFSParser.g:162:1: common_transformation_attributes : ta1= transformation_attribute ( COMMA ta2+= transformation_attribute )* -> ^( AST_TRANSFORMATION_ATTRIBUTES[contextState] $ta1 ( COMMA $ta2)* ) ;
	public final DFSParser.common_transformation_attributes_return common_transformation_attributes() throws RecognitionException {
		DFSParser.common_transformation_attributes_return retval = new DFSParser.common_transformation_attributes_return();
		retval.start = input.LT(1);
		int common_transformation_attributes_StartIndex = input.index();

		Object root_0 = null;

		Token COMMA53=null;
		List<Object> list_ta2=null;
		ParserRuleReturnScope ta1 =null;
		RuleReturnScope ta2 = null;
		Object COMMA53_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleSubtreeStream stream_transformation_attribute=new RewriteRuleSubtreeStream(adaptor,"rule transformation_attribute");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 22) ) { return retval; }

			// DFSParser.g:163:5: (ta1= transformation_attribute ( COMMA ta2+= transformation_attribute )* -> ^( AST_TRANSFORMATION_ATTRIBUTES[contextState] $ta1 ( COMMA $ta2)* ) )
			// DFSParser.g:163:9: ta1= transformation_attribute ( COMMA ta2+= transformation_attribute )*
			{
			pushFollow(FOLLOW_transformation_attribute_in_common_transformation_attributes1422);
			ta1=transformation_attribute();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_transformation_attribute.add(ta1.getTree());
			// DFSParser.g:163:38: ( COMMA ta2+= transformation_attribute )*
			loop17:
			while (true) {
				int alt17=2;
				int LA17_0 = input.LA(1);
				if ( (LA17_0==COMMA) ) {
					alt17=1;
				}

				switch (alt17) {
				case 1 :
					// DFSParser.g:163:39: COMMA ta2+= transformation_attribute
					{
					COMMA53=(Token)match(input,COMMA,FOLLOW_COMMA_in_common_transformation_attributes1425); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(COMMA53);

					pushFollow(FOLLOW_transformation_attribute_in_common_transformation_attributes1429);
					ta2=transformation_attribute();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_transformation_attribute.add(ta2.getTree());
					if (list_ta2==null) list_ta2=new ArrayList<Object>();
					list_ta2.add(ta2.getTree());
					}
					break;

				default :
					break loop17;
				}
			}

			// AST REWRITE
			// elements: ta2, COMMA, ta1
			// token labels: 
			// rule labels: ta1, retval
			// token list labels: 
			// rule list labels: ta2
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_ta1=new RewriteRuleSubtreeStream(adaptor,"rule ta1",ta1!=null?ta1.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_ta2=new RewriteRuleSubtreeStream(adaptor,"token ta2",list_ta2);
			root_0 = (Object)adaptor.nil();
			// 164:9: -> ^( AST_TRANSFORMATION_ATTRIBUTES[contextState] $ta1 ( COMMA $ta2)* )
			{
				// DFSParser.g:164:12: ^( AST_TRANSFORMATION_ATTRIBUTES[contextState] $ta1 ( COMMA $ta2)* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(new AstTransformationAttributes(AST_TRANSFORMATION_ATTRIBUTES, contextState), root_1);
				adaptor.addChild(root_1, stream_ta1.nextTree());
				// DFSParser.g:164:92: ( COMMA $ta2)*
				while ( stream_ta2.hasNext()||stream_COMMA.hasNext() ) {
					adaptor.addChild(root_1, stream_COMMA.nextNode());
					adaptor.addChild(root_1, stream_ta2.nextTree());
				}
				stream_ta2.reset();
				stream_COMMA.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 22, common_transformation_attributes_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "common_transformation_attributes"


	public static class transformation_attribute_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "transformation_attribute"
	// DFSParser.g:167:1: transformation_attribute : (oa= output_attribute -> $oa|id= all_identifier COLON de= df_expression -> ^( AST_TRANSFORMATION_ATTRIBUTE[contextState] ^( AST_ATTRIBUTE_NAME[contextState] $id) COLON $de) |de= df_expression -> $de);
	public final DFSParser.transformation_attribute_return transformation_attribute() throws RecognitionException {
		DFSParser.transformation_attribute_return retval = new DFSParser.transformation_attribute_return();
		retval.start = input.LT(1);
		int transformation_attribute_StartIndex = input.index();

		Object root_0 = null;

		Token COLON54=null;
		ParserRuleReturnScope oa =null;
		ParserRuleReturnScope id =null;
		ParserRuleReturnScope de =null;

		Object COLON54_tree=null;
		RewriteRuleTokenStream stream_COLON=new RewriteRuleTokenStream(adaptor,"token COLON");
		RewriteRuleSubtreeStream stream_df_expression=new RewriteRuleSubtreeStream(adaptor,"rule df_expression");
		RewriteRuleSubtreeStream stream_output_attribute=new RewriteRuleSubtreeStream(adaptor,"rule output_attribute");
		RewriteRuleSubtreeStream stream_all_identifier=new RewriteRuleSubtreeStream(adaptor,"rule all_identifier");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 23) ) { return retval; }

			// DFSParser.g:168:5: (oa= output_attribute -> $oa|id= all_identifier COLON de= df_expression -> ^( AST_TRANSFORMATION_ATTRIBUTE[contextState] ^( AST_ATTRIBUTE_NAME[contextState] $id) COLON $de) |de= df_expression -> $de)
			int alt18=3;
			switch ( input.LA(1) ) {
			case KW_OUTPUT:
				{
				switch ( input.LA(2) ) {
				case LEFT_PARENT:
					{
					alt18=1;
					}
					break;
				case COLON:
					{
					alt18=2;
					}
					break;
				case AMPERSAND:
				case ASTERISK:
				case BAR:
				case CARET:
				case COMMA:
				case EQUALS:
				case EXCLAMATION_MARK:
				case GREATER_THAN:
				case LESS_THAN:
				case MINUS_SIGN:
				case PLUS_SIGN:
				case RIGHT_PARENT:
				case SLASH:
					{
					alt18=3;
					}
					break;
				default:
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 18, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}
				}
				break;
			case SIMPLE_ID:
				{
				int LA18_2 = input.LA(2);
				if ( (LA18_2==COLON) ) {
					alt18=2;
				}
				else if ( (LA18_2==AMPERSAND||LA18_2==ASTERISK||LA18_2==BAR||LA18_2==CARET||LA18_2==COMMA||(LA18_2 >= EQUALS && LA18_2 <= EXCLAMATION_MARK)||LA18_2==GREATER_THAN||LA18_2==LESS_THAN||LA18_2==MINUS_SIGN||LA18_2==PLUS_SIGN||LA18_2==RIGHT_PARENT||LA18_2==SLASH) ) {
					alt18=3;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 18, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case KW_FLOAT:
				{
				int LA18_3 = input.LA(2);
				if ( (LA18_3==COLON) ) {
					alt18=2;
				}
				else if ( (LA18_3==AMPERSAND||LA18_3==ASTERISK||LA18_3==BAR||LA18_3==CARET||LA18_3==COMMA||(LA18_3 >= EQUALS && LA18_3 <= EXCLAMATION_MARK)||LA18_3==GREATER_THAN||LA18_3==LESS_THAN||LA18_3==MINUS_SIGN||LA18_3==PLUS_SIGN||LA18_3==RIGHT_PARENT||LA18_3==SLASH) ) {
					alt18=3;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 18, 3, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case COLON:
				{
				alt18=2;
				}
				break;
			case DOLLAR_SIGN:
			case EXCLAMATION_MARK:
			case FLOATING_POINT_LITERAL:
			case INTEGER_LITERAL:
			case LEFT_BRACKET:
			case LEFT_PARENT:
			case MINUS_SIGN:
			case PLUS_SIGN:
			case STRING_LITERAL:
				{
				alt18=3;
				}
				break;
			case KW_ANY:
			case KW_AS:
			case KW_BOOLEAN:
			case KW_DATE:
			case KW_DECIMAL:
			case KW_DOUBLE:
			case KW_FALSE:
			case KW_FILTER:
			case KW_INTEGER:
			case KW_JOIN:
			case KW_LONG:
			case KW_PARAMETERS:
			case KW_SHORT:
			case KW_SINK:
			case KW_SOURCE:
			case KW_STRING:
			case KW_TIMESTAMP:
			case KW_TRUE:
				{
				int LA18_6 = input.LA(2);
				if ( (LA18_6==COLON) ) {
					alt18=2;
				}
				else if ( (LA18_6==AMPERSAND||LA18_6==ASTERISK||LA18_6==BAR||LA18_6==CARET||LA18_6==COMMA||(LA18_6 >= EQUALS && LA18_6 <= EXCLAMATION_MARK)||LA18_6==GREATER_THAN||LA18_6==LESS_THAN||LA18_6==MINUS_SIGN||LA18_6==PLUS_SIGN||LA18_6==RIGHT_PARENT||LA18_6==SLASH) ) {
					alt18=3;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 18, 6, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 18, 0, input);
				throw nvae;
			}
			switch (alt18) {
				case 1 :
					// DFSParser.g:168:9: oa= output_attribute
					{
					pushFollow(FOLLOW_output_attribute_in_transformation_attribute1481);
					oa=output_attribute();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_output_attribute.add(oa.getTree());
					// AST REWRITE
					// elements: oa
					// token labels: 
					// rule labels: oa, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_oa=new RewriteRuleSubtreeStream(adaptor,"rule oa",oa!=null?oa.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 169:9: -> $oa
					{
						adaptor.addChild(root_0, stream_oa.nextTree());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// DFSParser.g:170:9: id= all_identifier COLON de= df_expression
					{
					pushFollow(FOLLOW_all_identifier_in_transformation_attribute1506);
					id=all_identifier();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_all_identifier.add(id.getTree());
					COLON54=(Token)match(input,COLON,FOLLOW_COLON_in_transformation_attribute1508); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COLON.add(COLON54);

					pushFollow(FOLLOW_df_expression_in_transformation_attribute1512);
					de=df_expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_df_expression.add(de.getTree());
					// AST REWRITE
					// elements: COLON, de, id
					// token labels: 
					// rule labels: de, id, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_de=new RewriteRuleSubtreeStream(adaptor,"rule de",de!=null?de.getTree():null);
					RewriteRuleSubtreeStream stream_id=new RewriteRuleSubtreeStream(adaptor,"rule id",id!=null?id.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 171:9: -> ^( AST_TRANSFORMATION_ATTRIBUTE[contextState] ^( AST_ATTRIBUTE_NAME[contextState] $id) COLON $de)
					{
						// DFSParser.g:171:12: ^( AST_TRANSFORMATION_ATTRIBUTE[contextState] ^( AST_ATTRIBUTE_NAME[contextState] $id) COLON $de)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(new AstTransformationAttribute(AST_TRANSFORMATION_ATTRIBUTE, contextState), root_1);
						// DFSParser.g:171:85: ^( AST_ATTRIBUTE_NAME[contextState] $id)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot(new AstIdentifier(AST_ATTRIBUTE_NAME, contextState), root_2);
						adaptor.addChild(root_2, stream_id.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_1, stream_COLON.nextNode());
						adaptor.addChild(root_1, stream_de.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// DFSParser.g:172:9: de= df_expression
					{
					pushFollow(FOLLOW_df_expression_in_transformation_attribute1558);
					de=df_expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_df_expression.add(de.getTree());
					// AST REWRITE
					// elements: de
					// token labels: 
					// rule labels: de, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_de=new RewriteRuleSubtreeStream(adaptor,"rule de",de!=null?de.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 173:9: -> $de
					{
						adaptor.addChild(root_0, stream_de.nextTree());
					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 23, transformation_attribute_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "transformation_attribute"


	public static class output_attribute_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "output_attribute"
	// DFSParser.g:176:1: output_attribute : KW_OUTPUT LEFT_PARENT (cd= column_definitions )? RIGHT_PARENT -> ^( AST_OUTPUT_ATTRIBUTE[contextState] KW_OUTPUT LEFT_PARENT ( $cd)? RIGHT_PARENT ) ;
	public final DFSParser.output_attribute_return output_attribute() throws RecognitionException {
		DFSParser.output_attribute_return retval = new DFSParser.output_attribute_return();
		retval.start = input.LT(1);
		int output_attribute_StartIndex = input.index();

		Object root_0 = null;

		Token KW_OUTPUT55=null;
		Token LEFT_PARENT56=null;
		Token RIGHT_PARENT57=null;
		ParserRuleReturnScope cd =null;

		Object KW_OUTPUT55_tree=null;
		Object LEFT_PARENT56_tree=null;
		Object RIGHT_PARENT57_tree=null;
		RewriteRuleTokenStream stream_RIGHT_PARENT=new RewriteRuleTokenStream(adaptor,"token RIGHT_PARENT");
		RewriteRuleTokenStream stream_KW_OUTPUT=new RewriteRuleTokenStream(adaptor,"token KW_OUTPUT");
		RewriteRuleTokenStream stream_LEFT_PARENT=new RewriteRuleTokenStream(adaptor,"token LEFT_PARENT");
		RewriteRuleSubtreeStream stream_column_definitions=new RewriteRuleSubtreeStream(adaptor,"rule column_definitions");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 24) ) { return retval; }

			// DFSParser.g:177:5: ( KW_OUTPUT LEFT_PARENT (cd= column_definitions )? RIGHT_PARENT -> ^( AST_OUTPUT_ATTRIBUTE[contextState] KW_OUTPUT LEFT_PARENT ( $cd)? RIGHT_PARENT ) )
			// DFSParser.g:177:9: KW_OUTPUT LEFT_PARENT (cd= column_definitions )? RIGHT_PARENT
			{
			KW_OUTPUT55=(Token)match(input,KW_OUTPUT,FOLLOW_KW_OUTPUT_in_output_attribute1590); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_KW_OUTPUT.add(KW_OUTPUT55);

			LEFT_PARENT56=(Token)match(input,LEFT_PARENT,FOLLOW_LEFT_PARENT_in_output_attribute1592); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LEFT_PARENT.add(LEFT_PARENT56);

			// DFSParser.g:177:33: (cd= column_definitions )?
			int alt19=2;
			int LA19_0 = input.LA(1);
			if ( (LA19_0==EOF||(LA19_0 >= KW_ANY && LA19_0 <= LEFT_BRACE)||LA19_0==SIMPLE_ID) ) {
				alt19=1;
			}
			switch (alt19) {
				case 1 :
					// DFSParser.g:177:33: cd= column_definitions
					{
					pushFollow(FOLLOW_column_definitions_in_output_attribute1596);
					cd=column_definitions();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_column_definitions.add(cd.getTree());
					}
					break;

			}

			RIGHT_PARENT57=(Token)match(input,RIGHT_PARENT,FOLLOW_RIGHT_PARENT_in_output_attribute1599); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RIGHT_PARENT.add(RIGHT_PARENT57);

			// AST REWRITE
			// elements: RIGHT_PARENT, KW_OUTPUT, LEFT_PARENT, cd
			// token labels: 
			// rule labels: cd, retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_cd=new RewriteRuleSubtreeStream(adaptor,"rule cd",cd!=null?cd.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 178:9: -> ^( AST_OUTPUT_ATTRIBUTE[contextState] KW_OUTPUT LEFT_PARENT ( $cd)? RIGHT_PARENT )
			{
				// DFSParser.g:178:12: ^( AST_OUTPUT_ATTRIBUTE[contextState] KW_OUTPUT LEFT_PARENT ( $cd)? RIGHT_PARENT )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(new AstOutputAttribute(AST_OUTPUT_ATTRIBUTE, contextState), root_1);
				adaptor.addChild(root_1, stream_KW_OUTPUT.nextNode());
				adaptor.addChild(root_1, stream_LEFT_PARENT.nextNode());
				// DFSParser.g:178:92: ( $cd)?
				if ( stream_cd.hasNext() ) {
					adaptor.addChild(root_1, stream_cd.nextTree());
				}
				stream_cd.reset();

				adaptor.addChild(root_1, stream_RIGHT_PARENT.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 24, output_attribute_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "output_attribute"


	public static class column_definitions_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "column_definitions"
	// DFSParser.g:181:1: column_definitions : cd1= column_definition ( COMMA cd2+= column_definition )* -> ^( AST_COLUMN_DEFINITIONS $cd1 ( COMMA $cd2)* ) ;
	public final DFSParser.column_definitions_return column_definitions() throws RecognitionException {
		DFSParser.column_definitions_return retval = new DFSParser.column_definitions_return();
		retval.start = input.LT(1);
		int column_definitions_StartIndex = input.index();

		Object root_0 = null;

		Token COMMA58=null;
		List<Object> list_cd2=null;
		ParserRuleReturnScope cd1 =null;
		RuleReturnScope cd2 = null;
		Object COMMA58_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleSubtreeStream stream_column_definition=new RewriteRuleSubtreeStream(adaptor,"rule column_definition");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 25) ) { return retval; }

			// DFSParser.g:182:5: (cd1= column_definition ( COMMA cd2+= column_definition )* -> ^( AST_COLUMN_DEFINITIONS $cd1 ( COMMA $cd2)* ) )
			// DFSParser.g:182:9: cd1= column_definition ( COMMA cd2+= column_definition )*
			{
			pushFollow(FOLLOW_column_definition_in_column_definitions1648);
			cd1=column_definition();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_column_definition.add(cd1.getTree());
			// DFSParser.g:182:31: ( COMMA cd2+= column_definition )*
			loop20:
			while (true) {
				int alt20=2;
				int LA20_0 = input.LA(1);
				if ( (LA20_0==COMMA) ) {
					alt20=1;
				}

				switch (alt20) {
				case 1 :
					// DFSParser.g:182:32: COMMA cd2+= column_definition
					{
					COMMA58=(Token)match(input,COMMA,FOLLOW_COMMA_in_column_definitions1651); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(COMMA58);

					pushFollow(FOLLOW_column_definition_in_column_definitions1655);
					cd2=column_definition();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_column_definition.add(cd2.getTree());
					if (list_cd2==null) list_cd2=new ArrayList<Object>();
					list_cd2.add(cd2.getTree());
					}
					break;

				default :
					break loop20;
				}
			}

			// AST REWRITE
			// elements: COMMA, cd1, cd2
			// token labels: 
			// rule labels: cd1, retval
			// token list labels: 
			// rule list labels: cd2
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_cd1=new RewriteRuleSubtreeStream(adaptor,"rule cd1",cd1!=null?cd1.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_cd2=new RewriteRuleSubtreeStream(adaptor,"token cd2",list_cd2);
			root_0 = (Object)adaptor.nil();
			// 183:9: -> ^( AST_COLUMN_DEFINITIONS $cd1 ( COMMA $cd2)* )
			{
				// DFSParser.g:183:12: ^( AST_COLUMN_DEFINITIONS $cd1 ( COMMA $cd2)* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_COLUMN_DEFINITIONS, "AST_COLUMN_DEFINITIONS"), root_1);
				adaptor.addChild(root_1, stream_cd1.nextTree());
				// DFSParser.g:183:42: ( COMMA $cd2)*
				while ( stream_COMMA.hasNext()||stream_cd2.hasNext() ) {
					adaptor.addChild(root_1, stream_COMMA.nextNode());
					adaptor.addChild(root_1, stream_cd2.nextTree());
				}
				stream_COMMA.reset();
				stream_cd2.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 25, column_definitions_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "column_definitions"


	public static class column_definition_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "column_definition"
	// DFSParser.g:186:1: column_definition : cn= column_name KW_AS dt= data_type ( STRING_LITERAL )? -> ^( AST_COLUMN_DEFINITION[contextState] $cn KW_AS ^( AST_DATA_TYPE[contextState] $dt) ( ^( AST_DATA_TYPE_FORMAT[contextState] STRING_LITERAL ) )? ) ;
	public final DFSParser.column_definition_return column_definition() throws RecognitionException {
		DFSParser.column_definition_return retval = new DFSParser.column_definition_return();
		retval.start = input.LT(1);
		int column_definition_StartIndex = input.index();

		Object root_0 = null;

		Token KW_AS59=null;
		Token STRING_LITERAL60=null;
		ParserRuleReturnScope cn =null;
		ParserRuleReturnScope dt =null;

		Object KW_AS59_tree=null;
		Object STRING_LITERAL60_tree=null;
		RewriteRuleTokenStream stream_STRING_LITERAL=new RewriteRuleTokenStream(adaptor,"token STRING_LITERAL");
		RewriteRuleTokenStream stream_KW_AS=new RewriteRuleTokenStream(adaptor,"token KW_AS");
		RewriteRuleSubtreeStream stream_column_name=new RewriteRuleSubtreeStream(adaptor,"rule column_name");
		RewriteRuleSubtreeStream stream_data_type=new RewriteRuleSubtreeStream(adaptor,"rule data_type");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 26) ) { return retval; }

			// DFSParser.g:187:5: (cn= column_name KW_AS dt= data_type ( STRING_LITERAL )? -> ^( AST_COLUMN_DEFINITION[contextState] $cn KW_AS ^( AST_DATA_TYPE[contextState] $dt) ( ^( AST_DATA_TYPE_FORMAT[contextState] STRING_LITERAL ) )? ) )
			// DFSParser.g:187:9: cn= column_name KW_AS dt= data_type ( STRING_LITERAL )?
			{
			pushFollow(FOLLOW_column_name_in_column_definition1703);
			cn=column_name();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_column_name.add(cn.getTree());
			KW_AS59=(Token)match(input,KW_AS,FOLLOW_KW_AS_in_column_definition1705); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_KW_AS.add(KW_AS59);

			pushFollow(FOLLOW_data_type_in_column_definition1709);
			dt=data_type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_data_type.add(dt.getTree());
			// DFSParser.g:187:43: ( STRING_LITERAL )?
			int alt21=2;
			int LA21_0 = input.LA(1);
			if ( (LA21_0==STRING_LITERAL) ) {
				alt21=1;
			}
			switch (alt21) {
				case 1 :
					// DFSParser.g:187:44: STRING_LITERAL
					{
					STRING_LITERAL60=(Token)match(input,STRING_LITERAL,FOLLOW_STRING_LITERAL_in_column_definition1712); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_STRING_LITERAL.add(STRING_LITERAL60);

					}
					break;

			}

			// AST REWRITE
			// elements: KW_AS, cn, dt, STRING_LITERAL
			// token labels: 
			// rule labels: dt, cn, retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_dt=new RewriteRuleSubtreeStream(adaptor,"rule dt",dt!=null?dt.getTree():null);
			RewriteRuleSubtreeStream stream_cn=new RewriteRuleSubtreeStream(adaptor,"rule cn",cn!=null?cn.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 188:9: -> ^( AST_COLUMN_DEFINITION[contextState] $cn KW_AS ^( AST_DATA_TYPE[contextState] $dt) ( ^( AST_DATA_TYPE_FORMAT[contextState] STRING_LITERAL ) )? )
			{
				// DFSParser.g:188:12: ^( AST_COLUMN_DEFINITION[contextState] $cn KW_AS ^( AST_DATA_TYPE[contextState] $dt) ( ^( AST_DATA_TYPE_FORMAT[contextState] STRING_LITERAL ) )? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(new AstColumnDefinition(AST_COLUMN_DEFINITION, contextState), root_1);
				adaptor.addChild(root_1, stream_cn.nextTree());
				adaptor.addChild(root_1, stream_KW_AS.nextNode());
				// DFSParser.g:188:81: ^( AST_DATA_TYPE[contextState] $dt)
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot(new AstDataType(AST_DATA_TYPE, contextState), root_2);
				adaptor.addChild(root_2, stream_dt.nextTree());
				adaptor.addChild(root_1, root_2);
				}

				// DFSParser.g:188:129: ( ^( AST_DATA_TYPE_FORMAT[contextState] STRING_LITERAL ) )?
				if ( stream_STRING_LITERAL.hasNext() ) {
					// DFSParser.g:188:129: ^( AST_DATA_TYPE_FORMAT[contextState] STRING_LITERAL )
					{
					Object root_2 = (Object)adaptor.nil();
					root_2 = (Object)adaptor.becomeRoot(new AstDataTypeFormat(AST_DATA_TYPE_FORMAT, contextState), root_2);
					adaptor.addChild(root_2, stream_STRING_LITERAL.nextNode());
					adaptor.addChild(root_1, root_2);
					}

				}
				stream_STRING_LITERAL.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 26, column_definition_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "column_definition"


	public static class column_name_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "column_name"
	// DFSParser.g:191:1: column_name : (id= all_identifier -> ^( AST_COLUMN_NAME[contextState] $id) | LEFT_BRACE (cnib= column_name_inside_braces )? RIGHT_BRACE -> LEFT_BRACE ^( AST_COLUMN_NAME[contextState] ( $cnib)? ) RIGHT_BRACE );
	public final DFSParser.column_name_return column_name() throws RecognitionException {
		DFSParser.column_name_return retval = new DFSParser.column_name_return();
		retval.start = input.LT(1);
		int column_name_StartIndex = input.index();

		Object root_0 = null;

		Token LEFT_BRACE61=null;
		Token RIGHT_BRACE62=null;
		ParserRuleReturnScope id =null;
		ParserRuleReturnScope cnib =null;

		Object LEFT_BRACE61_tree=null;
		Object RIGHT_BRACE62_tree=null;
		RewriteRuleTokenStream stream_RIGHT_BRACE=new RewriteRuleTokenStream(adaptor,"token RIGHT_BRACE");
		RewriteRuleTokenStream stream_LEFT_BRACE=new RewriteRuleTokenStream(adaptor,"token LEFT_BRACE");
		RewriteRuleSubtreeStream stream_column_name_inside_braces=new RewriteRuleSubtreeStream(adaptor,"rule column_name_inside_braces");
		RewriteRuleSubtreeStream stream_all_identifier=new RewriteRuleSubtreeStream(adaptor,"rule all_identifier");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 27) ) { return retval; }

			// DFSParser.g:192:5: (id= all_identifier -> ^( AST_COLUMN_NAME[contextState] $id) | LEFT_BRACE (cnib= column_name_inside_braces )? RIGHT_BRACE -> LEFT_BRACE ^( AST_COLUMN_NAME[contextState] ( $cnib)? ) RIGHT_BRACE )
			int alt23=2;
			int LA23_0 = input.LA(1);
			if ( (LA23_0==EOF||(LA23_0 >= KW_ANY && LA23_0 <= KW_TRUE)||LA23_0==SIMPLE_ID) ) {
				alt23=1;
			}
			else if ( (LA23_0==LEFT_BRACE) ) {
				alt23=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 23, 0, input);
				throw nvae;
			}

			switch (alt23) {
				case 1 :
					// DFSParser.g:192:9: id= all_identifier
					{
					pushFollow(FOLLOW_all_identifier_in_column_name1780);
					id=all_identifier();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_all_identifier.add(id.getTree());
					// AST REWRITE
					// elements: id
					// token labels: 
					// rule labels: id, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_id=new RewriteRuleSubtreeStream(adaptor,"rule id",id!=null?id.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 193:9: -> ^( AST_COLUMN_NAME[contextState] $id)
					{
						// DFSParser.g:193:12: ^( AST_COLUMN_NAME[contextState] $id)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(new AstIdentifier(AST_COLUMN_NAME, contextState), root_1);
						adaptor.addChild(root_1, stream_id.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// DFSParser.g:194:9: LEFT_BRACE (cnib= column_name_inside_braces )? RIGHT_BRACE
					{
					LEFT_BRACE61=(Token)match(input,LEFT_BRACE,FOLLOW_LEFT_BRACE_in_column_name1811); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LEFT_BRACE.add(LEFT_BRACE61);

					// DFSParser.g:194:24: (cnib= column_name_inside_braces )?
					int alt22=2;
					int LA22_0 = input.LA(1);
					if ( ((LA22_0 >= AMPERSAND && LA22_0 <= QUOTATION_MARK)||(LA22_0 >= RIGHT_BRACKET && LA22_0 <= 151)) ) {
						alt22=1;
					}
					switch (alt22) {
						case 1 :
							// DFSParser.g:194:24: cnib= column_name_inside_braces
							{
							pushFollow(FOLLOW_column_name_inside_braces_in_column_name1815);
							cnib=column_name_inside_braces();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_column_name_inside_braces.add(cnib.getTree());
							}
							break;

					}

					RIGHT_BRACE62=(Token)match(input,RIGHT_BRACE,FOLLOW_RIGHT_BRACE_in_column_name1818); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RIGHT_BRACE.add(RIGHT_BRACE62);

					// AST REWRITE
					// elements: cnib, RIGHT_BRACE, LEFT_BRACE
					// token labels: 
					// rule labels: cnib, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_cnib=new RewriteRuleSubtreeStream(adaptor,"rule cnib",cnib!=null?cnib.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 195:9: -> LEFT_BRACE ^( AST_COLUMN_NAME[contextState] ( $cnib)? ) RIGHT_BRACE
					{
						adaptor.addChild(root_0, stream_LEFT_BRACE.nextNode());
						// DFSParser.g:195:23: ^( AST_COLUMN_NAME[contextState] ( $cnib)? )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(new AstIdentifier(AST_COLUMN_NAME, contextState), root_1);
						// DFSParser.g:195:71: ( $cnib)?
						if ( stream_cnib.hasNext() ) {
							adaptor.addChild(root_1, stream_cnib.nextTree());
						}
						stream_cnib.reset();

						adaptor.addChild(root_0, root_1);
						}

						adaptor.addChild(root_0, stream_RIGHT_BRACE.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 27, column_name_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "column_name"


	public static class column_name_inside_braces_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "column_name_inside_braces"
	// DFSParser.g:198:1: column_name_inside_braces : (~ RIGHT_BRACE )+ ;
	public final DFSParser.column_name_inside_braces_return column_name_inside_braces() throws RecognitionException {
		DFSParser.column_name_inside_braces_return retval = new DFSParser.column_name_inside_braces_return();
		retval.start = input.LT(1);
		int column_name_inside_braces_StartIndex = input.index();

		Object root_0 = null;

		Token set63=null;

		Object set63_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 28) ) { return retval; }

			// DFSParser.g:199:5: ( (~ RIGHT_BRACE )+ )
			// DFSParser.g:199:9: (~ RIGHT_BRACE )+
			{
			root_0 = (Object)adaptor.nil();


			// DFSParser.g:199:9: (~ RIGHT_BRACE )+
			int cnt24=0;
			loop24:
			while (true) {
				int alt24=2;
				int LA24_0 = input.LA(1);
				if ( ((LA24_0 >= AMPERSAND && LA24_0 <= QUOTATION_MARK)||(LA24_0 >= RIGHT_BRACKET && LA24_0 <= 151)) ) {
					alt24=1;
				}

				switch (alt24) {
				case 1 :
					// DFSParser.g:
					{
					set63=input.LT(1);
					if ( (input.LA(1) >= AMPERSAND && input.LA(1) <= QUOTATION_MARK)||(input.LA(1) >= RIGHT_BRACKET && input.LA(1) <= 151) ) {
						input.consume();
						if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set63));
						state.errorRecovery=false;
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt24 >= 1 ) break loop24;
					if (state.backtracking>0) {state.failed=true; return retval;}
					EarlyExitException eee = new EarlyExitException(24, input);
					throw eee;
				}
				cnt24++;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 28, column_name_inside_braces_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "column_name_inside_braces"


	public static class df_expression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "df_expression"
	// DFSParser.g:203:1: df_expression : oe= or_expression -> ^( AST_EXPRESSION[contextState] $oe) ;
	public final DFSParser.df_expression_return df_expression() throws RecognitionException {
		DFSParser.df_expression_return retval = new DFSParser.df_expression_return();
		retval.start = input.LT(1);
		int df_expression_StartIndex = input.index();

		Object root_0 = null;

		ParserRuleReturnScope oe =null;

		RewriteRuleSubtreeStream stream_or_expression=new RewriteRuleSubtreeStream(adaptor,"rule or_expression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 29) ) { return retval; }

			// DFSParser.g:204:5: (oe= or_expression -> ^( AST_EXPRESSION[contextState] $oe) )
			// DFSParser.g:204:9: oe= or_expression
			{
			pushFollow(FOLLOW_or_expression_in_df_expression1889);
			oe=or_expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_or_expression.add(oe.getTree());
			// AST REWRITE
			// elements: oe
			// token labels: 
			// rule labels: oe, retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_oe=new RewriteRuleSubtreeStream(adaptor,"rule oe",oe!=null?oe.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 205:9: -> ^( AST_EXPRESSION[contextState] $oe)
			{
				// DFSParser.g:205:12: ^( AST_EXPRESSION[contextState] $oe)
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(new AstExpression(AST_EXPRESSION, contextState), root_1);
				adaptor.addChild(root_1, stream_oe.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 29, df_expression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "df_expression"


	public static class or_expression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "or_expression"
	// DFSParser.g:208:1: or_expression : ( and_expression -> and_expression ) ( BAR BAR ae= and_expression -> ^( AST_OR_EXPRESSION ^( AST_LEFT_OPERAND $or_expression) BAR BAR ^( AST_RIGHT_OPERAND $ae) ) )* ;
	public final DFSParser.or_expression_return or_expression() throws RecognitionException {
		DFSParser.or_expression_return retval = new DFSParser.or_expression_return();
		retval.start = input.LT(1);
		int or_expression_StartIndex = input.index();

		Object root_0 = null;

		Token BAR65=null;
		Token BAR66=null;
		ParserRuleReturnScope ae =null;
		ParserRuleReturnScope and_expression64 =null;

		Object BAR65_tree=null;
		Object BAR66_tree=null;
		RewriteRuleTokenStream stream_BAR=new RewriteRuleTokenStream(adaptor,"token BAR");
		RewriteRuleSubtreeStream stream_and_expression=new RewriteRuleSubtreeStream(adaptor,"rule and_expression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 30) ) { return retval; }

			// DFSParser.g:209:5: ( ( and_expression -> and_expression ) ( BAR BAR ae= and_expression -> ^( AST_OR_EXPRESSION ^( AST_LEFT_OPERAND $or_expression) BAR BAR ^( AST_RIGHT_OPERAND $ae) ) )* )
			// DFSParser.g:209:9: ( and_expression -> and_expression ) ( BAR BAR ae= and_expression -> ^( AST_OR_EXPRESSION ^( AST_LEFT_OPERAND $or_expression) BAR BAR ^( AST_RIGHT_OPERAND $ae) ) )*
			{
			// DFSParser.g:209:9: ( and_expression -> and_expression )
			// DFSParser.g:209:10: and_expression
			{
			pushFollow(FOLLOW_and_expression_in_or_expression1930);
			and_expression64=and_expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_and_expression.add(and_expression64.getTree());
			// AST REWRITE
			// elements: and_expression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 210:13: -> and_expression
			{
				adaptor.addChild(root_0, stream_and_expression.nextTree());
			}


			retval.tree = root_0;
			}

			}

			// DFSParser.g:211:9: ( BAR BAR ae= and_expression -> ^( AST_OR_EXPRESSION ^( AST_LEFT_OPERAND $or_expression) BAR BAR ^( AST_RIGHT_OPERAND $ae) ) )*
			loop25:
			while (true) {
				int alt25=2;
				int LA25_0 = input.LA(1);
				if ( (LA25_0==BAR) ) {
					alt25=1;
				}

				switch (alt25) {
				case 1 :
					// DFSParser.g:211:10: BAR BAR ae= and_expression
					{
					BAR65=(Token)match(input,BAR,FOLLOW_BAR_in_or_expression1958); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_BAR.add(BAR65);

					BAR66=(Token)match(input,BAR,FOLLOW_BAR_in_or_expression1960); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_BAR.add(BAR66);

					pushFollow(FOLLOW_and_expression_in_or_expression1964);
					ae=and_expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_and_expression.add(ae.getTree());
					// AST REWRITE
					// elements: BAR, ae, BAR, or_expression
					// token labels: 
					// rule labels: ae, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_ae=new RewriteRuleSubtreeStream(adaptor,"rule ae",ae!=null?ae.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 212:13: -> ^( AST_OR_EXPRESSION ^( AST_LEFT_OPERAND $or_expression) BAR BAR ^( AST_RIGHT_OPERAND $ae) )
					{
						// DFSParser.g:212:16: ^( AST_OR_EXPRESSION ^( AST_LEFT_OPERAND $or_expression) BAR BAR ^( AST_RIGHT_OPERAND $ae) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_OR_EXPRESSION, "AST_OR_EXPRESSION"), root_1);
						// DFSParser.g:212:36: ^( AST_LEFT_OPERAND $or_expression)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_LEFT_OPERAND, "AST_LEFT_OPERAND"), root_2);
						adaptor.addChild(root_2, stream_retval.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_1, stream_BAR.nextNode());
						adaptor.addChild(root_1, stream_BAR.nextNode());
						// DFSParser.g:212:79: ^( AST_RIGHT_OPERAND $ae)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_RIGHT_OPERAND, "AST_RIGHT_OPERAND"), root_2);
						adaptor.addChild(root_2, stream_ae.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

				default :
					break loop25;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 30, or_expression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "or_expression"


	public static class and_expression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "and_expression"
	// DFSParser.g:215:1: and_expression : ( not_expression -> not_expression ) ( AMPERSAND AMPERSAND ne= not_expression -> ^( AST_AND_EXPRESSION ^( AST_LEFT_OPERAND $and_expression) AMPERSAND AMPERSAND ^( AST_RIGHT_OPERAND $ne) ) )* ;
	public final DFSParser.and_expression_return and_expression() throws RecognitionException {
		DFSParser.and_expression_return retval = new DFSParser.and_expression_return();
		retval.start = input.LT(1);
		int and_expression_StartIndex = input.index();

		Object root_0 = null;

		Token AMPERSAND68=null;
		Token AMPERSAND69=null;
		ParserRuleReturnScope ne =null;
		ParserRuleReturnScope not_expression67 =null;

		Object AMPERSAND68_tree=null;
		Object AMPERSAND69_tree=null;
		RewriteRuleTokenStream stream_AMPERSAND=new RewriteRuleTokenStream(adaptor,"token AMPERSAND");
		RewriteRuleSubtreeStream stream_not_expression=new RewriteRuleSubtreeStream(adaptor,"rule not_expression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 31) ) { return retval; }

			// DFSParser.g:216:5: ( ( not_expression -> not_expression ) ( AMPERSAND AMPERSAND ne= not_expression -> ^( AST_AND_EXPRESSION ^( AST_LEFT_OPERAND $and_expression) AMPERSAND AMPERSAND ^( AST_RIGHT_OPERAND $ne) ) )* )
			// DFSParser.g:216:9: ( not_expression -> not_expression ) ( AMPERSAND AMPERSAND ne= not_expression -> ^( AST_AND_EXPRESSION ^( AST_LEFT_OPERAND $and_expression) AMPERSAND AMPERSAND ^( AST_RIGHT_OPERAND $ne) ) )*
			{
			// DFSParser.g:216:9: ( not_expression -> not_expression )
			// DFSParser.g:216:10: not_expression
			{
			pushFollow(FOLLOW_not_expression_in_and_expression2022);
			not_expression67=not_expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_not_expression.add(not_expression67.getTree());
			// AST REWRITE
			// elements: not_expression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 217:13: -> not_expression
			{
				adaptor.addChild(root_0, stream_not_expression.nextTree());
			}


			retval.tree = root_0;
			}

			}

			// DFSParser.g:218:9: ( AMPERSAND AMPERSAND ne= not_expression -> ^( AST_AND_EXPRESSION ^( AST_LEFT_OPERAND $and_expression) AMPERSAND AMPERSAND ^( AST_RIGHT_OPERAND $ne) ) )*
			loop26:
			while (true) {
				int alt26=2;
				int LA26_0 = input.LA(1);
				if ( (LA26_0==AMPERSAND) ) {
					alt26=1;
				}

				switch (alt26) {
				case 1 :
					// DFSParser.g:218:10: AMPERSAND AMPERSAND ne= not_expression
					{
					AMPERSAND68=(Token)match(input,AMPERSAND,FOLLOW_AMPERSAND_in_and_expression2050); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_AMPERSAND.add(AMPERSAND68);

					AMPERSAND69=(Token)match(input,AMPERSAND,FOLLOW_AMPERSAND_in_and_expression2052); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_AMPERSAND.add(AMPERSAND69);

					pushFollow(FOLLOW_not_expression_in_and_expression2056);
					ne=not_expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_not_expression.add(ne.getTree());
					// AST REWRITE
					// elements: AMPERSAND, ne, and_expression, AMPERSAND
					// token labels: 
					// rule labels: ne, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_ne=new RewriteRuleSubtreeStream(adaptor,"rule ne",ne!=null?ne.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 219:13: -> ^( AST_AND_EXPRESSION ^( AST_LEFT_OPERAND $and_expression) AMPERSAND AMPERSAND ^( AST_RIGHT_OPERAND $ne) )
					{
						// DFSParser.g:219:16: ^( AST_AND_EXPRESSION ^( AST_LEFT_OPERAND $and_expression) AMPERSAND AMPERSAND ^( AST_RIGHT_OPERAND $ne) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_AND_EXPRESSION, "AST_AND_EXPRESSION"), root_1);
						// DFSParser.g:219:37: ^( AST_LEFT_OPERAND $and_expression)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_LEFT_OPERAND, "AST_LEFT_OPERAND"), root_2);
						adaptor.addChild(root_2, stream_retval.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_1, stream_AMPERSAND.nextNode());
						adaptor.addChild(root_1, stream_AMPERSAND.nextNode());
						// DFSParser.g:219:93: ^( AST_RIGHT_OPERAND $ne)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_RIGHT_OPERAND, "AST_RIGHT_OPERAND"), root_2);
						adaptor.addChild(root_2, stream_ne.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

				default :
					break loop26;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 31, and_expression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "and_expression"


	public static class not_expression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "not_expression"
	// DFSParser.g:222:1: not_expression : ( EXCLAMATION_MARK ne= not_expression -> ^( AST_NOT_EXPRESSION EXCLAMATION_MARK ^( AST_OPERAND $ne) ) |ce= compare_expression -> $ce);
	public final DFSParser.not_expression_return not_expression() throws RecognitionException {
		DFSParser.not_expression_return retval = new DFSParser.not_expression_return();
		retval.start = input.LT(1);
		int not_expression_StartIndex = input.index();

		Object root_0 = null;

		Token EXCLAMATION_MARK70=null;
		ParserRuleReturnScope ne =null;
		ParserRuleReturnScope ce =null;

		Object EXCLAMATION_MARK70_tree=null;
		RewriteRuleTokenStream stream_EXCLAMATION_MARK=new RewriteRuleTokenStream(adaptor,"token EXCLAMATION_MARK");
		RewriteRuleSubtreeStream stream_not_expression=new RewriteRuleSubtreeStream(adaptor,"rule not_expression");
		RewriteRuleSubtreeStream stream_compare_expression=new RewriteRuleSubtreeStream(adaptor,"rule compare_expression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 32) ) { return retval; }

			// DFSParser.g:223:4: ( EXCLAMATION_MARK ne= not_expression -> ^( AST_NOT_EXPRESSION EXCLAMATION_MARK ^( AST_OPERAND $ne) ) |ce= compare_expression -> $ce)
			int alt27=2;
			int LA27_0 = input.LA(1);
			if ( (LA27_0==EXCLAMATION_MARK) ) {
				alt27=1;
			}
			else if ( (LA27_0==DOLLAR_SIGN||LA27_0==FLOATING_POINT_LITERAL||(LA27_0 >= INTEGER_LITERAL && LA27_0 <= KW_TRUE)||(LA27_0 >= LEFT_BRACKET && LA27_0 <= LEFT_PARENT)||LA27_0==MINUS_SIGN||LA27_0==PLUS_SIGN||LA27_0==SIMPLE_ID||LA27_0==STRING_LITERAL) ) {
				alt27=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 27, 0, input);
				throw nvae;
			}

			switch (alt27) {
				case 1 :
					// DFSParser.g:223:6: EXCLAMATION_MARK ne= not_expression
					{
					EXCLAMATION_MARK70=(Token)match(input,EXCLAMATION_MARK,FOLLOW_EXCLAMATION_MARK_in_not_expression2110); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_EXCLAMATION_MARK.add(EXCLAMATION_MARK70);

					pushFollow(FOLLOW_not_expression_in_not_expression2114);
					ne=not_expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_not_expression.add(ne.getTree());
					// AST REWRITE
					// elements: EXCLAMATION_MARK, ne
					// token labels: 
					// rule labels: ne, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_ne=new RewriteRuleSubtreeStream(adaptor,"rule ne",ne!=null?ne.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 224:9: -> ^( AST_NOT_EXPRESSION EXCLAMATION_MARK ^( AST_OPERAND $ne) )
					{
						// DFSParser.g:224:12: ^( AST_NOT_EXPRESSION EXCLAMATION_MARK ^( AST_OPERAND $ne) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_NOT_EXPRESSION, "AST_NOT_EXPRESSION"), root_1);
						adaptor.addChild(root_1, stream_EXCLAMATION_MARK.nextNode());
						// DFSParser.g:224:50: ^( AST_OPERAND $ne)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_OPERAND, "AST_OPERAND"), root_2);
						adaptor.addChild(root_2, stream_ne.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// DFSParser.g:225:6: ce= compare_expression
					{
					pushFollow(FOLLOW_compare_expression_in_not_expression2146);
					ce=compare_expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_compare_expression.add(ce.getTree());
					// AST REWRITE
					// elements: ce
					// token labels: 
					// rule labels: ce, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_ce=new RewriteRuleSubtreeStream(adaptor,"rule ce",ce!=null?ce.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 226:9: -> $ce
					{
						adaptor.addChild(root_0, stream_ce.nextTree());
					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 32, not_expression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "not_expression"


	public static class compare_expression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "compare_expression"
	// DFSParser.g:229:1: compare_expression : ( addition_expression -> addition_expression ) (co= compare_operator ae= addition_expression -> ^( AST_COMPARE_EXPRESSION ^( AST_LEFT_OPERAND $compare_expression) ^( AST_COMPARE_OPERATOR $co) ^( AST_RIGHT_OPERAND $ae) ) )* ;
	public final DFSParser.compare_expression_return compare_expression() throws RecognitionException {
		DFSParser.compare_expression_return retval = new DFSParser.compare_expression_return();
		retval.start = input.LT(1);
		int compare_expression_StartIndex = input.index();

		Object root_0 = null;

		ParserRuleReturnScope co =null;
		ParserRuleReturnScope ae =null;
		ParserRuleReturnScope addition_expression71 =null;

		RewriteRuleSubtreeStream stream_compare_operator=new RewriteRuleSubtreeStream(adaptor,"rule compare_operator");
		RewriteRuleSubtreeStream stream_addition_expression=new RewriteRuleSubtreeStream(adaptor,"rule addition_expression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 33) ) { return retval; }

			// DFSParser.g:230:5: ( ( addition_expression -> addition_expression ) (co= compare_operator ae= addition_expression -> ^( AST_COMPARE_EXPRESSION ^( AST_LEFT_OPERAND $compare_expression) ^( AST_COMPARE_OPERATOR $co) ^( AST_RIGHT_OPERAND $ae) ) )* )
			// DFSParser.g:230:9: ( addition_expression -> addition_expression ) (co= compare_operator ae= addition_expression -> ^( AST_COMPARE_EXPRESSION ^( AST_LEFT_OPERAND $compare_expression) ^( AST_COMPARE_OPERATOR $co) ^( AST_RIGHT_OPERAND $ae) ) )*
			{
			// DFSParser.g:230:9: ( addition_expression -> addition_expression )
			// DFSParser.g:230:10: addition_expression
			{
			pushFollow(FOLLOW_addition_expression_in_compare_expression2178);
			addition_expression71=addition_expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_addition_expression.add(addition_expression71.getTree());
			// AST REWRITE
			// elements: addition_expression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 231:13: -> addition_expression
			{
				adaptor.addChild(root_0, stream_addition_expression.nextTree());
			}


			retval.tree = root_0;
			}

			}

			// DFSParser.g:232:9: (co= compare_operator ae= addition_expression -> ^( AST_COMPARE_EXPRESSION ^( AST_LEFT_OPERAND $compare_expression) ^( AST_COMPARE_OPERATOR $co) ^( AST_RIGHT_OPERAND $ae) ) )*
			loop28:
			while (true) {
				int alt28=2;
				int LA28_0 = input.LA(1);
				if ( ((LA28_0 >= EQUALS && LA28_0 <= EXCLAMATION_MARK)||LA28_0==GREATER_THAN||LA28_0==LESS_THAN) ) {
					alt28=1;
				}

				switch (alt28) {
				case 1 :
					// DFSParser.g:232:10: co= compare_operator ae= addition_expression
					{
					pushFollow(FOLLOW_compare_operator_in_compare_expression2208);
					co=compare_operator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_compare_operator.add(co.getTree());
					pushFollow(FOLLOW_addition_expression_in_compare_expression2212);
					ae=addition_expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_addition_expression.add(ae.getTree());
					// AST REWRITE
					// elements: ae, compare_expression, co
					// token labels: 
					// rule labels: ae, co, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_ae=new RewriteRuleSubtreeStream(adaptor,"rule ae",ae!=null?ae.getTree():null);
					RewriteRuleSubtreeStream stream_co=new RewriteRuleSubtreeStream(adaptor,"rule co",co!=null?co.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 233:13: -> ^( AST_COMPARE_EXPRESSION ^( AST_LEFT_OPERAND $compare_expression) ^( AST_COMPARE_OPERATOR $co) ^( AST_RIGHT_OPERAND $ae) )
					{
						// DFSParser.g:233:16: ^( AST_COMPARE_EXPRESSION ^( AST_LEFT_OPERAND $compare_expression) ^( AST_COMPARE_OPERATOR $co) ^( AST_RIGHT_OPERAND $ae) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_COMPARE_EXPRESSION, "AST_COMPARE_EXPRESSION"), root_1);
						// DFSParser.g:233:41: ^( AST_LEFT_OPERAND $compare_expression)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_LEFT_OPERAND, "AST_LEFT_OPERAND"), root_2);
						adaptor.addChild(root_2, stream_retval.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// DFSParser.g:233:81: ^( AST_COMPARE_OPERATOR $co)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_COMPARE_OPERATOR, "AST_COMPARE_OPERATOR"), root_2);
						adaptor.addChild(root_2, stream_co.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// DFSParser.g:233:109: ^( AST_RIGHT_OPERAND $ae)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_RIGHT_OPERAND, "AST_RIGHT_OPERAND"), root_2);
						adaptor.addChild(root_2, stream_ae.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

				default :
					break loop28;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 33, compare_expression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "compare_expression"


	public static class compare_operator_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "compare_operator"
	// DFSParser.g:236:1: compare_operator : ( EQUALS EQUALS | EQUALS EQUALS EQUALS | LESS_THAN EQUALS GREATER_THAN | EXCLAMATION_MARK EQUALS | GREATER_THAN | LESS_THAN | GREATER_THAN EQUALS | LESS_THAN EQUALS );
	public final DFSParser.compare_operator_return compare_operator() throws RecognitionException {
		DFSParser.compare_operator_return retval = new DFSParser.compare_operator_return();
		retval.start = input.LT(1);
		int compare_operator_StartIndex = input.index();

		Object root_0 = null;

		Token EQUALS72=null;
		Token EQUALS73=null;
		Token EQUALS74=null;
		Token EQUALS75=null;
		Token EQUALS76=null;
		Token LESS_THAN77=null;
		Token EQUALS78=null;
		Token GREATER_THAN79=null;
		Token EXCLAMATION_MARK80=null;
		Token EQUALS81=null;
		Token GREATER_THAN82=null;
		Token LESS_THAN83=null;
		Token GREATER_THAN84=null;
		Token EQUALS85=null;
		Token LESS_THAN86=null;
		Token EQUALS87=null;

		Object EQUALS72_tree=null;
		Object EQUALS73_tree=null;
		Object EQUALS74_tree=null;
		Object EQUALS75_tree=null;
		Object EQUALS76_tree=null;
		Object LESS_THAN77_tree=null;
		Object EQUALS78_tree=null;
		Object GREATER_THAN79_tree=null;
		Object EXCLAMATION_MARK80_tree=null;
		Object EQUALS81_tree=null;
		Object GREATER_THAN82_tree=null;
		Object LESS_THAN83_tree=null;
		Object GREATER_THAN84_tree=null;
		Object EQUALS85_tree=null;
		Object LESS_THAN86_tree=null;
		Object EQUALS87_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 34) ) { return retval; }

			// DFSParser.g:237:5: ( EQUALS EQUALS | EQUALS EQUALS EQUALS | LESS_THAN EQUALS GREATER_THAN | EXCLAMATION_MARK EQUALS | GREATER_THAN | LESS_THAN | GREATER_THAN EQUALS | LESS_THAN EQUALS )
			int alt29=8;
			alt29 = dfa29.predict(input);
			switch (alt29) {
				case 1 :
					// DFSParser.g:237:9: EQUALS EQUALS
					{
					root_0 = (Object)adaptor.nil();


					EQUALS72=(Token)match(input,EQUALS,FOLLOW_EQUALS_in_compare_operator2272); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					EQUALS72_tree = (Object)adaptor.create(EQUALS72);
					adaptor.addChild(root_0, EQUALS72_tree);
					}

					EQUALS73=(Token)match(input,EQUALS,FOLLOW_EQUALS_in_compare_operator2274); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					EQUALS73_tree = (Object)adaptor.create(EQUALS73);
					adaptor.addChild(root_0, EQUALS73_tree);
					}

					}
					break;
				case 2 :
					// DFSParser.g:238:9: EQUALS EQUALS EQUALS
					{
					root_0 = (Object)adaptor.nil();


					EQUALS74=(Token)match(input,EQUALS,FOLLOW_EQUALS_in_compare_operator2284); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					EQUALS74_tree = (Object)adaptor.create(EQUALS74);
					adaptor.addChild(root_0, EQUALS74_tree);
					}

					EQUALS75=(Token)match(input,EQUALS,FOLLOW_EQUALS_in_compare_operator2286); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					EQUALS75_tree = (Object)adaptor.create(EQUALS75);
					adaptor.addChild(root_0, EQUALS75_tree);
					}

					EQUALS76=(Token)match(input,EQUALS,FOLLOW_EQUALS_in_compare_operator2288); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					EQUALS76_tree = (Object)adaptor.create(EQUALS76);
					adaptor.addChild(root_0, EQUALS76_tree);
					}

					}
					break;
				case 3 :
					// DFSParser.g:239:9: LESS_THAN EQUALS GREATER_THAN
					{
					root_0 = (Object)adaptor.nil();


					LESS_THAN77=(Token)match(input,LESS_THAN,FOLLOW_LESS_THAN_in_compare_operator2298); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					LESS_THAN77_tree = (Object)adaptor.create(LESS_THAN77);
					adaptor.addChild(root_0, LESS_THAN77_tree);
					}

					EQUALS78=(Token)match(input,EQUALS,FOLLOW_EQUALS_in_compare_operator2300); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					EQUALS78_tree = (Object)adaptor.create(EQUALS78);
					adaptor.addChild(root_0, EQUALS78_tree);
					}

					GREATER_THAN79=(Token)match(input,GREATER_THAN,FOLLOW_GREATER_THAN_in_compare_operator2302); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					GREATER_THAN79_tree = (Object)adaptor.create(GREATER_THAN79);
					adaptor.addChild(root_0, GREATER_THAN79_tree);
					}

					}
					break;
				case 4 :
					// DFSParser.g:240:9: EXCLAMATION_MARK EQUALS
					{
					root_0 = (Object)adaptor.nil();


					EXCLAMATION_MARK80=(Token)match(input,EXCLAMATION_MARK,FOLLOW_EXCLAMATION_MARK_in_compare_operator2312); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					EXCLAMATION_MARK80_tree = (Object)adaptor.create(EXCLAMATION_MARK80);
					adaptor.addChild(root_0, EXCLAMATION_MARK80_tree);
					}

					EQUALS81=(Token)match(input,EQUALS,FOLLOW_EQUALS_in_compare_operator2314); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					EQUALS81_tree = (Object)adaptor.create(EQUALS81);
					adaptor.addChild(root_0, EQUALS81_tree);
					}

					}
					break;
				case 5 :
					// DFSParser.g:241:9: GREATER_THAN
					{
					root_0 = (Object)adaptor.nil();


					GREATER_THAN82=(Token)match(input,GREATER_THAN,FOLLOW_GREATER_THAN_in_compare_operator2324); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					GREATER_THAN82_tree = (Object)adaptor.create(GREATER_THAN82);
					adaptor.addChild(root_0, GREATER_THAN82_tree);
					}

					}
					break;
				case 6 :
					// DFSParser.g:242:9: LESS_THAN
					{
					root_0 = (Object)adaptor.nil();


					LESS_THAN83=(Token)match(input,LESS_THAN,FOLLOW_LESS_THAN_in_compare_operator2334); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					LESS_THAN83_tree = (Object)adaptor.create(LESS_THAN83);
					adaptor.addChild(root_0, LESS_THAN83_tree);
					}

					}
					break;
				case 7 :
					// DFSParser.g:243:9: GREATER_THAN EQUALS
					{
					root_0 = (Object)adaptor.nil();


					GREATER_THAN84=(Token)match(input,GREATER_THAN,FOLLOW_GREATER_THAN_in_compare_operator2344); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					GREATER_THAN84_tree = (Object)adaptor.create(GREATER_THAN84);
					adaptor.addChild(root_0, GREATER_THAN84_tree);
					}

					EQUALS85=(Token)match(input,EQUALS,FOLLOW_EQUALS_in_compare_operator2346); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					EQUALS85_tree = (Object)adaptor.create(EQUALS85);
					adaptor.addChild(root_0, EQUALS85_tree);
					}

					}
					break;
				case 8 :
					// DFSParser.g:244:9: LESS_THAN EQUALS
					{
					root_0 = (Object)adaptor.nil();


					LESS_THAN86=(Token)match(input,LESS_THAN,FOLLOW_LESS_THAN_in_compare_operator2356); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					LESS_THAN86_tree = (Object)adaptor.create(LESS_THAN86);
					adaptor.addChild(root_0, LESS_THAN86_tree);
					}

					EQUALS87=(Token)match(input,EQUALS,FOLLOW_EQUALS_in_compare_operator2358); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					EQUALS87_tree = (Object)adaptor.create(EQUALS87);
					adaptor.addChild(root_0, EQUALS87_tree);
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 34, compare_operator_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "compare_operator"


	public static class addition_expression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "addition_expression"
	// DFSParser.g:247:1: addition_expression : ( mult_expression -> mult_expression ) (ao= addition_operator me= mult_expression -> ^( AST_ADDITION_EXPRESSION ^( AST_LEFT_OPERAND $addition_expression) ^( AST_ADDITION_OPERATOR $ao) ^( AST_RIGHT_OPERAND $me) ) )* ;
	public final DFSParser.addition_expression_return addition_expression() throws RecognitionException {
		DFSParser.addition_expression_return retval = new DFSParser.addition_expression_return();
		retval.start = input.LT(1);
		int addition_expression_StartIndex = input.index();

		Object root_0 = null;

		ParserRuleReturnScope ao =null;
		ParserRuleReturnScope me =null;
		ParserRuleReturnScope mult_expression88 =null;

		RewriteRuleSubtreeStream stream_mult_expression=new RewriteRuleSubtreeStream(adaptor,"rule mult_expression");
		RewriteRuleSubtreeStream stream_addition_operator=new RewriteRuleSubtreeStream(adaptor,"rule addition_operator");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 35) ) { return retval; }

			// DFSParser.g:248:5: ( ( mult_expression -> mult_expression ) (ao= addition_operator me= mult_expression -> ^( AST_ADDITION_EXPRESSION ^( AST_LEFT_OPERAND $addition_expression) ^( AST_ADDITION_OPERATOR $ao) ^( AST_RIGHT_OPERAND $me) ) )* )
			// DFSParser.g:248:9: ( mult_expression -> mult_expression ) (ao= addition_operator me= mult_expression -> ^( AST_ADDITION_EXPRESSION ^( AST_LEFT_OPERAND $addition_expression) ^( AST_ADDITION_OPERATOR $ao) ^( AST_RIGHT_OPERAND $me) ) )*
			{
			// DFSParser.g:248:9: ( mult_expression -> mult_expression )
			// DFSParser.g:248:10: mult_expression
			{
			pushFollow(FOLLOW_mult_expression_in_addition_expression2378);
			mult_expression88=mult_expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_mult_expression.add(mult_expression88.getTree());
			// AST REWRITE
			// elements: mult_expression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 249:13: -> mult_expression
			{
				adaptor.addChild(root_0, stream_mult_expression.nextTree());
			}


			retval.tree = root_0;
			}

			}

			// DFSParser.g:250:9: (ao= addition_operator me= mult_expression -> ^( AST_ADDITION_EXPRESSION ^( AST_LEFT_OPERAND $addition_expression) ^( AST_ADDITION_OPERATOR $ao) ^( AST_RIGHT_OPERAND $me) ) )*
			loop30:
			while (true) {
				int alt30=2;
				int LA30_0 = input.LA(1);
				if ( (LA30_0==MINUS_SIGN||LA30_0==PLUS_SIGN) ) {
					alt30=1;
				}

				switch (alt30) {
				case 1 :
					// DFSParser.g:250:10: ao= addition_operator me= mult_expression
					{
					pushFollow(FOLLOW_addition_operator_in_addition_expression2408);
					ao=addition_operator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_addition_operator.add(ao.getTree());
					pushFollow(FOLLOW_mult_expression_in_addition_expression2412);
					me=mult_expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_mult_expression.add(me.getTree());
					// AST REWRITE
					// elements: addition_expression, ao, me
					// token labels: 
					// rule labels: me, ao, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_me=new RewriteRuleSubtreeStream(adaptor,"rule me",me!=null?me.getTree():null);
					RewriteRuleSubtreeStream stream_ao=new RewriteRuleSubtreeStream(adaptor,"rule ao",ao!=null?ao.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 251:13: -> ^( AST_ADDITION_EXPRESSION ^( AST_LEFT_OPERAND $addition_expression) ^( AST_ADDITION_OPERATOR $ao) ^( AST_RIGHT_OPERAND $me) )
					{
						// DFSParser.g:251:16: ^( AST_ADDITION_EXPRESSION ^( AST_LEFT_OPERAND $addition_expression) ^( AST_ADDITION_OPERATOR $ao) ^( AST_RIGHT_OPERAND $me) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_ADDITION_EXPRESSION, "AST_ADDITION_EXPRESSION"), root_1);
						// DFSParser.g:251:42: ^( AST_LEFT_OPERAND $addition_expression)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_LEFT_OPERAND, "AST_LEFT_OPERAND"), root_2);
						adaptor.addChild(root_2, stream_retval.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// DFSParser.g:251:83: ^( AST_ADDITION_OPERATOR $ao)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_ADDITION_OPERATOR, "AST_ADDITION_OPERATOR"), root_2);
						adaptor.addChild(root_2, stream_ao.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// DFSParser.g:251:112: ^( AST_RIGHT_OPERAND $me)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_RIGHT_OPERAND, "AST_RIGHT_OPERAND"), root_2);
						adaptor.addChild(root_2, stream_me.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

				default :
					break loop30;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 35, addition_expression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "addition_expression"


	public static class addition_operator_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "addition_operator"
	// DFSParser.g:254:1: addition_operator : ( PLUS_SIGN | MINUS_SIGN );
	public final DFSParser.addition_operator_return addition_operator() throws RecognitionException {
		DFSParser.addition_operator_return retval = new DFSParser.addition_operator_return();
		retval.start = input.LT(1);
		int addition_operator_StartIndex = input.index();

		Object root_0 = null;

		Token set89=null;

		Object set89_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 36) ) { return retval; }

			// DFSParser.g:255:5: ( PLUS_SIGN | MINUS_SIGN )
			// DFSParser.g:
			{
			root_0 = (Object)adaptor.nil();


			set89=input.LT(1);
			if ( input.LA(1)==MINUS_SIGN||input.LA(1)==PLUS_SIGN ) {
				input.consume();
				if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set89));
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 36, addition_operator_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "addition_operator"


	public static class mult_expression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "mult_expression"
	// DFSParser.g:259:1: mult_expression : ( pow_expression -> pow_expression ) (mo= mult_operator pe= pow_expression -> ^( AST_MULT_EXPRESSION ^( AST_LEFT_OPERAND $mult_expression) ^( AST_MULT_OPERATOR $mo) ^( AST_RIGHT_OPERAND $pe) ) )* ;
	public final DFSParser.mult_expression_return mult_expression() throws RecognitionException {
		DFSParser.mult_expression_return retval = new DFSParser.mult_expression_return();
		retval.start = input.LT(1);
		int mult_expression_StartIndex = input.index();

		Object root_0 = null;

		ParserRuleReturnScope mo =null;
		ParserRuleReturnScope pe =null;
		ParserRuleReturnScope pow_expression90 =null;

		RewriteRuleSubtreeStream stream_pow_expression=new RewriteRuleSubtreeStream(adaptor,"rule pow_expression");
		RewriteRuleSubtreeStream stream_mult_operator=new RewriteRuleSubtreeStream(adaptor,"rule mult_operator");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 37) ) { return retval; }

			// DFSParser.g:260:5: ( ( pow_expression -> pow_expression ) (mo= mult_operator pe= pow_expression -> ^( AST_MULT_EXPRESSION ^( AST_LEFT_OPERAND $mult_expression) ^( AST_MULT_OPERATOR $mo) ^( AST_RIGHT_OPERAND $pe) ) )* )
			// DFSParser.g:260:9: ( pow_expression -> pow_expression ) (mo= mult_operator pe= pow_expression -> ^( AST_MULT_EXPRESSION ^( AST_LEFT_OPERAND $mult_expression) ^( AST_MULT_OPERATOR $mo) ^( AST_RIGHT_OPERAND $pe) ) )*
			{
			// DFSParser.g:260:9: ( pow_expression -> pow_expression )
			// DFSParser.g:260:10: pow_expression
			{
			pushFollow(FOLLOW_pow_expression_in_mult_expression2502);
			pow_expression90=pow_expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_pow_expression.add(pow_expression90.getTree());
			// AST REWRITE
			// elements: pow_expression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 261:13: -> pow_expression
			{
				adaptor.addChild(root_0, stream_pow_expression.nextTree());
			}


			retval.tree = root_0;
			}

			}

			// DFSParser.g:262:9: (mo= mult_operator pe= pow_expression -> ^( AST_MULT_EXPRESSION ^( AST_LEFT_OPERAND $mult_expression) ^( AST_MULT_OPERATOR $mo) ^( AST_RIGHT_OPERAND $pe) ) )*
			loop31:
			while (true) {
				int alt31=2;
				int LA31_0 = input.LA(1);
				if ( (LA31_0==ASTERISK||LA31_0==SLASH) ) {
					alt31=1;
				}

				switch (alt31) {
				case 1 :
					// DFSParser.g:262:10: mo= mult_operator pe= pow_expression
					{
					pushFollow(FOLLOW_mult_operator_in_mult_expression2532);
					mo=mult_operator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_mult_operator.add(mo.getTree());
					pushFollow(FOLLOW_pow_expression_in_mult_expression2536);
					pe=pow_expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_pow_expression.add(pe.getTree());
					// AST REWRITE
					// elements: mo, pe, mult_expression
					// token labels: 
					// rule labels: mo, pe, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_mo=new RewriteRuleSubtreeStream(adaptor,"rule mo",mo!=null?mo.getTree():null);
					RewriteRuleSubtreeStream stream_pe=new RewriteRuleSubtreeStream(adaptor,"rule pe",pe!=null?pe.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 263:13: -> ^( AST_MULT_EXPRESSION ^( AST_LEFT_OPERAND $mult_expression) ^( AST_MULT_OPERATOR $mo) ^( AST_RIGHT_OPERAND $pe) )
					{
						// DFSParser.g:263:16: ^( AST_MULT_EXPRESSION ^( AST_LEFT_OPERAND $mult_expression) ^( AST_MULT_OPERATOR $mo) ^( AST_RIGHT_OPERAND $pe) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_MULT_EXPRESSION, "AST_MULT_EXPRESSION"), root_1);
						// DFSParser.g:263:38: ^( AST_LEFT_OPERAND $mult_expression)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_LEFT_OPERAND, "AST_LEFT_OPERAND"), root_2);
						adaptor.addChild(root_2, stream_retval.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// DFSParser.g:263:75: ^( AST_MULT_OPERATOR $mo)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_MULT_OPERATOR, "AST_MULT_OPERATOR"), root_2);
						adaptor.addChild(root_2, stream_mo.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// DFSParser.g:263:100: ^( AST_RIGHT_OPERAND $pe)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_RIGHT_OPERAND, "AST_RIGHT_OPERAND"), root_2);
						adaptor.addChild(root_2, stream_pe.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

				default :
					break loop31;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 37, mult_expression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "mult_expression"


	public static class mult_operator_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "mult_operator"
	// DFSParser.g:266:1: mult_operator : ( ASTERISK | SLASH );
	public final DFSParser.mult_operator_return mult_operator() throws RecognitionException {
		DFSParser.mult_operator_return retval = new DFSParser.mult_operator_return();
		retval.start = input.LT(1);
		int mult_operator_StartIndex = input.index();

		Object root_0 = null;

		Token set91=null;

		Object set91_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 38) ) { return retval; }

			// DFSParser.g:267:5: ( ASTERISK | SLASH )
			// DFSParser.g:
			{
			root_0 = (Object)adaptor.nil();


			set91=input.LT(1);
			if ( input.LA(1)==ASTERISK||input.LA(1)==SLASH ) {
				input.consume();
				if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set91));
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 38, mult_operator_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "mult_operator"


	public static class pow_expression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "pow_expression"
	// DFSParser.g:271:1: pow_expression : ( unary_expression -> unary_expression ) ( CARET ue= unary_expression -> ^( AST_POW_EXPRESSION $pow_expression CARET ^( AST_OPERAND $ue) ) )* ;
	public final DFSParser.pow_expression_return pow_expression() throws RecognitionException {
		DFSParser.pow_expression_return retval = new DFSParser.pow_expression_return();
		retval.start = input.LT(1);
		int pow_expression_StartIndex = input.index();

		Object root_0 = null;

		Token CARET93=null;
		ParserRuleReturnScope ue =null;
		ParserRuleReturnScope unary_expression92 =null;

		Object CARET93_tree=null;
		RewriteRuleTokenStream stream_CARET=new RewriteRuleTokenStream(adaptor,"token CARET");
		RewriteRuleSubtreeStream stream_unary_expression=new RewriteRuleSubtreeStream(adaptor,"rule unary_expression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 39) ) { return retval; }

			// DFSParser.g:272:5: ( ( unary_expression -> unary_expression ) ( CARET ue= unary_expression -> ^( AST_POW_EXPRESSION $pow_expression CARET ^( AST_OPERAND $ue) ) )* )
			// DFSParser.g:272:7: ( unary_expression -> unary_expression ) ( CARET ue= unary_expression -> ^( AST_POW_EXPRESSION $pow_expression CARET ^( AST_OPERAND $ue) ) )*
			{
			// DFSParser.g:272:7: ( unary_expression -> unary_expression )
			// DFSParser.g:272:8: unary_expression
			{
			pushFollow(FOLLOW_unary_expression_in_pow_expression2624);
			unary_expression92=unary_expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_unary_expression.add(unary_expression92.getTree());
			// AST REWRITE
			// elements: unary_expression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 273:13: -> unary_expression
			{
				adaptor.addChild(root_0, stream_unary_expression.nextTree());
			}


			retval.tree = root_0;
			}

			}

			// DFSParser.g:274:9: ( CARET ue= unary_expression -> ^( AST_POW_EXPRESSION $pow_expression CARET ^( AST_OPERAND $ue) ) )*
			loop32:
			while (true) {
				int alt32=2;
				int LA32_0 = input.LA(1);
				if ( (LA32_0==CARET) ) {
					alt32=1;
				}

				switch (alt32) {
				case 1 :
					// DFSParser.g:274:10: CARET ue= unary_expression
					{
					CARET93=(Token)match(input,CARET,FOLLOW_CARET_in_pow_expression2652); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_CARET.add(CARET93);

					pushFollow(FOLLOW_unary_expression_in_pow_expression2656);
					ue=unary_expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_unary_expression.add(ue.getTree());
					// AST REWRITE
					// elements: pow_expression, ue, CARET
					// token labels: 
					// rule labels: ue, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_ue=new RewriteRuleSubtreeStream(adaptor,"rule ue",ue!=null?ue.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 275:13: -> ^( AST_POW_EXPRESSION $pow_expression CARET ^( AST_OPERAND $ue) )
					{
						// DFSParser.g:275:16: ^( AST_POW_EXPRESSION $pow_expression CARET ^( AST_OPERAND $ue) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_POW_EXPRESSION, "AST_POW_EXPRESSION"), root_1);
						adaptor.addChild(root_1, stream_retval.nextTree());
						adaptor.addChild(root_1, stream_CARET.nextNode());
						// DFSParser.g:275:59: ^( AST_OPERAND $ue)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_OPERAND, "AST_OPERAND"), root_2);
						adaptor.addChild(root_2, stream_ue.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

				default :
					break loop32;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 39, pow_expression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "pow_expression"


	public static class unary_expression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "unary_expression"
	// DFSParser.g:278:1: unary_expression : (uo= unary_operator ue= unary_expression -> ^( AST_UNARY_EXPRESSION ^( AST_UNARY_OPERATOR $uo) ^( AST_OPERAND $ue) ) |del= df_expression_literal -> $del);
	public final DFSParser.unary_expression_return unary_expression() throws RecognitionException {
		DFSParser.unary_expression_return retval = new DFSParser.unary_expression_return();
		retval.start = input.LT(1);
		int unary_expression_StartIndex = input.index();

		Object root_0 = null;

		ParserRuleReturnScope uo =null;
		ParserRuleReturnScope ue =null;
		ParserRuleReturnScope del =null;

		RewriteRuleSubtreeStream stream_unary_operator=new RewriteRuleSubtreeStream(adaptor,"rule unary_operator");
		RewriteRuleSubtreeStream stream_df_expression_literal=new RewriteRuleSubtreeStream(adaptor,"rule df_expression_literal");
		RewriteRuleSubtreeStream stream_unary_expression=new RewriteRuleSubtreeStream(adaptor,"rule unary_expression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 40) ) { return retval; }

			// DFSParser.g:279:5: (uo= unary_operator ue= unary_expression -> ^( AST_UNARY_EXPRESSION ^( AST_UNARY_OPERATOR $uo) ^( AST_OPERAND $ue) ) |del= df_expression_literal -> $del)
			int alt33=2;
			int LA33_0 = input.LA(1);
			if ( (LA33_0==MINUS_SIGN||LA33_0==PLUS_SIGN) ) {
				alt33=1;
			}
			else if ( (LA33_0==DOLLAR_SIGN||LA33_0==FLOATING_POINT_LITERAL||(LA33_0 >= INTEGER_LITERAL && LA33_0 <= KW_TRUE)||(LA33_0 >= LEFT_BRACKET && LA33_0 <= LEFT_PARENT)||LA33_0==SIMPLE_ID||LA33_0==STRING_LITERAL) ) {
				alt33=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 33, 0, input);
				throw nvae;
			}

			switch (alt33) {
				case 1 :
					// DFSParser.g:279:9: uo= unary_operator ue= unary_expression
					{
					pushFollow(FOLLOW_unary_operator_in_unary_expression2709);
					uo=unary_operator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_unary_operator.add(uo.getTree());
					pushFollow(FOLLOW_unary_expression_in_unary_expression2713);
					ue=unary_expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_unary_expression.add(ue.getTree());
					// AST REWRITE
					// elements: ue, uo
					// token labels: 
					// rule labels: ue, uo, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_ue=new RewriteRuleSubtreeStream(adaptor,"rule ue",ue!=null?ue.getTree():null);
					RewriteRuleSubtreeStream stream_uo=new RewriteRuleSubtreeStream(adaptor,"rule uo",uo!=null?uo.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 280:9: -> ^( AST_UNARY_EXPRESSION ^( AST_UNARY_OPERATOR $uo) ^( AST_OPERAND $ue) )
					{
						// DFSParser.g:280:12: ^( AST_UNARY_EXPRESSION ^( AST_UNARY_OPERATOR $uo) ^( AST_OPERAND $ue) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_UNARY_EXPRESSION, "AST_UNARY_EXPRESSION"), root_1);
						// DFSParser.g:280:35: ^( AST_UNARY_OPERATOR $uo)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_UNARY_OPERATOR, "AST_UNARY_OPERATOR"), root_2);
						adaptor.addChild(root_2, stream_uo.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// DFSParser.g:280:61: ^( AST_OPERAND $ue)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_OPERAND, "AST_OPERAND"), root_2);
						adaptor.addChild(root_2, stream_ue.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// DFSParser.g:281:9: del= df_expression_literal
					{
					pushFollow(FOLLOW_df_expression_literal_in_unary_expression2753);
					del=df_expression_literal();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_df_expression_literal.add(del.getTree());
					// AST REWRITE
					// elements: del
					// token labels: 
					// rule labels: del, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_del=new RewriteRuleSubtreeStream(adaptor,"rule del",del!=null?del.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 282:9: -> $del
					{
						adaptor.addChild(root_0, stream_del.nextTree());
					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 40, unary_expression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "unary_expression"


	public static class unary_operator_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "unary_operator"
	// DFSParser.g:285:1: unary_operator : ( PLUS_SIGN | MINUS_SIGN );
	public final DFSParser.unary_operator_return unary_operator() throws RecognitionException {
		DFSParser.unary_operator_return retval = new DFSParser.unary_operator_return();
		retval.start = input.LT(1);
		int unary_operator_StartIndex = input.index();

		Object root_0 = null;

		Token set94=null;

		Object set94_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 41) ) { return retval; }

			// DFSParser.g:286:5: ( PLUS_SIGN | MINUS_SIGN )
			// DFSParser.g:
			{
			root_0 = (Object)adaptor.nil();


			set94=input.LT(1);
			if ( input.LA(1)==MINUS_SIGN||input.LA(1)==PLUS_SIGN ) {
				input.consume();
				if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set94));
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 41, unary_operator_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "unary_operator"


	public static class df_expression_literal_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "df_expression_literal"
	// DFSParser.g:290:1: df_expression_literal : ( STRING_LITERAL -> ^( AST_STRING_LITERAL STRING_LITERAL ) | INTEGER_LITERAL -> ^( AST_INTEGER_LITERAL INTEGER_LITERAL ) | FLOATING_POINT_LITERAL -> ^( AST_FLOATING_POINT_LITERAL FLOATING_POINT_LITERAL ) | DOLLAR_SIGN SIMPLE_ID -> ^( AST_PARAMETER_INVOCATION DOLLAR_SIGN SIMPLE_ID ) |al= df_expression_array_literal -> $al|def= df_expression_function -> $def|id= identifier -> ^( AST_COLUMN_REFERENCE $id) | LEFT_PARENT (de= df_expression )? RIGHT_PARENT -> LEFT_PARENT ( $de)? RIGHT_PARENT );
	public final DFSParser.df_expression_literal_return df_expression_literal() throws RecognitionException {
		DFSParser.df_expression_literal_return retval = new DFSParser.df_expression_literal_return();
		retval.start = input.LT(1);
		int df_expression_literal_StartIndex = input.index();

		Object root_0 = null;

		Token STRING_LITERAL95=null;
		Token INTEGER_LITERAL96=null;
		Token FLOATING_POINT_LITERAL97=null;
		Token DOLLAR_SIGN98=null;
		Token SIMPLE_ID99=null;
		Token LEFT_PARENT100=null;
		Token RIGHT_PARENT101=null;
		ParserRuleReturnScope al =null;
		ParserRuleReturnScope def =null;
		ParserRuleReturnScope id =null;
		ParserRuleReturnScope de =null;

		Object STRING_LITERAL95_tree=null;
		Object INTEGER_LITERAL96_tree=null;
		Object FLOATING_POINT_LITERAL97_tree=null;
		Object DOLLAR_SIGN98_tree=null;
		Object SIMPLE_ID99_tree=null;
		Object LEFT_PARENT100_tree=null;
		Object RIGHT_PARENT101_tree=null;
		RewriteRuleTokenStream stream_SIMPLE_ID=new RewriteRuleTokenStream(adaptor,"token SIMPLE_ID");
		RewriteRuleTokenStream stream_RIGHT_PARENT=new RewriteRuleTokenStream(adaptor,"token RIGHT_PARENT");
		RewriteRuleTokenStream stream_LEFT_PARENT=new RewriteRuleTokenStream(adaptor,"token LEFT_PARENT");
		RewriteRuleTokenStream stream_FLOATING_POINT_LITERAL=new RewriteRuleTokenStream(adaptor,"token FLOATING_POINT_LITERAL");
		RewriteRuleTokenStream stream_DOLLAR_SIGN=new RewriteRuleTokenStream(adaptor,"token DOLLAR_SIGN");
		RewriteRuleTokenStream stream_STRING_LITERAL=new RewriteRuleTokenStream(adaptor,"token STRING_LITERAL");
		RewriteRuleTokenStream stream_INTEGER_LITERAL=new RewriteRuleTokenStream(adaptor,"token INTEGER_LITERAL");
		RewriteRuleSubtreeStream stream_identifier=new RewriteRuleSubtreeStream(adaptor,"rule identifier");
		RewriteRuleSubtreeStream stream_df_expression_array_literal=new RewriteRuleSubtreeStream(adaptor,"rule df_expression_array_literal");
		RewriteRuleSubtreeStream stream_df_expression=new RewriteRuleSubtreeStream(adaptor,"rule df_expression");
		RewriteRuleSubtreeStream stream_df_expression_function=new RewriteRuleSubtreeStream(adaptor,"rule df_expression_function");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 42) ) { return retval; }

			// DFSParser.g:291:5: ( STRING_LITERAL -> ^( AST_STRING_LITERAL STRING_LITERAL ) | INTEGER_LITERAL -> ^( AST_INTEGER_LITERAL INTEGER_LITERAL ) | FLOATING_POINT_LITERAL -> ^( AST_FLOATING_POINT_LITERAL FLOATING_POINT_LITERAL ) | DOLLAR_SIGN SIMPLE_ID -> ^( AST_PARAMETER_INVOCATION DOLLAR_SIGN SIMPLE_ID ) |al= df_expression_array_literal -> $al|def= df_expression_function -> $def|id= identifier -> ^( AST_COLUMN_REFERENCE $id) | LEFT_PARENT (de= df_expression )? RIGHT_PARENT -> LEFT_PARENT ( $de)? RIGHT_PARENT )
			int alt35=8;
			switch ( input.LA(1) ) {
			case STRING_LITERAL:
				{
				alt35=1;
				}
				break;
			case INTEGER_LITERAL:
				{
				alt35=2;
				}
				break;
			case FLOATING_POINT_LITERAL:
				{
				alt35=3;
				}
				break;
			case DOLLAR_SIGN:
				{
				alt35=4;
				}
				break;
			case LEFT_BRACKET:
				{
				alt35=5;
				}
				break;
			case KW_FLOAT:
				{
				alt35=6;
				}
				break;
			case KW_ANY:
			case KW_AS:
			case KW_BOOLEAN:
			case KW_DATE:
			case KW_DECIMAL:
			case KW_DOUBLE:
			case KW_FALSE:
			case KW_FILTER:
			case KW_INTEGER:
			case KW_JOIN:
			case KW_LONG:
			case KW_OUTPUT:
			case KW_PARAMETERS:
			case KW_SHORT:
			case KW_SINK:
			case KW_SOURCE:
			case KW_STRING:
			case KW_TIMESTAMP:
			case KW_TRUE:
			case SIMPLE_ID:
				{
				alt35=7;
				}
				break;
			case LEFT_PARENT:
				{
				alt35=8;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 35, 0, input);
				throw nvae;
			}
			switch (alt35) {
				case 1 :
					// DFSParser.g:291:9: STRING_LITERAL
					{
					STRING_LITERAL95=(Token)match(input,STRING_LITERAL,FOLLOW_STRING_LITERAL_in_df_expression_literal2814); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_STRING_LITERAL.add(STRING_LITERAL95);

					// AST REWRITE
					// elements: STRING_LITERAL
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 292:9: -> ^( AST_STRING_LITERAL STRING_LITERAL )
					{
						// DFSParser.g:292:12: ^( AST_STRING_LITERAL STRING_LITERAL )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_STRING_LITERAL, "AST_STRING_LITERAL"), root_1);
						adaptor.addChild(root_1, stream_STRING_LITERAL.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// DFSParser.g:293:9: INTEGER_LITERAL
					{
					INTEGER_LITERAL96=(Token)match(input,INTEGER_LITERAL,FOLLOW_INTEGER_LITERAL_in_df_expression_literal2840); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_INTEGER_LITERAL.add(INTEGER_LITERAL96);

					// AST REWRITE
					// elements: INTEGER_LITERAL
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 294:9: -> ^( AST_INTEGER_LITERAL INTEGER_LITERAL )
					{
						// DFSParser.g:294:12: ^( AST_INTEGER_LITERAL INTEGER_LITERAL )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_INTEGER_LITERAL, "AST_INTEGER_LITERAL"), root_1);
						adaptor.addChild(root_1, stream_INTEGER_LITERAL.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// DFSParser.g:295:9: FLOATING_POINT_LITERAL
					{
					FLOATING_POINT_LITERAL97=(Token)match(input,FLOATING_POINT_LITERAL,FOLLOW_FLOATING_POINT_LITERAL_in_df_expression_literal2866); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_FLOATING_POINT_LITERAL.add(FLOATING_POINT_LITERAL97);

					// AST REWRITE
					// elements: FLOATING_POINT_LITERAL
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 296:9: -> ^( AST_FLOATING_POINT_LITERAL FLOATING_POINT_LITERAL )
					{
						// DFSParser.g:296:12: ^( AST_FLOATING_POINT_LITERAL FLOATING_POINT_LITERAL )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_FLOATING_POINT_LITERAL, "AST_FLOATING_POINT_LITERAL"), root_1);
						adaptor.addChild(root_1, stream_FLOATING_POINT_LITERAL.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 4 :
					// DFSParser.g:297:9: DOLLAR_SIGN SIMPLE_ID
					{
					DOLLAR_SIGN98=(Token)match(input,DOLLAR_SIGN,FOLLOW_DOLLAR_SIGN_in_df_expression_literal2892); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOLLAR_SIGN.add(DOLLAR_SIGN98);

					SIMPLE_ID99=(Token)match(input,SIMPLE_ID,FOLLOW_SIMPLE_ID_in_df_expression_literal2894); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SIMPLE_ID.add(SIMPLE_ID99);

					// AST REWRITE
					// elements: SIMPLE_ID, DOLLAR_SIGN
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 298:9: -> ^( AST_PARAMETER_INVOCATION DOLLAR_SIGN SIMPLE_ID )
					{
						// DFSParser.g:298:12: ^( AST_PARAMETER_INVOCATION DOLLAR_SIGN SIMPLE_ID )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_PARAMETER_INVOCATION, "AST_PARAMETER_INVOCATION"), root_1);
						adaptor.addChild(root_1, stream_DOLLAR_SIGN.nextNode());
						adaptor.addChild(root_1, stream_SIMPLE_ID.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 5 :
					// DFSParser.g:299:9: al= df_expression_array_literal
					{
					pushFollow(FOLLOW_df_expression_array_literal_in_df_expression_literal2924);
					al=df_expression_array_literal();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_df_expression_array_literal.add(al.getTree());
					// AST REWRITE
					// elements: al
					// token labels: 
					// rule labels: al, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_al=new RewriteRuleSubtreeStream(adaptor,"rule al",al!=null?al.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 300:9: -> $al
					{
						adaptor.addChild(root_0, stream_al.nextTree());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 6 :
					// DFSParser.g:301:9: def= df_expression_function
					{
					pushFollow(FOLLOW_df_expression_function_in_df_expression_literal2949);
					def=df_expression_function();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_df_expression_function.add(def.getTree());
					// AST REWRITE
					// elements: def
					// token labels: 
					// rule labels: def, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_def=new RewriteRuleSubtreeStream(adaptor,"rule def",def!=null?def.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 302:9: -> $def
					{
						adaptor.addChild(root_0, stream_def.nextTree());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 7 :
					// DFSParser.g:303:9: id= identifier
					{
					pushFollow(FOLLOW_identifier_in_df_expression_literal2974);
					id=identifier();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_identifier.add(id.getTree());
					// AST REWRITE
					// elements: id
					// token labels: 
					// rule labels: id, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_id=new RewriteRuleSubtreeStream(adaptor,"rule id",id!=null?id.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 304:9: -> ^( AST_COLUMN_REFERENCE $id)
					{
						// DFSParser.g:304:12: ^( AST_COLUMN_REFERENCE $id)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_COLUMN_REFERENCE, "AST_COLUMN_REFERENCE"), root_1);
						adaptor.addChild(root_1, stream_id.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 8 :
					// DFSParser.g:305:9: LEFT_PARENT (de= df_expression )? RIGHT_PARENT
					{
					LEFT_PARENT100=(Token)match(input,LEFT_PARENT,FOLLOW_LEFT_PARENT_in_df_expression_literal3001); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LEFT_PARENT.add(LEFT_PARENT100);

					// DFSParser.g:305:23: (de= df_expression )?
					int alt34=2;
					int LA34_0 = input.LA(1);
					if ( (LA34_0==DOLLAR_SIGN||(LA34_0 >= EXCLAMATION_MARK && LA34_0 <= FLOATING_POINT_LITERAL)||(LA34_0 >= INTEGER_LITERAL && LA34_0 <= KW_TRUE)||(LA34_0 >= LEFT_BRACKET && LA34_0 <= LEFT_PARENT)||LA34_0==MINUS_SIGN||LA34_0==PLUS_SIGN||LA34_0==SIMPLE_ID||LA34_0==STRING_LITERAL) ) {
						alt34=1;
					}
					switch (alt34) {
						case 1 :
							// DFSParser.g:305:23: de= df_expression
							{
							pushFollow(FOLLOW_df_expression_in_df_expression_literal3005);
							de=df_expression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_df_expression.add(de.getTree());
							}
							break;

					}

					RIGHT_PARENT101=(Token)match(input,RIGHT_PARENT,FOLLOW_RIGHT_PARENT_in_df_expression_literal3008); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RIGHT_PARENT.add(RIGHT_PARENT101);

					// AST REWRITE
					// elements: de, LEFT_PARENT, RIGHT_PARENT
					// token labels: 
					// rule labels: de, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_de=new RewriteRuleSubtreeStream(adaptor,"rule de",de!=null?de.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 306:9: -> LEFT_PARENT ( $de)? RIGHT_PARENT
					{
						adaptor.addChild(root_0, stream_LEFT_PARENT.nextNode());
						// DFSParser.g:306:25: ( $de)?
						if ( stream_de.hasNext() ) {
							adaptor.addChild(root_0, stream_de.nextTree());
						}
						stream_de.reset();

						adaptor.addChild(root_0, stream_RIGHT_PARENT.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 42, df_expression_literal_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "df_expression_literal"


	public static class df_expression_array_literal_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "df_expression_array_literal"
	// DFSParser.g:309:1: df_expression_array_literal : LEFT_BRACKET (el1= df_expression ( COMMA el2+= df_expression )* )? RIGHT_BRACKET -> ^( AST_ARRAY_LITERAL LEFT_BRACKET ( ^( AST_ARRAY_ELEMENT $el1) ( COMMA ^( AST_ARRAY_ELEMENT $el2) )* )? RIGHT_BRACKET ) ;
	public final DFSParser.df_expression_array_literal_return df_expression_array_literal() throws RecognitionException {
		DFSParser.df_expression_array_literal_return retval = new DFSParser.df_expression_array_literal_return();
		retval.start = input.LT(1);
		int df_expression_array_literal_StartIndex = input.index();

		Object root_0 = null;

		Token LEFT_BRACKET102=null;
		Token COMMA103=null;
		Token RIGHT_BRACKET104=null;
		List<Object> list_el2=null;
		ParserRuleReturnScope el1 =null;
		RuleReturnScope el2 = null;
		Object LEFT_BRACKET102_tree=null;
		Object COMMA103_tree=null;
		Object RIGHT_BRACKET104_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleTokenStream stream_RIGHT_BRACKET=new RewriteRuleTokenStream(adaptor,"token RIGHT_BRACKET");
		RewriteRuleTokenStream stream_LEFT_BRACKET=new RewriteRuleTokenStream(adaptor,"token LEFT_BRACKET");
		RewriteRuleSubtreeStream stream_df_expression=new RewriteRuleSubtreeStream(adaptor,"rule df_expression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 43) ) { return retval; }

			// DFSParser.g:310:5: ( LEFT_BRACKET (el1= df_expression ( COMMA el2+= df_expression )* )? RIGHT_BRACKET -> ^( AST_ARRAY_LITERAL LEFT_BRACKET ( ^( AST_ARRAY_ELEMENT $el1) ( COMMA ^( AST_ARRAY_ELEMENT $el2) )* )? RIGHT_BRACKET ) )
			// DFSParser.g:310:9: LEFT_BRACKET (el1= df_expression ( COMMA el2+= df_expression )* )? RIGHT_BRACKET
			{
			LEFT_BRACKET102=(Token)match(input,LEFT_BRACKET,FOLLOW_LEFT_BRACKET_in_df_expression_array_literal3045); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LEFT_BRACKET.add(LEFT_BRACKET102);

			// DFSParser.g:310:22: (el1= df_expression ( COMMA el2+= df_expression )* )?
			int alt37=2;
			int LA37_0 = input.LA(1);
			if ( (LA37_0==DOLLAR_SIGN||(LA37_0 >= EXCLAMATION_MARK && LA37_0 <= FLOATING_POINT_LITERAL)||(LA37_0 >= INTEGER_LITERAL && LA37_0 <= KW_TRUE)||(LA37_0 >= LEFT_BRACKET && LA37_0 <= LEFT_PARENT)||LA37_0==MINUS_SIGN||LA37_0==PLUS_SIGN||LA37_0==SIMPLE_ID||LA37_0==STRING_LITERAL) ) {
				alt37=1;
			}
			switch (alt37) {
				case 1 :
					// DFSParser.g:310:23: el1= df_expression ( COMMA el2+= df_expression )*
					{
					pushFollow(FOLLOW_df_expression_in_df_expression_array_literal3050);
					el1=df_expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_df_expression.add(el1.getTree());
					// DFSParser.g:310:41: ( COMMA el2+= df_expression )*
					loop36:
					while (true) {
						int alt36=2;
						int LA36_0 = input.LA(1);
						if ( (LA36_0==COMMA) ) {
							alt36=1;
						}

						switch (alt36) {
						case 1 :
							// DFSParser.g:310:42: COMMA el2+= df_expression
							{
							COMMA103=(Token)match(input,COMMA,FOLLOW_COMMA_in_df_expression_array_literal3053); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_COMMA.add(COMMA103);

							pushFollow(FOLLOW_df_expression_in_df_expression_array_literal3057);
							el2=df_expression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_df_expression.add(el2.getTree());
							if (list_el2==null) list_el2=new ArrayList<Object>();
							list_el2.add(el2.getTree());
							}
							break;

						default :
							break loop36;
						}
					}

					}
					break;

			}

			RIGHT_BRACKET104=(Token)match(input,RIGHT_BRACKET,FOLLOW_RIGHT_BRACKET_in_df_expression_array_literal3063); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RIGHT_BRACKET.add(RIGHT_BRACKET104);

			// AST REWRITE
			// elements: LEFT_BRACKET, COMMA, el2, el1, RIGHT_BRACKET
			// token labels: 
			// rule labels: el1, retval
			// token list labels: 
			// rule list labels: el2
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_el1=new RewriteRuleSubtreeStream(adaptor,"rule el1",el1!=null?el1.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_el2=new RewriteRuleSubtreeStream(adaptor,"token el2",list_el2);
			root_0 = (Object)adaptor.nil();
			// 311:9: -> ^( AST_ARRAY_LITERAL LEFT_BRACKET ( ^( AST_ARRAY_ELEMENT $el1) ( COMMA ^( AST_ARRAY_ELEMENT $el2) )* )? RIGHT_BRACKET )
			{
				// DFSParser.g:311:12: ^( AST_ARRAY_LITERAL LEFT_BRACKET ( ^( AST_ARRAY_ELEMENT $el1) ( COMMA ^( AST_ARRAY_ELEMENT $el2) )* )? RIGHT_BRACKET )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_ARRAY_LITERAL, "AST_ARRAY_LITERAL"), root_1);
				adaptor.addChild(root_1, stream_LEFT_BRACKET.nextNode());
				// DFSParser.g:311:45: ( ^( AST_ARRAY_ELEMENT $el1) ( COMMA ^( AST_ARRAY_ELEMENT $el2) )* )?
				if ( stream_COMMA.hasNext()||stream_el2.hasNext()||stream_el1.hasNext() ) {
					// DFSParser.g:311:46: ^( AST_ARRAY_ELEMENT $el1)
					{
					Object root_2 = (Object)adaptor.nil();
					root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_ARRAY_ELEMENT, "AST_ARRAY_ELEMENT"), root_2);
					adaptor.addChild(root_2, stream_el1.nextTree());
					adaptor.addChild(root_1, root_2);
					}

					// DFSParser.g:311:72: ( COMMA ^( AST_ARRAY_ELEMENT $el2) )*
					while ( stream_COMMA.hasNext()||stream_el2.hasNext() ) {
						adaptor.addChild(root_1, stream_COMMA.nextNode());
						// DFSParser.g:311:79: ^( AST_ARRAY_ELEMENT $el2)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_ARRAY_ELEMENT, "AST_ARRAY_ELEMENT"), root_2);
						adaptor.addChild(root_2, stream_el2.nextTree());
						adaptor.addChild(root_1, root_2);
						}

					}
					stream_COMMA.reset();
					stream_el2.reset();

				}
				stream_COMMA.reset();
				stream_el2.reset();
				stream_el1.reset();

				adaptor.addChild(root_1, stream_RIGHT_BRACKET.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 43, df_expression_array_literal_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "df_expression_array_literal"


	public static class df_expression_function_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "df_expression_function"
	// DFSParser.g:315:1: df_expression_function : KW_FLOAT -> ^( AST_EXPRESSION_FUNCTION ) ;
	public final DFSParser.df_expression_function_return df_expression_function() throws RecognitionException {
		DFSParser.df_expression_function_return retval = new DFSParser.df_expression_function_return();
		retval.start = input.LT(1);
		int df_expression_function_StartIndex = input.index();

		Object root_0 = null;

		Token KW_FLOAT105=null;

		Object KW_FLOAT105_tree=null;
		RewriteRuleTokenStream stream_KW_FLOAT=new RewriteRuleTokenStream(adaptor,"token KW_FLOAT");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 44) ) { return retval; }

			// DFSParser.g:316:5: ( KW_FLOAT -> ^( AST_EXPRESSION_FUNCTION ) )
			// DFSParser.g:316:9: KW_FLOAT
			{
			KW_FLOAT105=(Token)match(input,KW_FLOAT,FOLLOW_KW_FLOAT_in_df_expression_function3124); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_KW_FLOAT.add(KW_FLOAT105);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 317:9: -> ^( AST_EXPRESSION_FUNCTION )
			{
				// DFSParser.g:317:12: ^( AST_EXPRESSION_FUNCTION )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(AST_EXPRESSION_FUNCTION, "AST_EXPRESSION_FUNCTION"), root_1);
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
				
			//PJAR: MODIFIKACE OD J. TRAVNICKA				
			if(state.backtracking==0){
				BitSet followSet = computeErrorRecoverySet();
				if(input.LA(1) != EOF && !followSet.member(input.LA(1))) {
			    		RecognitionException re = new NoViableAltException("", 0, 0, input);
			    		reportError(re);

			    		Token errorStart = input.LT(1);

			    		beginResync();
			    		consumeUntil(input, followSet);
			    		endResync();

			    		retval.stop = input.LT(-1);

			    		adaptor.addChild(root_0, (Object)adaptor.errorNode(input, errorStart, retval.stop, re));

			    		retval.tree = (Object)adaptor.rulePostProcessing(root_0);
					adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
				}		
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 44, df_expression_function_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "df_expression_function"

	// Delegated rules
	public DFSParser_DFSNonReserveredKW.all_identifier_return all_identifier() throws RecognitionException { return gDFSNonReserveredKW.all_identifier(); }

	public DFSParser_DFSNonReserveredKW.identifier_return identifier() throws RecognitionException { return gDFSNonReserveredKW.identifier(); }

	public DFSParser_DFSNonReserveredKW.reserved_words_return reserved_words() throws RecognitionException { return gDFSNonReserveredKW.reserved_words(); }

	public DFSParser_DFSNonReserveredKW.non_reserved_words_return non_reserved_words() throws RecognitionException { return gDFSNonReserveredKW.non_reserved_words(); }


	protected DFA29 dfa29 = new DFA29(this);
	static /*final*/ String DFA29_eotS;
	static /*final*/ String DFA29_eofS;
	static /*final*/ String DFA29_minS;
	static /*final*/ String DFA29_maxS;
	static /*final*/ String DFA29_acceptS;
	static /*final*/ String DFA29_specialS;
	static /*final*/ String[] DFA29_transitionS;

	static /*final*/ short[] DFA29_eot;
	static /*final*/ short[] DFA29_eof;
	static /*final*/ char[] DFA29_min;
	static /*final*/ char[] DFA29_max;
	static /*final*/ short[] DFA29_accept;
	static /*final*/ short[] DFA29_special;
	static /*final*/ short[][] DFA29_transition;

	static void auxTrans_29(){

	// PJAR: Modifikovana inicializace - kvuli "code too large" chybe.
	DFA29_eotS = "\16\uffff";
	DFA29_eofS = "\16\uffff";
	DFA29_minS = "\2\121\1\120\1\uffff\3\120\7\uffff";
	DFA29_maxS = "\1\155\1\121\1\176\1\uffff\3\176\7\uffff";
	DFA29_acceptS = "\3\uffff\1\4\3\uffff\1\6\1\7\1\5\1\2\1\1\1\3\1\10";
	DFA29_specialS = "\16\uffff}>";
	DFA29_transitionS = new String[]{
			"\1\1\1\3\1\uffff\1\4\30\uffff\1\2",
			"\1\5",
			"\1\7\1\6\1\uffff\1\7\1\uffff\25\7\1\uffff\2\7\3\uffff\1\7\4\uffff\1"+
			"\7\6\uffff\1\7\1\uffff\1\7",
			"",
			"\1\11\1\10\1\uffff\1\11\1\uffff\25\11\1\uffff\2\11\3\uffff\1\11\4\uffff"+
			"\1\11\6\uffff\1\11\1\uffff\1\11",
			"\1\13\1\12\1\uffff\1\13\1\uffff\25\13\1\uffff\2\13\3\uffff\1\13\4\uffff"+
			"\1\13\6\uffff\1\13\1\uffff\1\13",
			"\1\15\2\uffff\1\15\1\14\25\15\1\uffff\2\15\3\uffff\1\15\4\uffff\1\15"+
			"\6\uffff\1\15\1\uffff\1\15",
			"",
			"",
			"",
			"",
			"",
			"",
			""
	};

	DFA29_eot = DFA.unpackEncodedString(DFA29_eotS);
	DFA29_eof = DFA.unpackEncodedString(DFA29_eofS);
	DFA29_min = DFA.unpackEncodedStringToUnsignedChars(DFA29_minS);
	DFA29_max = DFA.unpackEncodedStringToUnsignedChars(DFA29_maxS);
	DFA29_accept = DFA.unpackEncodedString(DFA29_acceptS);
	DFA29_special = DFA.unpackEncodedString(DFA29_specialS);


		int numStates = DFA29_transitionS.length;
		DFA29_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA29_transition[i] = DFA.unpackEncodedString(DFA29_transitionS[i]);
		}
	}

	static {
	   auxTrans_29();
	};

	protected class DFA29 extends DFA {

		public DFA29(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 29;
			this.eot = DFA29_eot;
			this.eof = DFA29_eof;
			this.min = DFA29_min;
			this.max = DFA29_max;
			this.accept = DFA29_accept;
			this.special = DFA29_special;
			this.transition = DFA29_transition;
		}
		@Override
		public String getDescription() {
			return "236:1: compare_operator : ( EQUALS EQUALS | EQUALS EQUALS EQUALS | LESS_THAN EQUALS GREATER_THAN | EXCLAMATION_MARK EQUALS | GREATER_THAN | LESS_THAN | GREATER_THAN EQUALS | LESS_THAN EQUALS );";
		}
	}

	/* PJAR: rozdeleni deklarace a inicializace statickych promennych. Presunuti inicializace do samostatne metody */
	public static /* final */ BitSet FOLLOW_parameters_in_dfs101;
	public static /* final */ BitSet FOLLOW_transformations_in_dfs106;
	public static /* final */ BitSet FOLLOW_KW_PARAMETERS_in_parameters153;
	public static /* final */ BitSet FOLLOW_LEFT_BRACE_in_parameters155;
	public static /* final */ BitSet FOLLOW_parameter_in_parameters160;
	public static /* final */ BitSet FOLLOW_COMMA_in_parameters163;
	public static /* final */ BitSet FOLLOW_parameter_in_parameters167;
	public static /* final */ BitSet FOLLOW_RIGHT_BRACE_in_parameters173;
	public static /* final */ BitSet FOLLOW_all_identifier_in_parameter229;
	public static /* final */ BitSet FOLLOW_KW_AS_in_parameter231;
	public static /* final */ BitSet FOLLOW_data_type_in_parameter235;
	public static /* final */ BitSet FOLLOW_df_expression_in_parameter239;
	public static /* final */ BitSet FOLLOW_KW_INTEGER_in_data_type310;
	public static /* final */ BitSet FOLLOW_KW_BOOLEAN_in_data_type320;
	public static /* final */ BitSet FOLLOW_KW_DATE_in_data_type330;
	public static /* final */ BitSet FOLLOW_KW_TIMESTAMP_in_data_type340;
	public static /* final */ BitSet FOLLOW_KW_ANY_in_data_type350;
	public static /* final */ BitSet FOLLOW_KW_SHORT_in_data_type360;
	public static /* final */ BitSet FOLLOW_KW_DOUBLE_in_data_type370;
	public static /* final */ BitSet FOLLOW_KW_FLOAT_in_data_type380;
	public static /* final */ BitSet FOLLOW_KW_LONG_in_data_type390;
	public static /* final */ BitSet FOLLOW_DECIMAL_TYPE_in_data_type400;
	public static /* final */ BitSet FOLLOW_KW_STRING_in_data_type410;
	public static /* final */ BitSet FOLLOW_ARRAY_INTEGER_in_data_type420;
	public static /* final */ BitSet FOLLOW_ARRAY_BOOLEAN_in_data_type430;
	public static /* final */ BitSet FOLLOW_ARRAY_DATE_in_data_type440;
	public static /* final */ BitSet FOLLOW_ARRAY_TIMESTAMP_in_data_type450;
	public static /* final */ BitSet FOLLOW_ARRAY_ANY_in_data_type460;
	public static /* final */ BitSet FOLLOW_ARRAY_SHORT_in_data_type470;
	public static /* final */ BitSet FOLLOW_ARRAY_DOUBLE_in_data_type480;
	public static /* final */ BitSet FOLLOW_ARRAY_FLOAT_in_data_type490;
	public static /* final */ BitSet FOLLOW_ARRAY_LONG_in_data_type500;
	public static /* final */ BitSet FOLLOW_ARRAY_DECIMAL_in_data_type510;
	public static /* final */ BitSet FOLLOW_ARRAY_STRING_in_data_type520;
	public static /* final */ BitSet FOLLOW_param_map_data_type_in_data_type530;
	public static /* final */ BitSet FOLLOW_LEFT_PARENT_in_data_type540;
	public static /* final */ BitSet FOLLOW_RIGHT_PARENT_in_data_type542;
	public static /* final */ BitSet FOLLOW_LEFT_BRACKET_in_param_map_data_type561;
	public static /* final */ BitSet FOLLOW_data_type_in_param_map_data_type565;
	public static /* final */ BitSet FOLLOW_COMMA_in_param_map_data_type567;
	public static /* final */ BitSet FOLLOW_data_type_in_param_map_data_type571;
	public static /* final */ BitSet FOLLOW_RIGHT_BRACKET_in_param_map_data_type573;
	public static /* final */ BitSet FOLLOW_transformation_in_transformations629;
	public static /* final */ BitSet FOLLOW_OUTPUT_STREAM_in_transformations631;
	public static /* final */ BitSet FOLLOW_stream_name_in_transformations635;
	public static /* final */ BitSet FOLLOW_stream_name_in_inputStreams690;
	public static /* final */ BitSet FOLLOW_COMMA_in_inputStreams693;
	public static /* final */ BitSet FOLLOW_stream_name_in_inputStreams697;
	public static /* final */ BitSet FOLLOW_all_identifier_in_stream_name745;
	public static /* final */ BitSet FOLLOW_AT_SIGN_in_stream_name748;
	public static /* final */ BitSet FOLLOW_second_stream_name_in_stream_name752;
	public static /* final */ BitSet FOLLOW_all_identifier_in_second_stream_name813;
	public static /* final */ BitSet FOLLOW_LEFT_PARENT_in_second_stream_name844;
	public static /* final */ BitSet FOLLOW_all_identifier_in_second_stream_name848;
	public static /* final */ BitSet FOLLOW_COMMA_in_second_stream_name851;
	public static /* final */ BitSet FOLLOW_all_identifier_in_second_stream_name855;
	public static /* final */ BitSet FOLLOW_RIGHT_PARENT_in_second_stream_name859;
	public static /* final */ BitSet FOLLOW_source_transformation_in_transformation922;
	public static /* final */ BitSet FOLLOW_inputStreams_in_transformation947;
	public static /* final */ BitSet FOLLOW_transformation_with_inputs_in_transformation951;
	public static /* final */ BitSet FOLLOW_KW_SOURCE_in_source_transformation986;
	public static /* final */ BitSet FOLLOW_transformation_attributes_in_source_transformation991;
	public static /* final */ BitSet FOLLOW_filter_transformation_in_transformation_with_inputs1035;
	public static /* final */ BitSet FOLLOW_join_transformation_in_transformation_with_inputs1060;
	public static /* final */ BitSet FOLLOW_sink_transformation_in_transformation_with_inputs1085;
	public static /* final */ BitSet FOLLOW_general_transformation_in_transformation_with_inputs1110;
	public static /* final */ BitSet FOLLOW_KW_FILTER_in_filter_transformation1142;
	public static /* final */ BitSet FOLLOW_LEFT_PARENT_in_filter_transformation1144;
	public static /* final */ BitSet FOLLOW_df_expression_in_filter_transformation1148;
	public static /* final */ BitSet FOLLOW_COMMA_in_filter_transformation1151;
	public static /* final */ BitSet FOLLOW_common_transformation_attributes_in_filter_transformation1155;
	public static /* final */ BitSet FOLLOW_RIGHT_PARENT_in_filter_transformation1159;
	public static /* final */ BitSet FOLLOW_KW_JOIN_in_join_transformation1213;
	public static /* final */ BitSet FOLLOW_LEFT_PARENT_in_join_transformation1215;
	public static /* final */ BitSet FOLLOW_df_expression_in_join_transformation1219;
	public static /* final */ BitSet FOLLOW_COMMA_in_join_transformation1222;
	public static /* final */ BitSet FOLLOW_common_transformation_attributes_in_join_transformation1226;
	public static /* final */ BitSet FOLLOW_RIGHT_PARENT_in_join_transformation1230;
	public static /* final */ BitSet FOLLOW_KW_SINK_in_sink_transformation1284;
	public static /* final */ BitSet FOLLOW_transformation_attributes_in_sink_transformation1288;
	public static /* final */ BitSet FOLLOW_SIMPLE_ID_in_general_transformation1330;
	public static /* final */ BitSet FOLLOW_transformation_attributes_in_general_transformation1334;
	public static /* final */ BitSet FOLLOW_LEFT_PARENT_in_transformation_attributes1376;
	public static /* final */ BitSet FOLLOW_common_transformation_attributes_in_transformation_attributes1380;
	public static /* final */ BitSet FOLLOW_RIGHT_PARENT_in_transformation_attributes1383;
	public static /* final */ BitSet FOLLOW_transformation_attribute_in_common_transformation_attributes1422;
	public static /* final */ BitSet FOLLOW_COMMA_in_common_transformation_attributes1425;
	public static /* final */ BitSet FOLLOW_transformation_attribute_in_common_transformation_attributes1429;
	public static /* final */ BitSet FOLLOW_output_attribute_in_transformation_attribute1481;
	public static /* final */ BitSet FOLLOW_all_identifier_in_transformation_attribute1506;
	public static /* final */ BitSet FOLLOW_COLON_in_transformation_attribute1508;
	public static /* final */ BitSet FOLLOW_df_expression_in_transformation_attribute1512;
	public static /* final */ BitSet FOLLOW_df_expression_in_transformation_attribute1558;
	public static /* final */ BitSet FOLLOW_KW_OUTPUT_in_output_attribute1590;
	public static /* final */ BitSet FOLLOW_LEFT_PARENT_in_output_attribute1592;
	public static /* final */ BitSet FOLLOW_column_definitions_in_output_attribute1596;
	public static /* final */ BitSet FOLLOW_RIGHT_PARENT_in_output_attribute1599;
	public static /* final */ BitSet FOLLOW_column_definition_in_column_definitions1648;
	public static /* final */ BitSet FOLLOW_COMMA_in_column_definitions1651;
	public static /* final */ BitSet FOLLOW_column_definition_in_column_definitions1655;
	public static /* final */ BitSet FOLLOW_column_name_in_column_definition1703;
	public static /* final */ BitSet FOLLOW_KW_AS_in_column_definition1705;
	public static /* final */ BitSet FOLLOW_data_type_in_column_definition1709;
	public static /* final */ BitSet FOLLOW_STRING_LITERAL_in_column_definition1712;
	public static /* final */ BitSet FOLLOW_all_identifier_in_column_name1780;
	public static /* final */ BitSet FOLLOW_LEFT_BRACE_in_column_name1811;
	public static /* final */ BitSet FOLLOW_column_name_inside_braces_in_column_name1815;
	public static /* final */ BitSet FOLLOW_RIGHT_BRACE_in_column_name1818;
	public static /* final */ BitSet FOLLOW_or_expression_in_df_expression1889;
	public static /* final */ BitSet FOLLOW_and_expression_in_or_expression1930;
	public static /* final */ BitSet FOLLOW_BAR_in_or_expression1958;
	public static /* final */ BitSet FOLLOW_BAR_in_or_expression1960;
	public static /* final */ BitSet FOLLOW_and_expression_in_or_expression1964;
	public static /* final */ BitSet FOLLOW_not_expression_in_and_expression2022;
	public static /* final */ BitSet FOLLOW_AMPERSAND_in_and_expression2050;
	public static /* final */ BitSet FOLLOW_AMPERSAND_in_and_expression2052;
	public static /* final */ BitSet FOLLOW_not_expression_in_and_expression2056;
	public static /* final */ BitSet FOLLOW_EXCLAMATION_MARK_in_not_expression2110;
	public static /* final */ BitSet FOLLOW_not_expression_in_not_expression2114;
	public static /* final */ BitSet FOLLOW_compare_expression_in_not_expression2146;
	public static /* final */ BitSet FOLLOW_addition_expression_in_compare_expression2178;
	public static /* final */ BitSet FOLLOW_compare_operator_in_compare_expression2208;
	public static /* final */ BitSet FOLLOW_addition_expression_in_compare_expression2212;
	public static /* final */ BitSet FOLLOW_EQUALS_in_compare_operator2272;
	public static /* final */ BitSet FOLLOW_EQUALS_in_compare_operator2274;
	public static /* final */ BitSet FOLLOW_EQUALS_in_compare_operator2284;
	public static /* final */ BitSet FOLLOW_EQUALS_in_compare_operator2286;
	public static /* final */ BitSet FOLLOW_EQUALS_in_compare_operator2288;
	public static /* final */ BitSet FOLLOW_LESS_THAN_in_compare_operator2298;
	public static /* final */ BitSet FOLLOW_EQUALS_in_compare_operator2300;
	public static /* final */ BitSet FOLLOW_GREATER_THAN_in_compare_operator2302;
	public static /* final */ BitSet FOLLOW_EXCLAMATION_MARK_in_compare_operator2312;
	public static /* final */ BitSet FOLLOW_EQUALS_in_compare_operator2314;
	public static /* final */ BitSet FOLLOW_GREATER_THAN_in_compare_operator2324;
	public static /* final */ BitSet FOLLOW_LESS_THAN_in_compare_operator2334;
	public static /* final */ BitSet FOLLOW_GREATER_THAN_in_compare_operator2344;
	public static /* final */ BitSet FOLLOW_EQUALS_in_compare_operator2346;
	public static /* final */ BitSet FOLLOW_LESS_THAN_in_compare_operator2356;
	public static /* final */ BitSet FOLLOW_EQUALS_in_compare_operator2358;
	public static /* final */ BitSet FOLLOW_mult_expression_in_addition_expression2378;
	public static /* final */ BitSet FOLLOW_addition_operator_in_addition_expression2408;
	public static /* final */ BitSet FOLLOW_mult_expression_in_addition_expression2412;
	public static /* final */ BitSet FOLLOW_pow_expression_in_mult_expression2502;
	public static /* final */ BitSet FOLLOW_mult_operator_in_mult_expression2532;
	public static /* final */ BitSet FOLLOW_pow_expression_in_mult_expression2536;
	public static /* final */ BitSet FOLLOW_unary_expression_in_pow_expression2624;
	public static /* final */ BitSet FOLLOW_CARET_in_pow_expression2652;
	public static /* final */ BitSet FOLLOW_unary_expression_in_pow_expression2656;
	public static /* final */ BitSet FOLLOW_unary_operator_in_unary_expression2709;
	public static /* final */ BitSet FOLLOW_unary_expression_in_unary_expression2713;
	public static /* final */ BitSet FOLLOW_df_expression_literal_in_unary_expression2753;
	public static /* final */ BitSet FOLLOW_STRING_LITERAL_in_df_expression_literal2814;
	public static /* final */ BitSet FOLLOW_INTEGER_LITERAL_in_df_expression_literal2840;
	public static /* final */ BitSet FOLLOW_FLOATING_POINT_LITERAL_in_df_expression_literal2866;
	public static /* final */ BitSet FOLLOW_DOLLAR_SIGN_in_df_expression_literal2892;
	public static /* final */ BitSet FOLLOW_SIMPLE_ID_in_df_expression_literal2894;
	public static /* final */ BitSet FOLLOW_df_expression_array_literal_in_df_expression_literal2924;
	public static /* final */ BitSet FOLLOW_df_expression_function_in_df_expression_literal2949;
	public static /* final */ BitSet FOLLOW_identifier_in_df_expression_literal2974;
	public static /* final */ BitSet FOLLOW_LEFT_PARENT_in_df_expression_literal3001;
	public static /* final */ BitSet FOLLOW_df_expression_in_df_expression_literal3005;
	public static /* final */ BitSet FOLLOW_RIGHT_PARENT_in_df_expression_literal3008;
	public static /* final */ BitSet FOLLOW_LEFT_BRACKET_in_df_expression_array_literal3045;
	public static /* final */ BitSet FOLLOW_df_expression_in_df_expression_array_literal3050;
	public static /* final */ BitSet FOLLOW_COMMA_in_df_expression_array_literal3053;
	public static /* final */ BitSet FOLLOW_df_expression_in_df_expression_array_literal3057;
	public static /* final */ BitSet FOLLOW_RIGHT_BRACKET_in_df_expression_array_literal3063;
	public static /* final */ BitSet FOLLOW_KW_FLOAT_in_df_expression_function3124;

	/** PJAR: Inicializace statickych promennych v samostatne metode kvuli "code too large" chybe (generovany kod je prilis velky pro statickou inicializaci). */ 
	static void initializeFollow1() {
	FOLLOW_parameters_in_dfs101 = new BitSet(new long[]{0x0000000000000002L,0x100003FFFFC00000L});



	FOLLOW_parameter_in_parameters160 = new BitSet(new long[]{0x0000000000000000L,0x0100000000000800L});



	FOLLOW_all_identifier_in_parameter229 = new BitSet(new long[]{0x0000000000000000L,0x0000000000800000L});



	FOLLOW_KW_INTEGER_in_data_type310 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_KW_ANY_in_data_type350 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_KW_LONG_in_data_type390 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_ARRAY_BOOLEAN_in_data_type430 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_ARRAY_SHORT_in_data_type470 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_ARRAY_DECIMAL_in_data_type510 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_RIGHT_PARENT_in_data_type542 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_data_type_in_param_map_data_type571 = new BitSet(new long[]{0x0000000000000000L,0x0200000000000000L});



	FOLLOW_stream_name_in_transformations635 = new BitSet(new long[]{0x0000000000000002L,0x100003FFFFC00000L});



	FOLLOW_all_identifier_in_stream_name745 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000020L});



	FOLLOW_LEFT_PARENT_in_second_stream_name844 = new BitSet(new long[]{0x0000000000000000L,0x100003FFFFC00000L});



	FOLLOW_RIGHT_PARENT_in_second_stream_name859 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_KW_SOURCE_in_source_transformation986 = new BitSet(new long[]{0x0000000000000000L,0x0000100000000000L});



	FOLLOW_sink_transformation_in_transformation_with_inputs1085 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_df_expression_in_filter_transformation1148 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000800L});



	FOLLOW_KW_JOIN_in_join_transformation1213 = new BitSet(new long[]{0x0000000000000000L,0x0000100000000000L});



	FOLLOW_common_transformation_attributes_in_join_transformation1226 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000000L});



	FOLLOW_SIMPLE_ID_in_general_transformation1330 = new BitSet(new long[]{0x0000000000000000L,0x0000100000000000L});



	FOLLOW_RIGHT_PARENT_in_transformation_attributes1383 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_output_attribute_in_transformation_attribute1481 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_df_expression_in_transformation_attribute1558 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_RIGHT_PARENT_in_output_attribute1599 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_column_name_in_column_definition1703 = new BitSet(new long[]{0x0000000000000000L,0x0000000000800000L});



	FOLLOW_all_identifier_in_column_name1780 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_or_expression_in_df_expression1889 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_and_expression_in_or_expression1964 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000080L});



	FOLLOW_not_expression_in_and_expression2056 = new BitSet(new long[]{0x0000000000000012L});



	FOLLOW_addition_expression_in_compare_expression2178 = new BitSet(new long[]{0x0000000000000002L,0x0000200000160000L});



	FOLLOW_EQUALS_in_compare_operator2274 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_LESS_THAN_in_compare_operator2298 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});



	FOLLOW_EQUALS_in_compare_operator2314 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_EQUALS_in_compare_operator2346 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_addition_operator_in_addition_expression2408 = new BitSet(new long[]{0x0000000000000000L,0x50211BFFFFE90000L});



	FOLLOW_pow_expression_in_mult_expression2536 = new BitSet(new long[]{0x0000000000040002L,0x2000000000000000L});



	FOLLOW_unary_operator_in_unary_expression2709 = new BitSet(new long[]{0x0000000000000000L,0x50211BFFFFE90000L});



	FOLLOW_INTEGER_LITERAL_in_df_expression_literal2840 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_df_expression_array_literal_in_df_expression_literal2924 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_df_expression_in_df_expression_literal3005 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000000L});



	FOLLOW_COMMA_in_df_expression_array_literal3053 = new BitSet(new long[]{0x0000000000000000L,0x50211BFFFFED0000L});



	}

	static void initializeFollow2() {
	FOLLOW_transformations_in_dfs106 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_COMMA_in_parameters163 = new BitSet(new long[]{0x0000000000000000L,0x100003FFFFC00000L});



	FOLLOW_KW_AS_in_parameter231 = new BitSet(new long[]{0x000000000003FF80L,0x00001992CB404000L});



	FOLLOW_KW_BOOLEAN_in_data_type320 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_KW_SHORT_in_data_type360 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_DECIMAL_TYPE_in_data_type400 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_ARRAY_DATE_in_data_type440 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_ARRAY_DOUBLE_in_data_type480 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_ARRAY_STRING_in_data_type520 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_LEFT_BRACKET_in_param_map_data_type561 = new BitSet(new long[]{0x000000000003FF80L,0x00001992CB404000L});



	FOLLOW_RIGHT_BRACKET_in_param_map_data_type573 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_stream_name_in_inputStreams690 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000800L});



	FOLLOW_AT_SIGN_in_stream_name748 = new BitSet(new long[]{0x0000000000000000L,0x100013FFFFC00000L});



	FOLLOW_all_identifier_in_second_stream_name848 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000800L});



	FOLLOW_source_transformation_in_transformation922 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_transformation_attributes_in_source_transformation991 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_general_transformation_in_transformation_with_inputs1110 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_COMMA_in_filter_transformation1151 = new BitSet(new long[]{0x0000000000000000L,0x50211BFFFFED0000L});



	FOLLOW_LEFT_PARENT_in_join_transformation1215 = new BitSet(new long[]{0x0000000000000000L,0x50211BFFFFED0000L});



	FOLLOW_RIGHT_PARENT_in_join_transformation1230 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_transformation_attributes_in_general_transformation1334 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_transformation_attribute_in_common_transformation_attributes1422 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000800L});



	FOLLOW_all_identifier_in_transformation_attribute1506 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});



	FOLLOW_KW_OUTPUT_in_output_attribute1590 = new BitSet(new long[]{0x0000000000000000L,0x0000100000000000L});



	FOLLOW_column_definition_in_column_definitions1648 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000800L});



	FOLLOW_KW_AS_in_column_definition1705 = new BitSet(new long[]{0x000000000003FF80L,0x00001992CB404000L});



	FOLLOW_LEFT_BRACE_in_column_name1811 = new BitSet(new long[]{0xFFFFFFFFFFFFFFF0L,0xFFFFFFFFFFFFFFFFL,0x0000000000FFFFFFL});



	FOLLOW_and_expression_in_or_expression1930 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000080L});



	FOLLOW_not_expression_in_and_expression2022 = new BitSet(new long[]{0x0000000000000012L});



	FOLLOW_EXCLAMATION_MARK_in_not_expression2110 = new BitSet(new long[]{0x0000000000000000L,0x50211BFFFFED0000L});



	FOLLOW_compare_operator_in_compare_expression2208 = new BitSet(new long[]{0x0000000000000000L,0x50211BFFFFE90000L});



	FOLLOW_EQUALS_in_compare_operator2284 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});



	FOLLOW_EQUALS_in_compare_operator2300 = new BitSet(new long[]{0x0000000000000000L,0x0000000000100000L});



	FOLLOW_GREATER_THAN_in_compare_operator2324 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_LESS_THAN_in_compare_operator2356 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});



	FOLLOW_mult_expression_in_addition_expression2412 = new BitSet(new long[]{0x0000000000000002L,0x0021000000000000L});



	FOLLOW_unary_expression_in_pow_expression2624 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000200L});



	FOLLOW_unary_expression_in_unary_expression2713 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_FLOATING_POINT_LITERAL_in_df_expression_literal2866 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_df_expression_function_in_df_expression_literal2949 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_RIGHT_PARENT_in_df_expression_literal3008 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_df_expression_in_df_expression_array_literal3057 = new BitSet(new long[]{0x0000000000000000L,0x0200000000000800L});


	}

	static void initializeFollow3() {
	FOLLOW_KW_PARAMETERS_in_parameters153 = new BitSet(new long[]{0x0000000000000000L,0x0000040000000000L});



	FOLLOW_parameter_in_parameters167 = new BitSet(new long[]{0x0000000000000000L,0x0100000000000800L});



	FOLLOW_data_type_in_parameter235 = new BitSet(new long[]{0x0000000000000002L,0x50211BFFFFED0000L});



	FOLLOW_KW_DATE_in_data_type330 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_KW_DOUBLE_in_data_type370 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_KW_STRING_in_data_type410 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_ARRAY_TIMESTAMP_in_data_type450 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_ARRAY_FLOAT_in_data_type490 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_param_map_data_type_in_data_type530 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_data_type_in_param_map_data_type565 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000800L});



	FOLLOW_transformation_in_transformations629 = new BitSet(new long[]{0x0000000000000000L,0x0004000000000000L});



	FOLLOW_COMMA_in_inputStreams693 = new BitSet(new long[]{0x0000000000000000L,0x100003FFFFC00000L});



	FOLLOW_second_stream_name_in_stream_name752 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_COMMA_in_second_stream_name851 = new BitSet(new long[]{0x0000000000000000L,0x100003FFFFC00000L});



	FOLLOW_inputStreams_in_transformation947 = new BitSet(new long[]{0x0000000000000000L,0x1000002120000000L});



	FOLLOW_filter_transformation_in_transformation_with_inputs1035 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_KW_FILTER_in_filter_transformation1142 = new BitSet(new long[]{0x0000000000000000L,0x0000100000000000L});



	FOLLOW_common_transformation_attributes_in_filter_transformation1155 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000000L});



	FOLLOW_df_expression_in_join_transformation1219 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000800L});



	FOLLOW_KW_SINK_in_sink_transformation1284 = new BitSet(new long[]{0x0000000000000000L,0x0000100000000000L});



	FOLLOW_LEFT_PARENT_in_transformation_attributes1376 = new BitSet(new long[]{0x0000000000000000L,0x54211BFFFFED0000L});



	FOLLOW_COMMA_in_common_transformation_attributes1425 = new BitSet(new long[]{0x0000000000000000L,0x50211BFFFFED0000L});



	FOLLOW_COLON_in_transformation_attribute1508 = new BitSet(new long[]{0x0000000000000000L,0x50211BFFFFED0000L});



	FOLLOW_LEFT_PARENT_in_output_attribute1592 = new BitSet(new long[]{0x0000000000000000L,0x140007FFFFC00000L});



	FOLLOW_COMMA_in_column_definitions1651 = new BitSet(new long[]{0x0000000000000000L,0x100007FFFFC00000L});



	FOLLOW_data_type_in_column_definition1709 = new BitSet(new long[]{0x0000000000000002L,0x4000000000000000L});



	FOLLOW_column_name_inside_braces_in_column_name1815 = new BitSet(new long[]{0x0000000000000000L,0x0100000000000000L});



	FOLLOW_BAR_in_or_expression1958 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000080L});



	FOLLOW_AMPERSAND_in_and_expression2050 = new BitSet(new long[]{0x0000000000000010L});



	FOLLOW_not_expression_in_not_expression2114 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_addition_expression_in_compare_expression2212 = new BitSet(new long[]{0x0000000000000002L,0x0000200000160000L});



	FOLLOW_EQUALS_in_compare_operator2286 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});



	FOLLOW_GREATER_THAN_in_compare_operator2302 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_LESS_THAN_in_compare_operator2334 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_EQUALS_in_compare_operator2358 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_pow_expression_in_mult_expression2502 = new BitSet(new long[]{0x0000000000040002L,0x2000000000000000L});



	FOLLOW_CARET_in_pow_expression2652 = new BitSet(new long[]{0x0000000000000000L,0x50211BFFFFE90000L});



	FOLLOW_df_expression_literal_in_unary_expression2753 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_DOLLAR_SIGN_in_df_expression_literal2892 = new BitSet(new long[]{0x0000000000000000L,0x1000000000000000L});



	FOLLOW_identifier_in_df_expression_literal2974 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_LEFT_BRACKET_in_df_expression_array_literal3045 = new BitSet(new long[]{0x0000000000000000L,0x52211BFFFFED0000L});



	FOLLOW_RIGHT_BRACKET_in_df_expression_array_literal3063 = new BitSet(new long[]{0x0000000000000002L});

	}

	static void initializeFollow4() {
	FOLLOW_LEFT_BRACE_in_parameters155 = new BitSet(new long[]{0x0000000000000000L,0x110003FFFFC00000L});



	FOLLOW_RIGHT_BRACE_in_parameters173 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_df_expression_in_parameter239 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_KW_TIMESTAMP_in_data_type340 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_KW_FLOAT_in_data_type380 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_ARRAY_INTEGER_in_data_type420 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_ARRAY_ANY_in_data_type460 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_ARRAY_LONG_in_data_type500 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_LEFT_PARENT_in_data_type540 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000000L});



	FOLLOW_COMMA_in_param_map_data_type567 = new BitSet(new long[]{0x000000000003FF80L,0x00001992CB404000L});



	FOLLOW_OUTPUT_STREAM_in_transformations631 = new BitSet(new long[]{0x0000000000000000L,0x100003FFFFC00000L});



	FOLLOW_stream_name_in_inputStreams697 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000800L});



	FOLLOW_all_identifier_in_second_stream_name813 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_all_identifier_in_second_stream_name855 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000800L});



	FOLLOW_transformation_with_inputs_in_transformation951 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_join_transformation_in_transformation_with_inputs1060 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_LEFT_PARENT_in_filter_transformation1144 = new BitSet(new long[]{0x0000000000000000L,0x50211BFFFFED0000L});



	FOLLOW_RIGHT_PARENT_in_filter_transformation1159 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_COMMA_in_join_transformation1222 = new BitSet(new long[]{0x0000000000000000L,0x50211BFFFFED0000L});



	FOLLOW_transformation_attributes_in_sink_transformation1288 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_common_transformation_attributes_in_transformation_attributes1380 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000000L});



	FOLLOW_transformation_attribute_in_common_transformation_attributes1429 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000800L});



	FOLLOW_df_expression_in_transformation_attribute1512 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_column_definitions_in_output_attribute1596 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000000L});



	FOLLOW_column_definition_in_column_definitions1655 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000800L});



	FOLLOW_STRING_LITERAL_in_column_definition1712 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_RIGHT_BRACE_in_column_name1818 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_BAR_in_or_expression1960 = new BitSet(new long[]{0x0000000000000000L,0x50211BFFFFED0000L});



	FOLLOW_AMPERSAND_in_and_expression2052 = new BitSet(new long[]{0x0000000000000000L,0x50211BFFFFED0000L});



	FOLLOW_compare_expression_in_not_expression2146 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_EQUALS_in_compare_operator2272 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});



	FOLLOW_EQUALS_in_compare_operator2288 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_EXCLAMATION_MARK_in_compare_operator2312 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});



	FOLLOW_GREATER_THAN_in_compare_operator2344 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});



	FOLLOW_mult_expression_in_addition_expression2378 = new BitSet(new long[]{0x0000000000000002L,0x0021000000000000L});



	FOLLOW_mult_operator_in_mult_expression2532 = new BitSet(new long[]{0x0000000000000000L,0x50211BFFFFE90000L});



	FOLLOW_unary_expression_in_pow_expression2656 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000200L});



	FOLLOW_STRING_LITERAL_in_df_expression_literal2814 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_SIMPLE_ID_in_df_expression_literal2894 = new BitSet(new long[]{0x0000000000000002L});



	FOLLOW_LEFT_PARENT_in_df_expression_literal3001 = new BitSet(new long[]{0x0000000000000000L,0x54211BFFFFED0000L});



	FOLLOW_df_expression_in_df_expression_array_literal3050 = new BitSet(new long[]{0x0000000000000000L,0x0200000000000800L});



	FOLLOW_KW_FLOAT_in_df_expression_function3124 = new BitSet(new long[]{0x0000000000000002L});
	}

	/* PJAR Inicializace statickych metod rozdelena do 2 metod kvuli "code too large" chybe. */
	static {
	   initializeFollow1();	
	   initializeFollow2();
	   initializeFollow3();
	   initializeFollow4();		
	}
}
