package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AmazonRedshiftLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for AmazonRedshiftLinkedService resource.
 */
public class AmazonRedshiftLinkedServiceParser extends AbstractLinkedServiceParser<AmazonRedshiftLinkedService> {

    @Override
    protected AmazonRedshiftLinkedService parseTypeSpecificFields(String name,
                                                                  String description,
                                                                  String type,
                                                                  IParameters parameters,
                                                                  JsonNode typePropertiesNode) {
        IAdfFieldValue database = null;
        IAdfFieldValue port = null;
        IAdfFieldValue server = null;
        IAdfFieldValue username = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            database = processAdfFieldValue(typePropertiesNode.path("database"));
            port = processAdfFieldValue(typePropertiesNode.path("port"));
            server = processAdfFieldValue(typePropertiesNode.path("server"));
            username = processAdfFieldValue(typePropertiesNode.path("username"));
        }

        return new AmazonRedshiftLinkedService(name, description, parameters, database, port, server, username);
    }
}
