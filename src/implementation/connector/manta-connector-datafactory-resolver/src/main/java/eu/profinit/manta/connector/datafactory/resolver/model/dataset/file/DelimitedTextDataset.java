package eu.profinit.manta.connector.datafactory.resolver.model.dataset.file;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.dataset.DatasetType;
import eu.profinit.manta.connector.datafactory.model.dataset.file.IDelimitedTextDataset;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.DatasetLocation;

/**
 * Implementation of the Dataset resource of type DelimitedText.
 */
public class DelimitedTextDataset extends FileDataset implements IDelimitedTextDataset {

    public DelimitedTextDataset(String name,
                                IReference linkedServiceName,
                                IParameters parameters,
                                IDatasetSchema datasetSchema,
                                JsonNode datasetStructure,
                                DatasetLocation location) {
        super(name, linkedServiceName, parameters, datasetSchema, datasetStructure, location);
    }

    @Override
    public DatasetType getType() {
        return DatasetType.DELIMITED_TEXT;
    }
}
