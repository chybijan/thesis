package eu.profinit.manta.connector.datafactory.resolver.model.dataset.database;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.model.dataset.DatasetType;
import eu.profinit.manta.connector.datafactory.model.dataset.database.IGoogleBigQueryObjectDataset;

/**
 * Implementation of the Dataset resource of type GoogleBigQueryObject.
 */
public class GoogleBigQueryObjectDataset extends DatabaseDataset implements IGoogleBigQueryObjectDataset {

    public GoogleBigQueryObjectDataset(String name,
                                       IReference linkedServiceName,
                                       IParameters parameters,
                                       IDatasetSchema datasetSchema,
                                       JsonNode datasetStructure,
                                       IAdfFieldValue schema,
                                       IAdfFieldValue table) {
        super(name, linkedServiceName, parameters, datasetSchema, datasetStructure, schema, table);
    }

    @Override
    public DatasetType getType() {
        return DatasetType.GOOGLE_BIGQUERY_OBJECT;
    }
}
