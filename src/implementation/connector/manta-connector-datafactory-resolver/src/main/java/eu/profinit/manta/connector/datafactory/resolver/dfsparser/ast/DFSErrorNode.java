package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.CommonErrorNode;

public class DFSErrorNode extends DFSNode {

    private final CommonErrorNode errorNode;

    public DFSErrorNode(Token errorNodeToken, TokenStream input, Token start, Token stop,
                        RecognitionException e, DFSContextState contextState) {
        super(errorNodeToken, contextState);
        errorNode = new CommonErrorNode(input, start, stop, e);
    }


    @Override
    public boolean isNil() {
        return errorNode.isNil();
    }

    @Override
    public int getType() {
        return errorNode.getType();
    }

    @Override
    public String getText() {
        return errorNode.getText();
    }

    @Override
    public String getPositionLineAndColumn() {
        return formatPositionLineAndColumn(errorNode.start.getLine(), errorNode.start.getCharPositionInLine());
    }

    @Override
    public String toString() {
        return errorNode.toString();
    }

    @Override
    public DFSNode dupNode() {
        return new DFSErrorNode(
                this.getToken(),
                (TokenStream) errorNode.input,
                errorNode.start,
                errorNode.stop,
                errorNode.trappedException,
                getContextState()
        );
    }

}
