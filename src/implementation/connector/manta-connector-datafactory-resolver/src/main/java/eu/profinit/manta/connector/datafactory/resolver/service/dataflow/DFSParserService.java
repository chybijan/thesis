package eu.profinit.manta.connector.datafactory.resolver.service.dataflow;

import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowScript;

/**
 * Parser service encapsulating DFS parsing logic
 */
public interface DFSParserService {

    IDataFlowScript processScript(String script);

}
