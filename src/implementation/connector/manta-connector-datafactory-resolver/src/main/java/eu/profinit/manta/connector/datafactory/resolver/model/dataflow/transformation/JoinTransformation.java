package eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IJoinTransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.ITable;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class JoinTransformation extends AbstractTransformation implements IJoinTransformation {

    private final IExpression joinCondition;

    private final JoinType joinType;

    public JoinTransformation(String name, Set<String> substreams,
                              Collection<IADFStream> inputs, Collection<String> outputs, ITable table,
                              Map<String, IExpression> attributes,
                              IExpression joinCondition, JoinType joinType) {
        super(name, substreams, inputs, outputs, table, attributes);
        this.joinCondition = joinCondition;
        this.joinType = joinType;
    }

    @Override
    public IExpression getJoinCondition() {
        return joinCondition;
    }

    @Override
    public JoinType getJoinType() {
        return joinType;
    }
}
