package eu.profinit.manta.connector.datafactory.resolver.service.common;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

public interface AdfFieldValueParser {

    IAdfFieldValue parseAdfFieldValue(JsonNode adfFieldValueNode);

}
