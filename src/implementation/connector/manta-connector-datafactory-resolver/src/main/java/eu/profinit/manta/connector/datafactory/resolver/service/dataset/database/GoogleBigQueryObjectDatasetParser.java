package eu.profinit.manta.connector.datafactory.resolver.service.dataset.database;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.database.GoogleBigQueryObjectDataset;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for GoogleBigQueryObjectDataset resource.
 */
public class GoogleBigQueryObjectDatasetParser extends DatabaseDatasetParser<GoogleBigQueryObjectDataset> {

    @Override
    protected GoogleBigQueryObjectDataset parseDatabaseDatasetSpecificFields(String name,
                                                                             IReference lsName,
                                                                             IParameters parameters,
                                                                             IDatasetSchema datasetSchema,
                                                                             JsonNode datasetStructureNode,
                                                                             JsonNode typePropertiesNode) {
        IAdfFieldValue dataset = null;
        IAdfFieldValue table = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            dataset = processAdfFieldValue(typePropertiesNode.path("dataset"));
            table = processAdfFieldValue(typePropertiesNode.path("table"));
        }

        return new GoogleBigQueryObjectDataset(name, lsName, parameters, datasetSchema, datasetStructureNode, dataset, table);
    }
}
