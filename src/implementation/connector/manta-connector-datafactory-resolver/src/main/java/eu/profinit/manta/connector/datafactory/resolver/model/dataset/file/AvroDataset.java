package eu.profinit.manta.connector.datafactory.resolver.model.dataset.file;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.dataset.DatasetType;
import eu.profinit.manta.connector.datafactory.model.dataset.file.IAvroDataset;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.DatasetLocation;

/**
 * Implementation of the Dataset resource of type Avro.
 */
public class AvroDataset extends FileDataset implements IAvroDataset {

    public AvroDataset(String name,
                       IReference linkedServiceName,
                       IParameters parameters,
                       IDatasetSchema datasetSchema,
                       JsonNode datasetStructure,
                       DatasetLocation location) {
        super(name, linkedServiceName, parameters, datasetSchema, datasetStructure, location);
    }

    @Override
    public DatasetType getType() {
        return DatasetType.AVRO;
    }
}
