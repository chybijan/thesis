package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstColumnDefinition;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstOutputAttribute;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import org.antlr.runtime.Token;

import java.util.List;

public class AstOutputAttribute extends DFSNode implements IAstOutputAttribute {

    public AstOutputAttribute() {
    }

    public AstOutputAttribute(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstOutputAttribute(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    @Override
    public List<IAstColumnDefinition> findColumns() {
        return selectNodes("AST_COLUMN_DEFINITIONS/AST_COLUMN_DEFINITION");
    }
}
