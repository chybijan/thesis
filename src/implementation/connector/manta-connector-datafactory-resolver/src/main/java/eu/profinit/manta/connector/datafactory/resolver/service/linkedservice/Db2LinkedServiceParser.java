package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.Db2LinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for Db2LinkedService resource.
 */
public class Db2LinkedServiceParser extends AbstractLinkedServiceParser<Db2LinkedService> {

    @Override
    protected Db2LinkedService parseTypeSpecificFields(String name,
                                                       String description,
                                                       String type,
                                                       IParameters parameters,
                                                       JsonNode typePropertiesNode) {
        IAdfFieldValue connectionString = null;
        IAdfFieldValue database = null;
        IAdfFieldValue packageCollection = null;
        IAdfFieldValue server = null;
        IAdfFieldValue username = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            connectionString = processAdfFieldValue(typePropertiesNode.path("connectionString"));
            database = processAdfFieldValue(typePropertiesNode.path("database"));
            packageCollection = processAdfFieldValue(typePropertiesNode.path("packageCollection"));
            server = processAdfFieldValue(typePropertiesNode.path("server"));
            username = processAdfFieldValue(typePropertiesNode.path("username"));
        }

        return new Db2LinkedService(name, description, parameters, connectionString, database, packageCollection, server, username);
    }
}
