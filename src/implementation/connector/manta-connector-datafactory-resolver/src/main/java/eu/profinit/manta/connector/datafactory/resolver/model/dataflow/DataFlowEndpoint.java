package eu.profinit.manta.connector.datafactory.resolver.model.dataflow;

import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.ReferenceType;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowEndpoint;

import java.util.Optional;

public class DataFlowEndpoint implements IDataFlowEndpoint {

    private final String name;

    private final IReference datasetReference;

    private final IReference linkedServiceReference;

    private final IReference schemaLinkedServiceReference;

    public DataFlowEndpoint(String name, IReference datasetReference, IReference linkedServiceReference, IReference schemaLinkedServiceReference) {
        this.name = name;
        this.datasetReference = datasetReference;
        this.linkedServiceReference = linkedServiceReference;
        this.schemaLinkedServiceReference = schemaLinkedServiceReference;
    }


    @Override
    public String getName() {
        return name;
    }

    public Optional<IReference> getDatasetReference() {
        if(datasetReference == null){
            return Optional.empty();
        }
        else{
            return Optional.of(datasetReference);
        }
    }

    public Optional<IReference> getLinkedServiceReference() {
        if(linkedServiceReference == null){
            return Optional.empty();
        }
        else{
            return Optional.of(linkedServiceReference);
        }
    }

    public Optional<IReference> getSchemaLinkedServiceReference() {
        if(schemaLinkedServiceReference == null){
            return Optional.empty();
        }
        else{
            return Optional.of(schemaLinkedServiceReference);
        }
    }

    @Override
    public ReferenceType getType() {

        try{
            IReference ref = getReference();
            return ref.getType();
        }catch (IllegalStateException e){
            return ReferenceType.UNKNOWN;
        }

    }

    @Override
    public IReference getReference() throws IllegalStateException{

        Optional<IReference> reference = getDatasetReference();
        if(reference.isPresent())
            return reference.get();

        reference = getLinkedServiceReference();
        if(reference.isPresent())
            return reference.get();

        reference = getSchemaLinkedServiceReference();
        if(reference.isPresent())
            return reference.get();

        throw new IllegalStateException();
    }
}
