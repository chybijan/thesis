package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.ITable;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstExpression;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstFilterTransformation;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstTransformationAttributes;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.VoidExpression;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.FilterTransformation;
import org.antlr.runtime.Token;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AstFilterTransformation extends AstConcreteTransformation implements IAstFilterTransformation {

    public AstFilterTransformation() {
    }

    public AstFilterTransformation(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstFilterTransformation(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    @Override
    public IAstExpression findFilterCondition() {
        return (IAstExpression) selectSingleNode("AST_EXPRESSION");
    }

    @Override
    protected ITransformation createTransformation(
            String name, Set<String> substreams,
            Set<IADFStream> inputs, Set<String> outputs,
            ITable table) {

        IAstExpression astFilterCondition = findFilterCondition();
        IExpression expression = astFilterCondition != null ? astFilterCondition : new VoidExpression();

        IAstTransformationAttributes astAttributes = findTransformationAttributes();
        Map<String, IExpression> attributes = astAttributes != null ? astAttributes.resolveAttributes() : new HashMap<>();

        return new FilterTransformation(
                name, substreams,  inputs, outputs, table, attributes, expression);
    }

}
