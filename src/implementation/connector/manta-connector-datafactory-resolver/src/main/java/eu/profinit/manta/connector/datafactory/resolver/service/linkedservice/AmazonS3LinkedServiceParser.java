package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.AmazonS3LinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for AmazonS3LinkedService resource.
 */
public class AmazonS3LinkedServiceParser extends AbstractLinkedServiceParser<AmazonS3LinkedService> {

    @Override
    protected AmazonS3LinkedService parseTypeSpecificFields(String name,
                                                            String description,
                                                            String type,
                                                            IParameters parameters,
                                                            JsonNode typePropertiesNode) {
        IAdfFieldValue serviceUrl = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            serviceUrl = processAdfFieldValue(typePropertiesNode.path("serviceUrl"));
        }

        return new AmazonS3LinkedService(name, description, parameters, serviceUrl);
    }
}
