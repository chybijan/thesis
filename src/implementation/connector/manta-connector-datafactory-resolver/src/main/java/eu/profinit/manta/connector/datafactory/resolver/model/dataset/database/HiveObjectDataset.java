package eu.profinit.manta.connector.datafactory.resolver.model.dataset.database;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.dataset.DatasetType;
import eu.profinit.manta.connector.datafactory.model.dataset.database.IHiveObjectDataset;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;

/**
 * Implementation of the Dataset resource of type HiveObject.
 */
public class HiveObjectDataset extends DatabaseDataset implements IHiveObjectDataset {

    public HiveObjectDataset(String name,
                             IReference linkedServiceName,
                             IParameters parameters,
                             IDatasetSchema datasetSchema,
                             JsonNode datasetStructure,
                             IAdfFieldValue schema,
                             IAdfFieldValue table) {
        super(name, linkedServiceName, parameters, datasetSchema, datasetStructure, schema, table);
    }

    @Override
    public DatasetType getType() {
        return DatasetType.HIVE_OBJECT;
    }
}
