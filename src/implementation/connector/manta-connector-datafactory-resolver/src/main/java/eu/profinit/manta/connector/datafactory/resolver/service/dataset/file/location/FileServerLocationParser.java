package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file.location;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.FileServerLocation;

public class FileServerLocationParser extends FileDatasetLocationParser<FileServerLocation> {

    @Override
    public FileServerLocation parseFileDatasetSpecificLocation(IAdfFieldValue fileName, IAdfFieldValue folderPath, JsonNode locationNode) {
        return new FileServerLocation(folderPath, fileName);
    }
}
