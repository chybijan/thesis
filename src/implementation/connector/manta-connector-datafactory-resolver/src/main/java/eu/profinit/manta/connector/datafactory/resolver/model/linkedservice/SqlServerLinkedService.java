package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.ISqlServerLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;

/**
 * Implementation of the SqlServerLinkedService resource interface.
 */
public class SqlServerLinkedService extends LinkedService implements ISqlServerLinkedService {

    /**
     * The connection string.
     */
    private final IAdfFieldValue connectionString;

    public SqlServerLinkedService(String name,
                                  String description,
                                  IParameters parameters,
                                  IAdfFieldValue connectionString) {
        super(name, description, parameters);
        this.connectionString = connectionString;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.SQL_SERVER;
    }

    @Override
    public IAdfFieldValue getConnectionString() {
        return connectionString;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitSqlServerLinkedService(this);
    }
}
