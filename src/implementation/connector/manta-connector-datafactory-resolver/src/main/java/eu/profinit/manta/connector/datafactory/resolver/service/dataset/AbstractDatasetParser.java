package eu.profinit.manta.connector.datafactory.resolver.service.dataset;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;

import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.common.ReferenceType;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.resolver.model.Dataset;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.DatasetSchema;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;
import eu.profinit.manta.connector.datafactory.resolver.service.common.AdfFieldValueParser;
import eu.profinit.manta.connector.datafactory.resolver.service.common.ParametersParser;
import eu.profinit.manta.connector.datafactory.resolver.service.common.ReferenceParser;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Parent implementation of all Dataset resource parsers.
 * <br>
 * Parses the common part of all Dataset resources and delegates the processing of Dataset type-specific fields to subclasses.
 *
 * @param <T> class for Dataset resource, that can be parsed by the particular subclass parser
 */
public abstract class AbstractDatasetParser<T extends Dataset> implements DatasetParser<T> {

    private static final Logger LOGGER = new Logger(AbstractDatasetParser.class);

    /**
     * Service for parsing ADF field value.
     */
    private AdfFieldValueParser adfFieldValueParser;

    /**
     * Service for parsing ADF resource parameters.
     */
    private ParametersParser parametersParser;

    /**
     * Parser for linked service references
     */
    private ReferenceParser referenceParser;

    public void setReferenceParser(ReferenceParser referenceParser) {
        this.referenceParser = referenceParser;
    }

    @Override
    public final T parseDataset(JsonNode datasetNode) {
        T dataset = null;
        if (JsonUtil.isNotNullAndNotMissing(datasetNode)) {

            String name = JsonUtil.pathAsText(datasetNode, "name");

            JsonNode propertiesNode = datasetNode.path("properties");

            String type = JsonUtil.pathAsText(propertiesNode, "type");


            IReference lsName = referenceParser.parseReference(propertiesNode.path("linkedServiceName"), ReferenceType.LINKED_SERVICE);

            if(lsName == null){
                LOGGER.log(Categories.parsingErrors().missingLinkedServiceReferenceOfDataset().datasetName(name));
            }

            IParameters parameters = null;
            if (parametersParser != null) {
                parameters = parametersParser.parseParameters(propertiesNode.path("parameters"));
            }

            dataset = parseDatasetSpecificFields(
                    name,
                    type,
                    lsName,
                    parameters,
                    parseSchema(propertiesNode.path("schema")),
                    propertiesNode.path("structure"),
                    propertiesNode.path("typeProperties")
            );
        }
        return dataset;
    }

    /**
     * Parses fields specific to each dataset type.
     * <br>
     * The fields specific to every dataset type are stored in the "typeProperties" field.
     *
     * @param name                 dataset name
     * @param type                 dataset type
     * @param lsName               linked service reference
     * @param parameters           dataset parameters definition
     * @param datasetSchema        dataset schema
     * @param datasetStructureNode JSON node representing the "structure" field value
     * @param typePropertiesNode   JSON node representing "typeProperties" field value
     * @return instance of a dataset
     */
    protected abstract T parseDatasetSpecificFields(String name,
                                                    String type,
                                                    IReference lsName,
                                                    IParameters parameters,
                                                    IDatasetSchema datasetSchema,
                                                    JsonNode datasetStructureNode,
                                                    JsonNode typePropertiesNode);


    /**
     * Processes the value of Azure Data Factory resource field.
     *
     * @param adfFieldValueNode JSON node representing the value of the field
     * @return processed field's value
     */
    protected IAdfFieldValue processAdfFieldValue(JsonNode adfFieldValueNode) {
        IAdfFieldValue adfFieldValue = null;
        if (JsonUtil.isNotNullAndNotMissing(adfFieldValueNode)) {
            adfFieldValue = adfFieldValueParser.parseAdfFieldValue(adfFieldValueNode);
        }
        return adfFieldValue;
    }


    /**
     * Parses "schema" element of the Dataset resource.
     * <br>
     * <ul>
     *     <li>
     *          If the provided node is an array -> then the dataset schema is represented as an array of elements.
     *     </li>
     *     <li>
     *          If the provided node is an object ->
     *          then the dataset schema is represented as expression with resultType array.
     *     </li>
     *     <li>
     *         Otherwise - the structure is not supported by Manta and error is logged. "Empty" object is returned.
     *     </li>
     * </ul>
     *
     * @param schemaNode JSON node representing value of the "schema" field to parse
     * @return representation of dataset schema or null if the provided node is missing
     */
    private IDatasetSchema parseSchema(JsonNode schemaNode) {
        IDatasetSchema schema = null;
        List<IDatasetSchema.IDataElement> dataElements = new ArrayList<>();
        IAdfFieldValue value = null;
        if (JsonUtil.isNotNullAndNotMissing(schemaNode)) {
            if (schemaNode.isArray()) {

                for (JsonNode elemNode : schemaNode) {
                    IAdfFieldValue name = processAdfFieldValue(elemNode.path("name"));
                    IAdfFieldValue type = processAdfFieldValue(elemNode.path("type"));
                    dataElements.add(new DatasetSchema.DataElement(name, type));
                }
            } else if (schemaNode.isObject()) {
                value = processAdfFieldValue(schemaNode.path("value"));
            } else {
                LOGGER.log(Categories.parsingErrors()
                        .unexpectedElementStructure()
                        .value(schemaNode.asText())
                );
            }
            schema = new DatasetSchema(dataElements, value);
        }
        return schema;
    }

    public void setAdfFieldValueParser(AdfFieldValueParser adfFieldValueParser) {
        this.adfFieldValueParser = adfFieldValueParser;
    }

    public void setParametersParser(ParametersParser parametersParser) {
        this.parametersParser = parametersParser;
    }

}
