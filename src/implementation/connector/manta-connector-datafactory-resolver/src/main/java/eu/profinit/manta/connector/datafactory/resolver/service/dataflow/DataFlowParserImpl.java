package eu.profinit.manta.connector.datafactory.resolver.service.dataflow;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.IDataFlow;
import eu.profinit.manta.connector.datafactory.model.dataflow.DataFlowType;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowProperties;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.resolver.model.DataFlow;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;
import eu.profinit.manta.platform.logging.api.logging.Logger;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * Implementation of parsing logic for data flow resource.
 */
public class DataFlowParserImpl implements DataFlowParser {

    private static final Logger LOGGER = new Logger(DataFlowParserImpl.class);

    private static final String NAME_PATH = "name";
    private static final String PROPERTIES_PATH = "/properties";

    private static final String TYPE_PATH = "type";
    private static final String TYPE_PROPERTIES_PATH = "/typeProperties";

    protected static final String UNKNOWN_DATA_FLOW_NAME = "UNKNOWN_DATA_FLOW";

    /**
     * Map of properties parsers.
     */
    private final Map<DataFlowType, DataFlowPropertiesParser> dataFlowPropertiesParserMap;

    /**
     * Default data flow type which is used when unknown data flow type is encountered
     */
    private static final DataFlowType defaultType = DataFlowType.MAPPING_DATA_FLOW;

    /**
     * @param dataFlowPropertiesParserMap map of data flow properties parsers for different types of data flows (mapping, wrangling)
     */
    public DataFlowParserImpl(Map<DataFlowType, DataFlowPropertiesParser> dataFlowPropertiesParserMap) {
        this.dataFlowPropertiesParserMap = dataFlowPropertiesParserMap;
    }

    @Override
    public IDataFlow parseDataFlow(JsonNode dataFlowNode) {
        IDataFlow dataFlow = null;

        if (JsonUtil.isNotNullAndNotMissing(dataFlowNode)) {

            // Load name
            String name = JsonUtil.pathAsText(dataFlowNode, NAME_PATH);
            if(StringUtils.isBlank(name)){
                LOGGER.log(Categories.parsingErrors().missingDataFlowName().jsonString(dataFlowNode.asText()));
                name = UNKNOWN_DATA_FLOW_NAME;
            }

            // Load properties
            IDataFlowProperties properties = parsePropertiesNode(dataFlowNode.at(PROPERTIES_PATH));
            if(properties == null){
                LOGGER.log(Categories.parsingErrors().unknownDataFlowProperties().resourceName(name));
                return null;
            }

            // Create data flow
            dataFlow = new DataFlow(
                    name,
                    properties
            );
        }

        return dataFlow;
    }

    /**
     * Parse properties node
     *
     * @param propertiesNode Properties node with type and type properties
     * @return Dataflow properties,
     *         or null if propertiesNode is null, missing or data flow type is unssuported
     */
    private IDataFlowProperties parsePropertiesNode(JsonNode propertiesNode){

        IDataFlowProperties properties = null;

        if (JsonUtil.isNotNullAndNotMissing(propertiesNode)) {

            // Load type
            String strType = JsonUtil.pathAsText(propertiesNode, TYPE_PATH);

            DataFlowType type;

            try {

                // Load type
                type = DataFlowType.valueOfCI(strType);

            }catch (IllegalArgumentException e){

                LOGGER.log(Categories.parsingErrors().unknownDataFlowType().typeName(strType).defaultType(defaultType));
                type = defaultType;
            }

            // Find right parser
            DataFlowPropertiesParser propertiesParser = dataFlowPropertiesParserMap.get(type);

            if(propertiesParser == null){
                LOGGER.log(Categories.parsingErrors().unsupportedDataFlowType().type(type.toString()));
                return null;
            }

            // Parse properties
            properties = propertiesParser.parseProperties(propertiesNode.at(TYPE_PROPERTIES_PATH));
        }

        return properties;
    }
}
