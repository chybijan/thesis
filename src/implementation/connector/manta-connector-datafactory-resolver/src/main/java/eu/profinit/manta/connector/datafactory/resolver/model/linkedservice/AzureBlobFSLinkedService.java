package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.linkedservice.IAzureBlobFSLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;

/**
 * Implementation of the AzureBlobFSLinkedService resource interface.
 */
public class AzureBlobFSLinkedService extends LinkedService implements IAzureBlobFSLinkedService {

    /**
     * Endpoint for the Azure Data Lake Storage Gen2 service.
     */
    private final IAdfFieldValue url;

    public AzureBlobFSLinkedService(String name,
                                    String description,
                                    IParameters parameters,
                                    IAdfFieldValue url) {
        super(name, description, parameters);
        this.url = url;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.AZURE_BLOB_FS;
    }

    @Override
    public IAdfFieldValue getUrl() {
        return url;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitAzureBlobFS(this);
    }
}
