package eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.IHttpServerLocation;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.LocationType;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.visitor.ILocationVisitor;

/**
 * Implementation of the DatasetLocation resource of type HttpServer.
 */
public class HttpServerLocation extends DatasetLocation implements IHttpServerLocation {

    /**
     * A relative URL to the resource that contains the data.
     */
    private final IAdfFieldValue relativeUrl;

    /**
     * @param relativeUrl a relative URL to the resource that contains the data
     */
    public HttpServerLocation(IAdfFieldValue relativeUrl) {
        this.relativeUrl = relativeUrl;
    }

    @Override
    public LocationType getLocationType() {
        return LocationType.HTTP_SERVER_LOCATION;
    }

    @Override
    public IAdfFieldValue getRelativeUrl() {
        return relativeUrl;
    }

    @Override
    public <T> T accept(ILocationVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
