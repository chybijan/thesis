package eu.profinit.manta.connector.datafactory.resolver.model.dataset;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.dataset.DatasetType;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.model.dataset.IUnknownDataset;
import eu.profinit.manta.connector.datafactory.model.visitor.IDatasetVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.Dataset;

/**
 * Implementation of unrecognizable or unsupported dataset type by Manta.
 */
public class UnknownDataset extends Dataset implements IUnknownDataset {

    public UnknownDataset(String name,
                          IReference linkedServiceName,
                          IParameters parameters,
                          IDatasetSchema datasetSchema,
                          JsonNode datasetStructure) {
        super(name, linkedServiceName, parameters, datasetSchema, datasetStructure);
    }

    @Override
    public DatasetType getType() {
        return DatasetType.UNKNOWN;
    }

    @Override
    public <T> T accept(IDatasetVisitor<T> visitor) {
        return visitor.visitUnknownDataset(this);
    }
}
