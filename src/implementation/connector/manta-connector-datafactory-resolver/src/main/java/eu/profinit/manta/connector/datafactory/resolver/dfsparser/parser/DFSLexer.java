// $ANTLR 3.5-rc-2 DFSLexer.g 2022-04-24 18:44:26

	package eu.profinit.manta.connector.datafactory.resolver.dfsparser.parser;

    import eu.profinit.manta.ast.parser.base.MantaAbstractLexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.function.Function;

@SuppressWarnings("all")
public class DFSLexer extends MantaAbstractLexer {
	public static final int EOF=-1;
	public static final int AMPERSAND=4;
	public static final int ANY_CHAR=5;
	public static final int APOSTROPHE=6;
	public static final int ARRAY_ANY=7;
	public static final int ARRAY_BOOLEAN=8;
	public static final int ARRAY_DATE=9;
	public static final int ARRAY_DECIMAL=10;
	public static final int ARRAY_DOUBLE=11;
	public static final int ARRAY_FLOAT=12;
	public static final int ARRAY_INTEGER=13;
	public static final int ARRAY_LONG=14;
	public static final int ARRAY_SHORT=15;
	public static final int ARRAY_STRING=16;
	public static final int ARRAY_TIMESTAMP=17;
	public static final int ASTERISK=18;
	public static final int AST_ADDITION_EXPRESSION=19;
	public static final int AST_ADDITION_OPERATOR=20;
	public static final int AST_AND_EXPRESSION=21;
	public static final int AST_ARRAY_ELEMENT=22;
	public static final int AST_ARRAY_LITERAL=23;
	public static final int AST_ATTRIBUTE_NAME=24;
	public static final int AST_COLUMN_DEFINITION=25;
	public static final int AST_COLUMN_DEFINITIONS=26;
	public static final int AST_COLUMN_NAME=27;
	public static final int AST_COLUMN_REFERENCE=28;
	public static final int AST_COMPARE_EXPRESSION=29;
	public static final int AST_COMPARE_OPERATOR=30;
	public static final int AST_CONCRETE_TRANSFORMATION=31;
	public static final int AST_DATA_TYPE=32;
	public static final int AST_DATA_TYPE_FORMAT=33;
	public static final int AST_DFS=34;
	public static final int AST_ERROR=35;
	public static final int AST_EXPRESSION=36;
	public static final int AST_EXPRESSION_FUNCTION=37;
	public static final int AST_EXPRESSION_LITERAL=38;
	public static final int AST_FLOATING_POINT_LITERAL=39;
	public static final int AST_INPUT_STREAMS=40;
	public static final int AST_INTEGER_LITERAL=41;
	public static final int AST_LEFT_OPERAND=42;
	public static final int AST_MAP_DATA_TYPE=43;
	public static final int AST_MAP_KEY_TYPE=44;
	public static final int AST_MAP_VALUE_TYPE=45;
	public static final int AST_MULT_EXPRESSION=46;
	public static final int AST_MULT_OPERATOR=47;
	public static final int AST_NOT_EXPRESSION=48;
	public static final int AST_OPERAND=49;
	public static final int AST_OR_EXPRESSION=50;
	public static final int AST_OUTPUT_ATTRIBUTE=51;
	public static final int AST_PARAMETER=52;
	public static final int AST_PARAMETERS=53;
	public static final int AST_PARAMETER_DEFAULT_VALUE=54;
	public static final int AST_PARAMETER_INVOCATION=55;
	public static final int AST_PARAMETER_NAME=56;
	public static final int AST_POW_EXPRESSION=57;
	public static final int AST_RIGHT_OPERAND=58;
	public static final int AST_STREAM_NAME=59;
	public static final int AST_STRING_LITERAL=60;
	public static final int AST_SUBSTREAM_NAME=61;
	public static final int AST_TRANSFORMATION=62;
	public static final int AST_TRANSFORMATIONS=63;
	public static final int AST_TRANSFORMATION_ATTRIBUTE=64;
	public static final int AST_TRANSFORMATION_ATTRIBUTES=65;
	public static final int AST_TRANSFORMATION_NAME=66;
	public static final int AST_UNARY_EXPRESSION=67;
	public static final int AST_UNARY_OPERATOR=68;
	public static final int AT_SIGN=69;
	public static final int BACK_SLASH=70;
	public static final int BAR=71;
	public static final int BLANK=72;
	public static final int CARET=73;
	public static final int COLON=74;
	public static final int COMMA=75;
	public static final int CR=76;
	public static final int CROSSHATCH=77;
	public static final int DECIMAL_TYPE=78;
	public static final int DIGIT=79;
	public static final int DOLLAR_SIGN=80;
	public static final int EQUALS=81;
	public static final int EXCLAMATION_MARK=82;
	public static final int FLOATING_POINT_LITERAL=83;
	public static final int GREATER_THAN=84;
	public static final int INTEGER_LITERAL=85;
	public static final int KW_ANY=86;
	public static final int KW_AS=87;
	public static final int KW_BOOLEAN=88;
	public static final int KW_DATE=89;
	public static final int KW_DECIMAL=90;
	public static final int KW_DOUBLE=91;
	public static final int KW_FALSE=92;
	public static final int KW_FILTER=93;
	public static final int KW_FLOAT=94;
	public static final int KW_INTEGER=95;
	public static final int KW_JOIN=96;
	public static final int KW_LONG=97;
	public static final int KW_OUTPUT=98;
	public static final int KW_PARAMETERS=99;
	public static final int KW_SHORT=100;
	public static final int KW_SINK=101;
	public static final int KW_SOURCE=102;
	public static final int KW_STRING=103;
	public static final int KW_TIMESTAMP=104;
	public static final int KW_TRUE=105;
	public static final int LEFT_BRACE=106;
	public static final int LEFT_BRACKET=107;
	public static final int LEFT_PARENT=108;
	public static final int LESS_THAN=109;
	public static final int LETTER=110;
	public static final int LF=111;
	public static final int MINUS_SIGN=112;
	public static final int NEWLINE=113;
	public static final int OUTPUT_STREAM=114;
	public static final int PERCENT_SIGN=115;
	public static final int PERIOD=116;
	public static final int PLUS_SIGN=117;
	public static final int QUESTION_MARK=118;
	public static final int QUOTATION_MARK=119;
	public static final int RIGHT_BRACE=120;
	public static final int RIGHT_BRACKET=121;
	public static final int RIGHT_PARENT=122;
	public static final int SEMICOLON=123;
	public static final int SIMPLE_ID=124;
	public static final int SLASH=125;
	public static final int STRING_LITERAL=126;
	public static final int TAB=127;
	public static final int UNDERSCORE=128;
	public static final int UNICODE_BOM=129;
	public static final int WHITESPACE=130;

	// delegates
	// delegators
	public MantaAbstractLexer[] getDelegates() {
		return new MantaAbstractLexer[] {};
	}

	public DFSLexer() {} 
	public DFSLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public DFSLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "DFSLexer.g"; }

	// $ANTLR start "KW_PARAMETERS"
	public final void mKW_PARAMETERS() throws RecognitionException {
		try {
			int _type = KW_PARAMETERS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:89:15: ( 'PARAMETERS' )
			// DFSLexer.g:89:17: 'PARAMETERS'
			{
			match("PARAMETERS"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_PARAMETERS"

	// $ANTLR start "KW_AS"
	public final void mKW_AS() throws RecognitionException {
		try {
			int _type = KW_AS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:90:7: ( 'AS' )
			// DFSLexer.g:90:17: 'AS'
			{
			match("AS"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_AS"

	// $ANTLR start "KW_INTEGER"
	public final void mKW_INTEGER() throws RecognitionException {
		try {
			int _type = KW_INTEGER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:92:12: ( 'INTEGER' )
			// DFSLexer.g:92:17: 'INTEGER'
			{
			match("INTEGER"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_INTEGER"

	// $ANTLR start "KW_BOOLEAN"
	public final void mKW_BOOLEAN() throws RecognitionException {
		try {
			int _type = KW_BOOLEAN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:93:12: ( 'BOOLEAN' )
			// DFSLexer.g:93:17: 'BOOLEAN'
			{
			match("BOOLEAN"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_BOOLEAN"

	// $ANTLR start "KW_DATE"
	public final void mKW_DATE() throws RecognitionException {
		try {
			int _type = KW_DATE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:94:9: ( 'DATE' )
			// DFSLexer.g:94:17: 'DATE'
			{
			match("DATE"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_DATE"

	// $ANTLR start "KW_TIMESTAMP"
	public final void mKW_TIMESTAMP() throws RecognitionException {
		try {
			int _type = KW_TIMESTAMP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:95:14: ( 'TIMESTAMP' )
			// DFSLexer.g:95:17: 'TIMESTAMP'
			{
			match("TIMESTAMP"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_TIMESTAMP"

	// $ANTLR start "KW_ANY"
	public final void mKW_ANY() throws RecognitionException {
		try {
			int _type = KW_ANY;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:96:8: ( 'ANY' )
			// DFSLexer.g:96:17: 'ANY'
			{
			match("ANY"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_ANY"

	// $ANTLR start "KW_SHORT"
	public final void mKW_SHORT() throws RecognitionException {
		try {
			int _type = KW_SHORT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:97:10: ( 'SHORT' )
			// DFSLexer.g:97:17: 'SHORT'
			{
			match("SHORT"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_SHORT"

	// $ANTLR start "KW_DOUBLE"
	public final void mKW_DOUBLE() throws RecognitionException {
		try {
			int _type = KW_DOUBLE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:98:11: ( 'DOUBLE' )
			// DFSLexer.g:98:17: 'DOUBLE'
			{
			match("DOUBLE"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_DOUBLE"

	// $ANTLR start "KW_FLOAT"
	public final void mKW_FLOAT() throws RecognitionException {
		try {
			int _type = KW_FLOAT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:99:10: ( 'FLOAT' )
			// DFSLexer.g:99:17: 'FLOAT'
			{
			match("FLOAT"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_FLOAT"

	// $ANTLR start "KW_LONG"
	public final void mKW_LONG() throws RecognitionException {
		try {
			int _type = KW_LONG;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:100:9: ( 'LONG' )
			// DFSLexer.g:100:17: 'LONG'
			{
			match("LONG"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_LONG"

	// $ANTLR start "KW_DECIMAL"
	public final void mKW_DECIMAL() throws RecognitionException {
		try {
			int _type = KW_DECIMAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:101:12: ( 'DECIMAL' )
			// DFSLexer.g:101:17: 'DECIMAL'
			{
			match("DECIMAL"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_DECIMAL"

	// $ANTLR start "KW_STRING"
	public final void mKW_STRING() throws RecognitionException {
		try {
			int _type = KW_STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:102:11: ( 'STRING' )
			// DFSLexer.g:102:17: 'STRING'
			{
			match("STRING"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_STRING"

	// $ANTLR start "KW_TRUE"
	public final void mKW_TRUE() throws RecognitionException {
		try {
			int _type = KW_TRUE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:104:9: ( 'TRUE' )
			// DFSLexer.g:104:11: 'TRUE'
			{
			match("TRUE"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_TRUE"

	// $ANTLR start "KW_FALSE"
	public final void mKW_FALSE() throws RecognitionException {
		try {
			int _type = KW_FALSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:105:10: ( 'FALSE' )
			// DFSLexer.g:105:12: 'FALSE'
			{
			match("FALSE"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_FALSE"

	// $ANTLR start "KW_SOURCE"
	public final void mKW_SOURCE() throws RecognitionException {
		try {
			int _type = KW_SOURCE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:108:11: ( 'SOURCE' )
			// DFSLexer.g:108:18: 'SOURCE'
			{
			match("SOURCE"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_SOURCE"

	// $ANTLR start "KW_OUTPUT"
	public final void mKW_OUTPUT() throws RecognitionException {
		try {
			int _type = KW_OUTPUT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:109:11: ( 'OUTPUT' )
			// DFSLexer.g:109:18: 'OUTPUT'
			{
			match("OUTPUT"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_OUTPUT"

	// $ANTLR start "KW_SINK"
	public final void mKW_SINK() throws RecognitionException {
		try {
			int _type = KW_SINK;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:112:11: ( 'SINK' )
			// DFSLexer.g:112:18: 'SINK'
			{
			match("SINK"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_SINK"

	// $ANTLR start "KW_FILTER"
	public final void mKW_FILTER() throws RecognitionException {
		try {
			int _type = KW_FILTER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:115:11: ( 'FILTER' )
			// DFSLexer.g:115:17: 'FILTER'
			{
			match("FILTER"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_FILTER"

	// $ANTLR start "KW_JOIN"
	public final void mKW_JOIN() throws RecognitionException {
		try {
			int _type = KW_JOIN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:118:11: ( 'JOIN' )
			// DFSLexer.g:118:17: 'JOIN'
			{
			match("JOIN"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KW_JOIN"

	// $ANTLR start "DIGIT"
	public final void mDIGIT() throws RecognitionException {
		try {
			// DFSLexer.g:122:16: ( '0' .. '9' )
			// DFSLexer.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIGIT"

	// $ANTLR start "LETTER"
	public final void mLETTER() throws RecognitionException {
		try {
			// DFSLexer.g:123:17: ( 'a' .. 'z' | 'A' .. 'Z' )
			// DFSLexer.g:
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LETTER"

	// $ANTLR start "CR"
	public final void mCR() throws RecognitionException {
		try {
			// DFSLexer.g:125:13: ( '\\r' )
			// DFSLexer.g:125:18: '\\r'
			{
			match('\r'); 
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CR"

	// $ANTLR start "LF"
	public final void mLF() throws RecognitionException {
		try {
			// DFSLexer.g:126:13: ( '\\n' )
			// DFSLexer.g:126:18: '\\n'
			{
			match('\n'); 
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LF"

	// $ANTLR start "TAB"
	public final void mTAB() throws RecognitionException {
		try {
			// DFSLexer.g:127:14: ( '\\t' )
			// DFSLexer.g:127:19: '\\t'
			{
			match('\t'); 
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TAB"

	// $ANTLR start "BLANK"
	public final void mBLANK() throws RecognitionException {
		try {
			// DFSLexer.g:129:16: ( ' ' | TAB | '\\u00A0' )
			// DFSLexer.g:
			{
			if ( input.LA(1)=='\t'||input.LA(1)==' '||input.LA(1)=='\u00A0' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BLANK"

	// $ANTLR start "ANY_CHAR"
	public final void mANY_CHAR() throws RecognitionException {
		try {
			// DFSLexer.g:131:19: (~ 'a' | 'a' )
			// DFSLexer.g:
			{
			if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\uFFFF') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ANY_CHAR"

	// $ANTLR start "STRING_LITERAL"
	public final void mSTRING_LITERAL() throws RecognitionException {
		try {
			int _type = STRING_LITERAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:133:16: ( APOSTROPHE (~ ( APOSTROPHE | BACK_SLASH ) | ( APOSTROPHE APOSTROPHE ) | ( BACK_SLASH ANY_CHAR ) )* APOSTROPHE | QUOTATION_MARK (~ ( QUOTATION_MARK | BACK_SLASH ) | ( QUOTATION_MARK QUOTATION_MARK ) | ( BACK_SLASH ANY_CHAR ) )* QUOTATION_MARK )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0=='\'') ) {
				alt3=1;
			}
			else if ( (LA3_0=='\"') ) {
				alt3=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// DFSLexer.g:133:20: APOSTROPHE (~ ( APOSTROPHE | BACK_SLASH ) | ( APOSTROPHE APOSTROPHE ) | ( BACK_SLASH ANY_CHAR ) )* APOSTROPHE
					{
					mAPOSTROPHE(); 

					// DFSLexer.g:133:31: (~ ( APOSTROPHE | BACK_SLASH ) | ( APOSTROPHE APOSTROPHE ) | ( BACK_SLASH ANY_CHAR ) )*
					loop1:
					while (true) {
						int alt1=4;
						int LA1_0 = input.LA(1);
						if ( (LA1_0=='\'') ) {
							int LA1_1 = input.LA(2);
							if ( (LA1_1=='\'') ) {
								alt1=2;
							}

						}
						else if ( ((LA1_0 >= '\u0000' && LA1_0 <= '&')||(LA1_0 >= '(' && LA1_0 <= '[')||(LA1_0 >= ']' && LA1_0 <= '\uFFFF')) ) {
							alt1=1;
						}
						else if ( (LA1_0=='\\') ) {
							alt1=3;
						}

						switch (alt1) {
						case 1 :
							// DFSLexer.g:133:32: ~ ( APOSTROPHE | BACK_SLASH )
							{
							if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '&')||(input.LA(1) >= '(' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;
						case 2 :
							// DFSLexer.g:133:61: ( APOSTROPHE APOSTROPHE )
							{
							// DFSLexer.g:133:61: ( APOSTROPHE APOSTROPHE )
							// DFSLexer.g:133:62: APOSTROPHE APOSTROPHE
							{
							mAPOSTROPHE(); 

							mAPOSTROPHE(); 

							}

							}
							break;
						case 3 :
							// DFSLexer.g:133:87: ( BACK_SLASH ANY_CHAR )
							{
							// DFSLexer.g:133:87: ( BACK_SLASH ANY_CHAR )
							// DFSLexer.g:133:88: BACK_SLASH ANY_CHAR
							{
							mBACK_SLASH(); 

							mANY_CHAR(); 

							}

							}
							break;

						default :
							break loop1;
						}
					}

					mAPOSTROPHE(); 

					}
					break;
				case 2 :
					// DFSLexer.g:134:29: QUOTATION_MARK (~ ( QUOTATION_MARK | BACK_SLASH ) | ( QUOTATION_MARK QUOTATION_MARK ) | ( BACK_SLASH ANY_CHAR ) )* QUOTATION_MARK
					{
					mQUOTATION_MARK(); 

					// DFSLexer.g:134:44: (~ ( QUOTATION_MARK | BACK_SLASH ) | ( QUOTATION_MARK QUOTATION_MARK ) | ( BACK_SLASH ANY_CHAR ) )*
					loop2:
					while (true) {
						int alt2=4;
						int LA2_0 = input.LA(1);
						if ( (LA2_0=='\"') ) {
							int LA2_1 = input.LA(2);
							if ( (LA2_1=='\"') ) {
								alt2=2;
							}

						}
						else if ( ((LA2_0 >= '\u0000' && LA2_0 <= '!')||(LA2_0 >= '#' && LA2_0 <= '[')||(LA2_0 >= ']' && LA2_0 <= '\uFFFF')) ) {
							alt2=1;
						}
						else if ( (LA2_0=='\\') ) {
							alt2=3;
						}

						switch (alt2) {
						case 1 :
							// DFSLexer.g:134:45: ~ ( QUOTATION_MARK | BACK_SLASH )
							{
							if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;
						case 2 :
							// DFSLexer.g:134:78: ( QUOTATION_MARK QUOTATION_MARK )
							{
							// DFSLexer.g:134:78: ( QUOTATION_MARK QUOTATION_MARK )
							// DFSLexer.g:134:79: QUOTATION_MARK QUOTATION_MARK
							{
							mQUOTATION_MARK(); 

							mQUOTATION_MARK(); 

							}

							}
							break;
						case 3 :
							// DFSLexer.g:134:112: ( BACK_SLASH ANY_CHAR )
							{
							// DFSLexer.g:134:112: ( BACK_SLASH ANY_CHAR )
							// DFSLexer.g:134:113: BACK_SLASH ANY_CHAR
							{
							mBACK_SLASH(); 

							mANY_CHAR(); 

							}

							}
							break;

						default :
							break loop2;
						}
					}

					mQUOTATION_MARK(); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STRING_LITERAL"

	// $ANTLR start "NEWLINE"
	public final void mNEWLINE() throws RecognitionException {
		try {
			int _type = NEWLINE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:138:2: ( ( CR LF | CR | LF ) )
			// DFSLexer.g:138:4: ( CR LF | CR | LF )
			{
			// DFSLexer.g:138:4: ( CR LF | CR | LF )
			int alt4=3;
			int LA4_0 = input.LA(1);
			if ( (LA4_0=='\r') ) {
				int LA4_1 = input.LA(2);
				if ( (LA4_1=='\n') ) {
					alt4=1;
				}

				else {
					alt4=2;
				}

			}
			else if ( (LA4_0=='\n') ) {
				alt4=3;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 4, 0, input);
				throw nvae;
			}

			switch (alt4) {
				case 1 :
					// DFSLexer.g:138:6: CR LF
					{
					mCR(); 

					mLF(); 

					}
					break;
				case 2 :
					// DFSLexer.g:138:14: CR
					{
					mCR(); 

					}
					break;
				case 3 :
					// DFSLexer.g:138:19: LF
					{
					mLF(); 

					}
					break;

			}

			 _channel = HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEWLINE"

	// $ANTLR start "WHITESPACE"
	public final void mWHITESPACE() throws RecognitionException {
		try {
			int _type = WHITESPACE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:142:5: ( ( BLANK )+ )
			// DFSLexer.g:142:9: ( BLANK )+
			{
			// DFSLexer.g:142:9: ( BLANK )+
			int cnt5=0;
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( (LA5_0=='\t'||LA5_0==' '||LA5_0=='\u00A0') ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// DFSLexer.g:
					{
					if ( input.LA(1)=='\t'||input.LA(1)==' '||input.LA(1)=='\u00A0' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt5 >= 1 ) break loop5;
					EarlyExitException eee = new EarlyExitException(5, input);
					throw eee;
				}
				cnt5++;
			}

			 _channel = HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHITESPACE"

	// $ANTLR start "UNICODE_BOM"
	public final void mUNICODE_BOM() throws RecognitionException {
		try {
			int _type = UNICODE_BOM;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:147:5: ( '\\uFEFF' )
			// DFSLexer.g:147:9: '\\uFEFF'
			{
			match('\uFEFF'); 
			 _channel = HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "UNICODE_BOM"

	// $ANTLR start "INTEGER_LITERAL"
	public final void mINTEGER_LITERAL() throws RecognitionException {
		try {
			int _type = INTEGER_LITERAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:152:5: ( ( MINUS_SIGN | PLUS_SIGN )? ( DIGIT )+ )
			// DFSLexer.g:152:7: ( MINUS_SIGN | PLUS_SIGN )? ( DIGIT )+
			{
			// DFSLexer.g:152:7: ( MINUS_SIGN | PLUS_SIGN )?
			int alt6=2;
			int LA6_0 = input.LA(1);
			if ( (LA6_0=='+'||LA6_0=='-') ) {
				alt6=1;
			}
			switch (alt6) {
				case 1 :
					// DFSLexer.g:
					{
					if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			// DFSLexer.g:152:33: ( DIGIT )+
			int cnt7=0;
			loop7:
			while (true) {
				int alt7=2;
				int LA7_0 = input.LA(1);
				if ( ((LA7_0 >= '0' && LA7_0 <= '9')) ) {
					alt7=1;
				}

				switch (alt7) {
				case 1 :
					// DFSLexer.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt7 >= 1 ) break loop7;
					EarlyExitException eee = new EarlyExitException(7, input);
					throw eee;
				}
				cnt7++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INTEGER_LITERAL"

	// $ANTLR start "FLOATING_POINT_LITERAL"
	public final void mFLOATING_POINT_LITERAL() throws RecognitionException {
		try {
			int _type = FLOATING_POINT_LITERAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:157:5: ( ( MINUS_SIGN | PLUS_SIGN )? ( DIGIT )* PERIOD INTEGER_LITERAL ( ( 'e' | 'E' ) INTEGER_LITERAL )? | ( MINUS_SIGN | PLUS_SIGN )? ( DIGIT )* ( 'e' | 'E' ) INTEGER_LITERAL )
			int alt13=2;
			alt13 = dfa13.predict(input);
			switch (alt13) {
				case 1 :
					// DFSLexer.g:157:7: ( MINUS_SIGN | PLUS_SIGN )? ( DIGIT )* PERIOD INTEGER_LITERAL ( ( 'e' | 'E' ) INTEGER_LITERAL )?
					{
					// DFSLexer.g:157:7: ( MINUS_SIGN | PLUS_SIGN )?
					int alt8=2;
					int LA8_0 = input.LA(1);
					if ( (LA8_0=='+'||LA8_0=='-') ) {
						alt8=1;
					}
					switch (alt8) {
						case 1 :
							// DFSLexer.g:
							{
							if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

					}

					// DFSLexer.g:157:33: ( DIGIT )*
					loop9:
					while (true) {
						int alt9=2;
						int LA9_0 = input.LA(1);
						if ( ((LA9_0 >= '0' && LA9_0 <= '9')) ) {
							alt9=1;
						}

						switch (alt9) {
						case 1 :
							// DFSLexer.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop9;
						}
					}

					mPERIOD(); 

					mINTEGER_LITERAL(); 

					// DFSLexer.g:157:63: ( ( 'e' | 'E' ) INTEGER_LITERAL )?
					int alt10=2;
					int LA10_0 = input.LA(1);
					if ( (LA10_0=='E'||LA10_0=='e') ) {
						alt10=1;
					}
					switch (alt10) {
						case 1 :
							// DFSLexer.g:157:65: ( 'e' | 'E' ) INTEGER_LITERAL
							{
							if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							mINTEGER_LITERAL(); 

							}
							break;

					}

					}
					break;
				case 2 :
					// DFSLexer.g:158:7: ( MINUS_SIGN | PLUS_SIGN )? ( DIGIT )* ( 'e' | 'E' ) INTEGER_LITERAL
					{
					// DFSLexer.g:158:7: ( MINUS_SIGN | PLUS_SIGN )?
					int alt11=2;
					int LA11_0 = input.LA(1);
					if ( (LA11_0=='+'||LA11_0=='-') ) {
						alt11=1;
					}
					switch (alt11) {
						case 1 :
							// DFSLexer.g:
							{
							if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

					}

					// DFSLexer.g:158:33: ( DIGIT )*
					loop12:
					while (true) {
						int alt12=2;
						int LA12_0 = input.LA(1);
						if ( ((LA12_0 >= '0' && LA12_0 <= '9')) ) {
							alt12=1;
						}

						switch (alt12) {
						case 1 :
							// DFSLexer.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop12;
						}
					}

					if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					mINTEGER_LITERAL(); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FLOATING_POINT_LITERAL"

	// $ANTLR start "SIMPLE_ID"
	public final void mSIMPLE_ID() throws RecognitionException {
		try {
			int _type = SIMPLE_ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:164:5: ( LETTER ( LETTER | DIGIT | UNDERSCORE )* )
			// DFSLexer.g:164:7: LETTER ( LETTER | DIGIT | UNDERSCORE )*
			{
			mLETTER(); 

			// DFSLexer.g:164:14: ( LETTER | DIGIT | UNDERSCORE )*
			loop14:
			while (true) {
				int alt14=2;
				int LA14_0 = input.LA(1);
				if ( ((LA14_0 >= '0' && LA14_0 <= '9')||(LA14_0 >= 'A' && LA14_0 <= 'Z')||LA14_0=='_'||(LA14_0 >= 'a' && LA14_0 <= 'z')) ) {
					alt14=1;
				}

				switch (alt14) {
				case 1 :
					// DFSLexer.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop14;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SIMPLE_ID"

	// $ANTLR start "ARRAY_INTEGER"
	public final void mARRAY_INTEGER() throws RecognitionException {
		try {
			int _type = ARRAY_INTEGER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:170:15: ( 'INTEGER[]' )
			// DFSLexer.g:170:20: 'INTEGER[]'
			{
			match("INTEGER[]"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARRAY_INTEGER"

	// $ANTLR start "ARRAY_BOOLEAN"
	public final void mARRAY_BOOLEAN() throws RecognitionException {
		try {
			int _type = ARRAY_BOOLEAN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:171:15: ( 'BOOLEAN[]' )
			// DFSLexer.g:171:20: 'BOOLEAN[]'
			{
			match("BOOLEAN[]"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARRAY_BOOLEAN"

	// $ANTLR start "ARRAY_DATE"
	public final void mARRAY_DATE() throws RecognitionException {
		try {
			int _type = ARRAY_DATE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:172:12: ( 'DATE[]' )
			// DFSLexer.g:172:20: 'DATE[]'
			{
			match("DATE[]"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARRAY_DATE"

	// $ANTLR start "ARRAY_TIMESTAMP"
	public final void mARRAY_TIMESTAMP() throws RecognitionException {
		try {
			int _type = ARRAY_TIMESTAMP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:173:17: ( 'TIMESTAMP[]' )
			// DFSLexer.g:173:20: 'TIMESTAMP[]'
			{
			match("TIMESTAMP[]"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARRAY_TIMESTAMP"

	// $ANTLR start "ARRAY_ANY"
	public final void mARRAY_ANY() throws RecognitionException {
		try {
			int _type = ARRAY_ANY;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:174:11: ( 'ANY[]' )
			// DFSLexer.g:174:20: 'ANY[]'
			{
			match("ANY[]"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARRAY_ANY"

	// $ANTLR start "ARRAY_SHORT"
	public final void mARRAY_SHORT() throws RecognitionException {
		try {
			int _type = ARRAY_SHORT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:175:13: ( 'SHORT[]' )
			// DFSLexer.g:175:20: 'SHORT[]'
			{
			match("SHORT[]"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARRAY_SHORT"

	// $ANTLR start "ARRAY_DOUBLE"
	public final void mARRAY_DOUBLE() throws RecognitionException {
		try {
			int _type = ARRAY_DOUBLE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:176:14: ( 'DOUBLE[]' )
			// DFSLexer.g:176:20: 'DOUBLE[]'
			{
			match("DOUBLE[]"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARRAY_DOUBLE"

	// $ANTLR start "ARRAY_FLOAT"
	public final void mARRAY_FLOAT() throws RecognitionException {
		try {
			int _type = ARRAY_FLOAT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:177:13: ( 'FLOAT[]' )
			// DFSLexer.g:177:20: 'FLOAT[]'
			{
			match("FLOAT[]"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARRAY_FLOAT"

	// $ANTLR start "ARRAY_LONG"
	public final void mARRAY_LONG() throws RecognitionException {
		try {
			int _type = ARRAY_LONG;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:178:12: ( 'LONG[]' )
			// DFSLexer.g:178:20: 'LONG[]'
			{
			match("LONG[]"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARRAY_LONG"

	// $ANTLR start "ARRAY_DECIMAL"
	public final void mARRAY_DECIMAL() throws RecognitionException {
		try {
			int _type = ARRAY_DECIMAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:179:15: ( 'DECIMAL[]' )
			// DFSLexer.g:179:20: 'DECIMAL[]'
			{
			match("DECIMAL[]"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARRAY_DECIMAL"

	// $ANTLR start "ARRAY_STRING"
	public final void mARRAY_STRING() throws RecognitionException {
		try {
			int _type = ARRAY_STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:180:14: ( 'STRING[]' )
			// DFSLexer.g:180:20: 'STRING[]'
			{
			match("STRING[]"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARRAY_STRING"

	// $ANTLR start "DECIMAL_TYPE"
	public final void mDECIMAL_TYPE() throws RecognitionException {
		try {
			int _type = DECIMAL_TYPE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:183:5: ( KW_DECIMAL LEFT_PARENT ( DIGIT )* COMMA ( DIGIT )* RIGHT_PARENT )
			// DFSLexer.g:183:7: KW_DECIMAL LEFT_PARENT ( DIGIT )* COMMA ( DIGIT )* RIGHT_PARENT
			{
			mKW_DECIMAL(); 

			mLEFT_PARENT(); 

			// DFSLexer.g:183:30: ( DIGIT )*
			loop15:
			while (true) {
				int alt15=2;
				int LA15_0 = input.LA(1);
				if ( ((LA15_0 >= '0' && LA15_0 <= '9')) ) {
					alt15=1;
				}

				switch (alt15) {
				case 1 :
					// DFSLexer.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop15;
				}
			}

			mCOMMA(); 

			// DFSLexer.g:183:45: ( DIGIT )*
			loop16:
			while (true) {
				int alt16=2;
				int LA16_0 = input.LA(1);
				if ( ((LA16_0 >= '0' && LA16_0 <= '9')) ) {
					alt16=1;
				}

				switch (alt16) {
				case 1 :
					// DFSLexer.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop16;
				}
			}

			mRIGHT_PARENT(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DECIMAL_TYPE"

	// $ANTLR start "AT_SIGN"
	public final void mAT_SIGN() throws RecognitionException {
		try {
			int _type = AT_SIGN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:186:9: ( '@' )
			// DFSLexer.g:186:13: '@'
			{
			match('@'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AT_SIGN"

	// $ANTLR start "DOLLAR_SIGN"
	public final void mDOLLAR_SIGN() throws RecognitionException {
		try {
			int _type = DOLLAR_SIGN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:187:12: ( '$' )
			// DFSLexer.g:187:21: '$'
			{
			match('$'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DOLLAR_SIGN"

	// $ANTLR start "LEFT_PARENT"
	public final void mLEFT_PARENT() throws RecognitionException {
		try {
			int _type = LEFT_PARENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:188:13: ( '(' )
			// DFSLexer.g:188:16: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LEFT_PARENT"

	// $ANTLR start "RIGHT_PARENT"
	public final void mRIGHT_PARENT() throws RecognitionException {
		try {
			int _type = RIGHT_PARENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:189:14: ( ')' )
			// DFSLexer.g:189:17: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RIGHT_PARENT"

	// $ANTLR start "LEFT_BRACKET"
	public final void mLEFT_BRACKET() throws RecognitionException {
		try {
			int _type = LEFT_BRACKET;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:190:14: ( '[' )
			// DFSLexer.g:190:17: '['
			{
			match('['); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LEFT_BRACKET"

	// $ANTLR start "RIGHT_BRACKET"
	public final void mRIGHT_BRACKET() throws RecognitionException {
		try {
			int _type = RIGHT_BRACKET;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:191:15: ( ']' )
			// DFSLexer.g:191:18: ']'
			{
			match(']'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RIGHT_BRACKET"

	// $ANTLR start "LEFT_BRACE"
	public final void mLEFT_BRACE() throws RecognitionException {
		try {
			int _type = LEFT_BRACE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:192:12: ( '{' )
			// DFSLexer.g:192:15: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LEFT_BRACE"

	// $ANTLR start "RIGHT_BRACE"
	public final void mRIGHT_BRACE() throws RecognitionException {
		try {
			int _type = RIGHT_BRACE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:193:13: ( '}' )
			// DFSLexer.g:193:16: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RIGHT_BRACE"

	// $ANTLR start "COLON"
	public final void mCOLON() throws RecognitionException {
		try {
			int _type = COLON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:194:7: ( ':' )
			// DFSLexer.g:194:12: ':'
			{
			match(':'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COLON"

	// $ANTLR start "PERIOD"
	public final void mPERIOD() throws RecognitionException {
		try {
			int _type = PERIOD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:195:8: ( '.' )
			// DFSLexer.g:195:12: '.'
			{
			match('.'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PERIOD"

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			int _type = COMMA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:196:7: ( ',' )
			// DFSLexer.g:196:12: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMA"

	// $ANTLR start "MINUS_SIGN"
	public final void mMINUS_SIGN() throws RecognitionException {
		try {
			int _type = MINUS_SIGN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:197:12: ( '-' )
			// DFSLexer.g:197:15: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS_SIGN"

	// $ANTLR start "PLUS_SIGN"
	public final void mPLUS_SIGN() throws RecognitionException {
		try {
			int _type = PLUS_SIGN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:198:11: ( '+' )
			// DFSLexer.g:198:15: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS_SIGN"

	// $ANTLR start "BACK_SLASH"
	public final void mBACK_SLASH() throws RecognitionException {
		try {
			int _type = BACK_SLASH;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:199:11: ( '\\\\' )
			// DFSLexer.g:199:21: '\\\\'
			{
			match('\\'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BACK_SLASH"

	// $ANTLR start "APOSTROPHE"
	public final void mAPOSTROPHE() throws RecognitionException {
		try {
			int _type = APOSTROPHE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:200:12: ( '\\'' )
			// DFSLexer.g:200:15: '\\''
			{
			match('\''); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "APOSTROPHE"

	// $ANTLR start "QUOTATION_MARK"
	public final void mQUOTATION_MARK() throws RecognitionException {
		try {
			int _type = QUOTATION_MARK;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:201:16: ( '\"' )
			// DFSLexer.g:201:18: '\"'
			{
			match('\"'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "QUOTATION_MARK"

	// $ANTLR start "OUTPUT_STREAM"
	public final void mOUTPUT_STREAM() throws RecognitionException {
		try {
			int _type = OUTPUT_STREAM;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:202:15: ( '~>' )
			// DFSLexer.g:202:21: '~>'
			{
			match("~>"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OUTPUT_STREAM"

	// $ANTLR start "UNDERSCORE"
	public final void mUNDERSCORE() throws RecognitionException {
		try {
			int _type = UNDERSCORE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:203:12: ( '_' )
			// DFSLexer.g:203:21: '_'
			{
			match('_'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "UNDERSCORE"

	// $ANTLR start "EXCLAMATION_MARK"
	public final void mEXCLAMATION_MARK() throws RecognitionException {
		try {
			int _type = EXCLAMATION_MARK;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:204:18: ( '!' )
			// DFSLexer.g:204:20: '!'
			{
			match('!'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EXCLAMATION_MARK"

	// $ANTLR start "ASTERISK"
	public final void mASTERISK() throws RecognitionException {
		try {
			int _type = ASTERISK;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:205:10: ( '*' )
			// DFSLexer.g:205:14: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASTERISK"

	// $ANTLR start "CARET"
	public final void mCARET() throws RecognitionException {
		try {
			int _type = CARET;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:206:7: ( '^' )
			// DFSLexer.g:206:12: '^'
			{
			match('^'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CARET"

	// $ANTLR start "AMPERSAND"
	public final void mAMPERSAND() throws RecognitionException {
		try {
			int _type = AMPERSAND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:207:11: ( '&' )
			// DFSLexer.g:207:15: '&'
			{
			match('&'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AMPERSAND"

	// $ANTLR start "BAR"
	public final void mBAR() throws RecognitionException {
		try {
			int _type = BAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:208:5: ( '|' )
			// DFSLexer.g:208:10: '|'
			{
			match('|'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BAR"

	// $ANTLR start "SLASH"
	public final void mSLASH() throws RecognitionException {
		try {
			int _type = SLASH;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:209:7: ( '/' )
			// DFSLexer.g:209:12: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SLASH"

	// $ANTLR start "PERCENT_SIGN"
	public final void mPERCENT_SIGN() throws RecognitionException {
		try {
			int _type = PERCENT_SIGN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:210:14: ( '%' )
			// DFSLexer.g:210:17: '%'
			{
			match('%'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PERCENT_SIGN"

	// $ANTLR start "SEMICOLON"
	public final void mSEMICOLON() throws RecognitionException {
		try {
			int _type = SEMICOLON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:211:11: ( ';' )
			// DFSLexer.g:211:15: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SEMICOLON"

	// $ANTLR start "QUESTION_MARK"
	public final void mQUESTION_MARK() throws RecognitionException {
		try {
			int _type = QUESTION_MARK;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:212:15: ( '?' )
			// DFSLexer.g:212:18: '?'
			{
			match('?'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "QUESTION_MARK"

	// $ANTLR start "EQUALS"
	public final void mEQUALS() throws RecognitionException {
		try {
			int _type = EQUALS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:213:8: ( '=' )
			// DFSLexer.g:213:12: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EQUALS"

	// $ANTLR start "GREATER_THAN"
	public final void mGREATER_THAN() throws RecognitionException {
		try {
			int _type = GREATER_THAN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:214:14: ( '>' )
			// DFSLexer.g:214:17: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GREATER_THAN"

	// $ANTLR start "LESS_THAN"
	public final void mLESS_THAN() throws RecognitionException {
		try {
			int _type = LESS_THAN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:215:11: ( '<' )
			// DFSLexer.g:215:15: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LESS_THAN"

	// $ANTLR start "CROSSHATCH"
	public final void mCROSSHATCH() throws RecognitionException {
		try {
			int _type = CROSSHATCH;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// DFSLexer.g:216:12: ( '#' )
			// DFSLexer.g:216:15: '#'
			{
			match('#'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CROSSHATCH"

	@Override
	public void mTokens() throws RecognitionException {
		// DFSLexer.g:1:8: ( KW_PARAMETERS | KW_AS | KW_INTEGER | KW_BOOLEAN | KW_DATE | KW_TIMESTAMP | KW_ANY | KW_SHORT | KW_DOUBLE | KW_FLOAT | KW_LONG | KW_DECIMAL | KW_STRING | KW_TRUE | KW_FALSE | KW_SOURCE | KW_OUTPUT | KW_SINK | KW_FILTER | KW_JOIN | STRING_LITERAL | NEWLINE | WHITESPACE | UNICODE_BOM | INTEGER_LITERAL | FLOATING_POINT_LITERAL | SIMPLE_ID | ARRAY_INTEGER | ARRAY_BOOLEAN | ARRAY_DATE | ARRAY_TIMESTAMP | ARRAY_ANY | ARRAY_SHORT | ARRAY_DOUBLE | ARRAY_FLOAT | ARRAY_LONG | ARRAY_DECIMAL | ARRAY_STRING | DECIMAL_TYPE | AT_SIGN | DOLLAR_SIGN | LEFT_PARENT | RIGHT_PARENT | LEFT_BRACKET | RIGHT_BRACKET | LEFT_BRACE | RIGHT_BRACE | COLON | PERIOD | COMMA | MINUS_SIGN | PLUS_SIGN | BACK_SLASH | APOSTROPHE | QUOTATION_MARK | OUTPUT_STREAM | UNDERSCORE | EXCLAMATION_MARK | ASTERISK | CARET | AMPERSAND | BAR | SLASH | PERCENT_SIGN | SEMICOLON | QUESTION_MARK | EQUALS | GREATER_THAN | LESS_THAN | CROSSHATCH )
		int alt17=70;
		alt17 = dfa17.predict(input);
		switch (alt17) {
			case 1 :
				// DFSLexer.g:1:10: KW_PARAMETERS
				{
				mKW_PARAMETERS(); 

				}
				break;
			case 2 :
				// DFSLexer.g:1:24: KW_AS
				{
				mKW_AS(); 

				}
				break;
			case 3 :
				// DFSLexer.g:1:30: KW_INTEGER
				{
				mKW_INTEGER(); 

				}
				break;
			case 4 :
				// DFSLexer.g:1:41: KW_BOOLEAN
				{
				mKW_BOOLEAN(); 

				}
				break;
			case 5 :
				// DFSLexer.g:1:52: KW_DATE
				{
				mKW_DATE(); 

				}
				break;
			case 6 :
				// DFSLexer.g:1:60: KW_TIMESTAMP
				{
				mKW_TIMESTAMP(); 

				}
				break;
			case 7 :
				// DFSLexer.g:1:73: KW_ANY
				{
				mKW_ANY(); 

				}
				break;
			case 8 :
				// DFSLexer.g:1:80: KW_SHORT
				{
				mKW_SHORT(); 

				}
				break;
			case 9 :
				// DFSLexer.g:1:89: KW_DOUBLE
				{
				mKW_DOUBLE(); 

				}
				break;
			case 10 :
				// DFSLexer.g:1:99: KW_FLOAT
				{
				mKW_FLOAT(); 

				}
				break;
			case 11 :
				// DFSLexer.g:1:108: KW_LONG
				{
				mKW_LONG(); 

				}
				break;
			case 12 :
				// DFSLexer.g:1:116: KW_DECIMAL
				{
				mKW_DECIMAL(); 

				}
				break;
			case 13 :
				// DFSLexer.g:1:127: KW_STRING
				{
				mKW_STRING(); 

				}
				break;
			case 14 :
				// DFSLexer.g:1:137: KW_TRUE
				{
				mKW_TRUE(); 

				}
				break;
			case 15 :
				// DFSLexer.g:1:145: KW_FALSE
				{
				mKW_FALSE(); 

				}
				break;
			case 16 :
				// DFSLexer.g:1:154: KW_SOURCE
				{
				mKW_SOURCE(); 

				}
				break;
			case 17 :
				// DFSLexer.g:1:164: KW_OUTPUT
				{
				mKW_OUTPUT(); 

				}
				break;
			case 18 :
				// DFSLexer.g:1:174: KW_SINK
				{
				mKW_SINK(); 

				}
				break;
			case 19 :
				// DFSLexer.g:1:182: KW_FILTER
				{
				mKW_FILTER(); 

				}
				break;
			case 20 :
				// DFSLexer.g:1:192: KW_JOIN
				{
				mKW_JOIN(); 

				}
				break;
			case 21 :
				// DFSLexer.g:1:200: STRING_LITERAL
				{
				mSTRING_LITERAL(); 

				}
				break;
			case 22 :
				// DFSLexer.g:1:215: NEWLINE
				{
				mNEWLINE(); 

				}
				break;
			case 23 :
				// DFSLexer.g:1:223: WHITESPACE
				{
				mWHITESPACE(); 

				}
				break;
			case 24 :
				// DFSLexer.g:1:234: UNICODE_BOM
				{
				mUNICODE_BOM(); 

				}
				break;
			case 25 :
				// DFSLexer.g:1:246: INTEGER_LITERAL
				{
				mINTEGER_LITERAL(); 

				}
				break;
			case 26 :
				// DFSLexer.g:1:262: FLOATING_POINT_LITERAL
				{
				mFLOATING_POINT_LITERAL(); 

				}
				break;
			case 27 :
				// DFSLexer.g:1:285: SIMPLE_ID
				{
				mSIMPLE_ID(); 

				}
				break;
			case 28 :
				// DFSLexer.g:1:295: ARRAY_INTEGER
				{
				mARRAY_INTEGER(); 

				}
				break;
			case 29 :
				// DFSLexer.g:1:309: ARRAY_BOOLEAN
				{
				mARRAY_BOOLEAN(); 

				}
				break;
			case 30 :
				// DFSLexer.g:1:323: ARRAY_DATE
				{
				mARRAY_DATE(); 

				}
				break;
			case 31 :
				// DFSLexer.g:1:334: ARRAY_TIMESTAMP
				{
				mARRAY_TIMESTAMP(); 

				}
				break;
			case 32 :
				// DFSLexer.g:1:350: ARRAY_ANY
				{
				mARRAY_ANY(); 

				}
				break;
			case 33 :
				// DFSLexer.g:1:360: ARRAY_SHORT
				{
				mARRAY_SHORT(); 

				}
				break;
			case 34 :
				// DFSLexer.g:1:372: ARRAY_DOUBLE
				{
				mARRAY_DOUBLE(); 

				}
				break;
			case 35 :
				// DFSLexer.g:1:385: ARRAY_FLOAT
				{
				mARRAY_FLOAT(); 

				}
				break;
			case 36 :
				// DFSLexer.g:1:397: ARRAY_LONG
				{
				mARRAY_LONG(); 

				}
				break;
			case 37 :
				// DFSLexer.g:1:408: ARRAY_DECIMAL
				{
				mARRAY_DECIMAL(); 

				}
				break;
			case 38 :
				// DFSLexer.g:1:422: ARRAY_STRING
				{
				mARRAY_STRING(); 

				}
				break;
			case 39 :
				// DFSLexer.g:1:435: DECIMAL_TYPE
				{
				mDECIMAL_TYPE(); 

				}
				break;
			case 40 :
				// DFSLexer.g:1:448: AT_SIGN
				{
				mAT_SIGN(); 

				}
				break;
			case 41 :
				// DFSLexer.g:1:456: DOLLAR_SIGN
				{
				mDOLLAR_SIGN(); 

				}
				break;
			case 42 :
				// DFSLexer.g:1:468: LEFT_PARENT
				{
				mLEFT_PARENT(); 

				}
				break;
			case 43 :
				// DFSLexer.g:1:480: RIGHT_PARENT
				{
				mRIGHT_PARENT(); 

				}
				break;
			case 44 :
				// DFSLexer.g:1:493: LEFT_BRACKET
				{
				mLEFT_BRACKET(); 

				}
				break;
			case 45 :
				// DFSLexer.g:1:506: RIGHT_BRACKET
				{
				mRIGHT_BRACKET(); 

				}
				break;
			case 46 :
				// DFSLexer.g:1:520: LEFT_BRACE
				{
				mLEFT_BRACE(); 

				}
				break;
			case 47 :
				// DFSLexer.g:1:531: RIGHT_BRACE
				{
				mRIGHT_BRACE(); 

				}
				break;
			case 48 :
				// DFSLexer.g:1:543: COLON
				{
				mCOLON(); 

				}
				break;
			case 49 :
				// DFSLexer.g:1:549: PERIOD
				{
				mPERIOD(); 

				}
				break;
			case 50 :
				// DFSLexer.g:1:556: COMMA
				{
				mCOMMA(); 

				}
				break;
			case 51 :
				// DFSLexer.g:1:562: MINUS_SIGN
				{
				mMINUS_SIGN(); 

				}
				break;
			case 52 :
				// DFSLexer.g:1:573: PLUS_SIGN
				{
				mPLUS_SIGN(); 

				}
				break;
			case 53 :
				// DFSLexer.g:1:583: BACK_SLASH
				{
				mBACK_SLASH(); 

				}
				break;
			case 54 :
				// DFSLexer.g:1:594: APOSTROPHE
				{
				mAPOSTROPHE(); 

				}
				break;
			case 55 :
				// DFSLexer.g:1:605: QUOTATION_MARK
				{
				mQUOTATION_MARK(); 

				}
				break;
			case 56 :
				// DFSLexer.g:1:620: OUTPUT_STREAM
				{
				mOUTPUT_STREAM(); 

				}
				break;
			case 57 :
				// DFSLexer.g:1:634: UNDERSCORE
				{
				mUNDERSCORE(); 

				}
				break;
			case 58 :
				// DFSLexer.g:1:645: EXCLAMATION_MARK
				{
				mEXCLAMATION_MARK(); 

				}
				break;
			case 59 :
				// DFSLexer.g:1:662: ASTERISK
				{
				mASTERISK(); 

				}
				break;
			case 60 :
				// DFSLexer.g:1:671: CARET
				{
				mCARET(); 

				}
				break;
			case 61 :
				// DFSLexer.g:1:677: AMPERSAND
				{
				mAMPERSAND(); 

				}
				break;
			case 62 :
				// DFSLexer.g:1:687: BAR
				{
				mBAR(); 

				}
				break;
			case 63 :
				// DFSLexer.g:1:691: SLASH
				{
				mSLASH(); 

				}
				break;
			case 64 :
				// DFSLexer.g:1:697: PERCENT_SIGN
				{
				mPERCENT_SIGN(); 

				}
				break;
			case 65 :
				// DFSLexer.g:1:710: SEMICOLON
				{
				mSEMICOLON(); 

				}
				break;
			case 66 :
				// DFSLexer.g:1:720: QUESTION_MARK
				{
				mQUESTION_MARK(); 

				}
				break;
			case 67 :
				// DFSLexer.g:1:734: EQUALS
				{
				mEQUALS(); 

				}
				break;
			case 68 :
				// DFSLexer.g:1:741: GREATER_THAN
				{
				mGREATER_THAN(); 

				}
				break;
			case 69 :
				// DFSLexer.g:1:754: LESS_THAN
				{
				mLESS_THAN(); 

				}
				break;
			case 70 :
				// DFSLexer.g:1:764: CROSSHATCH
				{
				mCROSSHATCH(); 

				}
				break;

		}
	}


	protected DFA13 dfa13 = new DFA13(this);
	protected DFA17 dfa17 = new DFA17(this);
	static /*final*/ String DFA13_eotS;
	static /*final*/ String DFA13_eofS;
	static /*final*/ String DFA13_minS;
	static /*final*/ String DFA13_maxS;
	static /*final*/ String DFA13_acceptS;
	static /*final*/ String DFA13_specialS;
	static /*final*/ String[] DFA13_transitionS;

	static /*final*/ short[] DFA13_eot;
	static /*final*/ short[] DFA13_eof;
	static /*final*/ char[] DFA13_min;
	static /*final*/ char[] DFA13_max;
	static /*final*/ short[] DFA13_accept;
	static /*final*/ short[] DFA13_special;
	static /*final*/ short[][] DFA13_transition;

	static void auxTrans_13(){

	// PJAR: Modifikovana inicializace - kvuli "code too large" chybe.
	DFA13_eotS = "\5\uffff";
	DFA13_eofS = "\5\uffff";
	DFA13_minS = "\1\53\2\56\2\uffff";
	DFA13_maxS = "\3\145\2\uffff";
	DFA13_acceptS = "\3\uffff\1\1\1\2";
	DFA13_specialS = "\5\uffff}>";
	DFA13_transitionS = new String[]{
			"\1\1\1\uffff\1\1\1\3\1\uffff\12\2\13\uffff\1\4\37\uffff\1\4",
			"\1\3\1\uffff\12\2\13\uffff\1\4\37\uffff\1\4",
			"\1\3\1\uffff\12\2\13\uffff\1\4\37\uffff\1\4",
			"",
			""
	};

	DFA13_eot = DFA.unpackEncodedString(DFA13_eotS);
	DFA13_eof = DFA.unpackEncodedString(DFA13_eofS);
	DFA13_min = DFA.unpackEncodedStringToUnsignedChars(DFA13_minS);
	DFA13_max = DFA.unpackEncodedStringToUnsignedChars(DFA13_maxS);
	DFA13_accept = DFA.unpackEncodedString(DFA13_acceptS);
	DFA13_special = DFA.unpackEncodedString(DFA13_specialS);


		int numStates = DFA13_transitionS.length;
		DFA13_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA13_transition[i] = DFA.unpackEncodedString(DFA13_transitionS[i]);
		}
	}

	static {
	   auxTrans_13();
	};

	protected class DFA13 extends DFA {

		public DFA13(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 13;
			this.eot = DFA13_eot;
			this.eof = DFA13_eof;
			this.min = DFA13_min;
			this.max = DFA13_max;
			this.accept = DFA13_accept;
			this.special = DFA13_special;
			this.transition = DFA13_transition;
		}
		@Override
		public String getDescription() {
			return "156:1: FLOATING_POINT_LITERAL : ( ( MINUS_SIGN | PLUS_SIGN )? ( DIGIT )* PERIOD INTEGER_LITERAL ( ( 'e' | 'E' ) INTEGER_LITERAL )? | ( MINUS_SIGN | PLUS_SIGN )? ( DIGIT )* ( 'e' | 'E' ) INTEGER_LITERAL );";
		}
	}

	static /*final*/ String DFA17_eotS;
	static /*final*/ String DFA17_eofS;
	static /*final*/ String DFA17_minS;
	static /*final*/ String DFA17_maxS;
	static /*final*/ String DFA17_acceptS;
	static /*final*/ String DFA17_specialS;
	static /*final*/ String[] DFA17_transitionS;

	static /*final*/ short[] DFA17_eot;
	static /*final*/ short[] DFA17_eof;
	static /*final*/ char[] DFA17_min;
	static /*final*/ char[] DFA17_max;
	static /*final*/ short[] DFA17_accept;
	static /*final*/ short[] DFA17_special;
	static /*final*/ short[][] DFA17_transition;

	static void auxTrans_17(){

	// PJAR: Modifikovana inicializace - kvuli "code too large" chybe.
	DFA17_eotS = "\1\uffff\13\25\1\105\1\107\3\uffff\1\111\1\112\1\113\1\25"+
		"\13\uffff\1\115\20\uffff\1\25\1\117\22\25\7\uffff\1\110\1\uffff\1\25\1"+
		"\uffff\1\144\22\25\2\uffff\2\25\1\172\3\25\1\176\3\25\1\u0082\3\25\1\u0087"+
		"\1\25\1\u0089\3\25\2\uffff\3\25\1\uffff\1\u0091\2\25\1\uffff\1\u0095\1"+
		"\u0096\1\25\2\uffff\1\25\1\uffff\3\25\1\u009d\2\25\2\uffff\1\u00a1\1\u00a2"+
		"\3\uffff\1\u00a3\1\u00a4\1\25\1\u00a7\1\u00a9\2\uffff\1\u00ab\1\25\5\uffff"+
		"\1\25\7\uffff\2\25\1\u00b2\1\u00b3\3\uffff";
	DFA17_eofS = "\u00b4\uffff";
	DFA17_minS = "\1\11\1\101\2\116\1\117\1\101\1\111\1\110\1\101\1\117\1\125"+
		"\1\117\2\0\3\uffff\2\56\2\53\13\uffff\1\56\20\uffff\1\122\1\60\1\131\1"+
		"\124\1\117\1\124\1\125\1\103\1\115\1\125\1\117\1\122\1\125\1\116\1\117"+
		"\2\114\1\116\1\124\1\111\7\uffff\1\60\1\uffff\1\101\1\uffff\1\60\1\105"+
		"\1\114\1\105\1\102\1\111\2\105\1\122\1\111\1\122\1\113\1\101\1\123\1\124"+
		"\1\107\1\120\1\116\1\115\2\uffff\1\107\1\105\1\60\1\114\1\115\1\123\1"+
		"\60\1\124\1\116\1\103\1\60\1\124\2\105\1\60\1\125\1\60\2\105\1\101\2\uffff"+
		"\1\105\1\101\1\124\1\uffff\1\60\1\107\1\105\1\uffff\2\60\1\122\2\uffff"+
		"\1\124\1\uffff\1\124\1\122\1\116\1\60\1\114\1\101\2\uffff\2\60\3\uffff"+
		"\2\60\1\105\2\60\2\uffff\1\50\1\115\5\uffff\1\122\7\uffff\1\120\1\123"+
		"\2\60\3\uffff";
	DFA17_maxS = "\1\ufeff\1\101\1\123\1\116\2\117\1\122\1\124\1\114\1\117\1"+
		"\125\1\117\2\uffff\3\uffff\2\145\2\71\13\uffff\1\145\20\uffff\1\122\1"+
		"\172\1\131\1\124\1\117\1\124\1\125\1\103\1\115\1\125\1\117\1\122\1\125"+
		"\1\116\1\117\2\114\1\116\1\124\1\111\7\uffff\1\172\1\uffff\1\101\1\uffff"+
		"\1\172\1\105\1\114\1\105\1\102\1\111\2\105\1\122\1\111\1\122\1\113\1\101"+
		"\1\123\1\124\1\107\1\120\1\116\1\115\2\uffff\1\107\1\105\1\172\1\114\1"+
		"\115\1\123\1\172\1\124\1\116\1\103\1\172\1\124\2\105\1\172\1\125\1\172"+
		"\2\105\1\101\2\uffff\1\105\1\101\1\124\1\uffff\1\172\1\107\1\105\1\uffff"+
		"\2\172\1\122\2\uffff\1\124\1\uffff\1\124\1\122\1\116\1\172\1\114\1\101"+
		"\2\uffff\2\172\3\uffff\2\172\1\105\2\172\2\uffff\1\172\1\115\5\uffff\1"+
		"\122\7\uffff\1\120\1\123\2\172\3\uffff";
	DFA17_acceptS = "\16\uffff\1\26\1\27\1\30\4\uffff\1\33\1\50\1\51\1\52\1"+
		"\53\1\54\1\55\1\56\1\57\1\60\1\62\1\uffff\1\65\1\70\1\71\1\72\1\73\1\74"+
		"\1\75\1\76\1\77\1\100\1\101\1\102\1\103\1\104\1\105\1\106\24\uffff\1\66"+
		"\1\25\1\67\1\32\1\63\1\31\1\61\1\uffff\1\64\1\uffff\1\2\23\uffff\1\40"+
		"\1\7\24\uffff\1\36\1\5\3\uffff\1\16\3\uffff\1\22\3\uffff\1\44\1\13\1\uffff"+
		"\1\24\6\uffff\1\41\1\10\2\uffff\1\43\1\12\1\17\5\uffff\1\42\1\11\2\uffff"+
		"\1\46\1\15\1\20\1\23\1\21\1\uffff\1\34\1\3\1\35\1\4\1\45\1\14\1\47\4\uffff"+
		"\1\37\1\6\1\1";
	DFA17_specialS = "\14\uffff\1\0\1\1\u00a6\uffff}>";
	DFA17_transitionS = new String[]{
			"\1\17\1\16\2\uffff\1\16\22\uffff\1\17\1\44\1\15\1\60\1\27\1\52\1\47\1"+
			"\14\1\30\1\31\1\45\1\40\1\37\1\21\1\23\1\51\12\22\1\36\1\53\1\57\1\55"+
			"\1\56\1\54\1\26\1\2\1\4\1\25\1\5\1\24\1\10\2\25\1\3\1\13\1\25\1\11\2"+
			"\25\1\12\1\1\2\25\1\7\1\6\6\25\1\32\1\41\1\33\1\46\1\43\1\uffff\4\25"+
			"\1\24\25\25\1\34\1\50\1\35\1\42\41\uffff\1\17\ufe5e\uffff\1\20",
			"\1\61",
			"\1\63\4\uffff\1\62",
			"\1\64",
			"\1\65",
			"\1\66\3\uffff\1\70\11\uffff\1\67",
			"\1\71\10\uffff\1\72",
			"\1\73\1\76\5\uffff\1\75\4\uffff\1\74",
			"\1\100\7\uffff\1\101\2\uffff\1\77",
			"\1\102",
			"\1\103",
			"\1\104",
			"\0\106",
			"\0\106",
			"",
			"",
			"",
			"\1\110\1\uffff\12\22\13\uffff\1\110\37\uffff\1\110",
			"\1\110\1\uffff\12\22\13\uffff\1\110\37\uffff\1\110",
			"\1\110\1\uffff\1\110\2\uffff\12\110",
			"\1\110\1\uffff\1\110\2\uffff\12\114",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\110\1\uffff\12\22\13\uffff\1\110\37\uffff\1\110",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\116",
			"\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff\32\25",
			"\1\120",
			"\1\121",
			"\1\122",
			"\1\123",
			"\1\124",
			"\1\125",
			"\1\126",
			"\1\127",
			"\1\130",
			"\1\131",
			"\1\132",
			"\1\133",
			"\1\134",
			"\1\135",
			"\1\136",
			"\1\137",
			"\1\140",
			"\1\141",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\12\114\7\uffff\32\25\4\uffff\1\25\1\uffff\32\25",
			"",
			"\1\142",
			"",
			"\12\25\7\uffff\32\25\1\143\3\uffff\1\25\1\uffff\32\25",
			"\1\145",
			"\1\146",
			"\1\147",
			"\1\150",
			"\1\151",
			"\1\152",
			"\1\153",
			"\1\154",
			"\1\155",
			"\1\156",
			"\1\157",
			"\1\160",
			"\1\161",
			"\1\162",
			"\1\163",
			"\1\164",
			"\1\165",
			"\1\166",
			"",
			"",
			"\1\167",
			"\1\170",
			"\12\25\7\uffff\32\25\1\171\3\uffff\1\25\1\uffff\32\25",
			"\1\173",
			"\1\174",
			"\1\175",
			"\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff\32\25",
			"\1\177",
			"\1\u0080",
			"\1\u0081",
			"\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff\32\25",
			"\1\u0083",
			"\1\u0084",
			"\1\u0085",
			"\12\25\7\uffff\32\25\1\u0086\3\uffff\1\25\1\uffff\32\25",
			"\1\u0088",
			"\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff\32\25",
			"\1\u008a",
			"\1\u008b",
			"\1\u008c",
			"",
			"",
			"\1\u008d",
			"\1\u008e",
			"\1\u008f",
			"",
			"\12\25\7\uffff\32\25\1\u0090\3\uffff\1\25\1\uffff\32\25",
			"\1\u0092",
			"\1\u0093",
			"",
			"\12\25\7\uffff\32\25\1\u0094\3\uffff\1\25\1\uffff\32\25",
			"\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff\32\25",
			"\1\u0097",
			"",
			"",
			"\1\u0098",
			"",
			"\1\u0099",
			"\1\u009a",
			"\1\u009b",
			"\12\25\7\uffff\32\25\1\u009c\3\uffff\1\25\1\uffff\32\25",
			"\1\u009e",
			"\1\u009f",
			"",
			"",
			"\12\25\7\uffff\32\25\1\u00a0\3\uffff\1\25\1\uffff\32\25",
			"\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff\32\25",
			"",
			"",
			"",
			"\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff\32\25",
			"\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff\32\25",
			"\1\u00a5",
			"\12\25\7\uffff\32\25\1\u00a6\3\uffff\1\25\1\uffff\32\25",
			"\12\25\7\uffff\32\25\1\u00a8\3\uffff\1\25\1\uffff\32\25",
			"",
			"",
			"\1\u00ac\7\uffff\12\25\7\uffff\32\25\1\u00aa\3\uffff\1\25\1\uffff\32"+
			"\25",
			"\1\u00ad",
			"",
			"",
			"",
			"",
			"",
			"\1\u00ae",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\u00af",
			"\1\u00b0",
			"\12\25\7\uffff\32\25\1\u00b1\3\uffff\1\25\1\uffff\32\25",
			"\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff\32\25",
			"",
			"",
			""
	};

	DFA17_eot = DFA.unpackEncodedString(DFA17_eotS);
	DFA17_eof = DFA.unpackEncodedString(DFA17_eofS);
	DFA17_min = DFA.unpackEncodedStringToUnsignedChars(DFA17_minS);
	DFA17_max = DFA.unpackEncodedStringToUnsignedChars(DFA17_maxS);
	DFA17_accept = DFA.unpackEncodedString(DFA17_acceptS);
	DFA17_special = DFA.unpackEncodedString(DFA17_specialS);


		int numStates = DFA17_transitionS.length;
		DFA17_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA17_transition[i] = DFA.unpackEncodedString(DFA17_transitionS[i]);
		}
	}

	static {
	   auxTrans_17();
	};

	protected class DFA17 extends DFA {

		public DFA17(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 17;
			this.eot = DFA17_eot;
			this.eof = DFA17_eof;
			this.min = DFA17_min;
			this.max = DFA17_max;
			this.accept = DFA17_accept;
			this.special = DFA17_special;
			this.transition = DFA17_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( KW_PARAMETERS | KW_AS | KW_INTEGER | KW_BOOLEAN | KW_DATE | KW_TIMESTAMP | KW_ANY | KW_SHORT | KW_DOUBLE | KW_FLOAT | KW_LONG | KW_DECIMAL | KW_STRING | KW_TRUE | KW_FALSE | KW_SOURCE | KW_OUTPUT | KW_SINK | KW_FILTER | KW_JOIN | STRING_LITERAL | NEWLINE | WHITESPACE | UNICODE_BOM | INTEGER_LITERAL | FLOATING_POINT_LITERAL | SIMPLE_ID | ARRAY_INTEGER | ARRAY_BOOLEAN | ARRAY_DATE | ARRAY_TIMESTAMP | ARRAY_ANY | ARRAY_SHORT | ARRAY_DOUBLE | ARRAY_FLOAT | ARRAY_LONG | ARRAY_DECIMAL | ARRAY_STRING | DECIMAL_TYPE | AT_SIGN | DOLLAR_SIGN | LEFT_PARENT | RIGHT_PARENT | LEFT_BRACKET | RIGHT_BRACKET | LEFT_BRACE | RIGHT_BRACE | COLON | PERIOD | COMMA | MINUS_SIGN | PLUS_SIGN | BACK_SLASH | APOSTROPHE | QUOTATION_MARK | OUTPUT_STREAM | UNDERSCORE | EXCLAMATION_MARK | ASTERISK | CARET | AMPERSAND | BAR | SLASH | PERCENT_SIGN | SEMICOLON | QUESTION_MARK | EQUALS | GREATER_THAN | LESS_THAN | CROSSHATCH );";
		}
		@Override
		public int specialStateTransition(int _s, IntStream _input) throws NoViableAltException {
			IntStream input = _input;
			int __s = _s;
			switch ( _s ) {
					case 0 : 
						int LA17_12 = input.LA(1);
						// to avoid code too large in specialTransitions: enclose each decision if-else sequence in a lambda
						_s = ((Function<Integer, Integer>)(Integer s) -> {
						if ( ((LA17_12 >= '\u0000' && LA17_12 <= '\uFFFF')) ) {s = 70;}
						else s = 69;
						return s;
						}).apply(-1);
						if ( _s>=0 ) return _s;
						break;

					case 1 : 
						int LA17_13 = input.LA(1);
						// to avoid code too large in specialTransitions: enclose each decision if-else sequence in a lambda
						_s = ((Function<Integer, Integer>)(Integer s) -> {
						if ( ((LA17_13 >= '\u0000' && LA17_13 <= '\uFFFF')) ) {s = 70;}
						else s = 71;
						return s;
						}).apply(-1);
						if ( _s>=0 ) return _s;
						break;
			}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 17, __s, input);
			error(nvae);
			throw nvae;
		}
	}

}
