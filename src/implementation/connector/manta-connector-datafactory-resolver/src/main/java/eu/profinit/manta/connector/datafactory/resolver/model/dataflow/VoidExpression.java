package eu.profinit.manta.connector.datafactory.resolver.model.dataflow;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;

public class VoidExpression implements IExpression {
    @Override
    public String getRawForm() {
        return "";
    }

    @Override
    public String asString() {
        return "";
    }
}
