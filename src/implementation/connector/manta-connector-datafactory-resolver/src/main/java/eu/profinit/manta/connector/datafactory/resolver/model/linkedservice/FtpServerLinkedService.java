package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.IFtpServerLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;

/**
 * Implementation of the FtpServerLinkedService resource interface.
 */
public class FtpServerLinkedService extends AbstractGeneralFtpServerLinkedService implements IFtpServerLinkedService {



    public FtpServerLinkedService(String name,
                                  String description,
                                  IParameters parameters,
                                  IAdfFieldValue host,
                                  IAdfFieldValue port,
                                  IAdfFieldValue userName) {
        super(name, description, parameters, host, port, userName);

    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.FTP_SERVER;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitFtpServerLinkedService(this);
    }
}
