package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.IGoogleCloudStorageLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;

/**
 * Implementation of the AmazonS3LinkedService resource interface.
 */
public class GoogleCloudStorageLinkedService extends LinkedService implements IGoogleCloudStorageLinkedService {

    /**
     * This value specifies the endpoint to access with the Google Cloud Storage Connector.
     */
    private final IAdfFieldValue serviceUrl;

    public GoogleCloudStorageLinkedService(String name,
                                           String description,
                                           IParameters parameters,
                                           IAdfFieldValue serviceUrl) {
        super(name, description, parameters);
        this.serviceUrl = serviceUrl;
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.GOOGLE_CLOUD_STORAGE;
    }

    @Override
    public IAdfFieldValue getServiceUrl() {
        return serviceUrl;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitGoogleCloudStorageLinkedService(this);
    }
}
