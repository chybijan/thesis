package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.SnowflakeLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for SnowflakeLinkedService resource.
 */
public class SnowflakeLinkedServiceParser extends AbstractLinkedServiceParser<SnowflakeLinkedService> {

    @Override
    protected SnowflakeLinkedService parseTypeSpecificFields(String name,
                                                             String description,
                                                             String type,
                                                             IParameters parameters,
                                                             JsonNode typePropertiesNode) {
        IAdfFieldValue connectionString = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            connectionString = processAdfFieldValue(typePropertiesNode.path("connectionString"));
        }

        return new SnowflakeLinkedService(name, description, parameters, connectionString);
    }
}
