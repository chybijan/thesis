package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.Error;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.common.messages.CommonMessages;

public class GeneralErrors extends eu.profinit.manta.platform.logging.common.errordefinitions.categories.GeneralErrors {

    @Error(userMessage = "Invalid endpoint reference. Endpoint won't be used during analysis",
            technicalMessage = "Invalid endpoint reference. Endpoint won't be used during analysis",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public InvalidEndpointReferenceBuilder invalidEndpointReference() {
        return new InvalidEndpointReferenceBuilder(this);
    }

}
