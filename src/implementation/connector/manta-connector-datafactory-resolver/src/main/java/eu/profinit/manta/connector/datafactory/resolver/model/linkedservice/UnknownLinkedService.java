package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.IUnknownLinkedService;
import eu.profinit.manta.connector.datafactory.model.linkedservice.LinkedServiceType;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;

/**
 * Implementation of unrecognizable or unsupported linked service type by Manta.
 */
public class UnknownLinkedService extends LinkedService implements IUnknownLinkedService {

    public UnknownLinkedService(String name, String description, IParameters parameters) {
        super(name, description, parameters);
    }

    @Override
    public LinkedServiceType getType() {
        return LinkedServiceType.UNKNOWN;
    }

    @Override
    public <T> T accept(ILinkedServiceVisitor<T> visitor) {
        return visitor.visitUnknownLinkedService(this);
    }
}
