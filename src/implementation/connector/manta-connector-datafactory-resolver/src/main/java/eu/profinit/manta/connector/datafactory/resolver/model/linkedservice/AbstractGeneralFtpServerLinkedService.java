package eu.profinit.manta.connector.datafactory.resolver.model.linkedservice;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.model.linkedservice.IGeneralFtpServerLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;

public abstract class AbstractGeneralFtpServerLinkedService extends LinkedService implements IGeneralFtpServerLinkedService {

    /**
     * Host name of the server.
     */
    private final IAdfFieldValue host;

    /**
     * The TCP port number that the FTP server uses to listen for client connections.
     */
    private final IAdfFieldValue port;

    /**
     * Username to logon the FTP server.
     */
    private final IAdfFieldValue userName;

    protected AbstractGeneralFtpServerLinkedService(String name, String description, IParameters parameters,
                                                    IAdfFieldValue host,
                                                    IAdfFieldValue port,
                                                    IAdfFieldValue userName) {
        super(name, description, parameters);
        this.host = host;
        this.port = port;
        this.userName = userName;
    }

    @Override
    public IAdfFieldValue getHost() {
        return host;
    }

    @Override
    public IAdfFieldValue getPort() {
        return port;
    }

    @Override
    public IAdfFieldValue getUserName() {
        return userName;
    }
}
