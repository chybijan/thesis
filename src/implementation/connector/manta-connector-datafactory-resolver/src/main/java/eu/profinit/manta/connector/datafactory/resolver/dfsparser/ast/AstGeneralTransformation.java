package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.ITable;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstGeneralTransformation;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstTransformationAttributes;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.UnknownTransformation;
import org.antlr.runtime.Token;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AstGeneralTransformation extends AstConcreteTransformation implements IAstGeneralTransformation {

    public AstGeneralTransformation() {
    }

    public AstGeneralTransformation(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstGeneralTransformation(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    @Override
    protected ITransformation createTransformation(
            String name,
            Set<String> substreams,
            Set<IADFStream> inputs,
            Set<String> outputs,
            ITable table) {

        IAstTransformationAttributes astAttributes = findTransformationAttributes();

        Map<String, IExpression> attributes = new HashMap<>();

        if(astAttributes != null)
            attributes = astAttributes.resolveAttributes();

        return new UnknownTransformation(name, substreams, inputs, outputs, table, attributes);

    }

}
