package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstExpression;
import eu.profinit.manta.connector.datafactory.model.errors.InvalidExpressionTypeException;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import org.antlr.runtime.Token;

public class AstExpression extends DFSNode implements IAstExpression {
    public AstExpression() {
    }

    public AstExpression(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstExpression(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    public boolean resolveAsBoolean() throws InvalidExpressionTypeException {
        DFSNode literalNode = (DFSNode) selectSingleNode("//AST_COLUMN_REFERENCE");
        if(literalNode == null)
            throw new InvalidExpressionTypeException("\"null\" could not be converted to boolean");

        String literalString = literalNode.toNormalizedString().toLowerCase();
        if(literalString.equals("true"))
            return true;
        else if(literalString.equals("false"))
            return false;
        throw new InvalidExpressionTypeException("\"" + literalString + "\" could not be converted to boolean");

        // TODO properly resolve expression
    }

    @Override
    public String getRawForm() {
        return toNormalizedString();
    }

    @Override
    public String asString() {
        // TODO evaluate expression
        String rawExpression = getRawForm();
        if(rawExpression.length() >= 2 &&
                (rawExpression.charAt(0) == '\'' && rawExpression.charAt(rawExpression.length()-1) == '\'') ||
                (rawExpression.charAt(0) == '"' && rawExpression.charAt(rawExpression.length()-1) == '"'))
            return rawExpression.substring(1, rawExpression.length()-1);
        return rawExpression;
    }
}
