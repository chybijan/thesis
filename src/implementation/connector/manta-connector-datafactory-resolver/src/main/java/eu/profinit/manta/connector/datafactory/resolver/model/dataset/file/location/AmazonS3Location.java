package eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location;

import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.IAmazonS3Location;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.LocationType;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.visitor.ILocationVisitor;

/**
 * Implementation of the DatasetLocation resource of type AmazonS3.
 */
public class AmazonS3Location extends FileDatasetLocation implements IAmazonS3Location {

    /**
     * The S3 bucket name.
     */
    private final IAdfFieldValue bucketName;

    /**
     * The version of the S3 object, if S3 versioning is enabled.
     */
    private final IAdfFieldValue version;

    /**
     * @param bucketName the S3 bucket name
     * @param folderPath see {@link FileDatasetLocation#FileDatasetLocation(IAdfFieldValue, IAdfFieldValue)}
     * @param fileName   see {@link FileDatasetLocation#FileDatasetLocation(IAdfFieldValue, IAdfFieldValue)}
     * @param version    the version of the S3 object, if S3 versioning is enabled
     */
    public AmazonS3Location(IAdfFieldValue bucketName, IAdfFieldValue folderPath, IAdfFieldValue fileName, IAdfFieldValue version) {
        super(folderPath, fileName);
        this.bucketName = bucketName;
        this.version = version;
    }

    @Override
    public IAdfFieldValue getBucketName() {
        return bucketName;
    }

    @Override
    public IAdfFieldValue getVersion() {
        return version;
    }

    @Override
    public LocationType getLocationType() {
        return LocationType.AMAZON_S3_LOCATION;
    }

    @Override
    public <T> T accept(ILocationVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
