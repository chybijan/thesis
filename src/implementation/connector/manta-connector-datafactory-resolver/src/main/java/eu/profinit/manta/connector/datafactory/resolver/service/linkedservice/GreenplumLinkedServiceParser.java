package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.linkedservice.GreenplumLinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;

/**
 * Parser for GreenplumLinkedService resource.
 */
public class GreenplumLinkedServiceParser extends AbstractLinkedServiceParser<GreenplumLinkedService> {

    @Override
    protected GreenplumLinkedService parseTypeSpecificFields(String name,
                                                             String description,
                                                             String type,
                                                             IParameters parameters,
                                                             JsonNode typePropertiesNode) {
        IAdfFieldValue connectionString = null;
        if (JsonUtil.isNotNullAndNotMissing(typePropertiesNode)) {
            connectionString = processAdfFieldValue(typePropertiesNode.path("connectionString"));
        }

        return new GreenplumLinkedService(name, description, parameters, connectionString);
    }
}
