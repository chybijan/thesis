package eu.profinit.manta.connector.datafactory.resolver.errors;

import eu.profinit.manta.connector.datafactory.resolver.errors.categories.GeneralErrors;
import eu.profinit.manta.connector.datafactory.resolver.errors.categories.ParsingErrors;

public class Categories {

    private Categories() { throw new IllegalStateException("Utility class");}

    public static ParsingErrors parsingErrors() {
        return new ParsingErrors();
    }

    public static GeneralErrors generalErrors(){return new GeneralErrors();}

}
