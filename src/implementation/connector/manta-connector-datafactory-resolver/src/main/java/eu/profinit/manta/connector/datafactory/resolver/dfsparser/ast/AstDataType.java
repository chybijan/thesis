package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.DataFlowDataType;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstDataType;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.platform.logging.api.logging.Logger;
import org.antlr.runtime.Token;

public class AstDataType extends DFSNode implements IAstDataType {

    private static final Logger LOGGER = new Logger(AstDataType.class);

    public AstDataType() {
    }

    public AstDataType(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstDataType(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }


    @Override
    public DataFlowDataType getDataType() {
        DataFlowDataType type = DataFlowDataType.UNKNOWN_TYPE;

        try{
            String contents = toNormalizedString();

            // TODO refactor data type to contain key and value data types and decimal range
            // It will be best to split column type and parameter type
            // Because only parameter type can have those complicated values
            if(contents.startsWith("decimal")){
                type = DataFlowDataType.DF_DECIMAL;
            }
            else if(selectSingleNode("AST_MAP_DATA_TYPE") != null){
                type = DataFlowDataType.DF_MAP;
            }
            else{
                type = DataFlowDataType.valueOfCI(contents);
            }
        }catch (IllegalArgumentException e){
            LOGGER.log(Categories.parsingErrors().unknownDataType()
                    .type(toNormalizedString())
                    .usedType(DataFlowDataType.UNKNOWN_TYPE));
        }

        return type;

    }
}
