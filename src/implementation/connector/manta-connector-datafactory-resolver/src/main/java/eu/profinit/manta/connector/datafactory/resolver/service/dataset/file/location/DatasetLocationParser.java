package eu.profinit.manta.connector.datafactory.resolver.service.dataset.file.location;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.resolver.model.dataset.file.location.DatasetLocation;
import eu.profinit.manta.connector.datafactory.resolver.service.common.AdfFieldValueParser;

public abstract class DatasetLocationParser<T extends DatasetLocation> {

    protected AdfFieldValueParser adfFieldValueParser;

    public abstract T parseDatasetLocation(JsonNode locationNode);

    public void setAdfFieldValueParser(AdfFieldValueParser adfFieldValueParser) {
        this.adfFieldValueParser = adfFieldValueParser;
    }

}
