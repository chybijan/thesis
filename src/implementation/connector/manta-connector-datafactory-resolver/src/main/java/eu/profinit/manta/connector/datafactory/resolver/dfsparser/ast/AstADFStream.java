package eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast;

import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstADFStream;
import eu.profinit.manta.connector.datafactory.model.dfsparser.ast.IAstIdentifier;
import eu.profinit.manta.connector.datafactory.resolver.dfsparser.DFSContextState;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.ADFStream;
import eu.profinit.manta.platform.logging.api.logging.Logger;
import org.antlr.runtime.Token;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class AstADFStream extends DFSNode implements IAstADFStream{

    private static final Logger LOGGER = new Logger(AstADFStream.class);

    private static final String UNKNOWN_NAME = "UNKNOWN NAME";

    public AstADFStream() {
    }

    public AstADFStream(Token payload, DFSContextState contextState) {
        super(payload, contextState);
    }

    public AstADFStream(int tokenType, DFSContextState contextState) {
        super(tokenType, contextState);
    }

    @Override
    public IAstIdentifier findTransformationName() {
        return (IAstIdentifier) selectSingleNode("AST_TRANSFORMATION_NAME");
    }

    @Override
    public List<IAstIdentifier> findSubstreamNames() {
        return selectNodes("AST_SUBSTREAM_NAME");
    }

    @Override
    public Set<IADFStream> getADFStreams() {

        IAstIdentifier astName = findTransformationName();
        String transformationName = UNKNOWN_NAME;
        if(astName == null){
            LOGGER.log(Categories.parsingErrors().missingTransformationName());
        }else{
            transformationName = astName.getIdentifier();
        }

        String finalTransformationName = transformationName;

        Set<IADFStream> thisStreams = findSubstreamNames().stream()
                .map(e -> new ADFStream(finalTransformationName, e.getIdentifier()))
                .collect(Collectors.toSet());

        if(thisStreams.isEmpty())
            thisStreams.add(new ADFStream(transformationName,""));

        return thisStreams;
    }
}
