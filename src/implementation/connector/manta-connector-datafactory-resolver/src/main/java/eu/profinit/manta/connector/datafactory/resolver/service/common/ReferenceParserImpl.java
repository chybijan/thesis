package eu.profinit.manta.connector.datafactory.resolver.service.common;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.*;
import eu.profinit.manta.connector.datafactory.resolver.errors.Categories;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IArgument;
import eu.profinit.manta.connector.datafactory.resolver.model.common.Argument;
import eu.profinit.manta.connector.datafactory.resolver.model.common.Reference;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;
import eu.profinit.manta.platform.logging.api.logging.Logger;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

public class ReferenceParserImpl implements ReferenceParser {

    private static final Logger LOGGER = new Logger(ReferenceParserImpl.class);

    private static final String REFERENCE_NAME_PATH = "referenceName";

    private static final String ARGUMENTS_PATH = "/parameters";

    private static final String TYPE_PATH = "type";

    private final AdfFieldValueParser fialdValueParser;

    public ReferenceParserImpl(AdfFieldValueParser fialdValueParser) {
        this.fialdValueParser = fialdValueParser;
    }

    @Override
    public IReference parseReference(JsonNode referenceNode, ReferenceType defaultType) {

        IReference datasetReference = null;

        if(JsonUtil.isNotNullAndNotMissing(referenceNode)){

            // Load dataset name
            String referenceName = JsonUtil.pathAsText(referenceNode, REFERENCE_NAME_PATH);
            if(StringUtils.isBlank(referenceName)){
                LOGGER.log(Categories.parsingErrors().missingDatasetReferenceName().referenceNode(referenceNode));
                return null;
            }

            // Load reference type
            String strType = JsonUtil.pathAsText(referenceNode, TYPE_PATH);

            ReferenceType type;

            try {

                // Load type
                type = ReferenceType.valueOfCI(strType);

            }catch (IllegalArgumentException e){

                LOGGER.log(Categories.parsingErrors().unknownReferenceType().typeName(strType).defaultType(defaultType));
                type = defaultType;
            }

            // Load arguments
            JsonNode argumentsNode = referenceNode.at(ARGUMENTS_PATH);

            Collection<IArgument> arguments = parseArguments(argumentsNode);

            datasetReference = new Reference(referenceName, arguments, type);

        }

        return datasetReference;

    }

    /**
     * Parsers arguments into Collection of arguments
     *
     * @param argumentsNode json argument
     * @return Collection of arguments or empty collection if argumentsNode is null or missing
     */
    protected Collection<IArgument> parseArguments(JsonNode argumentsNode){

        Collection<IArgument> arguments = new ArrayList<>();

        if(JsonUtil.isNotNullAndNotMissing(argumentsNode)){

            argumentsNode.fields().forEachRemaining(argumentKeyValue -> {

                String argName = argumentKeyValue.getKey();
                IAdfFieldValue value = fialdValueParser.parseAdfFieldValue(argumentKeyValue.getValue());

                arguments.add(new Argument(argName, value));

            });
        }

        return arguments;
    }

}
