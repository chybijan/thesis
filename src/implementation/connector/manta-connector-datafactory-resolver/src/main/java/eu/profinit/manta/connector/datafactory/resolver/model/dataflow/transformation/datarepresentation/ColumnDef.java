package eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.datarepresentation;

import eu.profinit.manta.connector.datafactory.model.dataflow.DataFlowDataType;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumnDef;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.visitor.IColumnVisitor;

public class ColumnDef implements IColumnDef {

    private final String name;

    private final DataFlowDataType type;

    private final String format;

    public ColumnDef(String name, DataFlowDataType type, String format) {
        this.name = name;
        this.type = type;
        this.format = format;
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public DataFlowDataType getType() {
        return type;
    }

    @Override
    public String getFormat(){ return format;}

    @Override
    public <T> T accept(IColumnVisitor<T> visitor) {
        return visitor.visitColumnDef(this);
    }
}
