package eu.profinit.manta.connector.datafactory.resolver.model.common;

import eu.profinit.manta.connector.datafactory.model.common.IArgument;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.ReferenceType;

import java.util.Collection;
import java.util.Objects;

/**
 * Implementation of Dataset reference
 */
public class Reference implements IReference {

    private final String referenceName;

    private final Collection<IArgument> arguments;

    private final ReferenceType type;

    public Reference(String referenceName, Collection<IArgument> arguments, ReferenceType type) {
        this.referenceName = referenceName;
        this.arguments = arguments;
        this.type = type;
    }

    @Override
    public Collection<IArgument> getArguments() {
        return arguments;
    }

    @Override
    public String getReferenceName() {
        return referenceName;
    }

    @Override
    public ReferenceType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IReference reference = (IReference) o;
        return referenceName.equals(reference.getReferenceName()) && type == reference.getType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(referenceName, type);
    }
}
