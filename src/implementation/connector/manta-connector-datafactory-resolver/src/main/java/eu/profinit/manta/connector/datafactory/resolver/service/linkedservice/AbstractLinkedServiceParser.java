package eu.profinit.manta.connector.datafactory.resolver.service.linkedservice;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;
import eu.profinit.manta.connector.datafactory.resolver.model.LinkedService;
import eu.profinit.manta.connector.datafactory.resolver.service.JsonUtil;
import eu.profinit.manta.connector.datafactory.resolver.service.common.AdfFieldValueParser;
import eu.profinit.manta.connector.datafactory.resolver.service.common.ParametersParser;

public abstract class AbstractLinkedServiceParser<T extends LinkedService> implements LinkedServiceParser<T> {

    /**
     * Service for parsing ADF field value.
     */
    private AdfFieldValueParser adfFieldValueParser;

    /**
     * Service for parsing ADF resource parameters.
     */
    private ParametersParser parametersParser;

    @Override
    public T parseLinkedService(JsonNode linkedServiceNode) {
        T dataset = null;
        if (JsonUtil.isNotNullAndNotMissing(linkedServiceNode)) {

            String name = JsonUtil.pathAsText(linkedServiceNode, "name");

            JsonNode propertiesNode = linkedServiceNode.path("properties");

            String description = JsonUtil.pathAsText(propertiesNode, "name");
            String type = JsonUtil.pathAsText(propertiesNode, "type");

            IParameters parameters =
                    parametersParser.parseParameters(propertiesNode.path("parameters"));

            dataset = parseTypeSpecificFields(
                    name,
                    description,
                    type,
                    parameters,
                    propertiesNode.path("typeProperties")
            );
        }
        return dataset;
    }

    /**
     * Parses fields specific to each linked service type.
     * <br>
     * The fields specific to each linked service type are stored in the "typeProperties" field.
     *
     * @param name               linked service name
     * @param description        linked service description
     * @param type               linked service type
     * @param parameters         linked service parameters definition
     * @param typePropertiesNode JSON node representing "typeProperties" field value
     * @return instance of a linked service
     */
    protected abstract T parseTypeSpecificFields(String name,
                                                 String description,
                                                 String type,
                                                 IParameters parameters,
                                                 JsonNode typePropertiesNode);

    /**
     * Processes the value of Azure Data Factory resource field.
     *
     * @param adfFieldValueNode JSON node representing the value of the field
     * @return processed field's value
     */
    protected IAdfFieldValue processAdfFieldValue(JsonNode adfFieldValueNode) {
        IAdfFieldValue adfFieldValue = null;
        if (JsonUtil.isNotNullAndNotMissing(adfFieldValueNode)) {
            adfFieldValue = adfFieldValueParser.parseAdfFieldValue(adfFieldValueNode);
        }
        return adfFieldValue;
    }

    public void setAdfFieldValueParser(AdfFieldValueParser adfFieldValueParser) {
        this.adfFieldValueParser = adfFieldValueParser;
    }

    public void setParametersParser(ParametersParser parametersParser) {
        this.parametersParser = parametersParser;
    }

}
