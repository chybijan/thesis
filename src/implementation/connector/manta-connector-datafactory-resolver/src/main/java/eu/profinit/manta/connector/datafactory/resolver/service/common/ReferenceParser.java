package eu.profinit.manta.connector.datafactory.resolver.service.common;

import com.fasterxml.jackson.databind.JsonNode;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.ReferenceType;

public interface ReferenceParser {


    /**
     * Parse json node representing dataset reference
     *
     * @param referenceNode dataset reference node
     * @return dataset reference, or null if referenceNode is null, missing or node has wrong format
     */
    IReference parseReference(JsonNode referenceNode, ReferenceType defaultType);
}
