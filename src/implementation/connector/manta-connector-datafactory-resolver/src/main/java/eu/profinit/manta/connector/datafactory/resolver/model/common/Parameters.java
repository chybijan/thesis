package eu.profinit.manta.connector.datafactory.resolver.model.common;

import eu.profinit.manta.connector.datafactory.model.common.IParameterSpecification;
import eu.profinit.manta.connector.datafactory.model.common.IParameters;

import java.util.Map;

/**
 * Implementation of parameters specification for Azure Data Factory resource.
 */
public class Parameters implements IParameters {

    /**
     * Actual data, where a key is the name of the parameter, and value is parameter specification.
     */
    private final Map<String, IParameterSpecification> data;

    /**
     * @param data map, where a key is the name of the parameter, and value is parameter specification
     */
    public Parameters(Map<String, IParameterSpecification> data) {
        this.data = data;
    }

    @Override
    public IParameterSpecification get(String name) {
        if (data != null && name != null) {
            return data.getOrDefault(name, null);
        }
        return null;
    }

    @Override
    public int size() {
        if (data != null) {
            return data.size();
        }
        return 0;
    }
}
