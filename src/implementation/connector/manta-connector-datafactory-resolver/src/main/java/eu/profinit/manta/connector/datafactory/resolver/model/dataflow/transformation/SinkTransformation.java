package eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ISinkTransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.ITable;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class SinkTransformation extends AbstractTransformation implements ISinkTransformation {

    public SinkTransformation(String name, Set<String> substreams,
                              Collection<IADFStream> inputs, Collection<String> outputs, ITable table,
                              Map<String, IExpression> attributes) {
        super(name, substreams, inputs, outputs, table, attributes);
    }

}
