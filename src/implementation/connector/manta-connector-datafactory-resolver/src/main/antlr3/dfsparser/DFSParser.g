parser grammar DFSParser;

options {
    output = AST;
    memoize = true;
    superClass = AbstractDFSParser;
    tokenVocab = DFSLexer;
}

import DFSNonReserveredKW;

@header {
    package eu.profinit.manta.connector.datafactory.resolver.dfsparser.parser;

    import eu.profinit.manta.platform.logging.api.logging.Logger;

    import eu.profinit.manta.ast.parser.base.MantaAbstractParser;
    import eu.profinit.manta.ast.token.impl.SqlMantaToken;

    import eu.profinit.manta.connector.datafactory.resolver.dfsparser.AbstractDFSParser;
    import eu.profinit.manta.connector.datafactory.resolver.dfsparser.ast.*;

}

@members {

    private DFSParser gDFSParser = this;

}

//@rulecatch {
//    catch (RecognitionException exc) {
//        reportError(exc);
//        BitSet follow = computeErrorRecoverySet();
//
//        recoverInner(input, follow, exc);
//        retval.stop = input.LT(-1);
//        retval.tree = (CommonTree)createErrorNode(input, retval.start, retval.stop, exc);
//    }
//}


//// Begin ////////////////////////////////////////////////////////////////////////////////////////////////////////////

dfs
    :   param=parameters? trans=transformations?
        ->  ^(AST_DFS<AstDFS>[contextState] $param? $trans?)
    ;

parameters
    :   KW_PARAMETERS LEFT_BRACE (par1=parameter (COMMA par2+=parameter)*)? RIGHT_BRACE
        ->  ^(AST_PARAMETERS KW_PARAMETERS LEFT_BRACE ($par1 (COMMA $par2)*)? RIGHT_BRACE)
    ;

parameter
    :   id=all_identifier KW_AS dt=data_type dfe=df_expression?
        ->  ^(AST_PARAMETER<AstParameter>[contextState] ^(AST_PARAMETER_NAME<AstIdentifier>[contextState] $id) KW_AS ^(AST_DATA_TYPE<AstDataType>[contextState] $dt) ^(AST_PARAMETER_DEFAULT_VALUE $dfe?))
    ;

data_type
    :   KW_INTEGER
    |   KW_BOOLEAN
    |   KW_DATE
    |   KW_TIMESTAMP
    |   KW_ANY
    |   KW_SHORT
    |   KW_DOUBLE
    |   KW_FLOAT
    |   KW_LONG
    |   DECIMAL_TYPE
    |   KW_STRING
    |   ARRAY_INTEGER
    |   ARRAY_BOOLEAN
    |   ARRAY_DATE
    |   ARRAY_TIMESTAMP
    |   ARRAY_ANY
    |   ARRAY_SHORT
    |   ARRAY_DOUBLE
    |   ARRAY_FLOAT
    |   ARRAY_LONG
    |   ARRAY_DECIMAL
    |   ARRAY_STRING
    |   param_map_data_type
    |   LEFT_PARENT RIGHT_PARENT
    ;

param_map_data_type
    :   LEFT_BRACKET key=data_type COMMA value=data_type RIGHT_BRACKET
        -> ^(AST_MAP_DATA_TYPE LEFT_BRACKET ^(AST_MAP_KEY_TYPE $key) COMMA ^(AST_MAP_VALUE_TYPE $value) RIGHT_BRACKET)
    ;

transformations
    :   (tran+=transformation OUTPUT_STREAM sn+=stream_name)+
        -> ^(AST_TRANSFORMATIONS ^(AST_TRANSFORMATION<AstTransformation>[contextState] $tran OUTPUT_STREAM $sn)+ )
    ;

inputStreams
    :   sn1=stream_name (COMMA sn2+=stream_name)*
        -> ^(AST_INPUT_STREAMS $sn1 (COMMA $sn2)*)
    ;

stream_name
    :   id1=all_identifier (AT_SIGN ssn=second_stream_name)?
        -> ^(AST_STREAM_NAME<AstADFStream>[contextState] ^(AST_TRANSFORMATION_NAME<AstIdentifier>[contextState] $id1) (AT_SIGN $ssn)? )
    ;

second_stream_name
    :   id=all_identifier
        -> ^(AST_SUBSTREAM_NAME<AstIdentifier>[contextState] $id)
    |   LEFT_PARENT id1=all_identifier (COMMA id2+=all_identifier)* RIGHT_PARENT
        -> LEFT_PARENT ^(AST_SUBSTREAM_NAME<AstIdentifier>[contextState]  $id1) (COMMA ^(AST_SUBSTREAM_NAME<AstIdentifier>[contextState] $id2))* RIGHT_PARENT
    ;

transformation
    :   st=source_transformation
        -> $st
    |   is=inputStreams twi=transformation_with_inputs
        -> $is $twi
    ;

source_transformation
    :   KW_SOURCE  ta=transformation_attributes
        -> ^(AST_CONCRETE_TRANSFORMATION<AstSourceTransformation>[contextState] KW_SOURCE $ta)
    ;

transformation_with_inputs
    :   ft=filter_transformation
        -> $ft
    |   jt=join_transformation
        -> $jt
    |   st=sink_transformation
        -> $st
    |   gt=general_transformation
        -> $gt
    ;

filter_transformation
    :   KW_FILTER LEFT_PARENT fc=df_expression (COMMA cta=common_transformation_attributes)? RIGHT_PARENT
        -> ^(AST_CONCRETE_TRANSFORMATION<AstFilterTransformation>[contextState] KW_FILTER LEFT_PARENT $fc (COMMA $cta)? RIGHT_PARENT)
    ;

join_transformation
    :   KW_JOIN LEFT_PARENT fc=df_expression (COMMA cta=common_transformation_attributes)? RIGHT_PARENT
        -> ^(AST_CONCRETE_TRANSFORMATION<AstJoinTransformation>[contextState] KW_JOIN LEFT_PARENT $fc (COMMA $cta)? RIGHT_PARENT)
    ;

sink_transformation
    :   KW_SINK ta=transformation_attributes
        -> ^(AST_CONCRETE_TRANSFORMATION<AstSinkTransformation>[contextState] KW_SINK $ta)
    ;

general_transformation
    :   SIMPLE_ID ta=transformation_attributes
        -> ^(AST_CONCRETE_TRANSFORMATION<AstGeneralTransformation>[contextState] SIMPLE_ID $ta)
    ;

transformation_attributes
    :   LEFT_PARENT cta=common_transformation_attributes? RIGHT_PARENT
        -> LEFT_PARENT $cta? RIGHT_PARENT
    ;

common_transformation_attributes
    :   ta1=transformation_attribute (COMMA ta2+=transformation_attribute)*
        -> ^(AST_TRANSFORMATION_ATTRIBUTES<AstTransformationAttributes>[contextState] $ta1 (COMMA $ta2)*)
    ;

transformation_attribute
    :   oa=output_attribute
        -> $oa
    |   id=all_identifier COLON de=df_expression
        -> ^(AST_TRANSFORMATION_ATTRIBUTE<AstTransformationAttribute>[contextState] ^(AST_ATTRIBUTE_NAME<AstIdentifier>[contextState] $id) COLON $de)
    |   de=df_expression
        -> $de
    ;

output_attribute
    :   KW_OUTPUT LEFT_PARENT cd=column_definitions? RIGHT_PARENT
        -> ^(AST_OUTPUT_ATTRIBUTE<AstOutputAttribute>[contextState] KW_OUTPUT LEFT_PARENT $cd? RIGHT_PARENT)
    ;

column_definitions
    :   cd1=column_definition (COMMA cd2+=column_definition)*
        -> ^(AST_COLUMN_DEFINITIONS $cd1 (COMMA $cd2)*)
    ;

column_definition
    :   cn=column_name KW_AS dt=data_type (STRING_LITERAL)?
        -> ^(AST_COLUMN_DEFINITION<AstColumnDefinition>[contextState] $cn KW_AS ^(AST_DATA_TYPE<AstDataType>[contextState] $dt) ^(AST_DATA_TYPE_FORMAT<AstDataTypeFormat>[contextState] STRING_LITERAL)?)
    ;

column_name
    :   id=all_identifier
        -> ^(AST_COLUMN_NAME<AstIdentifier>[contextState] $id)
    |   LEFT_BRACE cnib=column_name_inside_braces? RIGHT_BRACE
        -> LEFT_BRACE ^(AST_COLUMN_NAME<AstIdentifier>[contextState] $cnib?) RIGHT_BRACE
    ;

column_name_inside_braces
    :   (~RIGHT_BRACE)+
    ;

// Data flow expressions
df_expression
    :   oe=or_expression
        -> ^(AST_EXPRESSION<AstExpression>[contextState] $oe)
    ;

or_expression
    :   (and_expression
            -> and_expression)
        (BAR BAR ae=and_expression
            -> ^(AST_OR_EXPRESSION ^(AST_LEFT_OPERAND $or_expression) BAR BAR ^(AST_RIGHT_OPERAND $ae)))*
    ;

and_expression
    :   (not_expression
            -> not_expression)
        (AMPERSAND AMPERSAND ne=not_expression
            -> ^(AST_AND_EXPRESSION ^(AST_LEFT_OPERAND $and_expression) AMPERSAND AMPERSAND ^(AST_RIGHT_OPERAND $ne)))*
    ;

not_expression
   :	EXCLAMATION_MARK ne=not_expression
        -> ^(AST_NOT_EXPRESSION EXCLAMATION_MARK ^(AST_OPERAND $ne))
   |	ce=compare_expression
        -> $ce
   ;

compare_expression
    :   (addition_expression
            -> addition_expression)
        (co=compare_operator ae=addition_expression
            -> ^(AST_COMPARE_EXPRESSION ^(AST_LEFT_OPERAND $compare_expression) ^(AST_COMPARE_OPERATOR $co) ^(AST_RIGHT_OPERAND $ae)))*
    ;

compare_operator
    :   EQUALS EQUALS
    |   EQUALS EQUALS EQUALS
    |   LESS_THAN EQUALS GREATER_THAN
    |   EXCLAMATION_MARK EQUALS
    |   GREATER_THAN
    |   LESS_THAN
    |   GREATER_THAN EQUALS
    |   LESS_THAN EQUALS
    ;

addition_expression
    :   (mult_expression
            -> mult_expression)
        (ao=addition_operator me=mult_expression
            -> ^(AST_ADDITION_EXPRESSION ^(AST_LEFT_OPERAND $addition_expression) ^(AST_ADDITION_OPERATOR $ao) ^(AST_RIGHT_OPERAND $me)))*
    ;

addition_operator
    :   PLUS_SIGN
    |   MINUS_SIGN
    ;

mult_expression
    :   (pow_expression
            -> pow_expression)
        (mo=mult_operator pe=pow_expression
            -> ^(AST_MULT_EXPRESSION ^(AST_LEFT_OPERAND $mult_expression) ^(AST_MULT_OPERATOR $mo) ^(AST_RIGHT_OPERAND $pe)))*
    ;

mult_operator
    :   ASTERISK
    |   SLASH
    ;

pow_expression
    :	(unary_expression
            -> unary_expression)
        (CARET ue=unary_expression
            -> ^(AST_POW_EXPRESSION $pow_expression CARET ^(AST_OPERAND $ue)))*
    ;

unary_expression
    :   uo=unary_operator ue=unary_expression
        -> ^(AST_UNARY_EXPRESSION ^(AST_UNARY_OPERATOR $uo) ^(AST_OPERAND $ue))
    |   del=df_expression_literal
        -> $del
    ;

unary_operator
    :   PLUS_SIGN
    |   MINUS_SIGN
    ;

df_expression_literal
    :   STRING_LITERAL
        -> ^(AST_STRING_LITERAL STRING_LITERAL)
    |   INTEGER_LITERAL
        -> ^(AST_INTEGER_LITERAL INTEGER_LITERAL)
    |   FLOATING_POINT_LITERAL
        -> ^(AST_FLOATING_POINT_LITERAL FLOATING_POINT_LITERAL)
    |   DOLLAR_SIGN SIMPLE_ID
        -> ^(AST_PARAMETER_INVOCATION DOLLAR_SIGN SIMPLE_ID)
    |   al=df_expression_array_literal
        -> $al
    |   def=df_expression_function
        -> $def
    |   id=identifier
        -> ^(AST_COLUMN_REFERENCE $id)
    |   LEFT_PARENT de=df_expression? RIGHT_PARENT
        -> LEFT_PARENT $de? RIGHT_PARENT
    ;

df_expression_array_literal
    :   LEFT_BRACKET (el1=df_expression (COMMA el2+=df_expression)*)? RIGHT_BRACKET
        -> ^(AST_ARRAY_LITERAL LEFT_BRACKET (^(AST_ARRAY_ELEMENT $el1) (COMMA ^(AST_ARRAY_ELEMENT $el2))* )? RIGHT_BRACKET)
    ;

// TODO function
df_expression_function
    :   KW_FLOAT
        -> ^(AST_EXPRESSION_FUNCTION)
    ;
