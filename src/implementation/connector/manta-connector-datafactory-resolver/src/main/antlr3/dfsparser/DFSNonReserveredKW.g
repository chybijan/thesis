parser grammar DFSNonReserveredKW;

//-----------------------------------------------------------------------------------------------------------------------------------------

options {
	output = AST;
	memoize=true;
	backtrack=true;
}

//-----------------------------------------------------------------------------------------------------------------------------------------

@members {
}

//@rulecatch {
//	catch (RecognitionException exc) {
//        reportError(exc);
//		BitSet follow = computeErrorRecoverySet();
//
//		gDFSParser.recoverInner(input, follow, exc);
//		retval.stop = input.LT(-1);
//		retval.tree = (CommonTree) createErrorNode(input, retval.start, retval.stop, exc);
//	}
//}

identifier
	:	SIMPLE_ID
	|	non_reserved_words
	;

all_identifier
	:	identifier
	|	reserved_words
	;

non_reserved_words // CHECK NON-RESERVED
	: KW_PARAMETERS
	| KW_SOURCE
    | KW_AS
    | KW_INTEGER
    | KW_BOOLEAN
    | KW_DATE
    | KW_TIMESTAMP
    | KW_ANY
    | KW_SHORT
    | KW_DOUBLE
    | KW_FLOAT
    | KW_LONG
    | KW_DECIMAL
    | KW_STRING
    | KW_SOURCE
    | KW_OUTPUT
    | KW_SINK
    | KW_STRING
    | KW_FILTER
    | KW_TRUE
    | KW_FALSE
    | KW_JOIN
   	;

reserved_words // CHECK RESERVED
	:
	;

