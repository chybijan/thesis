lexer grammar DFSLexer;

options {
    superClass = MantaAbstractLexer;
}

//-----------------------------------------------------------------------------------------------------------------------------------------

tokens {

    AST_ERROR;

    AST_DFS;

    AST_DATA_TYPE;

    AST_PARAMETERS;
    AST_PARAMETER;
    AST_PARAMETER_NAME;
    AST_PARAMETER_DEFAULT_VALUE;

    AST_MAP_DATA_TYPE;
    AST_MAP_KEY_TYPE;
    AST_MAP_VALUE_TYPE;

    AST_TRANSFORMATIONS;
    AST_TRANSFORMATION;
    AST_INPUT_STREAMS;
    AST_STREAM_NAME;
    AST_TRANSFORMATION_NAME;
    AST_SUBSTREAM_NAME;

    AST_TRANSFORMATION_ATTRIBUTES;
    AST_TRANSFORMATION_ATTRIBUTE;
    AST_ATTRIBUTE_NAME;

    AST_OUTPUT_ATTRIBUTE;
    AST_COLUMN_DEFINITIONS;
    AST_COLUMN_DEFINITION;
    AST_COLUMN_NAME;
    AST_DATA_TYPE_FORMAT;

    AST_CONCRETE_TRANSFORMATION;

    AST_EXPRESSION;

    AST_EXPRESSION_LITERAL;
    AST_EXPRESSION_FUNCTION;

    AST_OR_EXPRESSION;
    AST_LEFT_OPERAND;
    AST_RIGHT_OPERAND;
    AST_OPERAND;
    AST_AND_EXPRESSION;
    AST_NOT_EXPRESSION;
    AST_COMPARE_EXPRESSION;
    AST_COMPARE_OPERATOR;
    AST_ADDITION_EXPRESSION;
    AST_ADDITION_OPERATOR;
    AST_MULT_EXPRESSION;
    AST_MULT_OPERATOR;
    AST_POW_EXPRESSION;
    AST_UNARY_EXPRESSION;
    AST_UNARY_OPERATOR;

    AST_COLUMN_REFERENCE;
    AST_STRING_LITERAL;
    AST_INTEGER_LITERAL;
    AST_FLOATING_POINT_LITERAL;
    AST_PARAMETER_INVOCATION;

    AST_ARRAY_LITERAL;
    AST_ARRAY_ELEMENT;

}

//-----------------------------------------------------------------------------------------------------------------------------------------

@header {
	package eu.profinit.manta.connector.datafactory.resolver.dfsparser.parser;

    import eu.profinit.manta.ast.parser.base.MantaAbstractLexer;
}


// BEGIN CHECK KEYWORDS

// Nonreserved keywords
KW_PARAMETERS : 'PARAMETERS';
KW_AS :         'AS';

KW_INTEGER :    'INTEGER';
KW_BOOLEAN :    'BOOLEAN';
KW_DATE :       'DATE';
KW_TIMESTAMP :  'TIMESTAMP';
KW_ANY :        'ANY';
KW_SHORT :      'SHORT';
KW_DOUBLE :     'DOUBLE';
KW_FLOAT :      'FLOAT';
KW_LONG :       'LONG';
KW_DECIMAL :    'DECIMAL';
KW_STRING :     'STRING';

KW_TRUE : 'TRUE';
KW_FALSE : 'FALSE';

// Source transformation
KW_SOURCE :      'SOURCE';
KW_OUTPUT :      'OUTPUT';

// Sink transformation
KW_SINK   :      'SINK';

// Filter transformation
KW_FILTER :     'FILTER';

// Join transformation
KW_JOIN   :     'JOIN';

// END CHECK KEYWORDS

fragment DIGIT :			'0'..'9';
fragment LETTER :			'a'..'z' | 'A'..'Z';

fragment CR :				'\r';
fragment LF :				'\n';
fragment TAB :				'\t';

fragment BLANK :			' ' | TAB | '\u00A0'; // &nbsp;

fragment ANY_CHAR :         ~'a' | 'a';

STRING_LITERAL :			APOSTROPHE (~(APOSTROPHE | BACK_SLASH) | (APOSTROPHE APOSTROPHE) | (BACK_SLASH ANY_CHAR))* APOSTROPHE
               |            QUOTATION_MARK (~(QUOTATION_MARK | BACK_SLASH) | (QUOTATION_MARK QUOTATION_MARK) | (BACK_SLASH ANY_CHAR))* QUOTATION_MARK
               ;

NEWLINE
	:	( CR LF | CR | LF ) { $channel = HIDDEN; }
	;

WHITESPACE
    :   (BLANK)+
        { $channel = HIDDEN; }
    ;

UNICODE_BOM
    :   '\uFEFF'
        { $channel = HIDDEN; }
    ;

INTEGER_LITERAL
    : (MINUS_SIGN | PLUS_SIGN)? DIGIT+
    ;

// This allows even -12.-34e-56 which is not correct, but it should suffice
FLOATING_POINT_LITERAL
    : (MINUS_SIGN | PLUS_SIGN)? DIGIT* PERIOD INTEGER_LITERAL ( ('e' | 'E') INTEGER_LITERAL)?
    | (MINUS_SIGN | PLUS_SIGN)? DIGIT* ('e' | 'E') INTEGER_LITERAL
    ;

// Stream name: "alphanumeric characters are supported and must start with an alpha character"
// Parameter name is the same
SIMPLE_ID
    : LETTER (LETTER | DIGIT | UNDERSCORE)*
    ;

// I have seprated array data types from KW_DATATYPE
// because when parameter uses data type, it can specify default value via expression
// [] is valid expression, so it had to be separated
ARRAY_INTEGER :    'INTEGER[]';
ARRAY_BOOLEAN :    'BOOLEAN[]';
ARRAY_DATE :       'DATE[]';
ARRAY_TIMESTAMP :  'TIMESTAMP[]';
ARRAY_ANY :        'ANY[]';
ARRAY_SHORT :      'SHORT[]';
ARRAY_DOUBLE :     'DOUBLE[]';
ARRAY_FLOAT :      'FLOAT[]';
ARRAY_LONG :       'LONG[]';
ARRAY_DECIMAL :    'DECIMAL[]';
ARRAY_STRING :     'STRING[]';

DECIMAL_TYPE
    : KW_DECIMAL LEFT_PARENT (DIGIT)* COMMA (DIGIT)* RIGHT_PARENT
    ;

AT_SIGN :			'@';
DOLLAR_SIGN:        '$';
LEFT_PARENT :		'(';
RIGHT_PARENT :		')';
LEFT_BRACKET :		'[';
RIGHT_BRACKET :		']';
LEFT_BRACE :		'{';
RIGHT_BRACE :		'}';
COLON :				':';
PERIOD :			'.';
COMMA :				',';
MINUS_SIGN :		'-';
PLUS_SIGN :			'+';
BACK_SLASH:         '\\';
APOSTROPHE :		'\'';
QUOTATION_MARK :	'"';
OUTPUT_STREAM :     '~>';
UNDERSCORE :        '_';
EXCLAMATION_MARK :	'!';
ASTERISK :			'*';
CARET :				'^';
AMPERSAND :			'&';
BAR :				'|';
SLASH :				'/';
PERCENT_SIGN :		'%';
SEMICOLON :			';';
QUESTION_MARK :		'?';
EQUALS :			'=';
GREATER_THAN :		'>';
LESS_THAN :			'<';
CROSSHATCH :		'#';