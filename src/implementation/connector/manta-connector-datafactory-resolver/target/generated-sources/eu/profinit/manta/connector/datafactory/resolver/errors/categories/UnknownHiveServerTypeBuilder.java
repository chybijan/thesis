package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnknownHiveServerTypeBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.UnknownHiveServerTypeBuilder> {
  private static final String USER_MESSAGE = "Unknown Hive server type %{hiveServerType} for linked service \"%{linkedServiceName}\".";

  private static final String TECHNICAL_MESSAGE = "Unknown Hive server type %{hiveServerType} for linked service \"%{linkedServiceName}\".";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNKNOWN_HIVE_SERVER_TYPE";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnknownHiveServerTypeBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnknownHiveServerTypeBuilder getThis() {
    return this;
  }

  public UnknownHiveServerTypeBuilder linkedServiceName(Object value) {
    putArg("linkedServiceName", value);
    return this;
  }

  public UnknownHiveServerTypeBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnknownHiveServerTypeBuilder hiveServerType(Object value) {
    putArg("hiveServerType", value);
    return this;
  }
}
