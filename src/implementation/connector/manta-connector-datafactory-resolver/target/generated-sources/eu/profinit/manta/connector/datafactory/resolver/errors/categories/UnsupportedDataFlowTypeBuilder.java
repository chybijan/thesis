package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnsupportedDataFlowTypeBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.UnsupportedDataFlowTypeBuilder> {
  private static final String USER_MESSAGE = "Data flow type \"%{type}\" is not supported by Manta. Data flow resource cannot be parsed";

  private static final String TECHNICAL_MESSAGE = "Data flow type \"%{type}\" is not supported by Manta. Data flow resource cannot be parsed";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNSUPPORTED_DATA_FLOW_TYPE";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnsupportedDataFlowTypeBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnsupportedDataFlowTypeBuilder getThis() {
    return this;
  }

  public UnsupportedDataFlowTypeBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnsupportedDataFlowTypeBuilder type(Object value) {
    putArg("type", value);
    return this;
  }
}
