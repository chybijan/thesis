package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class MalformedAstBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.MalformedAstBuilder> {
  private static final String USER_MESSAGE = "%{message}. This error most likely happened because of previous errors.";

  private static final String TECHNICAL_MESSAGE = "%{message}. This error most likely happened because of previous errors.";

  private static final String SOLUTION = "Check the log for parsing errors and check that the input is correct. Otherwise please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "MALFORMED_AST";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  MalformedAstBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected MalformedAstBuilder getThis() {
    return this;
  }

  public MalformedAstBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public MalformedAstBuilder message(Object value) {
    putArg("message", value);
    return this;
  }
}
