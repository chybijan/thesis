package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class InvalidTransformationAttributeValueBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.InvalidTransformationAttributeValueBuilder> {
  private static final String USER_MESSAGE = "Data flow transformation attribute named: '%{name}' has invalid value: '%{value}'.Expected format: '%{expected}'";

  private static final String TECHNICAL_MESSAGE = "Data flow transformation attribute named: '%{name}' has invalid value: '%{value}'.Expected format: '%{expected}'";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "INVALID_TRANSFORMATION_ATTRIBUTE_VALUE";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  InvalidTransformationAttributeValueBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected InvalidTransformationAttributeValueBuilder getThis() {
    return this;
  }

  public InvalidTransformationAttributeValueBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public InvalidTransformationAttributeValueBuilder expected(Object value) {
    putArg("expected", value);
    return this;
  }

  public InvalidTransformationAttributeValueBuilder name(Object value) {
    putArg("name", value);
    return this;
  }

  public InvalidTransformationAttributeValueBuilder value(Object value) {
    putArg("value", value);
    return this;
  }
}
