package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnknownLinkedServiceTypeBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.UnknownLinkedServiceTypeBuilder> {
  private static final String USER_MESSAGE = "Unknown type %{linkedServiceType} for linked service \"%{linkedServiceName}\". %{defaultLinkedServiceType} is used instead.";

  private static final String TECHNICAL_MESSAGE = "Unknown type %{linkedServiceType} for linked service \"%{linkedServiceName}\". %{defaultLinkedServiceType} is used instead.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNKNOWN_LINKED_SERVICE_TYPE";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnknownLinkedServiceTypeBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnknownLinkedServiceTypeBuilder getThis() {
    return this;
  }

  public UnknownLinkedServiceTypeBuilder linkedServiceType(Object value) {
    putArg("linkedServiceType", value);
    return this;
  }

  public UnknownLinkedServiceTypeBuilder linkedServiceName(Object value) {
    putArg("linkedServiceName", value);
    return this;
  }

  public UnknownLinkedServiceTypeBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnknownLinkedServiceTypeBuilder defaultLinkedServiceType(Object value) {
    putArg("defaultLinkedServiceType", value);
    return this;
  }
}
