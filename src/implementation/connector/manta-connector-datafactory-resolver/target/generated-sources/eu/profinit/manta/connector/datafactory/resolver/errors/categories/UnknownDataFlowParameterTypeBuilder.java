package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnknownDataFlowParameterTypeBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.UnknownDataFlowParameterTypeBuilder> {
  private static final String USER_MESSAGE = "Unknown type \"%{parameterType}\" for parameter \"%{parameterName}\". Parameter type is not used during analysis";

  private static final String TECHNICAL_MESSAGE = "Unknown type %{parameterType} for parameter \"%{parameterName}\". Parameter type is not used during analysis";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNKNOWN_DATA_FLOW_PARAMETER_TYPE";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnknownDataFlowParameterTypeBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnknownDataFlowParameterTypeBuilder getThis() {
    return this;
  }

  public UnknownDataFlowParameterTypeBuilder parameterType(Object value) {
    putArg("parameterType", value);
    return this;
  }

  public UnknownDataFlowParameterTypeBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnknownDataFlowParameterTypeBuilder parameterName(Object value) {
    putArg("parameterName", value);
    return this;
  }
}
