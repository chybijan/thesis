package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class MissingDatasetReferenceNameBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.MissingDatasetReferenceNameBuilder> {
  private static final String USER_MESSAGE = "Dataset reference is missing name property.";

  private static final String TECHNICAL_MESSAGE = "Dataset reference is missing name property in JSON: \n"
      + "\"%{referenceNode}\"\n"
      + ".";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "MISSING_DATASET_REFERENCE_NAME";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  MissingDatasetReferenceNameBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected MissingDatasetReferenceNameBuilder getThis() {
    return this;
  }

  public MissingDatasetReferenceNameBuilder referenceNode(Object value) {
    putArg("referenceNode", value);
    return this;
  }

  public MissingDatasetReferenceNameBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }
}
