package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnknownGlobalParameterTypeBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.UnknownGlobalParameterTypeBuilder> {
  private static final String USER_MESSAGE = "Unknown type %{parameterType} for global parameter \"%{parameterName}\". %{usedParameterType} is used instead.";

  private static final String TECHNICAL_MESSAGE = "Unknown type %{parameterType} for global parameter \"%{parameterName}\". %{usedParameterType} is used instead.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNKNOWN_GLOBAL_PARAMETER_TYPE";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnknownGlobalParameterTypeBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnknownGlobalParameterTypeBuilder getThis() {
    return this;
  }

  public UnknownGlobalParameterTypeBuilder parameterType(Object value) {
    putArg("parameterType", value);
    return this;
  }

  public UnknownGlobalParameterTypeBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnknownGlobalParameterTypeBuilder usedParameterType(Object value) {
    putArg("usedParameterType", value);
    return this;
  }

  public UnknownGlobalParameterTypeBuilder parameterName(Object value) {
    putArg("parameterName", value);
    return this;
  }
}
