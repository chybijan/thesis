package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnsupportedDatasetTypeBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.UnsupportedDatasetTypeBuilder> {
  private static final String USER_MESSAGE = "Type %{datasetType} of the dataset \"%{datasetName}\" is not supported by Manta. The processing of type specific fields is skipped.";

  private static final String TECHNICAL_MESSAGE = "Type %{datasetType} of the dataset \"%{datasetName}\" is not supported by Manta. The processing of type specific fields is skipped.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNSUPPORTED_DATASET_TYPE";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.WARN;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnsupportedDatasetTypeBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnsupportedDatasetTypeBuilder getThis() {
    return this;
  }

  public UnsupportedDatasetTypeBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnsupportedDatasetTypeBuilder datasetName(Object value) {
    putArg("datasetName", value);
    return this;
  }

  public UnsupportedDatasetTypeBuilder datasetType(Object value) {
    putArg("datasetType", value);
    return this;
  }
}
