package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnknownDatasetTypeBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.UnknownDatasetTypeBuilder> {
  private static final String USER_MESSAGE = "Unknown type %{datasetType} for dataset \"%{datasetName}\". %{defaultDatasetType} is used instead.";

  private static final String TECHNICAL_MESSAGE = "Unknown type %{datasetType} for dataset \"%{datasetName}\". %{defaultDatasetType} is used instead.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNKNOWN_DATASET_TYPE";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnknownDatasetTypeBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnknownDatasetTypeBuilder getThis() {
    return this;
  }

  public UnknownDatasetTypeBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnknownDatasetTypeBuilder datasetName(Object value) {
    putArg("datasetName", value);
    return this;
  }

  public UnknownDatasetTypeBuilder datasetType(Object value) {
    putArg("datasetType", value);
    return this;
  }

  public UnknownDatasetTypeBuilder defaultDatasetType(Object value) {
    putArg("defaultDatasetType", value);
    return this;
  }
}
