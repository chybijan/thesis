package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnknownDataFlowTypeBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.UnknownDataFlowTypeBuilder> {
  private static final String USER_MESSAGE = "Unknown data flow type \"%{typeName}\". %{defaultType} is used instead.";

  private static final String TECHNICAL_MESSAGE = "Unknown data flow type \"%{typeName}\". %{defaultType} is used instead.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNKNOWN_DATA_FLOW_TYPE";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnknownDataFlowTypeBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnknownDataFlowTypeBuilder getThis() {
    return this;
  }

  public UnknownDataFlowTypeBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnknownDataFlowTypeBuilder typeName(Object value) {
    putArg("typeName", value);
    return this;
  }

  public UnknownDataFlowTypeBuilder defaultType(Object value) {
    putArg("defaultType", value);
    return this;
  }
}
