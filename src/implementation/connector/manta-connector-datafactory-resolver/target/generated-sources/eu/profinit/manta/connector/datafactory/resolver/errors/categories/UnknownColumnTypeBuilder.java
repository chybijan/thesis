package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnknownColumnTypeBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.UnknownColumnTypeBuilder> {
  private static final String USER_MESSAGE = "Unknown type %{columnType} for column \"%{columnName}\". %{usedColumnType} is used instead.";

  private static final String TECHNICAL_MESSAGE = "Unknown type %{columnType} for column \"%{columnName}\". %{usedColumnType} is used instead.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNKNOWN_COLUMN_TYPE";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnknownColumnTypeBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnknownColumnTypeBuilder getThis() {
    return this;
  }

  public UnknownColumnTypeBuilder columnType(Object value) {
    putArg("columnType", value);
    return this;
  }

  public UnknownColumnTypeBuilder usedColumnType(Object value) {
    putArg("usedColumnType", value);
    return this;
  }

  public UnknownColumnTypeBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnknownColumnTypeBuilder columnName(Object value) {
    putArg("columnName", value);
    return this;
  }
}
