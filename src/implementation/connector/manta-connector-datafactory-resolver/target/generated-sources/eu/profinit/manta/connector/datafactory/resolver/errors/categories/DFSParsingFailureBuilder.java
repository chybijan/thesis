package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class DFSParsingFailureBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.DFSParsingFailureBuilder> {
  private static final String USER_MESSAGE = "Failed to parse the input '%{input}'.";

  private static final String TECHNICAL_MESSAGE = "Failed to parse the input '%{input}'.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "DFSPARSING_FAILURE";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  DFSParsingFailureBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected DFSParsingFailureBuilder getThis() {
    return this;
  }

  public DFSParsingFailureBuilder input(Object value) {
    putArg("input", value);
    return this;
  }

  public DFSParsingFailureBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }
}
