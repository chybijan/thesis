package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class MissingSchemaLinkedServiceReferenceOfEndpointBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.MissingSchemaLinkedServiceReferenceOfEndpointBuilder> {
  private static final String USER_MESSAGE = "Endpoint \"%{endpointName}\" is missing schema linked service reference.";

  private static final String TECHNICAL_MESSAGE = "Endpoint \"%{endpointName}\" is missing schema linked service reference.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "MISSING_SCHEMA_LINKED_SERVICE_REFERENCE_OF_ENDPOINT";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.WARN;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  MissingSchemaLinkedServiceReferenceOfEndpointBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected MissingSchemaLinkedServiceReferenceOfEndpointBuilder getThis() {
    return this;
  }

  public MissingSchemaLinkedServiceReferenceOfEndpointBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public MissingSchemaLinkedServiceReferenceOfEndpointBuilder endpointName(Object value) {
    putArg("endpointName", value);
    return this;
  }
}
