package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnknownDataFlowPropertiesBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.UnknownDataFlowPropertiesBuilder> {
  private static final String USER_MESSAGE = "Data flow properties of resource named \"%{resourceName}\" couldn't be loaded. Data flow resource cannot be parsed.";

  private static final String TECHNICAL_MESSAGE = "Data flow properties of resource named \"%{resourceName}\" couldn't be loaded. Data flow resource cannot be parsed.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNKNOWN_DATA_FLOW_PROPERTIES";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnknownDataFlowPropertiesBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnknownDataFlowPropertiesBuilder getThis() {
    return this;
  }

  public UnknownDataFlowPropertiesBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnknownDataFlowPropertiesBuilder resourceName(Object value) {
    putArg("resourceName", value);
    return this;
  }
}
