package eu.profinit.manta.connector.datafactory.resolver.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnsupportedLinkedServiceTypeBuilder extends AbstractLoggingBuilder<eu.profinit.manta.connector.datafactory.resolver.errors.categories.UnsupportedLinkedServiceTypeBuilder> {
  private static final String USER_MESSAGE = "Type %{linkedServiceType} of the linked service \"%{linkedServiceName}\" is not supported by Manta. The processing of type specific fields is skipped.";

  private static final String TECHNICAL_MESSAGE = "Type %{linkedServiceType} of the linked service \"%{linkedServiceName}\" is not supported by Manta. The processing of type specific fields is skipped.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNSUPPORTED_LINKED_SERVICE_TYPE";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.WARN;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnsupportedLinkedServiceTypeBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnsupportedLinkedServiceTypeBuilder getThis() {
    return this;
  }

  public UnsupportedLinkedServiceTypeBuilder linkedServiceType(Object value) {
    putArg("linkedServiceType", value);
    return this;
  }

  public UnsupportedLinkedServiceTypeBuilder linkedServiceName(Object value) {
    putArg("linkedServiceName", value);
    return this;
  }

  public UnsupportedLinkedServiceTypeBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }
}
