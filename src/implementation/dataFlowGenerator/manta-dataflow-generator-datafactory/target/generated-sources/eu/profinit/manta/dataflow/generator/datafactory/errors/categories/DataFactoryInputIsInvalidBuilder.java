package eu.profinit.manta.dataflow.generator.datafactory.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class DataFactoryInputIsInvalidBuilder extends AbstractLoggingBuilder<eu.profinit.manta.dataflow.generator.datafactory.errors.categories.DataFactoryInputIsInvalidBuilder> {
  private static final String USER_MESSAGE = "Azure data factory input could not be analysed";

  private static final String TECHNICAL_MESSAGE = "Azure data factory input could not be analysed";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "DATA_FACTORY_INPUT_IS_INVALID";

  private static final Impact IMPACT = Impact.SCRIPT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  DataFactoryInputIsInvalidBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected DataFactoryInputIsInvalidBuilder getThis() {
    return this;
  }

  public DataFactoryInputIsInvalidBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }
}
