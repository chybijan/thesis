package eu.profinit.manta.dataflow.generator.datafactory.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class DatasetSchemaNotProcessedCorrectlyBuilder extends AbstractLoggingBuilder<eu.profinit.manta.dataflow.generator.datafactory.errors.categories.DatasetSchemaNotProcessedCorrectlyBuilder> {
  private static final String USER_MESSAGE = "Dataset schema of dataset named: '%{datasetName}' was not processed correctly. Default file definition used.";

  private static final String TECHNICAL_MESSAGE = "Dataset schema of dataset named: '%{datasetName}' was not processed correctly. Default file definition used.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "DATASET_SCHEMA_NOT_PROCESSED_CORRECTLY";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  DatasetSchemaNotProcessedCorrectlyBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected DatasetSchemaNotProcessedCorrectlyBuilder getThis() {
    return this;
  }

  public DatasetSchemaNotProcessedCorrectlyBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public DatasetSchemaNotProcessedCorrectlyBuilder datasetName(Object value) {
    putArg("datasetName", value);
    return this;
  }
}
