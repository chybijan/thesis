package eu.profinit.manta.dataflow.generator.datafactory.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class PrecedenceResolverBuilder extends AbstractLoggingBuilder<eu.profinit.manta.dataflow.generator.datafactory.errors.categories.PrecedenceResolverBuilder> {
  private static final String USER_MESSAGE = "Ignoring precedence resolver constraint with null argument.";

  private static final String TECHNICAL_MESSAGE = "Ignoring precedence resolver constraint with null argument.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "PRECEDENCE_RESOLVER";

  private static final Impact IMPACT = Impact.UNDEFINED;

  private static final Severity SEVERITY = Severity.WARN;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  PrecedenceResolverBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected PrecedenceResolverBuilder getThis() {
    return this;
  }

  public PrecedenceResolverBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }
}
