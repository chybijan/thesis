package eu.profinit.manta.dataflow.generator.datafactory.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnexpectedLinkedServiceTypeBuilder extends AbstractLoggingBuilder<eu.profinit.manta.dataflow.generator.datafactory.errors.categories.UnexpectedLinkedServiceTypeBuilder> {
  private static final String USER_MESSAGE = "Unexpected linked service type '%{linkedServiceType}' used for location of type '%{locationType}'.";

  private static final String TECHNICAL_MESSAGE = "Unexpected linked service type '%{linkedServiceType}' used for location of type '%{locationType}'.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNEXPECTED_LINKED_SERVICE_TYPE";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnexpectedLinkedServiceTypeBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnexpectedLinkedServiceTypeBuilder getThis() {
    return this;
  }

  public UnexpectedLinkedServiceTypeBuilder linkedServiceType(Object value) {
    putArg("linkedServiceType", value);
    return this;
  }

  public UnexpectedLinkedServiceTypeBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnexpectedLinkedServiceTypeBuilder locationType(Object value) {
    putArg("locationType", value);
    return this;
  }
}
