package eu.profinit.manta.dataflow.generator.datafactory.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnsupportedDatabaseLinkedServiceBuilder extends AbstractLoggingBuilder<eu.profinit.manta.dataflow.generator.datafactory.errors.categories.UnsupportedDatabaseLinkedServiceBuilder> {
  private static final String USER_MESSAGE = "Linked service named '%{name} is not recognized as database connection.";

  private static final String TECHNICAL_MESSAGE = "Linked service named '%{name} is not recognized as database connection.";

  private static final String SOLUTION = "This functionality is not supported at this time. For more information, please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNSUPPORTED_DATABASE_LINKED_SERVICE";

  private static final Impact IMPACT = Impact.SCRIPT;

  private static final Severity SEVERITY = Severity.WARN;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnsupportedDatabaseLinkedServiceBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnsupportedDatabaseLinkedServiceBuilder getThis() {
    return this;
  }

  public UnsupportedDatabaseLinkedServiceBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnsupportedDatabaseLinkedServiceBuilder name(Object value) {
    putArg("name", value);
    return this;
  }
}
