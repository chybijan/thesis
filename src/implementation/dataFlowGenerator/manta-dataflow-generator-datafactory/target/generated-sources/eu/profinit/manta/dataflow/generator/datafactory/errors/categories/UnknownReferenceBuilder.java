package eu.profinit.manta.dataflow.generator.datafactory.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnknownReferenceBuilder extends AbstractLoggingBuilder<eu.profinit.manta.dataflow.generator.datafactory.errors.categories.UnknownReferenceBuilder> {
  private static final String USER_MESSAGE = "Unknown reference named: '%{referenceName}'. Resource referenced by this reference won't be included in lineage.";

  private static final String TECHNICAL_MESSAGE = "Unknown reference named: '%{referenceName}'. Resource referenced by this reference won't be included in lineage.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNKNOWN_REFERENCE";

  private static final Impact IMPACT = Impact.SCRIPT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnknownReferenceBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnknownReferenceBuilder getThis() {
    return this;
  }

  public UnknownReferenceBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnknownReferenceBuilder referenceName(Object value) {
    putArg("referenceName", value);
    return this;
  }
}
