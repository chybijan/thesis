package eu.profinit.manta.dataflow.generator.datafactory.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class LinkedServiceNotFoundBuilder extends AbstractLoggingBuilder<eu.profinit.manta.dataflow.generator.datafactory.errors.categories.LinkedServiceNotFoundBuilder> {
  private static final String USER_MESSAGE = "Linked service '%{name}' not found.";

  private static final String TECHNICAL_MESSAGE = "Linked service '%{name}' not found.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "LINKED_SERVICE_NOT_FOUND";

  private static final Impact IMPACT = Impact.SCRIPT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  LinkedServiceNotFoundBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected LinkedServiceNotFoundBuilder getThis() {
    return this;
  }

  public LinkedServiceNotFoundBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public LinkedServiceNotFoundBuilder name(Object value) {
    putArg("name", value);
    return this;
  }
}
