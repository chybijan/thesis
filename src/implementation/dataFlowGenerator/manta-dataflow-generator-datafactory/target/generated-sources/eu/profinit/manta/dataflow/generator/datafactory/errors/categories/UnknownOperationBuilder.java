package eu.profinit.manta.dataflow.generator.datafactory.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnknownOperationBuilder extends AbstractLoggingBuilder<eu.profinit.manta.dataflow.generator.datafactory.errors.categories.UnknownOperationBuilder> {
  private static final String USER_MESSAGE = "Unknown operation: '%{operation}' encountered. Lineage might be incomplete.";

  private static final String TECHNICAL_MESSAGE = "Unknown operation: '%{operation}' encountered. Lineage might be incomplete.";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNKNOWN_OPERATION";

  private static final Impact IMPACT = Impact.SINGLE_INPUT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnknownOperationBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnknownOperationBuilder getThis() {
    return this;
  }

  public UnknownOperationBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnknownOperationBuilder operation(Object value) {
    putArg("operation", value);
    return this;
  }
}
