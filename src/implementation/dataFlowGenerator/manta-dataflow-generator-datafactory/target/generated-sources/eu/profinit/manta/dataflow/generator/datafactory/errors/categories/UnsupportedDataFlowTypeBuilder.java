package eu.profinit.manta.dataflow.generator.datafactory.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnsupportedDataFlowTypeBuilder extends AbstractLoggingBuilder<eu.profinit.manta.dataflow.generator.datafactory.errors.categories.UnsupportedDataFlowTypeBuilder> {
  private static final String USER_MESSAGE = "Unsupported data flow type: %{type}. Skipping analysis.";

  private static final String TECHNICAL_MESSAGE = "Unsupported data flow type: %{type}. Skipping analysis.";

  private static final String SOLUTION = "This functionality is not supported at this time. For more information, please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNSUPPORTED_DATA_FLOW_TYPE";

  private static final Impact IMPACT = Impact.SCRIPT;

  private static final Severity SEVERITY = Severity.WARN;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnsupportedDataFlowTypeBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnsupportedDataFlowTypeBuilder getThis() {
    return this;
  }

  public UnsupportedDataFlowTypeBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnsupportedDataFlowTypeBuilder type(Object value) {
    putArg("type", value);
    return this;
  }
}
