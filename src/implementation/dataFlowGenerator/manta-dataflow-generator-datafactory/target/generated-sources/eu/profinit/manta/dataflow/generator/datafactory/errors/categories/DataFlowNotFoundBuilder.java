package eu.profinit.manta.dataflow.generator.datafactory.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class DataFlowNotFoundBuilder extends AbstractLoggingBuilder<eu.profinit.manta.dataflow.generator.datafactory.errors.categories.DataFlowNotFoundBuilder> {
  private static final String USER_MESSAGE = "Data flow was not found during analysis";

  private static final String TECHNICAL_MESSAGE = "Data flow was not found during analysis";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "DATA_FLOW_NOT_FOUND";

  private static final Impact IMPACT = Impact.SCRIPT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  DataFlowNotFoundBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected DataFlowNotFoundBuilder getThis() {
    return this;
  }

  public DataFlowNotFoundBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }
}
