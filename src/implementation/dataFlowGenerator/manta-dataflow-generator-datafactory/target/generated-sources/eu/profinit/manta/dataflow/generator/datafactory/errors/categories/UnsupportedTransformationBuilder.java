package eu.profinit.manta.dataflow.generator.datafactory.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class UnsupportedTransformationBuilder extends AbstractLoggingBuilder<eu.profinit.manta.dataflow.generator.datafactory.errors.categories.UnsupportedTransformationBuilder> {
  private static final String USER_MESSAGE = "Unsupported transformation %{transformation}. Skipping analysis.";

  private static final String TECHNICAL_MESSAGE = "Unsupported transformation %{transformation}. No analyzer found for the component of the type %{type}. Skipping analysis.";

  private static final String SOLUTION = "This functionality is not supported at this time. For more information, please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "UNSUPPORTED_TRANSFORMATION";

  private static final Impact IMPACT = Impact.SCRIPT;

  private static final Severity SEVERITY = Severity.WARN;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  UnsupportedTransformationBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected UnsupportedTransformationBuilder getThis() {
    return this;
  }

  public UnsupportedTransformationBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public UnsupportedTransformationBuilder transformation(Object value) {
    putArg("transformation", value);
    return this;
  }

  public UnsupportedTransformationBuilder type(Object value) {
    putArg("type", value);
    return this;
  }
}
