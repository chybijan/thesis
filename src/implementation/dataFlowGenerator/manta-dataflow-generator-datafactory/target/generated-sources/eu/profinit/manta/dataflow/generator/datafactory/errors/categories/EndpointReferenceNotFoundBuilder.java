package eu.profinit.manta.dataflow.generator.datafactory.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.api.logging.AbstractLoggingBuilder;

@SuppressWarnings({"java:S1128"})
public class EndpointReferenceNotFoundBuilder extends AbstractLoggingBuilder<eu.profinit.manta.dataflow.generator.datafactory.errors.categories.EndpointReferenceNotFoundBuilder> {
  private static final String USER_MESSAGE = "Data flow endpoint '%{endpointName}' reference does not exist. Lineage might be incomplete";

  private static final String TECHNICAL_MESSAGE = "Data flow endpoint '%{endpointName}' reference does not exist. Lineage might be incomplete";

  private static final String SOLUTION = "Please contact MANTA Support at portal.getmanta.com and submit a support bundle/log export.";

  private static final String ERROR_TYPE = "ENDPOINT_REFERENCE_NOT_FOUND";

  private static final Impact IMPACT = Impact.SCRIPT;

  private static final Severity SEVERITY = Severity.ERROR;

  private static final String[] SUBTYPE_PARAMS = new String[] {"subtype"};

  EndpointReferenceNotFoundBuilder(BaseCategory category) {
    super(category, ERROR_TYPE, USER_MESSAGE, TECHNICAL_MESSAGE, SOLUTION, IMPACT, SEVERITY, SUBTYPE_PARAMS);
  }

  @Override
  protected EndpointReferenceNotFoundBuilder getThis() {
    return this;
  }

  public EndpointReferenceNotFoundBuilder subtype(Object value) {
    putArg("subtype", value);
    return this;
  }

  public EndpointReferenceNotFoundBuilder endpointName(Object value) {
    putArg("endpointName", value);
    return this;
  }
}
