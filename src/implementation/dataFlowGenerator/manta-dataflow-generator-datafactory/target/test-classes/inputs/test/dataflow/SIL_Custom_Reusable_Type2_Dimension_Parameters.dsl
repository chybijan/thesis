parameters{
	KeyCol as string,
	TargetSchemaName as string,
	TargetTableName as string,
	SqlViewName as string,
	ViewSchemaName as string,
	W_INSERT_DT as string
}
source(allowSchemaDrift: true,
	validateSchema: false,
	inferDriftedColumnTypes: true,
	isolationLevel: 'READ_UNCOMMITTED',
	query: (concat('SELECT * FROM ',concat($ViewSchemaName,'.',$SqlViewName))),
	format: 'query',
	staged: true) ~> SourceDimensionStage
source(output(
		LKP_ROW_WID as decimal(18,0),
		LKP_ROW_WID_SKEY as double,
		LKP_ROW_WID_SKEY_MAX as double,
		LKP_INTEGRATION_ID as string,
		LKP_CREATED_ON_DT as timestamp,
		LKP_DATASOURCE_NUM_ID as decimal(10,0)
	),
	allowSchemaDrift: true,
	validateSchema: false,
	isolationLevel: 'READ_UNCOMMITTED',
	query: (concat('SELECT TGT.ROW_WID as LKP_ROW_WID,TGT.ROW_WID_SKEY as LKP_ROW_WID_SKEY,ISNULL(b.ROW_WID_SKEY_MAX,TGT.ROW_WID_SKEY) as LKP_ROW_WID_SKEY_MAX,TGT.INTEGRATION_ID as LKP_INTEGRATION_ID, TGT.CREATED_ON_DT as LKP_CREATED_ON_DT,TGT.DATASOURCE_NUM_ID As LKP_DATASOURCE_NUM_ID FROM ',concat($TargetSchemaName,'.',$TargetTableName,' TGT'),' LEFT JOIN (SELECT   integration_id,MAX (row_wid_skey) row_wid_skey_max FROM ',concat($TargetSchemaName,'.',$TargetTableName,' TGT'),' WHERE current_flg =\'N\'  GROUP BY integration_id) b  ON   (TGT.Integration_Id = b.Integration_Id) where TGT.current_flg = \'Y\'')),
	format: 'query',
	staged: false) ~> LKPTGT
source(output(
		CURRENT_ROW_WID as decimal(18,0)
	),
	allowSchemaDrift: true,
	validateSchema: false,
	isolationLevel: 'READ_UNCOMMITTED',
	query: (concat('SELECT MAX(ROW_WID) AS CURRENT_ROW_WID FROM ',concat($TargetSchemaName,'.',$TargetTableName))),
	format: 'query',
	staged: false) ~> TgtLkpMaxRowWid
SourceDimensionStage derive(each(match(instr(name,'D_REKEY_FLG')>0), 'D_REKEY_FLG' = toString($$)),
		each(match(instr(name,'NEW_VERSION_FLG')>0), 'D_NEW_VERSION_FLG' = toString($$)),
		each(match(instr(name,'INTEGRATION_ID')>0&&startsWith(name,'INTEGRATION_ID')&&endsWith(name,'INTEGRATION_ID')), 'D_INTEGRATION_ID' = toString($$)),
		each(match(instr(name,'DATASOURCE_NUM_ID')>0&&startsWith(name,'DATASOURCE_NUM_ID')&&endsWith(name,'DATASOURCE_NUM_ID')), 'D_DATASOURCE_NUM_ID' = toInteger($$)),
		each(match(instr(name,'REKEY_FILTER_FLG')>0), 'D_REKEY_FILTER_FLG' = toString($$)),
		each(match(instr(name,'D_CREATED_ON_DT')>0), 'M_CREATED_ON_DT' = toString($$)),
		each(match(instr(name,'D_UPDATED_ON_DT')>0), 'M_UPDATED_ON_DT' = toString($$))) ~> DerivedColumnDrift
JoinLkpTarget derive(CREATED_ON_DT = iif(isNull(LKP_ROW_WID),iif(isNull(M_CREATED_ON_DT),toTimestamp($W_INSERT_DT, 'yyyy-MM-dd\'T\'HH:mm:ss'),toTimestamp(M_CREATED_ON_DT)),LKP_CREATED_ON_DT),
		NEW_FLG = iif(isNull(LKP_ROW_WID),'Y','N'),
		INSERT_DT = toTimestamp($W_INSERT_DT, 'yyyy-MM-dd\'T\'HH:mm:ss'),
		UPD_ON_DT = iif(isNull(M_UPDATED_ON_DT),toTimestamp($W_INSERT_DT, 'yyyy-MM-dd\'T\'HH:mm:ss'),toTimestamp(M_UPDATED_ON_DT)),
		EFF_TO_DT = iif(isNull(M_UPDATED_ON_DT),toTimestamp($W_INSERT_DT, 'yyyy-MM-dd\'T\'HH:mm:ss'),toTimestamp(M_UPDATED_ON_DT)),
		UPDATE_DT = toTimestamp($W_INSERT_DT, 'yyyy-MM-dd\'T\'HH:mm:ss'),
		REKEY_FLG = 'Y') ~> DerivedColumnNewFlag
DerivedColumnNewFlag split(iif((NEW_FLG =='Y') || ((NEW_FLG =='N') && (D_REKEY_FLG=='N')) || ((D_NEW_VERSION_FLG=='Y') && (D_REKEY_FLG=='Y')) ,true(),false()),
	iif(((NEW_FLG =='N') && (D_REKEY_FLG=='N')) || (D_NEW_VERSION_FLG=='Y' && D_REKEY_FLG=='Y'),true(),false()),
	D_REKEY_FILTER_FLG == 'Y',
	disjoint: true) ~> ConditionalSplit@(InsertGroup, UpdateGroup, ReKeyGroup, Default)
ConditionalSplit@InsertGroup keyGenerate(output(ROW_WID_SEQUENCE as long),
	startAt: 1L) ~> SurrogateKeyRowWid
SurrogateKeyRowWid, TgtLkpMaxRowWid join(true(),
	joinType:'cross',
	broadcast: 'auto')~> Join
Join derive(ROW_WID = ROW_WID_SEQUENCE + iif(isNull(CURRENT_ROW_WID),0,toInteger(CURRENT_ROW_WID)),
		EFFECTIVE_FROM_DT = iif(isNull(LKP_ROW_WID),iif(isNull(M_CREATED_ON_DT),toTimestamp($W_INSERT_DT, 'yyyy-MM-dd\'T\'HH:mm:ss'),toTimestamp(M_CREATED_ON_DT)),iif(isNull(M_UPDATED_ON_DT),INSERT_DT,toTimestamp(M_UPDATED_ON_DT))),
		EFFECTIVE_FROM_DATE = iif(isNull(LKP_ROW_WID),iif(isNull(M_CREATED_ON_DT),toTimestamp($W_INSERT_DT, 'yyyy-MM-dd\'T\'HH:mm:ss'),toTimestamp(M_CREATED_ON_DT)),iif(isNull(M_UPDATED_ON_DT),INSERT_DT,toTimestamp(M_UPDATED_ON_DT))),
		ROW_WID_SKEY = iif(NEW_FLG == 'Y',ROW_WID_SEQUENCE + iif(isNull(CURRENT_ROW_WID),0,toInteger(CURRENT_ROW_WID)),floor(toInteger(LKP_ROW_WID_SKEY_MAX))),
		CURRENT_FLG = 'Y',
		UPDATED_ON_DT = UPD_ON_DT,
		W_INSERT_DT = INSERT_DT) ~> DerivedColumnInsKeys
MapDrifted, LKPTGT join(rtrim(ltrim(D_INTEGRATION_ID)) == rtrim(ltrim(LKP_INTEGRATION_ID))
	&& D_DATASOURCE_NUM_ID == LKP_DATASOURCE_NUM_ID,
	joinType:'left',
	broadcast: 'auto')~> JoinLkpTarget
ConditionalSplit@UpdateGroup derive(UPD_ROW_WID = LKP_ROW_WID,
		UPD_ROW_WID_SKEY = LKP_ROW_WID_SKEY_MAX +0.00001,
		CURRENT_FLG = 'N') ~> DerivedColumnUpdKeys
DerivedColumnUpdKeys select(mapColumn(
		ROW_WID = UPD_ROW_WID,
		ROW_WID_SKEY = UPD_ROW_WID_SKEY,
		CURRENT_FLG,
		W_UPDATE_DT = UPDATE_DT,
		EFFECTIVE_TO_DT = EFF_TO_DT,
		EFFECTIVE_TO_DATE = EFF_TO_DT
	),
	skipDuplicateMapInputs: false,
	skipDuplicateMapOutputs: true) ~> SelectUpdate
SelectUpdate alterRow(updateIf(true())) ~> AlterRowUpdate
DerivedColumnRekey keyGenerate(output(ROW_WID as long),
	startAt: 1L) ~> SurrogateRekey
DerivedColumnDrift derive(D_REKEY_FLG = toString(byName('D_REKEY_FLG')),
		D_NEW_VERSION_FLG = toString(byName('D_NEW_VERSION_FLG')),
		D_INTEGRATION_ID = toString(byName('D_INTEGRATION_ID')),
		D_DATASOURCE_NUM_ID = toInteger(byName('D_DATASOURCE_NUM_ID')),
		D_REKEY_FILTER_FLG = toString(byName('D_REKEY_FILTER_FLG')),
		M_CREATED_ON_DT = toString(byName('M_CREATED_ON_DT')),
		M_UPDATED_ON_DT = toString(byName('M_UPDATED_ON_DT'))) ~> MapDrifted
ConditionalSplit@ReKeyGroup derive(UPDATED_ON_DT = UPD_ON_DT,
		W_INSERT_DT = INSERT_DT,
		W_UPDATE_DT = UPDATE_DT) ~> DerivedColumnRekey
DerivedColumnInsKeys sink(allowSchemaDrift: true,
	validateSchema: false,
	deletable:false,
	insertable:true,
	updateable:false,
	upsertable:false,
	format: 'table',
	staged: true,
	allowCopyCommand: true,
	skipDuplicateMapInputs: true,
	skipDuplicateMapOutputs: true,
	saveOrder: 1,
	errorHandlingOption: 'stopOnFirstError',
	mapColumn(
		each(patternMatch(`.*[^_]$`))
	)) ~> TargetDimensionInsert
AlterRowUpdate sink(allowSchemaDrift: true,
	validateSchema: false,
	deletable:false,
	insertable:false,
	updateable:true,
	upsertable:false,
	keys:[($KeyCol)],
	format: 'table',
	staged: true,
	allowCopyCommand: true,
	skipDuplicateMapInputs: true,
	skipDuplicateMapOutputs: true,
	saveOrder: 2,
	errorHandlingOption: 'stopOnFirstError',
	mapColumn(
		each(patternMatch(`.*[^_]$`))
	)) ~> TargetDimensionUpdate
SurrogateRekey sink(allowSchemaDrift: true,
	validateSchema: false,
	deletable:false,
	insertable:true,
	updateable:false,
	upsertable:false,
	truncate:true,
	format: 'table',
	staged: true,
	allowCopyCommand: true,
	skipDuplicateMapInputs: true,
	skipDuplicateMapOutputs: true,
	saveOrder: 1,
	errorHandlingOption: 'stopOnFirstError',
	mapColumn(
		each(patternMatch(`.*[^_]$`))
	)) ~> RekeyStage