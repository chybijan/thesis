source(output(
		column_name as string
	),
	allowSchemaDrift: true,
	validateSchema: false,
	format: 'table',
	tableName: '\'table_name\'',
	schemaName: '\'schema_name\'',
	store: 'postgres',
	isolationLevel: 'READ_UNCOMMITTED') ~> source1
source1 filter(1 == 2) ~> filter1
filter1 sink(allowSchemaDrift: true,
	validateSchema: false,
	emptyLinesAsHeader: 1,
	format: 'delimited',
	container: 'containos',
	folderPath: 'foldinos',
	columnDelimiter: ',',
	escapeChar: '\\',
	quoteChar: '\"',
	columnNamesAsHeader: false,
	skipDuplicateMapInputs: true,
	skipDuplicateMapOutputs: true) ~> sink1