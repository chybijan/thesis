package eu.profinit.manta.dataflow.generator.datafactory.analyzer;

import eu.profinit.manta.connector.datafactory.ADFJsonReader;
import eu.profinit.manta.connector.datafactory.model.IADF;
import eu.profinit.manta.connector.datafactory.model.IDataFlow;
import eu.profinit.manta.connector.datafactory.model.IFactory;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.DataFlowAnalyzer;
import eu.profinit.manta.dataflow.generator.datafactory.errors.Categories;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.dataflow.model.NodeType;
import eu.profinit.manta.platform.logging.api.logging.Logger;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;

import java.util.Collection;
import java.util.Optional;

public class ADFAnalyzer implements IAnalyzer<IADF, Node>{

    private static final Logger LOGGER = new Logger(ADFAnalyzer.class);

    private DataFlowAnalyzer dataFlowAnalyzer;

    private static final String UNKNOWN_FACTORY_NAME = "UNKNOWN_FACTORY";

    public void setDataFlowAnalyzer(DataFlowAnalyzer dataFlowAnalyzer) {
        this.dataFlowAnalyzer = dataFlowAnalyzer;
    }

    @Override
    public Node analyze(IADF toAnalyze, AnalyzerContext context, Node parentNode, ADFGraphHelper helper) {

        // Set all datasets and linked services to context
        context.setDatasets(toAnalyze.getDatasets());
        context.setLinkedServices(toAnalyze.getLinkedServices());

        // create root node for data factory
        Optional<IFactory> factoryOptional = toAnalyze.getFactory();
        String factoryName = UNKNOWN_FACTORY_NAME;
        if(factoryOptional.isPresent()){
            IFactory factory = factoryOptional.get();
            factoryName = factory.getName();
        }
        Node rootNode = helper.addFactoryNode(factoryName, null);

        // Analyze Pipeline or dataflow with context

        // Analyze dataflow
        Optional<IDataFlow> optDataFlow = toAnalyze.getDataFlow();
        if(!optDataFlow.isPresent()){
            LOGGER.log(Categories.dataFlowErrors().dataFlowNotFound());
        }else{
            dataFlowAnalyzer.analyze(optDataFlow.get(), context, rootNode, helper);
        }

        return rootNode;

    }
}
