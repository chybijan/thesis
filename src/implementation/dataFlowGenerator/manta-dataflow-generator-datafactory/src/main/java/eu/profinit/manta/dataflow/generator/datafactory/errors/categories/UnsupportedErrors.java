package eu.profinit.manta.dataflow.generator.datafactory.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.BaseCategory;
import eu.profinit.manta.platform.logging.api.categorization.Error;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.common.messages.CommonMessages;


public class UnsupportedErrors extends BaseCategory {

    @Error(userMessage = "Unsupported transformation %{transformation}. Skipping analysis.",
           technicalMessage = "Unsupported transformation %{transformation}. No analyzer found for the component of the type %{type}. Skipping analysis.",
           solution = CommonMessages.UNSUPPORTED_FEATURE_CONTACT_MANTA_SUPPORT,
           severity = Severity.WARN,
           lineageImpact = Impact.SCRIPT)
    public UnsupportedTransformationBuilder unsupportedTransformation() {
        return new UnsupportedTransformationBuilder(this);
    }

    @Error(userMessage = "Unsupported data flow type: %{type}. Skipping analysis.",
            technicalMessage = "Unsupported data flow type: %{type}. Skipping analysis.",
            solution = CommonMessages.UNSUPPORTED_FEATURE_CONTACT_MANTA_SUPPORT,
            severity = Severity.WARN,
            lineageImpact = Impact.SCRIPT)
    public UnsupportedDataFlowTypeBuilder unsupportedDataFlowType() {
        return new UnsupportedDataFlowTypeBuilder(this);
    }

    @Error(userMessage = "Linked service named '%{name} is not recognized as database connection.",
            technicalMessage = "Linked service named '%{name} is not recognized as database connection.",
            solution = CommonMessages.UNSUPPORTED_FEATURE_CONTACT_MANTA_SUPPORT,
            severity = Severity.WARN,
            lineageImpact = Impact.SCRIPT)
    public UnsupportedDatabaseLinkedServiceBuilder unsupportedDatabaseLinkedService() {
        return new UnsupportedDatabaseLinkedServiceBuilder(this);
    }

    @Error(userMessage = "Unsupported location type: %{type}. Skipping analysis.",
            technicalMessage = "Unsupported location type: %{type}. Skipping analysis.",
            solution = CommonMessages.UNSUPPORTED_FEATURE_CONTACT_MANTA_SUPPORT,
            severity = Severity.WARN,
            lineageImpact = Impact.SCRIPT)
    public UnsupportedLocationBuilder unsupportedLocation() {
        return new UnsupportedLocationBuilder(this);
    }




}
