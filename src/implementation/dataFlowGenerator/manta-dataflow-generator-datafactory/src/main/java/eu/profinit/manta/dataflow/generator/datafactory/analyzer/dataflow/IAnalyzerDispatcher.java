package eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow;

import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import eu.profinit.manta.dataflow.model.Node;

/**
 * Interface for dispatchers, that delegates analysis of components to suitable analyzers.
 */
public interface IAnalyzerDispatcher {

    /**
     * Attempts to delegate the analysis of the given transformatino to an analyzer capable of analyzing
     * that type of transformaion.
     * 
     * @param transformation Transformation to be analyzed.
     * @param context Context to be used by the analysis.
     * @param parentNode Node to use as parent node for analyzed transformation node
     * @param graphHelper Graph helper to be used by the analysis.
     */
    void analyzeTransformation(ITransformation transformation, AnalyzerContext context, Node parentNode, ADFGraphHelper graphHelper);
}
