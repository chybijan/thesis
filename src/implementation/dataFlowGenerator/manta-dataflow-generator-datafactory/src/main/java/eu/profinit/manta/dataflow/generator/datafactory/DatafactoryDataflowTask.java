package eu.profinit.manta.dataflow.generator.datafactory;

import eu.profinit.manta.connector.datafactory.model.IADF;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.ADFAnalyzer;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContextImpl;
import eu.profinit.manta.dataflow.generator.datafactory.connection.sinkconnector.SinkEndpointConnector;
import eu.profinit.manta.dataflow.generator.datafactory.errors.Categories;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ILocationNodeCreator;
import eu.profinit.manta.dataflow.generator.modelutils.AbstractGraphTask;
import eu.profinit.manta.dataflow.generator.modelutils.nodecreator.NodeCreator;
import eu.profinit.manta.dataflow.model.Graph;
import eu.profinit.manta.dataflow.model.NodeType;
import eu.profinit.manta.platform.logging.api.logging.Logger;

public class DatafactoryDataflowTask extends AbstractGraphTask<IADF> {

    private static final Logger LOGGER = new Logger(DatafactoryDataflowTask.class);

    private ADFAnalyzer analyzer;

    private ILocationNodeCreator locationNodeCreator;

    private NodeCreator nodeCreator;

    public ADFAnalyzer getAnalyzer() {
        return analyzer;
    }

    public void setAnalyzer(ADFAnalyzer analyzer) {
        this.analyzer = analyzer;
    }

    public void setLocationNodeCreator(ILocationNodeCreator locationNodeCreator) {
        this.locationNodeCreator = locationNodeCreator;
    }

    public void setNodeCreator(NodeCreator nodeCreator) {
        this.nodeCreator = nodeCreator;
    }

    @Override
    protected void doExecute(IADF input, Graph outputGraph) {

        if(input == null || !input.isValid()){
            LOGGER.log(Categories.dataFlowErrors().dataFactoryInputIsInvalid());
            return;
        }

        ADFGraphHelper graphHelper = new ADFGraphHelper(
                outputGraph,
                getScriptResource(),
                getScriptResource(),
                NodeType.COLUMN_FLOW,
                locationNodeCreator,
                nodeCreator);

        // Create context
        AnalyzerContext context = new AnalyzerContextImpl();

        analyzer.analyze(input, context, null, graphHelper);

    }


}
