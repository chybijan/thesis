package eu.profinit.manta.dataflow.generator.datafactory.connection;

import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumnDef;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumnMatch;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumnRef;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.visitor.IColumnVisitor;
import eu.profinit.manta.dataflow.model.Node;

import java.util.Collection;
import java.util.Collections;

// TODO
public class ColumnConnectorVisitor implements IColumnVisitor<Collection<Node>> {
    @Override
    public Collection<Node> visitColumnDef(IColumnDef columnDef) {
        return Collections.emptyList();
    }

    @Override
    public Collection<Node> visitColumnRef(IColumnRef columnRef) {
        return Collections.emptyList();
    }

    @Override
    public Collection<Node> visitColumnMatch(IColumnMatch columnMatch) {
        return Collections.emptyList();
    }
}
