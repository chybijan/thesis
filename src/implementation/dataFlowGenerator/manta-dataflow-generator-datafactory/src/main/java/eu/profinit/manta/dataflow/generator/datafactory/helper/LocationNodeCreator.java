package eu.profinit.manta.dataflow.generator.datafactory.helper;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.*;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.visitor.ILocationVisitor;
import eu.profinit.manta.connector.datafactory.model.linkedservice.*;
import eu.profinit.manta.dataflow.generator.modelutils.nodecreator.NodeCreator;
import eu.profinit.manta.dataflow.model.Node;

import java.io.File;

/**
 * Class creating nodes for location
 */
public class LocationNodeCreator implements ILocationNodeCreator {

    /**
     * Create node for location
     *
     * @param location location to create node for
     * @param ls       linked service of location
     * @param helper   graph helper
     * @return new node representing location
     * @throws IllegalStateException         when location and linked service do not match
     * @throws UnsupportedOperationException if some location is not supported yet
     */
    @Override
    public Node createLocationNode(IDatasetLocation location, ILinkedService ls, ADFGraphHelper helper) {
        LocationNodeCreatorVisitor creatorVisitor = new LocationNodeCreatorVisitor(helper, ls);
        return location.accept(creatorVisitor);
    }

    private static class LocationNodeCreatorVisitor implements ILocationVisitor<Node> {

        private final ADFGraphHelper helper;

        private final ILinkedService ls;

        String getLocationErrorText(IDatasetLocation location, ILinkedService ls) {
            return "Location " + location.getLocationType().toString() + " does not match linked service " + ls.getType();
        }

        private LocationNodeCreatorVisitor(ADFGraphHelper helper, ILinkedService ls) {
            this.helper = helper;
            this.ls = ls;
        }

        @Override
        public Node visit(IAmazonS3Location location) {

            //Check linked service type
            if (!(ls instanceof IAmazonS3LinkedService)) {
                throw new IllegalStateException(getLocationErrorText(location, ls));
            }

            IAmazonS3LinkedService amazonLs = (IAmazonS3LinkedService) ls;
            File file = createFile(location.getBucketName(), location.getFolderPath(), location.getFileName());

            return helper.createFileNode(file, amazonLs.getServiceUrl().getStringValue());

        }

        @Override
        public Node visit(IAzureBlobFSLocation location) {

            //Check linked service type
            if (!(ls instanceof IAzureBlobFSLinkedService)) {
                throw new IllegalStateException(getLocationErrorText(location, ls));
            }

            IAzureBlobFSLinkedService azureBlobFsLs = (IAzureBlobFSLinkedService) ls;
            File file = createFile(location.getFileSystem(), location.getFolderPath(), location.getFileName());
            return helper.createFileNode(file, azureBlobFsLs.getUrl().getStringValue());

        }

        @Override
        public Node visit(IAzureBlobStorageLocation location) {

            //Check linked service type
            if (!(ls instanceof IAzureBlobStorageLinkedService)) {
                throw new IllegalStateException(getLocationErrorText(location, ls));
            }

            IAzureBlobStorageLinkedService azureBlobLs = (IAzureBlobStorageLinkedService) ls;


            IAdfFieldValue host = azureBlobLs.getServiceEndpoint();

            if (host == null)
                host = azureBlobLs.getSasUri();
            if (host == null)
                host = azureBlobLs.getConnectionString();

            String hostString = (host != null)
                    ? host.getStringValue()
                    : NodeCreator.DEFAULT_HOSTNAME;

            File file = createFile(location.getContainer(), location.getFolderPath(), location.getFileName());
            return helper.createFileNode(file, hostString);
        }

        @Override
        public Node visit(IAzureDataLakeStoreLocation location) {
            //Check linked service type
            if (!(ls instanceof IAzureDataLakeStoreLinkedService)) {
                throw new IllegalStateException(getLocationErrorText(location, ls));
            }

            IAzureDataLakeStoreLinkedService azureDatalakeLs = (IAzureDataLakeStoreLinkedService) ls;
            File file = createFile(null, location.getFolderPath(), location.getFileName());
            return helper.createFileNode(file, azureDatalakeLs.getDataLakeStoreUri().getStringValue());
        }

        @Override
        public Node visit(IAzureFileStorageLocation location) {
            //Check linked service type
            if (!(ls instanceof IAzureFileStorageLinkedService)) {
                throw new IllegalStateException(getLocationErrorText(location, ls));
            }

            IAzureFileStorageLinkedService azureFileStorageLs = (IAzureFileStorageLinkedService) ls;
            File file = createFile(null, location.getFolderPath(), location.getFileName());

            return helper.createFileNode(file, azureFileStorageLs.getHost().getStringValue());
        }

        @Override
        public Node visit(IFileServerLocation location) {
            //Check linked service type
            if (!(ls instanceof IFileServerLinkedService)) {
                throw new IllegalStateException(getLocationErrorText(location, ls));
            }

            IFileServerLinkedService fileServerLs = (IFileServerLinkedService) ls;
            File file = createFile(null, location.getFolderPath(), location.getFileName());

            return helper.createFileNode(file, fileServerLs.getHost().getStringValue());
        }

        @Override
        public Node visit(IFtpServerLocation location) {
            //Check linked service type
            if (!(ls instanceof IFtpServerLinkedService)) {
                throw new IllegalStateException(getLocationErrorText(location, ls));
            }

            IFtpServerLinkedService ftpServerLs = (IFtpServerLinkedService) ls;
            File file = createFile(null, location.getFolderPath(), location.getFileName());

            return helper.createFileNode(file, ftpServerLs.getHost().getStringValue());
        }

        @Override
        public Node visit(IGoogleCloudStorageLocation location) {
            //Check linked service type
            if (!(ls instanceof IGoogleCloudStorageLinkedService)) {
                throw new IllegalStateException(getLocationErrorText(location, ls));
            }

            IGoogleCloudStorageLinkedService googleCloudLs = (IGoogleCloudStorageLinkedService) ls;
            File file = createFile(location.getBucketName(), location.getFolderPath(), location.getFileName());

            return helper.createFileNode(file, googleCloudLs.getServiceUrl().getStringValue());
        }

        @Override
        public Node visit(IHdfsLocation location) {
            //Check linked service type
            if (!(ls instanceof IHdfsLinkedService)) {
                throw new IllegalStateException(getLocationErrorText(location, ls));
            }

            IHdfsLinkedService hdfsLs = (IHdfsLinkedService) ls;
            File file = createFile(null, location.getFolderPath(), location.getFileName());

            return helper.createFileNode(file, hdfsLs.getUrl().getStringValue());
        }

        @Override
        public Node visit(IHttpServerLocation location) {
            // TODO create service node
            throw new UnsupportedOperationException("Http is not supported node type");
        }

        @Override
        public Node visit(ISftpLocation location) {
            //Check linked service type
            if (!(ls instanceof ISftpServerLinkedService)) {
                throw new IllegalStateException(getLocationErrorText(location, ls));
            }

            ISftpServerLinkedService sftpLs = (ISftpServerLinkedService) ls;
            File file = createFile(null, location.getFolderPath(), location.getFileName());

            return helper.createFileNode(file, sftpLs.getHost().getStringValue());
        }

        /**
         * Create file from three basic elements. If any parameter is null, it is not added to File path
         *
         * @param container  first part of file path
         * @param folderPath middle part of file path
         * @param fileName   file name
         * @return File class
         */
        private File createFile(IAdfFieldValue container, IAdfFieldValue folderPath, IAdfFieldValue fileName) {

            StringBuilder pathBuilder = new StringBuilder();

            appendIfNotNull(container, pathBuilder);
            appendIfNotNull(folderPath, pathBuilder);
            appendIfNotNull(fileName, pathBuilder);

            return new File(pathBuilder.toString());
        }

        /**
         * Adds string value of file to string builder
         *
         * @param file        file field
         * @param pathBuilder string builder containing path
         */
        private void appendIfNotNull(IAdfFieldValue file, StringBuilder pathBuilder) {

            // Check if container exists
            if (file == null)
                return;

            // Check if folder separator should be added
            if (pathBuilder.length() != 0)
                pathBuilder.append("/");

            pathBuilder.append(file.getStringValue());

        }
    }

}
