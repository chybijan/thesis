package eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowEndpoint;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ISinkTransformation;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedStream;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedTransformation;
import eu.profinit.manta.dataflow.generator.datafactory.connection.sinkconnector.SinkEndpointConnector;
import eu.profinit.manta.dataflow.generator.datafactory.errors.Categories;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.dataflow.model.NodeType;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import java.util.*;

public class SinkAnalyzer extends TransformationAnalyzer<ISinkTransformation>{

    private static final Logger LOGGER = new Logger(SinkAnalyzer.class);

    private SinkEndpointConnector sinkConnector;

    public SinkAnalyzer() {
        super(ISinkTransformation.class);
    }

    public void setSinkConnector(SinkEndpointConnector sinkConnector) {
        this.sinkConnector = sinkConnector;
    }

    @Override
    public Map<IADFStream, ProcessedStream> analyze(ISinkTransformation toAnalyze, AnalyzerContext context, Node parentNode, ADFGraphHelper helper) {
        LOGGER.info("Analyzing sink '" + toAnalyze.getName() + "'");
        Node transformationNode = helper.addNode(toAnalyze.getName(), NodeType.DATAFACTORY_SINK_TRANSFORMATION, parentNode);

        Map<IADFStream, ProcessedStream> processedStreams = createStreams(toAnalyze, context, transformationNode, helper);
        ProcessedTransformation processedTransformation = new ProcessedTransformation(transformationNode, processedStreams);

        // Get underlying endpoint
        Optional<IDataFlowEndpoint> sourceEndpointOpt = context.getDataflowEndpointByName(toAnalyze.getName());
        if(!sourceEndpointOpt.isPresent()){
            LOGGER.log(Categories.dataFlowErrors().dataEndpointNotFound()
                    .name(toAnalyze.getName())
                    .endpointPlace("Sink transformation"));
        }else{
            connectToEndpoint(sourceEndpointOpt.get(), sinkConnector, toAnalyze, processedTransformation, context, helper, false);
        }

        return processedStreams;
    }

}
