package eu.profinit.manta.dataflow.generator.datafactory.connection.sinkconnector;

import eu.profinit.manta.connector.datafactory.model.linkedservice.*;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedTransformation;

import java.util.Optional;


/**
 * Class connecting inline dataset to transformation
 */
public class SinkInlineDatasetConnectorVisitor implements ILinkedServiceVisitor<Optional<ProcessedTransformation>> {

    // TODO because inline datasets are not not in customers samples so it can be omitted for now

    @Override
    public Optional<ProcessedTransformation> visitAmazonRedshiftLinkedService(IAmazonRedshiftLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitAmazonS3LinkedService(IAmazonS3LinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitAzureBlobFS(IAzureBlobFSLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitAzureBlobStorageLinkedService(IAzureBlobStorageLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitAzureDataLakeStoreLinkedService(IAzureDataLakeStoreLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitAzureFileStorageLinkedService(IAzureFileStorageLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitAzurePostgresSqlLinkedService(IAzurePostgreSqlLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitAzureSqlDatabaseLinkedService(IAzureSqlDatabaseLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitAzureSqlDWLLinkedService(IAzureSqlDWLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitAzureSqlMILLinkedService(IAzureSqlMILinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitDb2LinkedService(IDb2LinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitFileServerLinkedService(IFileServerLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitFtpServerLinkedService(IFtpServerLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitGoogleBigQueryLinkedService(IGoogleBigQueryLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitGoogleCloudStorageLinkedService(IGoogleCloudStorageLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitGreenplumLinkedService(IGreenplumLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitHdfsLinkedService(IHdfsLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitHiveLinkedService(IHiveLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitNetezzaLinkedService(INetezzaLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitOracleLinkedService(IOracleLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitPostgreSqlLinkedService(IPostgreSqlLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitSapHanaLinkedService(ISapHanaLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitSftpServerLinkedService(ISftpServerLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitSnowflakeLinkedService(ISnowflakeLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitSqlServerLinkedService(ISqlServerLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitSybaseLinkedService(ISybaseLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitTeradataLinkedService(ITeradataLinkedService ls) {
        return Optional.empty();
    }

    @Override
    public Optional<ProcessedTransformation> visitUnknownLinkedService(IUnknownLinkedService ls) {
        return Optional.empty();
    }
}
