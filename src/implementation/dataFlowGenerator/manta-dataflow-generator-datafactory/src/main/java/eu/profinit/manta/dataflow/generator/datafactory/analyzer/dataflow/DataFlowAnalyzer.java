package eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow;

 import eu.profinit.manta.connector.datafactory.model.IDataFlow;
import eu.profinit.manta.connector.datafactory.model.dataflow.DataFlowType;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowEndpoint;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowProperties;
import eu.profinit.manta.connector.datafactory.model.dataflow.mapping.IMappingDataFlowProperties;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.IAnalyzer;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.errors.Categories;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import eu.profinit.manta.dataflow.generator.datafactory.helper.PrecendenceResolver;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import java.util.*;

public class DataFlowAnalyzer implements IAnalyzer<IDataFlow, Node> {

    private static final Logger LOGGER = new Logger(DataFlowAnalyzer.class);

    private IAnalyzerDispatcher analyzerDispatcher;

    public void setAnalyzerDispatcher(IAnalyzerDispatcher analyzerDispatcher) {
        this.analyzerDispatcher = analyzerDispatcher;
    }

    @Override
    public Node analyze(IDataFlow toAnalyze, AnalyzerContext context, Node parentNode, ADFGraphHelper helper) {
        LOGGER.info("Analyzing dataflow: " + toAnalyze.getName());

        IDataFlowProperties properties = toAnalyze.getProperties();
        DataFlowType type = properties.getType();
        switch (type){
            case MAPPING_DATA_FLOW:
                Node dataFlowNode = helper.addMappingDataFlowNode(toAnalyze, parentNode);
                analyzeMappingDF((IMappingDataFlowProperties) properties, context, dataFlowNode, helper);
                return dataFlowNode;
            case WRANGLING_DATA_FLOW:
                analyzeWranglingDF();
                return null;
            default:
                LOGGER.log(Categories.unsupportedErrors().unsupportedDataFlowType().type(type.toString()));
                return null;
        }
    }

    private void analyzeWranglingDF() {
        LOGGER.info("Wrangling data flow is not supported");
    }

    private void analyzeMappingDF(IMappingDataFlowProperties properties, AnalyzerContext context, Node parentNode, ADFGraphHelper helper){
        LOGGER.info("Analyzing mapping data flow");

        //Analyze Sources and Sinks
        Map<String, IDataFlowEndpoint> sources = properties.getSources();
        Map<String, IDataFlowEndpoint> sinks = properties.getSinks();

        Map<String, IDataFlowEndpoint> endpoints = new HashMap<>(sources);
        endpoints.putAll(sinks);
        context.setDataflowEndpoints(endpoints);

        List<String> transformations = new ArrayList<>(properties.getTransformationNames());
        transformations.addAll(endpoints.keySet());

        List<ITransformation> sortedTransformations = sortTransformations(transformations, properties.getScript().getTransformations());

        for(ITransformation transformation : sortedTransformations){
            analyzerDispatcher.analyzeTransformation(transformation, context, parentNode, helper);
        }
    }

    private List<ITransformation> sortTransformations(List<String> transformations, Map<String, ITransformation> transformationPool) {

        PrecendenceResolver<ITransformation> precendenceResolver = new PrecendenceResolver<>();

        for(String transformationName : transformations){

            ITransformation transformation = transformationPool.get(transformationName);
            if(transformation != null){
                precendenceResolver.addNode(transformation);

                // Find all output transformations and registre connection
                Collection<String> outputNames = transformation.getOutputs();
                for(String outputName : outputNames){
                    ITransformation outputTransformation = transformationPool.get(outputName);
                    if(outputTransformation != null){
                        precendenceResolver.addConstraint(transformation, outputTransformation);
                    }
                }
            }
        }

        return precendenceResolver.resolve();

    }

}
