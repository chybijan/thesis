package eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow;

import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.transformation.TransformationAnalyzer;
import eu.profinit.manta.dataflow.generator.datafactory.errors.Categories;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.platform.logging.api.logging.Logger;
import org.apache.commons.lang3.Validate;

import java.util.*;

/**
 * Implementation of {@link IAnalyzerDispatcher} that holds a mapping of component types
 * to analyzers and uses it to delegate the analysis of a given
 * component to correct analyzer.
 * */
public class AnalyzerDispatcher implements IAnalyzerDispatcher{

    private static final Logger LOGGER = new Logger(AnalyzerDispatcher.class);

    private final Map<Class<?>, TransformationAnalyzer<?>> analyzers = new HashMap<>();

    public void setAnalyzers(Collection<TransformationAnalyzer<?>> analyzersToAdd) {
        for (TransformationAnalyzer<?> analyzer : analyzersToAdd) {
            analyzers.put(analyzer.getAnalyzedType(), analyzer);
        }
    }

    @Override
    public void analyzeTransformation(ITransformation transformation, AnalyzerContext context, Node parentNode, ADFGraphHelper graphHelper) {
        LOGGER.info("Analyzing transformation: " + transformation.getName());

        TransformationAnalyzer<? extends ITransformation> transformationAnalyzer = getAnalyzer(transformation);

        if (transformationAnalyzer != null) {
            delegateAnalysis(transformationAnalyzer, transformation, context, parentNode, graphHelper);
        } else {
            LOGGER.log(Categories.unsupportedErrors().unsupportedTransformation().type(transformation.getClass().getSimpleName()).transformation(transformation));
        }

    }

    private TransformationAnalyzer<? extends ITransformation> getAnalyzer(ITransformation transformation){
        Class<? extends ITransformation> componentClass = transformation.getClass();

        Deque<Class<?>> queue = new LinkedList<>();
        queue.addLast(componentClass);
        while (!queue.isEmpty()) {
            Class<?> currentClass = queue.removeFirst();
            TransformationAnalyzer<?> result = analyzers.get(currentClass);

            if (result != null) {
                return result;
            }

            // Examine the extended class and/or implemented interfaces if no analyzer
            // exists for the current type itself.

            if (currentClass.getSuperclass() != null) {
                queue.addLast(currentClass.getSuperclass());
            }
            for (Class<?> interf : currentClass.getInterfaces()) {
                queue.addLast(interf);
            }
        }
        return null;
    }

    private <T extends ITransformation> void delegateAnalysis(TransformationAnalyzer<T> analyzer, ITransformation transformation,
                                                                AnalyzerContext context,
                                                                Node parentNode, ADFGraphHelper graphHelper){
        Validate.isTrue(analyzer.getAnalyzedType().isAssignableFrom(transformation.getClass()),
                "Analyzer " + analyzer.getClass().getName() + " cannot be used to analyze a component of type "
                        + transformation.getClass().getName());

        @SuppressWarnings("unchecked")
        T convertedTransformation = (T) transformation;

        // Analyze transformation and add resulting streams into context
        context.addAvailableStreams(analyzer.analyze(convertedTransformation, context, parentNode, graphHelper));
    }
}
