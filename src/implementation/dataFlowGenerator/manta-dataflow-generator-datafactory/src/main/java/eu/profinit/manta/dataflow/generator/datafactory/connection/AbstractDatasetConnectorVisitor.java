package eu.profinit.manta.dataflow.generator.datafactory.connection;

import eu.profinit.manta.connector.common.connections.Connection;
import eu.profinit.manta.connector.common.connections.query.Column;
import eu.profinit.manta.connector.common.connections.query.Resultset;
import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IAdfFieldValue;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.model.dataset.IUnknownDataset;
import eu.profinit.manta.connector.datafactory.model.dataset.database.IDatabaseDataset;
import eu.profinit.manta.connector.datafactory.model.dataset.database.ISybaseTableDataset;
import eu.profinit.manta.connector.datafactory.model.dataset.file.IFileDataset;
import eu.profinit.manta.connector.datafactory.model.visitor.IDatasetVisitor;
import eu.profinit.manta.dataflow.generator.common.query.DataflowQueryResult;
import eu.profinit.manta.dataflow.generator.common.query.DataflowQueryService;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedStream;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedTransformation;
import eu.profinit.manta.dataflow.generator.datafactory.errors.Categories;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractDatasetConnectorVisitor implements IDatasetVisitor<Optional<ProcessedTransformation>> {

    private static final Logger LOGGER = new Logger(AbstractDatasetConnectorVisitor.class);

    /**
     * Transformation to connect to
     */
    protected final ITransformation transformation;

    /**
     * Transformation node to connect to
     */
    protected final ProcessedTransformation processedTransformation;

    protected final AnalyzerContext context;

    protected final ADFGraphHelper graphHelper;

    protected final DataflowQueryService queryService;

    protected final boolean allowSchemaDrift;

    protected AbstractDatasetConnectorVisitor(ITransformation transformation, ProcessedTransformation processedTransformation, AnalyzerContext context, ADFGraphHelper graphHelper, DataflowQueryService queryService, boolean allowSchemaDrift) {
        this.transformation = transformation;
        this.processedTransformation = processedTransformation;
        this.context = context;
        this.graphHelper = graphHelper;
        this.queryService = queryService;
        this.allowSchemaDrift = allowSchemaDrift;
    }

    @Override
    public Optional<ProcessedTransformation> visitSybaseTable(ISybaseTableDataset sybaseTableDataset) {
        // TODO there is table name attribute additional to table
        return visitDatabaseDataset(sybaseTableDataset);
    }

    /**
     * Creates database node and connects it to transformation node
     * @param databaseDataset database dataset
     * @return created database columns. Order is important.
     */
    @Override
    public Optional<ProcessedTransformation> visitDatabaseDataset(IDatabaseDataset databaseDataset) {

        LinkedServiceToConnectionTranslatorVisitor lsToConnection = new LinkedServiceToConnectionTranslatorVisitor();

        // Get connection to the database
        IReference lsReference = databaseDataset.getLinkedServiceReference();
        Optional<ILinkedService> lsOpt = context.getLinkedService(lsReference);
        if(lsOpt.isEmpty()){
            return Optional.empty();
        }

        // Get connection from linked service
        Connection dbConnection;
        try{
            dbConnection = lsOpt.get().accept(lsToConnection);
        }catch (UnsupportedOperationException e){
            LOGGER.log(Categories.unsupportedErrors().unsupportedDatabaseLinkedService().name(lsOpt.get().getName()));
            return Optional.empty();
        }

        // Get source format
        Optional<IExpression> formatOpt = transformation.findAttributeByName("format");

        DBFormat format = DBFormat.TABLE;
        if(formatOpt.isPresent()){
            String rawExpression = formatOpt.get().asString();
            try{
                format = DBFormat.valueOf(rawExpression.toUpperCase());
            }catch (IllegalArgumentException e){
                LOGGER.log(Categories.dataFlowErrors().invalidTransformationAttributeValue()
                        .name("format")
                        .value(rawExpression)
                        .expected("query | table"));
            }
        }

        switch (format){
            case QUERY:
                Optional<IExpression> queryOpt = transformation.findAttributeByName("query");

                return (queryOpt.isPresent())
                        ? connectByQuery(dbConnection, queryOpt.get().asString())
                        : connectByTable(dbConnection, databaseDataset);
            case TABLE:
            default:
                return connectByTable(dbConnection, databaseDataset);
        }

    }

    enum DBFormat{
        TABLE, QUERY
    }

    protected Optional<ProcessedTransformation> connectByQuery(Connection connection, String query){

        DataflowQueryResult queryResult = queryService.getDataFlow(processedTransformation.getTransformationNode(), "QUERY", query, connection);

        // Updated processed streams
        Map<IADFStream, ProcessedStream> newProcessedStreams = new HashMap<>(processedTransformation.getProcessedStreams());

        // If schema drift is allowed generate additional columns
        if(allowSchemaDrift){
            newProcessedStreams = generateDriftedStreamsFromResultset(queryResult, processedTransformation.getProcessedStreams());
        }

        // Connect result with transformation by name
        for(ProcessedStream stream : newProcessedStreams.values()){
            queryResult.connectAllResultsetsTo(stream.getColumnNodes(), DataflowQueryResult.MatchStrategy.MATCH_BY_NAME_CI);
        }

        Node merged = queryResult.mergeTo(graphHelper.Graph());

        processedTransformation.getTransformationNode().addAttribute("QUERY", query);

        return (merged != null)
                ? Optional.of(new ProcessedTransformation(processedTransformation.getTransformationNode(), newProcessedStreams))
                : Optional.empty();
    }

    private Map<IADFStream, ProcessedStream> generateDriftedStreamsFromResultset(DataflowQueryResult queryResult, Map<IADFStream, ProcessedStream> originalStreams){

        Map<IADFStream, ProcessedStream> streams = new HashMap<>();

        List<Resultset> metadata = queryResult.getOutputMetadata();
        if(!metadata.isEmpty()){
            for(Resultset resultset : metadata){
                List<? extends Column> columns = resultset.getColumns();

                for(Map.Entry<IADFStream, ProcessedStream> stream : originalStreams.entrySet()){
                    ProcessedStream newStream = new ProcessedStream(stream.getValue().getStreamNode(), new ArrayList<>());
                    for(Column column : columns){
                        // TODO this is not entirely true generated columns are called _col1_, _col2_, .... but this has to suffice for now. It is important only in expressions
                        newStream.addColumnNode(graphHelper.addColumnNode(column.getName(), stream.getValue().getStreamNode()));
                    }
                    streams.put(stream.getKey(), newStream);
                }
            }
        }

        return streams;
    }

    protected abstract Optional<ProcessedTransformation> connectByTable(Connection connection, IDatabaseDataset dataset);

    /**
     * Deduces column nodes with help of query service
     * @param connection connection to dataset
     * @param dataset dataset
     * @return created columns
     */
    protected List<Node> addOrDeduceColumnNodes(Connection connection, IDatabaseDataset dataset){

        Set<String> datasetColumnNames = dataset.getDatasetSchema().getDataElements().stream()
                .map(dataElement -> dataElement.getName().getStringValue()).collect(Collectors.toSet());

        if(datasetColumnNames.isEmpty()){
            for(ProcessedStream stream : processedTransformation.getProcessedStreams().values()){
                for(Node columnNode : stream.getColumnNodes()){
                    datasetColumnNames.add(columnNode.getName());
                }
            }
        }

        Optional<IAdfFieldValue> schemaOptional = dataset.getSchema();
        Optional<IAdfFieldValue> tableOptional = dataset.getTable();

        return queryService.addOrDeduceColumnNodes(
                connection.getDatabaseName(),
                schemaOptional.isPresent() ? schemaOptional.get().getStringValue() : connection.getSchemaName(),
                tableOptional.isPresent() ? tableOptional.get().getStringValue() : "",
                new ArrayList<>(datasetColumnNames),
                processedTransformation.getTransformationNode(),
                connection,
                graphHelper.Graph());
    }


    @Override
    public Optional<ProcessedTransformation> visitFileDataset(IFileDataset fileDataset) {

        // TODO Attributes to consider rowUrlColumn, wildcardPaths

        IReference lsReference = fileDataset.getLinkedServiceReference();
        Optional<ILinkedService> lsOpt = context.getLinkedService(lsReference);

        if(lsOpt.isPresent()){
            try{
                Node locationNode = graphHelper.createFileNodeFromLocation(fileDataset.getLocation(), lsOpt.get());

                return Optional.of(createFileColumns(fileDataset, locationNode));

            }catch (IllegalStateException e){
                LOGGER.log(Categories.dataFlowErrors().unexpectedLinkedServiceType()
                        .linkedServiceType(lsOpt.get().getType().toString())
                        .locationType(fileDataset.getLocation().getLocationType().toString()));
            }catch (UnsupportedOperationException e){
                LOGGER.log(Categories.unsupportedErrors().unsupportedLocation().type(fileDataset.getLocation().getLocationType().toString()));
            }

        }

        return Optional.empty();

    }

    private ProcessedTransformation createFileColumns(IFileDataset fileDataset, Node locationNode) {

        ProcessedTransformation updatedTransformation = new ProcessedTransformation(processedTransformation.getTransformationNode(), processedTransformation.getProcessedStreams());

        for(Map.Entry<IADFStream, ProcessedStream> stream : updatedTransformation.getProcessedStreams().entrySet()){

            ProcessedStream newStream;

            IDatasetSchema datasetSchema = fileDataset.getDatasetSchema();
            if (datasetSchema != null) {

                List<Node> fileColumns = graphHelper.createDatasetSchemaColumnNodes(datasetSchema, locationNode);

                // If something does is not processed correctly use default numbered columns
                if (fileColumns.isEmpty()) {
                    LOGGER.log(Categories.dataFlowErrors().datasetSchemaNotProcessedCorrectly().datasetName(fileDataset.getName()));
                    newStream = createDefaultFileColumns(locationNode, stream.getValue());
                }else{
                    newStream = connectFileNodesByNameAndPosition(fileColumns, stream.getValue());
                }
            } else {
                newStream = createDefaultFileColumns(locationNode, stream.getValue());
            }

            updatedTransformation.addProcessedStream(stream.getKey(), newStream);
        }

        return updatedTransformation;

    }

    private ProcessedStream createDefaultFileColumns(Node fileNode, ProcessedStream processedStream) {
            List<Node> fileStreamColumns = graphHelper.createFileColumnNodes(fileNode, processedStream.getColumnNodes().size());
            return connectDefaultFileColumns(fileStreamColumns, processedStream);
    }

    /**
     * Connect file columns to stream by name, and columns that were not connected by name by position
     * @param fileColumns file columns
     * @param processedStream sprocessed stream
     * @return Updated processed stream
     */
    protected abstract ProcessedStream connectFileNodesByNameAndPosition(List<Node> fileColumns, ProcessedStream processedStream);

    /**
     * Connects file columns to transformation
     * @param fileColumns file columns
     * @param processedStream processed stream to connect
     * @return updated processed stream
     */
    protected abstract ProcessedStream connectDefaultFileColumns(List<Node> fileColumns, ProcessedStream processedStream);

    @Override
    public Optional<ProcessedTransformation> visitUnknownDataset(IUnknownDataset unknownDataset) {
        // TODO
        return Optional.empty();
    }

    /**
     * Generate drifted column according to ADF logic
     * Drifted columns are named col_1, col_2, ... based on position
     * @param unconnectedColumns columns which will be connected to generated nodes
     * @param processedStream processed stream
     * @return updated processed stream
     */
    protected ProcessedStream generateDriftedColumns(List<Node> unconnectedColumns, ProcessedStream processedStream){

        ProcessedStream updatedProcessedStream = new ProcessedStream(processedStream.getStreamNode(), processedStream.getColumnNodes());

        for(int i = 0; i < unconnectedColumns.size(); i++){
            Node newColumnNode = graphHelper.addColumnNode("_col" + i + "_", updatedProcessedStream.getStreamNode());
            updatedProcessedStream.addColumnNode(newColumnNode);
            graphHelper.addDirectFlow(unconnectedColumns.get(i), newColumnNode);
        }

        return updatedProcessedStream;
    }

}
