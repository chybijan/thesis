package eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowEndpoint;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumn;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.IAnalyzer;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedStream;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedTransformation;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.expression.IExpressionAnalyzer;
import eu.profinit.manta.dataflow.generator.datafactory.connection.IEndpointConnector;
import eu.profinit.manta.dataflow.generator.datafactory.errors.Categories;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.dataflow.model.expressions.TransformationClassification;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import java.util.*;
import java.util.stream.Collectors;

/**
 *  Transformation analyzer is parent of all analyzers
 *  It defined that every analyze method will return Collection of column nodes added in the analyze method
 * @param <T> type of analyzer
 */
public abstract class TransformationAnalyzer<T extends ITransformation> implements IAnalyzer<T, Map<IADFStream, ProcessedStream>>{

    private static final Logger LOGGER = new Logger(TransformationAnalyzer.class);

    private static final String DEFAULT_STREAM_NAME = "default stream";

    private final Class<T> analyzedType;

    private ColumnNameResolverVisitor columnNameResolver;



    protected IExpressionAnalyzer expressionAnalyzer;

    protected TransformationAnalyzer(Class<T> analyzedType) {
        this.analyzedType = analyzedType;
    }

    public void setColumnNameResolver(ColumnNameResolverVisitor columnNameResolver) {
        this.columnNameResolver = columnNameResolver;
    }

    public void setExpressionAnalyzer(IExpressionAnalyzer expressionAnalyzer) {
        this.expressionAnalyzer = expressionAnalyzer;
    }

    public Class<T> getAnalyzedType() {
        return analyzedType;
    }

    /**
     * Finds available processed streams transformation uses as a input
     * @param transformation transformation from which inputs are found
     * @param context analyzer context where available processed inputs are stored
     * @return Map of adfStream identifier and input proccessed stream
     */
    protected Map<IADFStream, ProcessedStream> findAvailableInputStreams(T transformation, AnalyzerContext context){
        Map<IADFStream, ProcessedStream> inputProcessedStreams = new HashMap<>();
        for(IADFStream inputStream : transformation.getInputs()){
            Optional<ProcessedStream> inputProcessedStreamOpt = context.findAvailableStream(inputStream);
            inputProcessedStreamOpt.ifPresent(processedStream -> inputProcessedStreams.put(inputStream, processedStream));
        }
        return inputProcessedStreams;
    }

    /**
     * Creates stream nodes for transformation.
     * Consumes processed Streams from context
     * @param transformation for which we want to create streams
     * @param transformationNode parent node for streams. Usually transformation node.
     * @param helper graph helper
     * @return created streams
     */
    protected Map<IADFStream, ProcessedStream> createStreams(T transformation, AnalyzerContext context, Node transformationNode, ADFGraphHelper helper){

        Collection<IADFStream> streams = transformation.getStreams();
        Map<IADFStream, ProcessedStream> processedStreams = new HashMap<>();

        for(IADFStream stream : streams){

            String substreamName = stream.getSubstreamName();
            if(substreamName.isEmpty())
                substreamName = DEFAULT_STREAM_NAME;

            // TODO all streams should not have node asociated with it. Proccessed streams will only create virtual stream, which will be connected to one and only stream. (just make sure line below runs only once this TODO and DEFAULT_STREAM it should already work :D)
            Node streamNode = helper.addSchemaNode(substreamName, transformationNode);

            List<Node> columnNodes = createColumns(transformation, context, streamNode, helper);
            processedStreams.put(stream, new ProcessedStream(streamNode, columnNodes));

        }

        return processedStreams;

    }

    /**
     * Create column nodes of transformation.
     * Consumes processed Streams from context.
     * @param transformation transformation for which to create column nodes
     * @param streamNode parent node of columns. Usually stream node.
     * @param helper graph helper
     * @return created nodes
     */
    protected List<Node> createColumns(T transformation, AnalyzerContext context, Node streamNode, ADFGraphHelper helper){

        Collection<IColumn> columns = transformation.getTable().getColumns();
        List<Node> columnNodes = new ArrayList<>();

        // Create special types of column (Definitions, references, matchings, ...)
        for(IColumn column : columns){
            Set<String> columnNames = column.accept(columnNameResolver);
            for(String columnName : columnNames){
                Node columnNode = helper.addColumnNode(columnName, streamNode);
                // add transforming attribute NON_TRANSFORMING as default to all created schema columns nodes
                helper.addAttributeTransformation(columnNode, TransformationClassification.NON_TRANSFORMING);
                columnNodes.add(columnNode);
            }
        }

        // For now just copy and connect all available streams into newly created nodes
        // TODO this won't probably work for every transformation, column visitor will be more utilized like in Derive column transformation and parametrization could also help later on
        for(IADFStream inputStream : transformation.getInputs()){
            Optional<ProcessedStream> processedStreamOpt = context.findAvailableStream(inputStream);

            if(processedStreamOpt.isPresent()){
               List<Node> inputNodes = processedStreamOpt.get().getColumnNodes();

                // Make sure all input has it's counterpart nodes exist
                Collection<Node> copiedColumns = helper.addColumnNodes(inputNodes.stream().map(Node::getName).collect(Collectors.toSet()), streamNode);
                columnNodes.addAll(copiedColumns);
                helper.connectNodesByName(inputNodes, columnNodes);
            }else{
                LOGGER.log(Categories.dataFlowErrors().transformationStreamNotFound()
                        .name(inputStream.getName())
                        .substream(inputStream.getSubstreamName()));
            }
        }

        return columnNodes;
    }

    /**
     * Connects transformation to endpoint
     * @param endpoint endpoint to connect to
     * @param connector connector which contains logic for connecting
     * @param transformation transformation to be connected. Transformation should contain all the required information to succesfully be connected.
     * @param processedTransformation processed transformation
     * @param helper graph helper
     * @param allowSchemaDrift flag which specifies wheter transformation node should be updated if endpoint has different schema
     * @return Connected transformation or empty if error was encountered
     */
    protected Optional<ProcessedTransformation> connectToEndpoint(IDataFlowEndpoint endpoint, IEndpointConnector connector, T transformation, ProcessedTransformation processedTransformation, AnalyzerContext context, ADFGraphHelper helper, boolean allowSchemaDrift){

        try{
            IReference endpointReference = endpoint.getReference();
            return connectByEndpointReference(endpointReference, connector, transformation, processedTransformation, context, helper, allowSchemaDrift);
        }catch (IllegalStateException e){
            LOGGER.log(Categories.dataFlowErrors().endpointReferenceNotFound().endpointName(endpoint.getName()));
            return Optional.empty();
        }

    }

    /**
     * Connects transformation to endpoint specified by reference
     * @param endpointReference reference to endpoint
     * @param transformation transformation to connect
     * @param processedTransformation processed transformation
     * @param helper graph helper
     * @return Connected transformation or empty if error was encountered
     */
    protected Optional<ProcessedTransformation> connectByEndpointReference(IReference endpointReference, IEndpointConnector connector, T transformation, ProcessedTransformation processedTransformation, AnalyzerContext context, ADFGraphHelper helper, boolean allowSchemaDrift){

        switch (endpointReference.getType()){
            case DATASET:
                return connector.connectToDataset(endpointReference, transformation, processedTransformation, context, helper, allowSchemaDrift);
            case LINKED_SERVICE:
                return connector.connectToInlineDataset(endpointReference, context);
            case SCHEMA_LINKED_SERVICE: // TODO implement whatever this means
            case UNKNOWN:
                LOGGER.log(Categories.dataFlowErrors().unknownReference().referenceName(endpointReference.getReferenceName()));
                return Optional.empty();
            default:
                return Optional.empty();
        }
    }

}
