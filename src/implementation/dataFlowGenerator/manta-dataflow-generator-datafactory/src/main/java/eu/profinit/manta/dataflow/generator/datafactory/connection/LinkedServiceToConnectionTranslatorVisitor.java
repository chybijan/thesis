package eu.profinit.manta.dataflow.generator.datafactory.connection;

import eu.profinit.manta.connector.common.connections.Connection;
import eu.profinit.manta.connector.common.connections.ConnectionType;
import eu.profinit.manta.connector.common.connections.impl.ConnectionImpl;
import eu.profinit.manta.connector.datafactory.model.linkedservice.*;
import eu.profinit.manta.connector.datafactory.model.visitor.ILinkedServiceVisitor;

/**
 * Class connecting inline dataset to transformation
 */
public class LinkedServiceToConnectionTranslatorVisitor implements ILinkedServiceVisitor<Connection> {

    @Override
    public Connection visitAmazonRedshiftLinkedService(IAmazonRedshiftLinkedService ls) {
        return new ConnectionImpl(ConnectionType.POSTGRESQL.getId(),
                null,
                ls.getServer().getStringValue(),
                ls.getDatabase().getStringValue(),
                null,
                ls.getUsername().getStringValue());
    }

    @Override
    public Connection visitAzurePostgresSqlLinkedService(IAzurePostgreSqlLinkedService ls) {
        return new ConnectionImpl(ConnectionType.POSTGRESQL.getId(),
                ls.getConnectionString().getStringValue());
    }

    @Override
    public Connection visitAzureSqlDatabaseLinkedService(IAzureSqlDatabaseLinkedService ls) {
        return new ConnectionImpl(ConnectionType.MSSQL.getId(),
                ls.getConnectionString().getStringValue());
    }

    @Override
    public Connection visitAzureSqlDWLLinkedService(IAzureSqlDWLinkedService ls) {
        return new ConnectionImpl(ConnectionType.MSSQL.getId(),
                ls.getConnectionString().getStringValue());
    }

    @Override
    public Connection visitAzureSqlMILLinkedService(IAzureSqlMILinkedService ls) {
        return new ConnectionImpl(ConnectionType.MSSQL.getId(),
                ls.getConnectionString().getStringValue());
    }

    @Override
    public Connection visitDb2LinkedService(IDb2LinkedService ls) {
        return new ConnectionImpl(ConnectionType.DB2.getId(),
                ls.getConnectionString().getStringValue());
    }

    @Override
    public Connection visitGoogleBigQueryLinkedService(IGoogleBigQueryLinkedService ls) {
        return new ConnectionImpl(ConnectionType.BIGQUERY.getId(),
                null,
                null,
                ls.getProject().getStringValue(),
                null,
                null);
    }

    @Override
    public Connection visitGreenplumLinkedService(IGreenplumLinkedService ls) {
        return new ConnectionImpl(ConnectionType.POSTGRESQL.getId(), ls.getConnectionString().getStringValue());
    }

    @Override
    public Connection visitHiveLinkedService(IHiveLinkedService ls) {
        // TODO because it cannot be used in data flows so it can be omitted for now and this is probably not correct
        return new ConnectionImpl(ConnectionType.HIVE.getId(),
                null,
                ls.getHost().getStringValue(),
                null,
                null,
                null);
    }

    @Override
    public Connection visitNetezzaLinkedService(INetezzaLinkedService ls) {
        return new ConnectionImpl(ConnectionType.NETEZZA.getId(),
                ls.getConnectionString().getStringValue());
    }

    @Override
    public Connection visitOracleLinkedService(IOracleLinkedService ls) {
        return new ConnectionImpl(ConnectionType.ORACLE.getId(),
                ls.getConnectionString().getStringValue());
    }

    @Override
    public Connection visitPostgreSqlLinkedService(IPostgreSqlLinkedService ls) {
        return new ConnectionImpl(ConnectionType.POSTGRESQL.getId(),
                ls.getConnectionString().getStringValue());
    }

    @Override
    public Connection visitSapHanaLinkedService(ISapHanaLinkedService ls) {
        return new ConnectionImpl(ConnectionType.SAPHANA.getId(),
                ls.getConnectionString().getStringValue());
    }

    @Override
    public Connection visitSnowflakeLinkedService(ISnowflakeLinkedService ls) {
        return new ConnectionImpl(ConnectionType.SNOWFLAKE.getId(),
                ls.getConnectionString().getStringValue());
    }

    @Override
    public Connection visitSqlServerLinkedService(ISqlServerLinkedService ls) {
        return new ConnectionImpl(ConnectionType.UNKNOWN.getId(),
                ls.getConnectionString().getStringValue());
    }

    @Override
    public Connection visitSybaseLinkedService(ISybaseLinkedService ls) {
        return new ConnectionImpl(ConnectionType.SYBASE.getId(),
                null,
                ls.getServer().getStringValue(),
                ls.getDatabase().getStringValue(),
                ls.getSchema().getStringValue(),
                ls.getUsername().getStringValue());
    }

    @Override
    public Connection visitTeradataLinkedService(ITeradataLinkedService ls) {
        return new ConnectionImpl(ConnectionType.TERADATA.getId(),
                ls.getConnectionString().getStringValue(),
                ls.getServer().getStringValue(),
                null,
                null,
                ls.getUsername().getStringValue());
    }

    @Override
    public Connection visitUnknownLinkedService(IUnknownLinkedService ls) {
        return new ConnectionImpl(ConnectionType.UNKNOWN.getId(),
                null);
    }

    @Override
    public Connection visitAmazonS3LinkedService(IAmazonS3LinkedService ls) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Connection visitAzureBlobFS(IAzureBlobFSLinkedService ls) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Connection visitAzureBlobStorageLinkedService(IAzureBlobStorageLinkedService ls) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Connection visitAzureDataLakeStoreLinkedService(IAzureDataLakeStoreLinkedService ls) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Connection visitAzureFileStorageLinkedService(IAzureFileStorageLinkedService ls) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Connection visitFileServerLinkedService(IFileServerLinkedService ls) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Connection visitFtpServerLinkedService(IFtpServerLinkedService ls) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Connection visitGoogleCloudStorageLinkedService(IGoogleCloudStorageLinkedService ls) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Connection visitHdfsLinkedService(IHdfsLinkedService ls) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Connection visitSftpServerLinkedService(ISftpServerLinkedService ls) {
        throw new UnsupportedOperationException();
    }
}
