package eu.profinit.manta.dataflow.generator.datafactory.connection.sourceconnector;

import eu.profinit.manta.connector.common.connections.Connection;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.connector.datafactory.model.dataset.database.IDatabaseDataset;
import eu.profinit.manta.dataflow.generator.common.query.DataflowQueryService;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedStream;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedTransformation;
import eu.profinit.manta.dataflow.generator.datafactory.connection.AbstractDatasetConnectorVisitor;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import eu.profinit.manta.dataflow.model.Node;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Class connecting transformation do source dataset
 */
public class SourceDatasetConnectorVisitor extends AbstractDatasetConnectorVisitor {

    public SourceDatasetConnectorVisitor(ITransformation transformation, ProcessedTransformation processedTransformation, AnalyzerContext context, ADFGraphHelper graphHelper, DataflowQueryService queryService, boolean allowSchemaDrift) {
        super(transformation, processedTransformation, context, graphHelper, queryService, allowSchemaDrift);
    }

    @Override
    protected Optional<ProcessedTransformation> connectByTable(Connection connection, IDatabaseDataset dataset){

        ProcessedTransformation updatedTransformation = new ProcessedTransformation(processedTransformation.getTransformationNode(), processedTransformation.getProcessedStreams());

        List<Node> columns = addOrDeduceColumnNodes(connection, dataset);

        for(Map.Entry<IADFStream, ProcessedStream> stream : processedTransformation.getProcessedStreams().entrySet()){
            graphHelper.connectNodesByNameCI(columns, stream.getValue().getColumnNodes());
            updatedTransformation.addProcessedStream(stream.getKey(), checkAndGenerateDrifted(columns, stream.getValue()));
        }

        return Optional.of(updatedTransformation);
    }

    @Override
    protected ProcessedStream connectFileNodesByNameAndPosition(List<Node> fileColumns, ProcessedStream processedStream){
        graphHelper.connectFileNodesByNameAndPositionAsSource(fileColumns, processedStream.getColumnNodes());
        return checkAndGenerateDrifted(fileColumns, processedStream);
    }

    @Override
    protected ProcessedStream connectDefaultFileColumns(List<Node> fileColumns, ProcessedStream processedStream){
        graphHelper.connectDefaultFileColumnsAsSource(fileColumns, processedStream.getColumnNodes());
        return checkAndGenerateDrifted(fileColumns, processedStream);
    }

    private ProcessedStream checkAndGenerateDrifted(List<Node> driftingSource, ProcessedStream processedStream){
        List<Node> driftedColumns = graphHelper.getUnconnectedOnOutput(driftingSource);

        if(allowSchemaDrift)
            return generateDriftedColumns(driftedColumns, processedStream);

        return processedStream;
    }

}
