package eu.profinit.manta.dataflow.generator.datafactory.analyzer.context;

import eu.profinit.manta.connector.datafactory.model.IDataset;
import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowEndpoint;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedStream;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

/**
 * Context for analyzers
 *
 * Context will contain everything pipeline or dataflow needs for it to succesfully anlyze
 */
public interface AnalyzerContext {

    /**
     * @param datasetReference IReference with unique name of dataset
     * @return concrete dataset from reference
     */
    Optional<IDataset> getDataset(IReference datasetReference);

    /**
     * @param linkedServiceReference IReference on linked service
     * @return conrete linked servise from referenece
     */
    Optional<ILinkedService> getLinkedService(IReference linkedServiceReference);

    /**
     * @param endpointName name of searched endpoint
     * @return Concrete endpoint, found by name
     */
    Optional<IDataFlowEndpoint> getDataflowEndpointByName(String endpointName);

    void setDatasets(Collection<IDataset> datasets);

    void setLinkedServices(Collection<ILinkedService> linkedServices);

    void setDataflowEndpoints(Map<String, IDataFlowEndpoint> endpoints);

    /**
     * Adds new available streams
     * @param newAvailableStreams streams to be added
     */
    void addAvailableStreams(Map<IADFStream, ProcessedStream> newAvailableStreams);

    /**
     * Find available processed stream
     * @return processed stream or Optional.empty if stream is not present
     */
    Optional<ProcessedStream> findAvailableStream(IADFStream streamToLookFor);

    /**
     * Find and delete available processed stream
     * @return processed stream or Optional.empty if stream is not present
     */
    Optional<ProcessedStream> consumeAvailableStream(IADFStream streamToLookFor);

}
