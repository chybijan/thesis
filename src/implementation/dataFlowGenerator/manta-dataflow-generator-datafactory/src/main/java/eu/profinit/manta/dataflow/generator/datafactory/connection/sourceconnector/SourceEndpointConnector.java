package eu.profinit.manta.dataflow.generator.datafactory.connection.sourceconnector;

import eu.profinit.manta.connector.datafactory.model.IDataset;
import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.dataflow.generator.common.query.DataflowQueryService;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedTransformation;
import eu.profinit.manta.dataflow.generator.datafactory.connection.IEndpointConnector;
import eu.profinit.manta.dataflow.generator.datafactory.errors.Categories;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import java.util.Optional;

public class SourceEndpointConnector implements IEndpointConnector {

    private static final Logger LOGGER = new Logger(SourceEndpointConnector.class);

    private final DataflowQueryService queryService;

    public SourceEndpointConnector(DataflowQueryService queryService) {
        this.queryService = queryService;
    }

    @Override
    public Optional<ProcessedTransformation> connectToDataset(IReference endpointReference, ITransformation transformation, ProcessedTransformation processedTransformation, AnalyzerContext context, ADFGraphHelper helper, boolean allowSchemaDrift) {
        SourceDatasetConnectorVisitor datasetConnector = new SourceDatasetConnectorVisitor(transformation, processedTransformation, context, helper, queryService, allowSchemaDrift);

        Optional<IDataset> datasetOpt = context.getDataset(endpointReference);
        if(datasetOpt.isPresent())
            return datasetOpt.get().accept(datasetConnector);
        else{
            LOGGER.log(Categories.dataFlowErrors().datasetNotFound().name(endpointReference.getReferenceName()));
            return Optional.empty();
        }
    }

    @Override
    public Optional<ProcessedTransformation> connectToInlineDataset(IReference endpointReference, AnalyzerContext context) {
        SourceInlineDatasetConnectorVisitor inlineDatasetConnector = new SourceInlineDatasetConnectorVisitor();

        Optional<ILinkedService> linkedServiceOpt = context.getLinkedService(endpointReference);
        if(linkedServiceOpt.isPresent())
            return linkedServiceOpt.get().accept(inlineDatasetConnector);
        else{
            LOGGER.log(Categories.dataFlowErrors().linkedServiceNotFound().name(endpointReference.getReferenceName()));
            return Optional.empty();
        }
    }
}
