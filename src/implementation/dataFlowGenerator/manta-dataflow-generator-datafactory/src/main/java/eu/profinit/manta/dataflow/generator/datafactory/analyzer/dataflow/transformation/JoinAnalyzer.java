package eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IFilterTransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IJoinTransformation;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedStream;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.dataflow.model.NodeType;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class JoinAnalyzer extends TransformationAnalyzer<IJoinTransformation>{

    private static final Logger LOGGER = new Logger(JoinAnalyzer.class);

    public JoinAnalyzer() {
        super(IJoinTransformation.class);
    }

    @Override
    public Map<IADFStream, ProcessedStream> analyze(IJoinTransformation toAnalyze, AnalyzerContext context, Node parentNode, ADFGraphHelper helper) {
        LOGGER.info("Analyzing join: " + toAnalyze.getName());
        Node transformationNode = helper.addNode(toAnalyze.getName(), NodeType.DATAFACTORY_JOIN_TRANSFORMATION, parentNode);

        Map<IADFStream, ProcessedStream> inputProcessedStreams = findAvailableInputStreams(toAnalyze, context);

        Map<IADFStream, ProcessedStream> newProcessedStreams = createStreams(toAnalyze, context, transformationNode, helper);

        IExpression joinCondition = toAnalyze.getJoinCondition();

        // Connect indirect columns based on condition
        Set<Node> columnNodes = new HashSet<>();
        newProcessedStreams.values().forEach(stream -> columnNodes.addAll(stream.getColumnNodes()));

        // TODO test this after expression analyzer is implemented
        expressionAnalyzer.analyzeIndirectFlows(joinCondition, inputProcessedStreams, columnNodes);

        // Add condition attribute
        // TODO add FILTER ON to gui to be nice
        transformationNode.addAttribute("JOIN CONDITION", joinCondition.asString());

        transformationNode.addAttribute("JOIN TYPE", toAnalyze.getJoinType().toString());

        return newProcessedStreams;

    }

}
