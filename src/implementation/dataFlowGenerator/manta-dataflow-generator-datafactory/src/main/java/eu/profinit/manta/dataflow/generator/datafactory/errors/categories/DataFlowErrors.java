package eu.profinit.manta.dataflow.generator.datafactory.errors.categories;

import eu.profinit.manta.platform.logging.api.categorization.Error;
import eu.profinit.manta.platform.logging.api.categorization.Impact;
import eu.profinit.manta.platform.logging.api.categorization.Severity;
import eu.profinit.manta.platform.logging.common.messages.CommonMessages;

public class DataFlowErrors extends eu.profinit.manta.platform.logging.common.errordefinitions.technologies.reporting.DataflowErrors {

    @Error(
            userMessage = "Azure data factory input could not be analysed",
            technicalMessage = "Azure data factory input could not be analysed",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SCRIPT
    )
    public DataFactoryInputIsInvalidBuilder dataFactoryInputIsInvalid() {
        return new DataFactoryInputIsInvalidBuilder(this);
    }

    @Error(
            userMessage = "Data flow was not found during analysis",
            technicalMessage = "Data flow was not found during analysis",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SCRIPT
    )
    public DataFlowNotFoundBuilder dataFlowNotFound() {
        return new DataFlowNotFoundBuilder(this);
    }

    @Error(userMessage = "Ignoring precedence resolver constraint with null argument.",
            technicalMessage = "Ignoring precedence resolver constraint with null argument.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.WARN,
            lineageImpact = Impact.UNDEFINED)
    public PrecedenceResolverBuilder precedenceResolver() {
        return new PrecedenceResolverBuilder(this);
    }

    @Error(
            userMessage = "Data flow endpoint with name \"%{name}\" not found. Lineage might be incomplete",
            technicalMessage = "Data flow endpoint with name \"%{name}\" not found. Lineage might be incomplete." +
                    "Place of endpoint: %{endpointPlace}",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SCRIPT
    )
    public DataEndpointNotFoundBuilder dataEndpointNotFound() {
        return new DataEndpointNotFoundBuilder(this);
    }

    @Error(
            userMessage = "Data flow endpoint '%{endpointName}' reference does not exist. Lineage might be incomplete",
            technicalMessage = "Data flow endpoint '%{endpointName}' reference does not exist. Lineage might be incomplete",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SCRIPT
    )
    public EndpointReferenceNotFoundBuilder endpointReferenceNotFound() {
        return new EndpointReferenceNotFoundBuilder(this);
    }

    @Error(
            userMessage = "Dataset '%{name}' not found.",
            technicalMessage = "Dataset '%{name}' not found.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SCRIPT
    )
    public DatasetNotFoundBuilder datasetNotFound() {
        return new DatasetNotFoundBuilder(this);
    }

    @Error(
            userMessage = "Linked service '%{name}' not found.",
            technicalMessage = "Linked service '%{name}' not found.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SCRIPT
    )
    public LinkedServiceNotFoundBuilder linkedServiceNotFound() {
        return new LinkedServiceNotFoundBuilder(this);
    }

    @Error(
            userMessage = "Transformation stream '%{name}@%{substream}' not found. Some transformations might be unconnected",
            technicalMessage = "Transformation stream '%{name}@%{substream}' not found. Some transformations might be unconnected",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SCRIPT
    )
    public TransformationStreamNotFoundBuilder transformationStreamNotFound() {
        return new TransformationStreamNotFoundBuilder(this);
    }

    @Error(
            userMessage = "Unknown reference named: '%{referenceName}'. Resource referenced by this reference won't be included in lineage.",
            technicalMessage = "Unknown reference named: '%{referenceName}'. Resource referenced by this reference won't be included in lineage.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SCRIPT
    )
    public UnknownReferenceBuilder unknownReference() {
        return new UnknownReferenceBuilder(this);
    }

    @Error(userMessage = "Data flow transformation attribute named: '%{name}' has invalid value: '%{value}'." +
            "Expected format: '%{expected}'",
            technicalMessage = "Data flow transformation attribute named: '%{name}' has invalid value: '%{value}'." +
                    "Expected format: '%{expected}'",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public InvalidTransformationAttributeValueBuilder invalidTransformationAttributeValue() {
        return new InvalidTransformationAttributeValueBuilder(this);
    }

    @Error(userMessage = "Unexpected linked service type '%{linkedServiceType}' used for location of type '%{locationType}'.",
            technicalMessage = "Unexpected linked service type '%{linkedServiceType}' used for location of type '%{locationType}'.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnexpectedLinkedServiceTypeBuilder unexpectedLinkedServiceType() {
        return new UnexpectedLinkedServiceTypeBuilder(this);
    }

    @Error(userMessage = "Dataset schema of dataset named: '%{datasetName}' was not processed correctly. Default file definition used.",
            technicalMessage = "Dataset schema of dataset named: '%{datasetName}' was not processed correctly. Default file definition used.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public DatasetSchemaNotProcessedCorrectlyBuilder datasetSchemaNotProcessedCorrectly() {
        return new DatasetSchemaNotProcessedCorrectlyBuilder(this);
    }

    @Error(userMessage = "Unknown operation: '%{operation}' encountered. Lineage might be incomplete.",
            technicalMessage = "Unknown operation: '%{operation}' encountered. Lineage might be incomplete.",
            solution = CommonMessages.CONTACT_MANTA_SUPPORT,
            severity = Severity.ERROR,
            lineageImpact = Impact.SINGLE_INPUT)
    public UnknownOperationBuilder unknownOperation() {
        return new UnknownOperationBuilder(this);
    }
}
