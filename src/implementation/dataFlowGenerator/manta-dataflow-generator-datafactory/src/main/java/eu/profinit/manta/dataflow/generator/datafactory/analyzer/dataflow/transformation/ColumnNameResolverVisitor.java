package eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumn;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumnDef;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumnMatch;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.IColumnRef;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.datarepresentation.visitor.IColumnVisitor;

import java.util.HashSet;
import java.util.Set;

public class ColumnNameResolverVisitor implements IColumnVisitor<Set<String>> {

    @Override
    public Set<String> visitColumnDef(IColumnDef columnDef) {
        Set<String> columnNames = new HashSet<>();
        columnNames.add(columnDef.getName());
        return columnNames;
    }

    @Override
    public Set<String> visitColumnRef(IColumnRef columnRef) {
        Set<String> columnNames = new HashSet<>();
        // TODO evaluate expression as string
        columnNames.add(columnRef.getName().getRawForm());
        return columnNames;
    }

    @Override
    public Set<String> visitColumnMatch(IColumnMatch columnMatch) {
        Set<String> columnNames = new HashSet<>();
        for(IColumn column : columnMatch.getColumns()){
            columnNames.addAll(column.accept(this));
        }
        return columnNames;
    }
}
