package eu.profinit.manta.dataflow.generator.datafactory.analyzer.context;

import eu.profinit.manta.connector.datafactory.model.IDataset;
import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowEndpoint;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedStream;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class AnalyzerContextImpl implements AnalyzerContext{

    /**
     * Key is unique name of dataset, value is the dataset
     */
    private Map<String, IDataset> datasets;

    /**
     * Key is unique name of linked service, value is the linked service
     */
    private Map<String, ILinkedService> linkedServices;

    /**
     * Key is unique name of endpoint, value is the endpoint
     */
    private Map<String, IDataFlowEndpoint> endpoints;

    /**
     * Streams which were already created and something can be connected to it
     */
    private Map<IADFStream, ProcessedStream> availableStreams;

    public AnalyzerContextImpl(){
        datasets = new HashMap<>();
        linkedServices = new HashMap<>();
        endpoints = new HashMap<>();
        availableStreams = new HashMap<>();
    }

    @Override
    public Optional<IDataset> getDataset(IReference datasetReference) {

        IDataset dataset = datasets.get(datasetReference.getReferenceName());
        return dataset == null ? Optional.empty() : Optional.of(dataset);

    }

    @Override
    public Optional<ILinkedService> getLinkedService(IReference linkedServiceReference) {

        ILinkedService linkedService = linkedServices.get(linkedServiceReference.getReferenceName());
        return linkedService == null ? Optional.empty() : Optional.of(linkedService);

    }

    @Override
    public Optional<IDataFlowEndpoint> getDataflowEndpointByName(String endpointName) {
        IDataFlowEndpoint endpoint = endpoints.get(endpointName);
        return endpoint == null ? Optional.empty() : Optional.of(endpoint);
    }

    @Override
    public void setDatasets(Collection<IDataset> datasets) {

        for(IDataset dataset : datasets){
            this.datasets.put(dataset.getName(), dataset);
        }

    }

    @Override
    public void setLinkedServices(Collection<ILinkedService> linkedServices) {

        for(ILinkedService linkedService : linkedServices){
            this.linkedServices.put(linkedService.getName(), linkedService);
        }

    }

    @Override
    public void setDataflowEndpoints(Map<String, IDataFlowEndpoint> endpoints) {
        this.endpoints = endpoints;
    }

    @Override
    public void addAvailableStreams(Map<IADFStream, ProcessedStream> newAvailableStreams) {
        availableStreams.putAll(newAvailableStreams);
    }

    @Override
    public Optional<ProcessedStream> findAvailableStream(IADFStream streamToLookFor) {
        return Optional.ofNullable(availableStreams.get(streamToLookFor));
    }

    @Override
    public Optional<ProcessedStream> consumeAvailableStream(IADFStream streamToLookFor) {
        Optional<ProcessedStream> processedStreamOpt = findAvailableStream(streamToLookFor);
        if(processedStreamOpt.isPresent())
            availableStreams.remove(streamToLookFor);

        return processedStreamOpt;
    }
}
