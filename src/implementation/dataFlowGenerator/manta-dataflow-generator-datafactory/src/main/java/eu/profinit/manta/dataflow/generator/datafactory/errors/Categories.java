package eu.profinit.manta.dataflow.generator.datafactory.errors;

import eu.profinit.manta.dataflow.generator.datafactory.errors.categories.DataFlowErrors;
import eu.profinit.manta.dataflow.generator.datafactory.errors.categories.UnsupportedErrors;
import eu.profinit.manta.platform.logging.common.errordefinitions.CommonErrors;


public class Categories extends CommonErrors {

    public static DataFlowErrors dataFlowErrors() {
        return new DataFlowErrors();
    }

    public static UnsupportedErrors unsupportedErrors() {
        return new UnsupportedErrors();
    }
}
