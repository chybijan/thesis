package eu.profinit.manta.dataflow.generator.datafactory.analyzer;

import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import eu.profinit.manta.dataflow.model.Node;

import java.util.Collection;

/**
 * Interface for all individual analyzers
 * @param <T> Analyzed resource
 */
public interface IAnalyzer<T, R> {

    R analyze(T toAnalyze, AnalyzerContext context, Node parentNode, ADFGraphHelper helper);

}
