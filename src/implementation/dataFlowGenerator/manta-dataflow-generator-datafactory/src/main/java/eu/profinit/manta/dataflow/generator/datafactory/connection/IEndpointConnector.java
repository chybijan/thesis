package eu.profinit.manta.dataflow.generator.datafactory.connection;

import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedTransformation;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;

import java.util.Optional;

public interface IEndpointConnector {

    /**
     * Connects transformation to dataset
     * @param endpointReference reference to a dataset endpoint
     * @param transformation transformation data
     * @param processedTransformation processed tranformation
     * @param context analyzer context
     * @param helper graph helper
     * @param allowSchemaDrift flag which signals if transformation node schema should be updated if dataset has different schema
     * @return Connected transformation or empty if error was encountered
     */
    Optional<ProcessedTransformation> connectToDataset(IReference endpointReference, ITransformation transformation, ProcessedTransformation processedTransformation, AnalyzerContext context, ADFGraphHelper helper, boolean allowSchemaDrift);

    /**
     * Connects transformation to inline dataset
     * @param endpointReference reference to a linked service endpoint
     * @param context analyzer context
     * @return Connected transformation or empty if error was encountered
     */
    Optional<ProcessedTransformation> connectToInlineDataset(IReference endpointReference, AnalyzerContext context);
}
