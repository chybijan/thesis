package eu.profinit.manta.dataflow.generator.datafactory.helper;

import eu.profinit.manta.connector.datafactory.model.IDataFlow;
import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.dataset.IDatasetSchema;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.IDatasetLocation;
import eu.profinit.manta.dataflow.generator.common.helper.AbstractGraphHelper;
import eu.profinit.manta.dataflow.generator.modelutils.nodecreator.NodeCreator;
import eu.profinit.manta.dataflow.model.*;
import eu.profinit.manta.dataflow.model.expressions.TransformationClassification;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class ADFGraphHelper extends AbstractGraphHelper {

    private static final String UNKNOWN_FACTORY_NAME = "UNKNOWN FACTORY";

    private static final String DATAFACTORY_COLUMN_DESCRIPTION = "DATAFACTORY_COLUMN_DESCRIPTION";

    private final ILocationNodeCreator locationNodeCreator;

    private final NodeCreator nodeCreator;

    public ADFGraphHelper(Graph graph, Resource dbResource, Resource scriptResource, NodeType columnFlowNodeType, ILocationNodeCreator locationNodeCreator, NodeCreator nodeCreator) {
        super(graph, dbResource, scriptResource, columnFlowNodeType);
        this.locationNodeCreator = locationNodeCreator;
        this.nodeCreator = nodeCreator;
    }

    public String getUnknownFactoryName(){
        return UNKNOWN_FACTORY_NAME;
    }

    public Graph Graph(){
        return getGraph();
    }


    public Node addFactoryNode(String factoryName, Node parentNode){
        return addNode(factoryName, NodeType
                .DATAFACTORY_FACTORY, parentNode);
    }

    public Node addMappingDataFlowNode(IDataFlow dataFlow, Node parentNode){
        return addNode(dataFlow.getName(), NodeType.DATAFACTORY_MAPPING_DATAFLOW, parentNode);
    }

    public Node addSchemaNode(String schemaName, Node parentNode){
        return addNode(schemaName, NodeType.DATAFACTORY_STREAM, parentNode);
    }

    public Node addColumnNode(String columnName, Node parentNode){
        return addNode(columnName, NodeType.COLUMN_FLOW, parentNode);
    }

    public Collection<Node> addColumnNodes(Collection<String> columnNames, Node parentNode){

        Set<Node> createdNodes = new HashSet<>();
        for(String columnName : columnNames){
            createdNodes.add(addColumnNode(columnName, parentNode));
        }

        return createdNodes;
    }

    public Node createFileNodeFromLocation(IDatasetLocation location, ILinkedService ls){
        return locationNodeCreator.createLocationNode(location, ls, this);
    }

    public Node createFileNode(File file, String host) {
        return nodeCreator.createFileNode(getGraph(), host, file);
    }

    public List<Node> createFileColumnNodes(Node fileNode, int nodeCount) {
        return nodeCreator.addFileColumnNodes(getGraph(), fileNode, nodeCount);
    }

    /**
     * Method adds transforming attribute to node's attributes or overwrite current attribute value.
     * @param node node
     * @param transformationClassification transformation
     */
    public void addAttributeTransformation(Node node, TransformationClassification transformationClassification) {
        if (node.getAttributes().get(AttributeNames.NODE_TRANSFORMATION_TAG) == null) {
            node.addAttribute(AttributeNames.NODE_TRANSFORMATION_TAG, transformationClassification);
        } else {
            for (Object attributeValue : node.getAttributes().get(AttributeNames.NODE_TRANSFORMATION_TAG)) {
                if (transformationClassification.equals(TransformationClassification.TRANSFORMING)
                        || (transformationClassification.equals(TransformationClassification.MINOR)
                        && !attributeValue.equals(TransformationClassification.TRANSFORMING))) {
                    node.getAttributes().get(AttributeNames.NODE_TRANSFORMATION_TAG).clear();
                    node.addAttribute(AttributeNames.NODE_TRANSFORMATION_TAG, transformationClassification);
                }
            }
        }
    }

    /**
     * Connects two collection of nodes by node name
     * @param sources source nodes
     * @param targets target nodes
     */
    public void connectNodesByName(Collection<Node> sources, Collection<Node> targets){
        for(Node source : sources){
            for(Node target : targets){
                if(source.getName().equals(target.getName())){
                    addDirectFlow(source, target);
                    break;
                }
            }
        }
    }

    /**
     * Connects two collection of nodes by node name case insensitive
     * @param sources source nodes
     * @param targets target nodes
     */
    public void connectNodesByNameCI(Collection<Node> sources, Collection<Node> targets){
        for(Node source : sources){
            for(Node target : targets){
                if(source.getName().equalsIgnoreCase(target.getName())){
                    addDirectFlow(source, target);
                    break;
                }
            }
        }
    }

    /**
     * Connects two Lists of nodes by position
     * @param sources source nodes
     * @param targets target nodes
     */
    public void connectNodesByPosition(List<Node> sources, List<Node> targets){
        int minSize = Math.min(sources.size(), targets.size());

        for(int i = 0; i < minSize; ++i){
            addDirectFlow(sources.get(i), targets.get(i));
        }
    }

    public void connectDefaultFileColumnsAsSource(List<Node> fileStreamColumns, List<Node> targetNodes) {

        int minSize = Math.min(fileStreamColumns.size(), targetNodes.size());

        for(int i = 0; i < minSize; ++i){
            Node fileSourceNode = fileStreamColumns.get(i);
            Node targetNode = targetNodes.get(i);
            addDirectFlow(fileSourceNode, targetNode);
            fileSourceNode.addAttribute(DATAFACTORY_COLUMN_DESCRIPTION, targetNode.getName());
        }
    }

    public void connectDefaultFileColumnsAsSink(List<Node> fileStreamColumns, List<Node> sourceNodes) {
        int minSize = Math.min(fileStreamColumns.size(), sourceNodes.size());

        for(int i = 0; i < minSize; ++i){
            Node fileNode = fileStreamColumns.get(i);
            Node sourceNode = sourceNodes.get(i);
            addDirectFlow(sourceNode, fileNode);
            fileNode.addAttribute(DATAFACTORY_COLUMN_DESCRIPTION, sourceNode.getName());
        }
    }

    /**
     * Create file column nodes from dataset schema
     * @param datasetSchema dataset schema
     * @param parentNode parent node
     * @return created column nodes
     */
    public List<Node> createDatasetSchemaColumnNodes(IDatasetSchema datasetSchema, Node parentNode) {

        List<Node> createdColumns = new ArrayList<>();

        if(datasetSchema.getExpression() != null){
            // TODO create nodes from expression
            // For now just create numbered column
            createdColumns.addAll(createFileColumnNodes(parentNode, 1));
        }else{
            int defaultColumnNameCount = 1;

            for(IDatasetSchema.IDataElement schemaElement : datasetSchema.getDataElements()){

                // Name is for some reason not required
                if(schemaElement.getName() != null){
                    createdColumns.add(addColumnNode(schemaElement.getName().getStringValue(), parentNode));
                }else{
                    createdColumns.add(addColumnNode(Integer.toString(defaultColumnNameCount), parentNode));
                    defaultColumnNameCount += 1;
                }
            }
        }

        return createdColumns;
    }

    /**
     * Connect File nodes by name and unconnected by position
     * @param fileNodes source file nodes
     * @param targetNodes target nodes
     */
    public void connectFileNodesByNameAndPositionAsSource(List<Node> fileNodes, List<Node> targetNodes) {
        connectNodesByName(fileNodes, targetNodes);

        List<Node> unconnectedSources = getUnconnectedOnOutput(fileNodes);
        List<Node> unconnectedTargets = getUnconnectedOnInput(targetNodes);

        connectDefaultFileColumnsAsSource(unconnectedSources, unconnectedTargets);
    }

    /**
     * Connect File nodes by name and unconnected by position
     * @param fileNodes file nodes
     * @param sourceNodes source nodes
     */
    public void connectFileNodesByNameAndPositionAsSink(List<Node> fileNodes, List<Node> sourceNodes) {
        connectNodesByName(fileNodes, sourceNodes);

        List<Node> unconnectedSources = getUnconnectedOnOutput(sourceNodes);
        List<Node> unconnectedFiles = getUnconnectedOnInput(fileNodes);

        connectDefaultFileColumnsAsSink(unconnectedFiles, unconnectedSources);
    }

    public List<Node> getUnconnectedOnOutput(List<Node> nodes){
        return nodes.stream()
                .filter(n -> n.getOutgoingEdges().isEmpty())
                .collect(Collectors.toList());
    }

    public List<Node> getUnconnectedOnInput(List<Node> nodes){
        return nodes.stream()
                .filter(n -> n.getIncomingEdges().isEmpty())
                .collect(Collectors.toList());
    }

}
