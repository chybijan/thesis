package eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IUnknownTransformation;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedStream;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.dataflow.model.NodeType;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import java.util.*;

public class UnkownTransformationAnalyzer extends TransformationAnalyzer<IUnknownTransformation>{

    private static final Logger LOGGER = new Logger(UnkownTransformationAnalyzer.class);

    public UnkownTransformationAnalyzer() {
        super(IUnknownTransformation.class);
    }

    @Override
    public Map<IADFStream, ProcessedStream> analyze(IUnknownTransformation toAnalyze, AnalyzerContext context, Node parentNode, ADFGraphHelper helper) {
        LOGGER.info("Analyzing unknown transformation");
        Node transformationNode = helper.addNode(toAnalyze.getName(), NodeType.DATAFACTORY_TRANSFORMATION, parentNode);

        Map<IADFStream, ProcessedStream> processedStreams = createStreams(toAnalyze, context, transformationNode, helper);

        return processedStreams;

    }

}
