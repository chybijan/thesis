package eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.transformation;

import eu.profinit.manta.connector.datafactory.model.dataflow.IDataFlowEndpoint;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ISourceTransformation;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedStream;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedTransformation;
import eu.profinit.manta.dataflow.generator.datafactory.connection.IEndpointConnector;
import eu.profinit.manta.dataflow.generator.datafactory.errors.Categories;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.dataflow.model.NodeType;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import java.util.*;

public class SourceAnalyzer extends TransformationAnalyzer<ISourceTransformation>{

    private static final Logger LOGGER = new Logger(SourceAnalyzer.class);

    private IEndpointConnector sourceConnector;

    public SourceAnalyzer() {
        super(ISourceTransformation.class);
    }

    public void setSourceConnector(IEndpointConnector sourceConnector) {
        this.sourceConnector = sourceConnector;
    }

    @Override
    public Map<IADFStream, ProcessedStream> analyze(ISourceTransformation toAnalyze, AnalyzerContext context, Node parentNode, ADFGraphHelper helper) {
        LOGGER.info("Analyzing source");
        Node transformationNode = helper.addNode(toAnalyze.getName(), NodeType.DATAFACTORY_SOURCE_TRANSFORMATION, parentNode);

        Map<IADFStream, ProcessedStream> processedStreams = createStreams(toAnalyze, context, transformationNode, helper);
        ProcessedTransformation processedTransformation = new ProcessedTransformation(transformationNode, processedStreams);

        // Get underlying endpoint
        Optional<IDataFlowEndpoint> sourceEndpointOpt = context.getDataflowEndpointByName(toAnalyze.getName());
        if(!sourceEndpointOpt.isPresent()){
            LOGGER.log(Categories.dataFlowErrors().dataEndpointNotFound()
                    .name(toAnalyze.getName())
                    .endpointPlace("Source transformation"));
        }else{
            Optional<ProcessedTransformation> updatedTransformation = connectToEndpoint(sourceEndpointOpt.get(), sourceConnector, toAnalyze, processedTransformation, context, helper, toAnalyze.getAllowSchemaDrift());
            if(updatedTransformation.isPresent())
                processedStreams = updatedTransformation.get().getProcessedStreams();
        }

        return processedStreams;
    }

}
