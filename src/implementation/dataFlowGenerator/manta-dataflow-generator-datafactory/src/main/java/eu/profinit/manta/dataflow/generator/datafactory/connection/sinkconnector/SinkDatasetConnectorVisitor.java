package eu.profinit.manta.dataflow.generator.datafactory.connection.sinkconnector;

import eu.profinit.manta.connector.common.connections.Connection;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.ITransformation;
import eu.profinit.manta.connector.datafactory.model.dataset.database.IDatabaseDataset;
import eu.profinit.manta.dataflow.generator.common.query.DataflowQueryService;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedStream;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedTransformation;
import eu.profinit.manta.dataflow.generator.datafactory.connection.AbstractDatasetConnectorVisitor;
import eu.profinit.manta.dataflow.generator.datafactory.errors.Categories;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import eu.profinit.manta.dataflow.model.Node;
import eu.profinit.manta.platform.logging.api.logging.Logger;

import java.util.List;
import java.util.Optional;

/**
 * Class connecting transformation do source dataset
 */
public class SinkDatasetConnectorVisitor extends AbstractDatasetConnectorVisitor {

    private static final Logger LOGGER = new Logger(SinkDatasetConnectorVisitor.class);

    public SinkDatasetConnectorVisitor(ITransformation transformation, ProcessedTransformation processedTransformation, AnalyzerContext context, ADFGraphHelper graphHelper, DataflowQueryService queryService, boolean allowSchemaDrift) {
        super(transformation, processedTransformation, context, graphHelper, queryService, allowSchemaDrift);
    }

    @Override
    protected Optional<ProcessedTransformation> connectByQuery(Connection connection, String query){

        LOGGER.log(Categories.dataFlowErrors().unknownOperation().operation("Connect sink by query"));

        return super.connectByQuery(connection, query);
    }

    @Override
    protected Optional<ProcessedTransformation> connectByTable(Connection connection, IDatabaseDataset dataset){

        List<Node> columns = addOrDeduceColumnNodes(connection, dataset);

        for(ProcessedStream stream: processedTransformation.getProcessedStreams().values())
            graphHelper.connectNodesByNameCI(stream.getColumnNodes(), columns);

        return Optional.of(processedTransformation);
    }

    @Override
    protected ProcessedStream connectFileNodesByNameAndPosition(List<Node> fileColumns, ProcessedStream processedStream){
        graphHelper.connectFileNodesByNameAndPositionAsSink(fileColumns, processedStream.getColumnNodes());
        return new ProcessedStream(processedStream.getStreamNode(), processedStream.getColumnNodes());
    }

    @Override
    protected ProcessedStream connectDefaultFileColumns(List<Node> fileColumns, ProcessedStream processedStream){
        graphHelper.connectDefaultFileColumnsAsSink(fileColumns, processedStream.getColumnNodes());
        return new ProcessedStream(processedStream.getStreamNode(), processedStream.getColumnNodes());
    }

}
