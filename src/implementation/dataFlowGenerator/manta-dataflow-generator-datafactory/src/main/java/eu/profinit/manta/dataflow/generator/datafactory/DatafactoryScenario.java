package eu.profinit.manta.dataflow.generator.datafactory;

import eu.profinit.manta.connector.datafactory.model.IADF;
import eu.profinit.manta.dataflow.generator.modelutils.GraphScenario;

public class DatafactoryScenario extends GraphScenario<IADF> {

    /**
     * Kód licence pro Manta Flow.
     */
    private static final String MANTA_FLOW_SS_CODE = "mf_datafactory";

    /**
     * Popis produktu Manta Flow Data factory zobrazený při nedostatečné licenci.
     */
    private static final String MANTA_FLOW_SS_DESCRIPTION = "Manta Flow Data factory";

    @Override protected boolean isInputCorrect(IADF input) {
        return input != null && input.isValid();
    }

    @Override protected void checkInput(IADF input) {
        if (!checkTechnology(MANTA_FLOW_SS_CODE)) {
            raiseInsufficentLicenseException(MANTA_FLOW_SS_DESCRIPTION);
        }
    }

}
