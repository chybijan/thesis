package eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow;

import eu.profinit.manta.dataflow.model.Node;

import java.util.List;

/**
 * Class representing already added stream to graph
 * Transformation analyzers use this to pass data between each other
 */
public class ProcessedStream {

    private final List<Node> columnNodes;

    private final Node streamNode;

    public ProcessedStream(Node streamNode, List<Node> columnNodes) {
        this.columnNodes = columnNodes;
        this.streamNode = streamNode;
    }

    public List<Node> getColumnNodes() {
        return columnNodes;
    }

    public Node getStreamNode() {
        return streamNode;
    }

    public void addColumnNode(Node node){
        columnNodes.add(node);
    }
}
