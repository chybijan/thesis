package eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow;

import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.dataflow.model.Node;

import java.util.Map;

/**
 * Class representing already created transformation.
 * Analyzers use this to keep order of streams and stream columns
 */
public class ProcessedTransformation {

    private final Node transformationNode;

    private final Map<IADFStream, ProcessedStream> processedStreams;

    public ProcessedTransformation(Node transformationNode, Map<IADFStream, ProcessedStream> processedStreams) {
        this.transformationNode = transformationNode;
        this.processedStreams = processedStreams;
    }

    public Map<IADFStream, ProcessedStream> getProcessedStreams() {
        return processedStreams;
    }

    public Node getTransformationNode() {
        return transformationNode;
    }

    public void addProcessedStream(IADFStream key, ProcessedStream stream){
        processedStreams.put(key, stream);
    }
}
