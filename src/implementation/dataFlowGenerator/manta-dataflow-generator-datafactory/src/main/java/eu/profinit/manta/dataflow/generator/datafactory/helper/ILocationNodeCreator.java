package eu.profinit.manta.dataflow.generator.datafactory.helper;

import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.dataset.file.location.IDatasetLocation;
import eu.profinit.manta.dataflow.model.Node;

public interface ILocationNodeCreator {

    /**
     * Create node for location
     * @param location location to create node for
     * @param ls linked service of location
     * @param helper graph helper
     * @return new node representing location
     * @throws IllegalStateException when location and linked service do not match
     */
    Node createLocationNode(IDatasetLocation location, ILinkedService ls, ADFGraphHelper helper);

}
