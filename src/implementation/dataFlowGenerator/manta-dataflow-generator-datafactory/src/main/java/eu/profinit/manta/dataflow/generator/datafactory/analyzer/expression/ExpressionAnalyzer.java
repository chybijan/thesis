package eu.profinit.manta.dataflow.generator.datafactory.analyzer.expression;

import eu.profinit.manta.connector.datafactory.model.dataflow.IExpression;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedStream;
import eu.profinit.manta.dataflow.model.Node;

import java.util.Map;
import java.util.Set;

public class ExpressionAnalyzer implements IExpressionAnalyzer{

    /**
     * Analyze expression and handle all column references as indirect flows
     * @param expression expression to be analyzed
     * @param availableInputs inputs to choose from
     * @param destinationColumns destination column that we can connect to
     */
    @Override
    public void analyzeIndirectFlows(IExpression expression, Map<IADFStream, ProcessedStream> availableInputs, Set<Node> destinationColumns){
        //TODO
    }

    /**
     * Analyze expression and handle all column references as direct flows
     * @param expression expression to be analyzed
     * @param availableInputs inputs to choose from
     * @param destinationColumns destination column that we can connect to
     */
    @Override
    public void analyzeDirectFlows(IExpression expression, Map<IADFStream, ProcessedStream> availableInputs, Set<Node> destinationColumns){
        // TODO
    }

    /**
     * Analyze expression and find all direct and indirect flows
     * @param expression expression to be analyzed
     * @param availableInputs inputs to choose from
     * @param destinationColumns destination column that we can connect to
     */
    @Override
    public void analyze(IExpression expression, Map<IADFStream, ProcessedStream> availableInputs, Set<Node> destinationColumns){
        // TODO
    }

}
