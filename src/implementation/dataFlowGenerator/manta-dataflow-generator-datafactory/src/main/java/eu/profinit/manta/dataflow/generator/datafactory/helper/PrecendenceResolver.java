package eu.profinit.manta.dataflow.generator.datafactory.helper;

import eu.profinit.manta.dataflow.generator.datafactory.errors.Categories;
import eu.profinit.manta.platform.logging.api.logging.Logger;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

import java.util.*;

/**
 * Helper class performing a topological sort.
 * A client simply adds nodes and oriented edges (contraints) between them by calling appropriate methods.
 * When the structure is built, it can be resolved into an ordered collection of the nodes
 * representing one possible topological sort (assuming there exists at least one).
 * 
 * Any subset of nodes, that cannot be sorted because of cyclic dependency, will be ignored in the result.
 *
 * @param <T> Type of the nodes.
 */
public class PrecendenceResolver<T> {


    private static final Logger LOGGER = new Logger(PrecendenceResolver.class);

    private MultiValuedMap<T, T> predToSucc = new ArrayListValuedHashMap<>();

    private MultiValuedMap<T, T> succToPred = new ArrayListValuedHashMap<>();

    private Set<T> nodeSet = new HashSet<>();

    /**
     * Adds a new node to the collection of underlying nodes, if it does not already exists there.
     * @param node Node to be added
     */
    public void addNode(T node) {
        nodeSet.add(node);
    }

    /**
     * Adds nodes into the collection via {@link #addNode(Object)} and stores the information, that {@code pred} needs to
     * precede {@code succ}, i.e. creates an edge from {@code pred} to {@code succ}.
     * @param pred Predecessor node.
     * @param succ Successor node.
     */
    public void addConstraint(T pred, T succ) {
        if (pred == null || succ == null) {
            LOGGER.log(Categories.dataFlowErrors().precedenceResolver());
            return;
        }

        addNode(pred);
        addNode(succ);
        predToSucc.put(pred, succ);
        succToPred.put(succ, pred);
    }

    /**
     * Sorts the underlying nodes topologically. Calling this method renders this instance useless for further manipulation,
     * as it permanently destroys the original underlying structure describing the dependencies and removes already sorted nodes.
     * <p>
     * Any subset of nodes, that cannot be sorted because of cyclic dependency, will be ignored in the result.
     * So for example, if you have four nodes 1, 2, 3, 4 with dependencies like: 1 --&gt; 2, 2 --&gt; 3, 3 --&gt; 1 and 4 --&gt; 1,
     * then the result a list with only one element "4", because the rest forms a cycle.
     * </p>
     *
     * @return One of possibly many topological sorts of the underlying nodes.
     */
    public List<T> resolve() {
        return resolve(null);
    }

    /**
     * Sorts the underlying component nodes topologically starting from node passed as argument.
     * Topological sort starting from node finds only topological sort for nodes which belong to same component as starting node
     * and there exists directed path from starting nodes to them.
     * If starting node is null then all nodes can be starting nodes (classic topological sort).
     * If starting node does not belong to graph then empty list is returned.
     * Calling this method destroys the original structure describing the dependencies for sorted nodes and removes sorted nodes.
     * <p>
     * Any subset of nodes, that cannot be sorted because of cyclic dependency, will be ignored in the result.
     * So for example, if you have four nodes 1, 2, 3, 4 with dependencies like: 1 --&gt; 2, 2 --&gt; 3, 3 --&gt; 1 and 4 --&gt; 1,
     * then the result a list with only one element "4", because the rest forms a cycle.
     * </p>
     * @param startingNode Node representing starting node or null (all nodes can be starting nodes).
     * @return One of possibly many topological sorts of the underlying nodes or empty list if starting node does not belong to graph.
     */
    public List<T> resolve(T startingNode) {

        List<T> result = new LinkedList<>();
        Queue<T> itemsQueue = new LinkedList<>();
        for (T node : nodeSet) {
            if (startingNode == null) {
                if (!succToPred.containsKey(node)) {
                    itemsQueue.add(node);
                }
            } else {
                if (startingNode == node) {
                    itemsQueue.add(node);
                }
            }
        }

        while (!itemsQueue.isEmpty()) {
            T currentNode = itemsQueue.poll();
            result.add(currentNode);
            nodeSet.remove(currentNode);

            for (T successor : predToSucc.get(currentNode)) {
                if (nodeSet.contains(successor) && isReadyToProcess(successor)) {
                    itemsQueue.add(successor);
                }
            }
        }

        return result;
    }

    // Returns true if the given node has no predecessors
    private boolean isReadyToProcess(T successor) {
        for (T predecesor : succToPred.get(successor)) {
            if (nodeSet.contains(predecesor)) {
                return false;
            }
        }
        return true;
    }
}
