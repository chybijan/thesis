package eu.profinit.manta.dataflow.generator.datafactory;

import eu.profinit.manta.commons.testutils.graphvis.GraphUtils;
import eu.profinit.manta.commons.testutils.graphvis.GraphVisUtils;
import eu.profinit.manta.commons.testutils.logger.LoggerUtils;
import eu.profinit.manta.connector.datafactory.model.IADF;
import eu.profinit.manta.dataflow.model.Graph;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.Level;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class DataFlowAnalyzersTest extends DatafactoryTestBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataFlowAnalyzersTest.class);

    // Directories with test resources.
    private static final List<String> TEST_INPUT_DIRECTORIES = Arrays.asList("src/test/resources/inputs");

    // Directory to write outputs into.
    private static final String TEST_OUTPUT_DIRECTORY = "target/test-classes/transformationsOut";

    @BeforeClass
    public static void init() {
        LoggerUtils.reinitLoggerForLoggingApi(null);
        LoggerUtils.getTestEvaluationAppender().setEventsLimit(1000, Level.INFO);
        loadContext("spring/datafactoryFlowTest.xml");
        createDictionary("oracle", "ora1", "ora1");
        //createDictionary("mssql", "ms1", "ms1");
    }

    @Parameters(name = "{index}: {1}")
    public static Collection<Object[]> getParameters() {

        List<Object[]> result = new ArrayList<>();

        for (String dir : TEST_INPUT_DIRECTORIES) {

            File file = FileUtils.getFile(dir);

            if (!file.isDirectory()) {
                LOGGER.error("Path {} is not a directory.", dir);
                return Collections.emptyList();
            }

            if (!file.canRead()) {
                LOGGER.error("Input test directory {} cannot be read.", dir);
                return Collections.emptyList();
            }

            Collection<File> inputFiles = FileUtils.listFiles(file, new String[]{"json",}, true);

            for (File inputFile : inputFiles) {
                if(Objects.equals(inputFile.getParentFile().getName(), "dataflow"))
                    result.add(new Object[]{inputFile, inputFile.getName()});
            }
        }

        return result;
    }

    private File inputFile;

    public DataFlowAnalyzersTest(File inputFile, String filename) {
        this.inputFile = inputFile;
    }

    @Test
    public void test() throws IOException {
        flow(inputFile);
    }

    // Test flow.
    private void flow(File inputFile) throws IOException {
        Graph graph = null;

        LOGGER.info("Reading and parsing file {}.", inputFile.getAbsolutePath());
        IADF adf = readScript(inputFile);

        LOGGER.info("Generating dataflow for file {}.", inputFile.getAbsolutePath());
        graph = process(adf);
        assertNotNull(graph);

        String inputBaseName = FilenameUtils.getBaseName(inputFile.getName());
        GraphUtils.printGraphToFile(graph, FileUtils.getFile(TEST_OUTPUT_DIRECTORY, inputBaseName + ".txt"));
        GraphVisUtils.printGraphImageToFileIfEnabled(graph, FileUtils.getFile(TEST_OUTPUT_DIRECTORY, inputBaseName + ".png"));

        File expectedResultFile = FileUtils.getFile(inputFile.getParent(), inputBaseName + "_expected.txt");
        if (expectedResultFile.exists()) {
            String expectedResult = null;
            try {
                expectedResult = FileUtils.readFileToString(expectedResultFile);
            } catch (IOException e) {
                // transformationErrorWhileReading
                LOGGER.warn(
                        "The file with expected result for input {} was found, but there was an error during its reading."
                                + " Skipping 'actual==expected' comparison",
                        inputFile.getName());
                throw e;
            }

            LOGGER.info("Comparing actual result of processing {} to its expected form.", inputFile.getName());

            char[] sortedExcpected = GraphUtils.normalizeEol(expectedResult).toCharArray();
            //Arrays.sort(sortedExcpected);

            char[] sortedActual = GraphUtils.normalizeEol(GraphUtils.normalizeEol(GraphUtils.toString(graph))).toCharArray();
            //Arrays.sort(sortedActual);

            String expectedGraph = new String(sortedExcpected);
            String actualGraph = new String(sortedActual);

            assertEquals(String.format("Actual graph does not match the expected graph for %s", inputFile.getName()),
                    expectedGraph, actualGraph);
        } else {
            LOGGER.info(
                    "Input {} has no expected result defined (file {} does not exists) and "
                            + "so no 'actual==expected' comparison will be done",
                    inputFile.getName(), expectedResultFile.getName());
        }
    }
}
