package eu.profinit.manta.dataflow.generator.datafactory;

import eu.profinit.manta.connector.common.StringFileReader;
import eu.profinit.manta.connector.common.dictionary.AbstractDataDictionary;
import eu.profinit.manta.connector.common.dictionary.dao.jdbc.H2DictionaryDaoFactory;
import eu.profinit.manta.connector.datafactory.ADFJsonReader;
import eu.profinit.manta.connector.datafactory.model.IADF;
import eu.profinit.manta.connector.datafactory.resolver.service.ParserServiceImpl;
import eu.profinit.manta.connector.db2.Db2Reader;
import eu.profinit.manta.connector.db2.dictionary.Db2DataDictionary;
import eu.profinit.manta.connector.db2.dictionary.dialect.Db2Dialect;
import eu.profinit.manta.connector.db2.model.dictionary.Db2ResolverEntitiesFactory;
import eu.profinit.manta.connector.netezza.NetezzaReader;
import eu.profinit.manta.connector.netezza.dictionary.NetezzaDataDictionary;
import eu.profinit.manta.connector.netezza.dictionary.dialect.NetezzaDialect;
import eu.profinit.manta.connector.netezza.model.NormalizationCase;
import eu.profinit.manta.connector.netezza.model.dictionary.NetezzaResolverEntitiesFactory;
import eu.profinit.manta.connector.oracle.OracleReader;
import eu.profinit.manta.connector.oracle.dictionary.OracleDataDictionary;
import eu.profinit.manta.connector.oracle.dictionary.dialect.OracleDialect;
import eu.profinit.manta.connector.oracle.model.dictionary.OracleResolverEntitiesFactory;
import eu.profinit.manta.dataflow.model.Graph;
import eu.profinit.manta.dataflow.model.Resource;
import eu.profinit.manta.dataflow.model.impl.GraphImpl;
import eu.profinit.manta.platform.automation.InputReader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.io.IOException;

public class DatafactoryTestBase {

    /** Input directory with DDL scripts. */
    private static final File SCRIPT_FOLDER = new File("src/test/resources/ddl");

    /** Output directory to store persisted dictionaries. */
    protected static final File DICTIONARY_FOLDER = new File("target/test-classes/dictionary");

    protected static ClassPathXmlApplicationContext springContext;

    private static eu.profinit.manta.plsql.parser.service.ParserServiceImpl oracleParserService;

    //private static eu.profinit.manta.mssql.parser.service.ParserServiceImpl mssqlParserService;

    private static eu.profinit.manta.connector.db2.resolver.service.ParserServiceImpl db2ParserService;

    private static eu.profinit.manta.connector.netezza.resolver.service.ParserServiceImpl netezzaParserService;

    private static DatafactoryDataflowTask datafactoryTask;

    protected static Resource datafactoryResource;

    protected static void loadContext(String configPath) {
        springContext = new ClassPathXmlApplicationContext(configPath);
        oracleParserService = springContext.getBean(eu.profinit.manta.plsql.parser.service.ParserServiceImpl.class);
        datafactoryTask = springContext.getBean(DatafactoryDataflowTask.class);
        datafactoryResource = springContext.getBean("datafactoryScriptResource", Resource.class);
    }

    protected Graph process(IADF adf) {
        Graph result = new GraphImpl(datafactoryResource);
        datafactoryTask.doExecute(adf, result);
        return result;
    }

    /**
     * Reads a datafactory script from the given file.
     *
     * @param inputFile File from which a datafactory script should be read.
     * @return Parsed datafactory Script.
     */
    protected IADF readScript(File inputFile) {
        Validate.isTrue(inputFile.isFile(), "Input file " + inputFile.getAbsolutePath() + " not found.");
        ADFJsonReader reader = new ADFJsonReader();
        reader.setParserService(springContext.getBean("datafactoryParserService", ParserServiceImpl.class));
        reader.setInputFile(inputFile.getParentFile().getParentFile());
        try {
            return reader.read();
        } catch (IOException ex) {
            throw new RuntimeException("Error reading datafactory package from file " + inputFile.getAbsolutePath());
        } finally {
            reader.close();
        }
    }

    /**
     * Creates and persists a dictionary in the given dialect with the given dictionary ID based on the parsed input.
     *
     * @param dialect dialect
     * @param dictionaryId dictionary ID of the dictionary to be created
     * @param scriptFolder subfolder containing create scripts
     */
    protected static void createDictionary(String dialect, String dictionaryId, String scriptFolder) {
        Validate.notNull(springContext, "Context not initialized");

        AbstractDataDictionary<?> dictionary;
        try (StringFileReader stringReader = new StringFileReader()) {
            stringReader.setInputFile(FileUtils.getFile(SCRIPT_FOLDER, dialect, scriptFolder));
            stringReader.setEncoding("utf8");

            File contextDirectory = new File(DICTIONARY_FOLDER, dialect);

            InputReader<?> reader;
            switch (dialect) {
            case "oracle":
                OracleReader oraReader = new OracleReader();
                oraReader.setResetBeforeRead(false);
                oraReader.setIsDefaultSchemaFromFolder(true);
                oraReader.setParserService(oracleParserService);
                H2DictionaryDaoFactory<OracleResolverEntitiesFactory> oraDaoFactory = new H2DictionaryDaoFactory<>();
                oraDaoFactory.setCreateMissing(true);
                OracleDataDictionary oraDict = new OracleDataDictionary(
                        oraDaoFactory.getDataDictionaryDao(contextDirectory, dictionaryId), new OracleDialect(), true,
                        dictionaryId);
                oraReader.setEntitiesFactory(oraDict);
                oraReader.setInputReader(stringReader);
                reader = oraReader;
                dictionary = oraDict;
                break;
            /*case "mssql":
                MssqlReader msReader = new MssqlReader();
                msReader.setResetBeforeRead(false);
                msReader.setIsDefaultSchemaFromFolder(true);
                msReader.setParserService(mssqlParserService);
                H2DictionaryDaoFactory<MssqlResolverEntitiesFactory> msDaoFactory = new H2DictionaryDaoFactory<>();
                msDaoFactory.setCreateMissing(true);
                MssqlDataDictionary msDict = new MssqlDataDictionary(
                        msDaoFactory.getDataDictionaryDao(contextDirectory, dictionaryId), new MssqlDialect(), true,
                        dictionaryId);
                msReader.setEntitiesFactory(msDict);
                msReader.setInputReader(stringReader);
                reader = msReader;
                dictionary = msDict;
                break;*/
            case "db2":
                Db2Reader db2Reader = new Db2Reader();
                db2Reader.setResetBeforeRead(false);
                db2Reader.setIsDefaultSchemaFromFolder(true);
                db2Reader.setParserService(db2ParserService);
                H2DictionaryDaoFactory<Db2ResolverEntitiesFactory> db2DaoFactory = new H2DictionaryDaoFactory<>();
                db2DaoFactory.setCreateMissing(true);
                Db2DataDictionary db2Dict = new Db2DataDictionary(
                        db2DaoFactory.getDataDictionaryDao(contextDirectory, dictionaryId), new Db2Dialect(), true,
                        dictionaryId);
                db2Reader.setEntitiesFactory(db2Dict);
                db2Reader.setInputReader(stringReader);
                reader = db2Reader;
                dictionary = db2Dict;
                break;
            case "netezza":
                NetezzaReader netezzaReader = new NetezzaReader();
                netezzaReader.setResetBeforeRead(false);
                netezzaReader.setIsDefaultSchemaFromFolder(true);
                netezzaReader.setParserService(netezzaParserService);
                H2DictionaryDaoFactory<NetezzaResolverEntitiesFactory> nzDaoFactory = new H2DictionaryDaoFactory<>();
                nzDaoFactory.setCreateMissing(true);
                NetezzaDataDictionary netezzaDict = new NetezzaDataDictionary(
                        nzDaoFactory.getDataDictionaryDao(contextDirectory, dictionaryId),
                        new NetezzaDialect(NormalizationCase.UPPER_CASE), true, dictionaryId);
                netezzaReader.setEntitiesFactory(netezzaDict);
                netezzaReader.setInputReader(stringReader);
                reader = netezzaReader;
                dictionary = netezzaDict;
                break;

            default:
                throw new IllegalArgumentException("reader provider not found for dialect " + dialect);
            }

            try {
                while (reader.canRead()) {
                    try {
                        reader.read();
                        dictionary.persist();
                    } catch (IOException e) {
                        throw new RuntimeException("Error reading script " + reader.getInputName(), e);
                    }
                }
            } finally {
                dictionary.close();
            }
        }
    }
}
