package eu.profinit.manta.dataflow.generator.datafactory;

import eu.profinit.manta.dataflow.generator.datafactory.helper.PrecendenceResolver;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Provides several test cases for verifying the functionality of {@link PrecendenceResolver}.
 */
public class PrecedenceResolverTest {

    static class IntWrapper {
        private int value;

        IntWrapper(int value) {
            this.value = value;
        }

        @Override
        public int hashCode() {
            return value;
        }

        @Override
        public boolean equals(Object obj) {
            if (! (obj instanceof IntWrapper))
                return false;
            IntWrapper other = (IntWrapper) obj;
            return this.value == other.value;
        }
    }

    @Test
    public void testAddSameNodeMultipleTimes() {
        PrecendenceResolver<IntWrapper> resolver = new PrecendenceResolver<>();
        IntWrapper w1 = new IntWrapper(24);
        IntWrapper w2 = new IntWrapper(24);
        resolver.addNode(w1);
        resolver.addNode(w2);
        
        assertEquals(1, resolver.resolve().size());
    }

    @Test
    public void testGraphWithoutEdges() {
        PrecendenceResolver<Integer> resolver = new PrecendenceResolver<>();
        // four isolated nodes
        resolver.addNode(1);
        resolver.addNode(2);
        resolver.addNode(3);
        resolver.addNode(4);

        List<Integer> resolvedList = resolver.resolve();

        // Result must have those 4 elements (any order)
        assertEquals(4, resolvedList.size());
        assertTrue(resolvedList.contains(1));
        assertTrue(resolvedList.contains(2));
        assertTrue(resolvedList.contains(3));
        assertTrue(resolvedList.contains(4));

        assertFalse(resolvedList.contains(0));
        assertFalse(resolvedList.contains(5));
    }

    @Test
    public void testLinearGraph() {
        PrecendenceResolver<Integer> resolver = new PrecendenceResolver<>();
        // 1 --> 2 --> 3
        resolver.addConstraint(1, 2);
        resolver.addConstraint(2, 3);
        
        List<Integer> resolvedList = resolver.resolve();
        
        assertEquals(3, resolvedList.size());
        assertEquals(1, resolvedList.get(0).intValue());
        assertEquals(2, resolvedList.get(1).intValue());
        assertEquals(3, resolvedList.get(2).intValue());
    }
    
    @Test
    public void testCycle() {
        PrecendenceResolver<Integer> resolver = new PrecendenceResolver<>();
        // 1 --> 2 --> 3, 3 --> 1, 4 --> 1
        resolver.addConstraint(1, 2);
        resolver.addConstraint(2, 3);
        resolver.addConstraint(3, 1);
        resolver.addConstraint(4, 1);
        
        List<Integer> resolvedList = resolver.resolve();
        
        // Size should be 1 as the cycle 1 --> 2 --> 3 should remain unresolved
        assertEquals(1, resolvedList.size());
        // first value must be 4
        assertEquals(4, resolvedList.get(0).intValue());
    }

    @Test
    public void testStartingNodeSort() {
        PrecendenceResolver<Integer> resolver = new PrecendenceResolver<>();
        // 1 --> 2 --> 3
        resolver.addConstraint(1, 2);
        resolver.addConstraint(2, 3);

        List<Integer> resolvedList = resolver.resolve(2);

        assertEquals(2, resolvedList.size());
        assertEquals(2, resolvedList.get(0).intValue());
        assertEquals(3, resolvedList.get(1).intValue());

        List<Integer> resolvedList2 = resolver.resolve(1);

        assertEquals(1, resolvedList2.size());
        assertEquals(1, resolvedList2.get(0).intValue());
    }
}
