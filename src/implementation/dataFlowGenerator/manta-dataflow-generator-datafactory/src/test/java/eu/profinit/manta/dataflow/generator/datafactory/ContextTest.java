package eu.profinit.manta.dataflow.generator.datafactory;

import eu.profinit.manta.connector.datafactory.model.IDataset;
import eu.profinit.manta.connector.datafactory.model.ILinkedService;
import eu.profinit.manta.connector.datafactory.model.common.IReference;
import eu.profinit.manta.connector.datafactory.model.common.ReferenceType;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IADFStream;
import eu.profinit.manta.connector.datafactory.resolver.model.common.Reference;
import eu.profinit.manta.connector.datafactory.resolver.model.dataflow.transformation.ADFStream;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContextImpl;

import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.ProcessedStream;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ContextTest {

    private AnalyzerContext analyzerContext;

    @Mock
    private ILinkedService linkedServiceMock;

    @Mock
    private IDataset datasetMock;

    @Mock
    private ProcessedStream processedStreamMock;

    @Mock
    private ProcessedStream secondProcessedStreamMock;

    private final IADFStream mockStreamKey = new ADFStream("name", "sub");
    private final IADFStream secondStream = new ADFStream("name2", "sub");

    @Before
    public void init(){
        analyzerContext = new AnalyzerContextImpl();

        when(linkedServiceMock.getName()).thenReturn("ls");
        List<ILinkedService> linkedServices = new ArrayList<>();
        linkedServices.add(linkedServiceMock);

        when(datasetMock.getName()).thenReturn("dataset");
        List<IDataset> datasets = new ArrayList<>();
        datasets.add(datasetMock);

        analyzerContext.setLinkedServices(linkedServices);
        analyzerContext.setDatasets(datasets);

        Map<IADFStream, ProcessedStream> processedStreams = new HashMap<>();
        processedStreams.put(mockStreamKey, processedStreamMock);
        analyzerContext.addAvailableStreams(processedStreams);

        Map<IADFStream, ProcessedStream> secondStreams = new HashMap<>();
        secondStreams.put(secondStream, secondProcessedStreamMock);

        analyzerContext.addAvailableStreams(secondStreams);

    }

    @Test
    public void getDatasetTest(){

        IReference datasetReference = new Reference("dataset", new ArrayList<>(), ReferenceType.DATASET);

        Optional<IDataset> dataset = analyzerContext.getDataset(datasetReference);
        assertTrue(dataset.isPresent());
    }

    @Test
    public void failGetDatasetTest(){

        IReference datasetReference = new Reference("datasetWhat", new ArrayList<>(), ReferenceType.DATASET);

        Optional<IDataset> dataset = analyzerContext.getDataset(datasetReference);
        assertTrue(dataset.isEmpty());
    }

    @Test
    public void getLinkedServiceTest(){

        IReference lsReference = new Reference("ls", new ArrayList<>(), ReferenceType.LINKED_SERVICE);

        Optional<ILinkedService> ls = analyzerContext.getLinkedService(lsReference);
        assertTrue(ls.isPresent());
    }

    @Test
    public void failGetLinkedServiceTest(){

        IReference lsReference = new Reference("lsWhat", new ArrayList<>(), ReferenceType.LINKED_SERVICE);

        Optional<ILinkedService> ls = analyzerContext.getLinkedService(lsReference);
        assertTrue(ls.isEmpty());
    }


    @Test
    public void findAvailableStream() {
        assertTrue(analyzerContext.findAvailableStream(secondStream).isPresent());
        assertTrue(analyzerContext.findAvailableStream(mockStreamKey).isPresent());
        assertTrue(analyzerContext.findAvailableStream(mockStreamKey).isPresent());
        assertTrue(analyzerContext.findAvailableStream(secondStream).isPresent());
    }

    @Test
    public void failFindAvailableStream() {
        assertTrue(analyzerContext.findAvailableStream(new ADFStream("what", "")).isEmpty());
    }

    @Test
    public void consumeAvailableStream() {
        assertTrue(analyzerContext.consumeAvailableStream(secondStream).isPresent());
        assertTrue(analyzerContext.consumeAvailableStream(mockStreamKey).isPresent());
        assertTrue(analyzerContext.findAvailableStream(mockStreamKey).isEmpty());
        assertTrue(analyzerContext.findAvailableStream(secondStream).isEmpty());
    }

}
