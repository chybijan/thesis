package eu.profinit.manta.dataflow.generator.datafactory;

import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IFilterTransformation;
import eu.profinit.manta.connector.datafactory.model.dataflow.transformation.IUnknownTransformation;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.context.AnalyzerContext;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.AnalyzerDispatcher;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.transformation.FilterAnalyzer;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.transformation.TransformationAnalyzer;
import eu.profinit.manta.dataflow.generator.datafactory.analyzer.dataflow.transformation.UnkownTransformationAnalyzer;
import eu.profinit.manta.dataflow.generator.datafactory.helper.ADFGraphHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AnalyzerDispatcherTest {

    // Mocks
    @Mock
    private UnkownTransformationAnalyzer unkownTransformationAnalyzerMock;

    @Mock
    private FilterAnalyzer filterAnalyzerMock;

    @Mock
    private IFilterTransformation filterMock;

    @Mock
    private IUnknownTransformation unknownTransformationMock;

    @Mock
    private AnalyzerContext contextMock;

    @Mock
    private ADFGraphHelper graphHelperMock;

    // Dispatcher
    private AnalyzerDispatcher dispatcher;

    @Before
    public void init() {
        // Setup mocks and the dispatcher

        when(filterAnalyzerMock.getAnalyzedType()).thenReturn(IFilterTransformation.class);
        when(unkownTransformationAnalyzerMock.getAnalyzedType()).thenReturn(IUnknownTransformation.class);
        dispatcher = new AnalyzerDispatcher();
    }

    private void addAnalyzers(AnalyzerDispatcher dispatcher, TransformationAnalyzer<?>... analyzers) {
        dispatcher.setAnalyzers(Arrays.asList(analyzers));
    }

    @Test
    public void testSuccessfulDelegationWhenTypeIsMapped() {
        addAnalyzers(dispatcher, unkownTransformationAnalyzerMock, filterAnalyzerMock);
        dispatcher.analyzeTransformation(filterMock,
                contextMock, null, graphHelperMock);
        Mockito.verify(filterAnalyzerMock, times(1)).analyze(filterMock, contextMock, null, graphHelperMock);
        Mockito.verify(unkownTransformationAnalyzerMock, times(0)).analyze(unknownTransformationMock, contextMock, null, graphHelperMock);
    }


}
