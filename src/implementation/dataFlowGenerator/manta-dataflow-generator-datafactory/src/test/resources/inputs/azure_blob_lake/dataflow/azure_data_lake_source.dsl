source(allowSchemaDrift: true,
	validateSchema: false,
	ignoreNoFilesFound: false) ~> source1
source1 sink(allowSchemaDrift: true,
	validateSchema: false,
	input(
		Column_1 as string,
		Column_2 as string,
		Column_3 as string
	),
	skipDuplicateMapInputs: true,
	skipDuplicateMapOutputs: true) ~> sink1