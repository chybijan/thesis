source(output(
		Column_1 as string,
		Column_2 as string,
		Column_3 as string
	),
	allowSchemaDrift: true,
	validateSchema: false,
	ignoreNoFilesFound: false) ~> source1
source(output(
		Column_1 as string,
		Column_2 as string,
		Column_3 as string
	),
	allowSchemaDrift: true,
	validateSchema: false,
	ignoreNoFilesFound: false) ~> source2
source(output(
		array as integer[],
		boolean as boolean,
		color as string,
		null as string,
		number as integer,
		object as (a as string, c as string),
		string as string
	),
	allowSchemaDrift: true,
	validateSchema: false,
	ignoreNoFilesFound: false,
	documentForm: 'documentPerLine',
	wildcardPaths:['testJson.json']) ~> source3
source(output(
		Company as (Employee as (Address as (City as string, State as string, Zip as integer), ContactNo as integer, Email as string, FirstName as string, LastName as string))
	),
	allowSchemaDrift: true,
	validateSchema: false,
	ignoreNoFilesFound: false,
	validationMode: 'none',
	namespaces: true) ~> source4
source1, source2 join(source1@Column_1 == source2@Column_2,
	joinType:'inner',
	broadcast: 'auto')~> join1
source3, source4 join(color <=> Company.Employee.FirstName,
	joinType:'inner',
	broadcast: 'auto')~> join2
join1, join2 join(source2@Column_1 == object.a,
	joinType:'inner',
	broadcast: 'auto')~> join3
join3 sink(allowSchemaDrift: true,
	validateSchema: false,
	skipDuplicateMapInputs: true,
	skipDuplicateMapOutputs: true) ~> sink1