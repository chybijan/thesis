This git repository contains Jan Chybík's bachelor thesis 'Dataflow analysis in Azure Data Factory' and its source codes.
The data flow scanner implemented in this work cannot be executed on its own, because it is a part of the Manta project.

Folder structure:
-src...Source codes
    -implementation...Implementation source codes
    -thesis... Source codes of the thesis's text in Latex format.
-text..Texts of the thesis
    - lectures...Lectures used in the thesis
    - thesis.pdf...Text of the thesis

